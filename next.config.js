module.exports = {
	images: {
		domains: process.env.NEXT_PUBLIC_IMAGE_DOMAINS.split(', ')
	},
	i18n: {
		defaultLocale: 'en-AU',
		locales: ['en-AU', 'en-SG'],
		localeDetection: false,
		domains: [
			{
				domain: process.env.NEXT_PUBLIC_SITE_URL,
				defaultLocale: 'en-AU'
			},
			{
				domain: process.env.NEXT_PUBLIC_SITE_URL_ASIA,
				defaultLocale: 'en-SG'
			}
		]
	},
	compiler: {
		// Enables the styled-components SWC transform
		styledComponents: true
	},
	eslint: {
		ignoreDuringBuilds: true
	},
	webpack: (config, { isServer }) => {
		if (!isServer) {
			// eslint-disable-next-line no-param-reassign
			config.resolve = {
				...config.resolve,
				fallback: {
					...config.resolve.fallback,
					fs: false
				}
			};
		}

		config.module.rules.push({
			test: /\.svg$/,
			use: [
				{
					loader: '@svgr/webpack',
					options: {
						svgoConfig: {
							plugins: {
								removeViewBox: false
							}
						}
					}
				}
			]
		});
		return config;
	}
};
