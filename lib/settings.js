// Prefix for the blog URLs
export const blogPrefix = '/journal/';

// Prefix for the shop URLs
export const shopPrefix = '/product/';

// Prefix for the shop URLs
export const showroomPrefix = '/showrooms/';

// Prefix for the shop URLs
export const projectPrefix = '/project/';

// Prefix for the brand URLs
export const brandPrefix = '/brands/';

// Sorting options for archive page
export const archiveSortOptions = [
	{
		value: 'latest',
		slug: 'date',
		sortby: 'DESC',
		label: 'Latest'
	},
	{
		value: 'oldest',
		slug: 'date',
		sortby: 'ASC',
		label: 'Oldest'
	},
	{
		value: 'a-z',
		slug: 'title',
		sortby: 'ASC',
		label: 'A - Z'
	},
	{
		value: 'featured',
		slug: 'featured',
		sortby: 'ASC',
		label: 'Featured'
	},
	{
		value: 'z-a',
		slug: 'title',
		sortby: 'DESC',
		label: 'Z - A'
	}
];

export const defaultArchiveSortOption = 'latest';
