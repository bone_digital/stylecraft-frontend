/**
 * Triggers a page view
 * @param url
 */
export const pageview = (url) => {
	const GA = process.env.NEXT_PUBLIC_MEASUREMENT_ID;
	if( !GA )
	{
		return;
	}

	window.gtag('config', GA, {
		path_url: url,
	})
};

/**
 * Used to fire an event in GTM
 * See: https://developers.google.com/analytics/devguides/collection/gtagjs/events
 * @param action
 * @param category
 * @param label
 * @param value
 */
export const event = ({ action, category, label, value }) => {
	window.gtag('event', action, {
		event_category: category,
		event_label: label,
		value: value,
	})
}