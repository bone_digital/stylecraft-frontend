/**
 * Triggers a page view
 * @param url
 */
export const pageview = (url) => {
	const PINTEREST_ID = process.env.NEXT_PUBLIC_PINTEREST_ID;
	if( !PINTEREST_ID )
	{
		return;
	}

	window.pintrk('page');
};

export const event = (eventName, eventData = {}) => {
	const PINTEREST_ID = process.env.NEXT_PUBLIC_PINTEREST_ID;
	if( !PINTEREST_ID )
	{
		return;
	}

	//console.log(`Pinterest Event - ${eventName}`);
	window.pintrk('track', eventName, eventData);
}
