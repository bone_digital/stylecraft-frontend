export function getProductQueryFilters(availableFilters, queryObj) {
	const allowList = [
		'range',
		'designer',
		'brand',
		'category',
		'new',
		'australian_made',
		'lead_time',
		'environmental',
		'price',
		'shop'
	];

	const selections = [];

	for (const [key, value] of Object.entries(queryObj)) {
		const queryContainsFilter =
			allowList.filter((filter) => {
				return filter === key;
			}).length > 0;

		if (queryContainsFilter) {
			let filterArray = [];

			if (Array.isArray(value)) {
				filterArray = value;
			} else {
				filterArray = value.split(',');
			}

			filterArray.forEach((filterValue) => {
				let type = null;

				if (['range'].includes(key)) {
					type = 'ranges';
				}
				else if (['brand'].includes(key)) {
					type = 'brands';
				}
				else if (['designer'].includes(key)) {
					type = 'designers';
				}
				else if (['category'].includes(key)) {
					type = 'productCategories';
				}
				else if (['lead_time'].includes(key)) {
					type = 'leadtimes';
				}
				else if (['environmental'].includes(key)) {
					type = 'environmentalCertifications';
				}
				else if (['price'].includes(key)) {
					type = 'priceRanges';
				}
				else if (['whats-new', 'whats_new'].includes(key)) {
					// type = 'whatsNew';
					filterArray.push({
						filter: `whats-new`,
						name: `What's New`,
						slug: `whats-new`,
						parent: `whats-new`
					});

					return;
				}
				else if (['australian-made', 'australian_made'].includes(key)) {
					selections.push({
						filter: 'australian-made',
						name: `Australian Made`,
						slug: `australian-made`,
						parent: `australian-made`
					});

					return;
				}
				else if (['shop'].includes(key)) {
					selections.push({
						filter: 'shop',
						parent: 'shop',
						slug: 'true',
						name: 'Shop'
					});

					return;
				}


				let filterCategory = type !== null && availableFilters?.hasOwnProperty(type)
					? availableFilters[type]
					: [];

				let filterNode = filterCategory?.edges?.find(({ node }) => {
					return node?.slug === filterValue;
				});

				if (!filterNode && filterCategory?.edges?.length) {
					for (const node of filterCategory.edges) {
						if (node.node?.children?.edges) {
							for (const innerNode of node.node?.children
								?.edges) {
								if (innerNode.node?.slug === filterValue) {
									filterNode = innerNode;
								}
							}
						}
					}
				}

				if (filterNode && filterNode !== null) {
					selections.push({
						id: filterNode?.node?.id,
						filter: key.toString(),
						parent: key.toString(),
						slug: filterNode?.node?.slug,
						name: filterNode?.node?.name
					});
				}
			});
		}
	}

	return selections;
}



export const getSelectedFilters = (
	posts,
	page = {
		current: 1
	},
	max,
	loadMoreTrigger = false,
	sortBy = null,
	userInteracted,
	handleActiveFilters,
	designerFilters,
	brandFilters,
	productFilters,
	leadtimeFilters,
	environmentFilters,
	priceFilters,
	rangeFilters,
	australianMade,
	newProducts,
	defaultRange = null,
	archiveType = 'category'
) => {
	const isWholeseller = false;
	const locale = null;
	let activeFilters = [];

	if (brandFilters && brandFilters.length) {
		let brandArray = brandFilters.map((filter) => {
			return filter?.slug;
		});

		if (
			defaultRange &&
			defaultRange !== null &&
			archiveType === 'brand'
		) {
			brandArray.push(defaultRange);
		}

		activeFilters.push({ name: 'brand', value: brandArray });
	} else if (defaultRange !== null && archiveType === 'brand') {
		activeFilters.push({ name: 'brand', value: defaultRange });
	}

	if (designerFilters && designerFilters.length) {
		let designerArray = designerFilters.map((filter) => {
			return filter?.slug;
		});

		activeFilters.push({ name: 'designer', value: designerArray });
	}

	if (productFilters && productFilters.length) {
		let productArray = productFilters.map((filter) => {
			return filter?.slug;
		});

		if (
			defaultRange &&
			defaultRange !== null &&
			archiveType === 'category'
		) {
			productArray.push(defaultRange);
		}

		activeFilters.push({ name: 'category', value: productArray });
	} else if (defaultRange !== null && archiveType === 'category') {
		activeFilters.push({ name: 'category', value: defaultRange });
	}

	if (leadtimeFilters && leadtimeFilters.length) {
		let leadtimeArray = leadtimeFilters.map((filter) => {
			return filter?.slug;
		});

		activeFilters.push({ name: 'lead_time', value: leadtimeArray });
	}

	if (environmentFilters && environmentFilters.length) {
		let environmentArray = environmentFilters.map((filter) => {
			return filter?.slug;
		});

		activeFilters.push({
			name: 'environmental',
			value: environmentArray
		});
	}

	if (rangeFilters && rangeFilters.length) {
		let rangeArray = rangeFilters.map((filter) => {
			return filter?.slug;
		});

		if (
			defaultRange &&
			defaultRange !== null &&
			archiveType === 'range'
		) {
			rangeArray.push(defaultRange);
		}

		activeFilters.push({ name: 'range', value: rangeArray });

		queryURL += `${rangeQuery}&`;
	} else if (defaultRange !== null && archiveType === 'range') {
		activeFilters.push({ name: 'range', value: defaultRange });
	}

	if (priceFilters && priceFilters.length) {
		let priceArray = priceFilters.map((filter) => {
			return filter?.slug;
		});

		activeFilters.push({ name: 'price', value: priceArray });
	}

	if (australianMade === true) {
		activeFilters.push({ name: 'australian_made', value: true });
	}
	if (isWholeseller === true) {
		activeFilters.push({ name: 'wholesale', value: true });
	}

	if (locale !== null) {
		activeFilters.push({ name: 'locale', value: locale });
	}

	if (newProducts === true) {
		activeFilters.push({ name: 'new', value: true });
	}

	if (sortBy) {
		activeFilters.push({ name: 'sort', value: sortBy?.slug });
		activeFilters.push({ name: 'order', value: sortBy?.sortby });
	}

	activeFilters.push({ name: 'page', value: page?.current });
	return activeFilters;
};


export function createFilterUrl(windowLocation, filters, pageSlug) {
	if (!windowLocation) {
		return '';
	}
	const newUrl = new URL(windowLocation);
	const searchParams = new URLSearchParams(newUrl.search);

	/**
	 * set search param for active filters,
	 * remove params with no active ones
	 */
	filters?.forEach((filter) => {
		const name = filter.filter || null;
		if (!name) {
			return;
		}

		const value = filter.value || null;
		if (value) {
			searchParams.set(name, value);
		}
	});

	searchParams.forEach((value, key) => {
		const queryHasKey =
			filters?.filter(({ filter }) => {
				return filter === key;
			}).length > 0;

		if (!queryHasKey && key != 'shop') {
			searchParams.delete(key);
		}

		if (!value || value === '' && key != 'shop') {
			searchParams.delete(key);
		}
	});


	const params = Object.fromEntries(searchParams.entries());

	let query = {
		...params
	};

	if (pageSlug !== '') {
		query.slug = pageSlug;
	}

	newUrl.search = searchParams.toString();
	return newUrl.toString();
}


export function paramStringFromUrl(url) {
	const urlObj = new URL(url);
	return urlObj.searchParams.toString();
}
