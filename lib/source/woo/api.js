const API_URL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}wc/v3/`;

/**
 * Runs a fetch request and returns the response.
 * @param endpoint Woocommerce API endpoint excluding /wp-json/wc/v3/
 * @returns {Promise<any>}
 */
async function fetchRESTAPI(endpoint, postBody = null, method = 'POST') {
	const url = `${API_URL}${endpoint}`;

	const headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Basic ' + Buffer.from(process.env.WOO_CONSUMER_KEY + ":" + process.env.WOO_CONSUMER_SECRET).toString('base64')
	}

	const reqBody = postBody ? JSON.stringify(postBody) : null;

	try {
		const data = await fetch(url, {
			headers,
			method,
			body: reqBody
		});

		const json = await data.json();

		return json;
	} catch (error) {
		throw new Error(error.message);
	}
}

/**
 * Returns all orders for current user
 *
 * @param userId
 * @return {Promise<*>}
 */
export async function getAllOrders(userId) {
	const endpoint = `orders?customer=${userId}`;

	const data = await fetchRESTAPI(endpoint, null, 'GET');

	const orders = data || [];

	return orders;
}

/**
 * Returns current user / customer
 *
 * @param userId
 * @return {Promise<*>}
 */
export async function getCustomer(userId) {
	const endpoint = `customers/${userId}`;

	const data = await fetchRESTAPI(endpoint, null, 'GET');

	const orders = data || [];

	return orders;
}
