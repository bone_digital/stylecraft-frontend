import { gql } from '@apollo/client';
import { PageBuilderQuery } from './pageBuilder';
import { STAFF_FRAGMENT } from './posts';

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const ModuleSettings = `
	backgroundColor
	fieldGroupName
`;

export const ALL_PAGES_QUERY = gql`
	query ALL_PAGES_QUERY {
		pages(where: { notIn: "11" }) {
			edges {
				node {
					id
					title
					slug
				}
			}
		}
	}
`;

export const PAGE_QUERY = gql`
	query PAGE_QUERY($slug: ID!) {
		page(id: $slug, idType: URI) {
			slug
			databaseId
			title
			content
			seo {
				fullHead
			}
			${PageBuilderQuery}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const PAGE_QUERY_BY_ID = gql`
	query PAGE_QUERY($id: ID!) {
		page(id: $id, idType: DATABASE_ID) {
			slug
			databaseId
			title
			seo {
				fullHead
			}
			${PageBuilderQuery}
		}
	}
`;

export const PAGE_DRAFT_QUERY = gql`
	query PAGE_QUERY($slug: ID!) {
		page(id: $slug, idType: DATABASE_ID) {
			preview {
				node {
					slug
					title
					content
					seo {
						fullHead
					}
					${PageBuilderQuery}
				}
			}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const HOME_PAGE_QUERY = gql`
	query HOME_PAGE_QUERY {
		page(id: "home", idType: URI) {
			slug
			title
			content
			seo {
				fullHead
			}
			${PageBuilderQuery}
			home {
				heroModule {
					${ModuleSettings}
					gallery {
						title
						image {
							${ImageFields}
						}
						link {
							url
						}
					}
				}
			}
			revisions {
				nodes {
					id
					title
					content
				}
			}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const HOME_DRAFT_PAGE_QUERY = gql`
	query HOME_DRAFT_PAGE_QUERY($id: ID!) {
		page(id: $id, idType: DATABASE_ID) {
			preview {
				node {
					slug
					title
					content
					seo {
						fullHead
					}
					${PageBuilderQuery}
					home {
						heroModule {
							${ModuleSettings}
							gallery {
								title
								image {
									${ImageFields}
								}
								link {
									url
								}
							}
						}
					}
				}
			}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;
