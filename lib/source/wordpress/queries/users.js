import { gql } from '@apollo/client';

export const CURRENT_USER_QUERY = gql`
	query CURRENT_USER_QUERY {
		viewer {
			id
			firstName
			lastName
			email
			roles {
				edges {
					node {
						id
						name
					}
				}
			}
			wooSessionToken
		}
	}
`;

// # // (Optional) Install & activate WPGraphQL-JWT-Authentication to add a login mutation that returns a JSON Web Token.

// # https://github.com/imranhsayed/woo-next
