import { gql } from '@apollo/client';
import { PageBuilderModuleFragments } from './pageBuilder';
const BrandPageBuilderQuery = PageBuilderModuleFragments.toString().replace(
	/Page_/g,
	'Brand_'
);

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const BRAND_CARD_FRAGMENT = `
	slug
	brandId
	link
	title
	brand {
		description
		excerpt
	}
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
`;

export const SimpleProductFields = `
	id
	link
	name
	featuredImage {
		node {
			${ImageFields}
		}
	}
	product {
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
`;

export const VariableProductFields = `
	id
	link
	name
	databaseId
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	product {
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
`;

export const RELATED_BRAND_QUERY = gql`
	query RELATED_BRAND_QUERY($id: ID!) {
		brands(first: 3, where: { notIn: [$id]} ) {
			edges {
				node {
					${BRAND_CARD_FRAGMENT}
				}
			}
		}
	}
`;

export const ALL_BRANDS_QUERY = gql`
	query ALL_BRANDS_QUERY {
		brands {
			edges {
				node {
					${BRAND_CARD_FRAGMENT}
				}
			}
		}
	}
`;

export const BRAND_QUERY = gql`
	query BRAND_QUERY($id: ID!) {
		brand(id: $id, idType: URI) {
			pageBuilder {
				modules {
					${BrandPageBuilderQuery}
				}
			}
			slug
			title
			seo {
				fullHead
			}
			id
			brandId
			link
			title
			brand {
				count
				description
				excerpt
				locales
				defaultRelatedProducts {
					post {
						... on SimpleProduct {
							${SimpleProductFields}
						}
						... on VariableProduct {
							${VariableProductFields}
						}
					}
				}
				relatedPosts {
					post {
						... on Post {
							slug
							postId
							title
							date
							link
							featuredImage {
								node {
									${ImageFields}
								}
							}
						}
					}
				}
			}
			featuredImage {
				node {
					${ImageFields}
				}
			}
			contentType {
				node {
					name
				}
			}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const BRAND_DRAFT_QUERY = gql`
	query BRAND_DRAFT_QUERY($id: ID!) {
		brand(id: $id, idType: DATABASE_ID) {
			preview {
				node {
					pageBuilder {
					modules {
						${BrandPageBuilderQuery}
					}
				}
				slug
				title
				seo {
					fullHead
				}
				id
				brandId
				link
				title
				brand {
					count
					description
					excerpt
					locales
					relatedPosts {
						post {
							... on Post {
								slug
								postId
								title
								date
								link
								featuredImage {
									node {
										${ImageFields}
									}
								}
							}
						}
					}
				}
				featuredImage {
					node {
						${ImageFields}
					}
				}
				contentType {
					node {
						name
					}
				}
				}
			}
		}
		showrooms {
			edges {
				node {
					title
					slug
					showroom {
						address {
							city
							country
							countryShort
							longitude
							latitude
							placeId
							state
							postCode
							stateShort
							streetAddress
							streetNumber
							zoom
							streetName
						}
						formattedAddress
						phone
						fax
						email
						showroomStaff {
							staff {
								... on Staff {
								id
									title
									staff {
										role
										lastName
										firstName
									}
									featuredImage {
										node {
											${ImageFields}
										}
									}
									teams {
										edges {
											node {
												name
												slug
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;
