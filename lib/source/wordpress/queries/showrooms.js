import { gql } from '@apollo/client';

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const SHOWROOM_FRAGMENT = `
	id
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
	link
	slug
	showroom {
		openingHours
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		address {
			city
			country
			countryShort
			longitude
			latitude
			placeId
			state
			postCode
			stateShort
			streetAddress
			streetNumber
			zoom
			streetName
		}
		formattedAddress
		phone
		fax
		email
		showroomStaff {
			staff {
				... on Staff {
				id
					title
					staff {
						role
						lastName
						firstName
					}
					featuredImage {
						node {
							${ImageFields}
						}
					}
					teams {
						edges {
							node {
								name
								slug
							}
						}
					}
				}
			}
		}
	}
`;

export const SHOWROOM_LOOP_FRAGMENT = `
	id
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
	link
	slug
	showroom {
		openingHours
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		address {
			city
			country
			countryShort
			longitude
			latitude
			placeId
			state
			postCode
			stateShort
			streetAddress
			streetNumber
			zoom
			streetName
		}
		formattedAddress
		phone
		fax
		email
	}
`;

export const ModuleSettings = `
	backgroundColor
	fieldGroupName
`;

const SHOWROOM_POST_FRAGMENT = `
	fragment ShowroomPostFields on Showroom {
		slug
		showroomId
		title
		date
		seo {
			fullHead
		}
		featuredImage {
			node {
				${ImageFields}
			}
		}
		showroom {
			backgroundColor
			headingColor
			gallery {
				image {
					${ImageFields}
				}
			}
			openingHours
			linkButton {
				prefix
				linkInner {
					target
					title
					url
				}
			}
			columns {
				copy
				linkButton {
					prefix
					linkInner {
						target
						title
						url
					}
				}
			}
			address {
				city
				country
				countryShort
				longitude
				latitude
				placeId
				state
				postCode
				stateShort
				streetAddress
				streetNumber
				zoom
				streetName
			}
			formattedAddress
			phone
			fax
			email
			showroomStaff {
				staff {
					... on Staff {
					id
						title
						staff {
							role
							lastName
							firstName
						}
						featuredImage {
							node {
								${ImageFields}
							}
						}
						teams {
							edges {
								node {
									name
									slug
								}
							}
						}
					}
				}
			}
		}
		relatedShowrooms {
			relatedPosts {
				post {
					... on Showroom {
						${SHOWROOM_FRAGMENT}
					}
				}
			}
		}
		postBuilder {
			modules {
				... on Showroom_Postbuilder_Modules_ImageVideo {
					${ModuleSettings}
					alignment
					columns {
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
					copy
					gallery {
						image {
							${ImageFields}
						}
					}
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					leftType
					rightType
					title
					video {
						url
						thumbnail {
							${ImageFields}
						}
					}
				}
				... on Showroom_Postbuilder_Modules_ImageGrid {
					${ModuleSettings}
					copy
					title
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					gallery {
						image {
							${ImageFields}
						}
					}
				}
				... on Showroom_Postbuilder_Modules_Stores {
					${ModuleSettings}
				}
				... on Showroom_Postbuilder_Modules_ColumnsBlock {
					${ModuleSettings}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					alignment
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					leftType
					copy
					title
					columns {
						image {
							${ImageFields}
						}
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
				}
			}
		}
	}
`;

export const SHOWROOM_STAFF_FRAGMENT = `
	fragment ShowroomStaffFields on Showroom {
		title
		slug
		showroom {
			showroomStaff {
				staff {
					... on Staff {
					id
						title
						staff {
							role
							lastName
							firstName
						}
						featuredImage {
							node {
								${ImageFields}
							}
						}
						teams {
							edges {
								node {
									name
									slug
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const RELATED_SHOWROOM_QUERY = gql`
	query RELATED_SHOWROOM_QUERY($id: ID!) {
		posts: showrooms(first: 3, where: { notIn: [$id]} ) {
			edges {
				node {
					... on Showroom {
						${SHOWROOM_FRAGMENT}
					}
				}
			}
		}
	}
`;

export const ALL_SHOWROOMS_QUERY = gql`
	query ALL_SHOWROOMS_QUERY {
		showrooms {
			edges {
				node {
					${SHOWROOM_LOOP_FRAGMENT}
				}
			}
		}
	}
`;

export const SHOWROOM_QUERY = gql`
	${SHOWROOM_POST_FRAGMENT}
	${SHOWROOM_STAFF_FRAGMENT}
	query SHOWROOM_QUERY($id: ID!) {
		showroom(id: $id, idType: URI) {
			...ShowroomPostFields
		}
		staffShowroom: showrooms {
			edges {
				node {
					...ShowroomStaffFields
				}
			}
		}
	}
`;

export const SHOWROOM_DRAFT_QUERY = gql`
	${SHOWROOM_POST_FRAGMENT}
	${SHOWROOM_STAFF_FRAGMENT}
	query SHOWROOM_DRAFT_QUERY($id: ID!) {
		showroom(id: $id, idType: DATABASE_ID) {
			slug
			status
			preview {
				node {
					...ShowroomPostFields
				}
			}
		}
		staffShowroom: showrooms {
			edges {
				node {
					...ShowroomStaffFields
				}
			}
		}
	}
`;
