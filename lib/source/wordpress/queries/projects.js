import { gql } from '@apollo/client';
import { STAFF_FRAGMENT } from './posts';
import {
	PostFields,
	PageFields,
	ShowroomFields,
	BrandFields,
	SimpleProductFields,
	VariableProductFields
} from './pageBuilder';

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

const POST_HEADER = `
	postHeader {
		author
		gallery {
			image {
				${ImageFields}
			}
		}
		intro
		backgroundColor
		headingColor
		additionalDetails {
			copy
			subtitle
		}
		columns {
			copy
			linkButton {
				prefix
				linkInner {
					target
					title
					url
				}
			}
		}
	}
`;

export const ModuleSettings = `
	backgroundColor
	fieldGroupName
`;

const POST_CARD_FRAGMENT = `
	slug
	projectId
	title
	date
	link
	featuredImage {
		node {
			${ImageFields}
		}
	}
	terms: projectCategories {
		edges {
			node {
				slug
				name
				link
			}
		}
	}
`;

const PROJECT_FRAGMENT = `
	fragment ProjectPostFields on Project {
		slug
		projectId
		title
		date
		seo {
			fullHead
		}
		featuredImage {
			node {
				${ImageFields}
			}
		}
		relatedProjects {
			relatedPosts {
				post {
					... on Project {
						${POST_CARD_FRAGMENT}
					}
				}
			}
		}
		post {
			${POST_HEADER}
		}
		postBuilder {
			modules {
				... on Project_Postbuilder_Modules_ImageVideo {
					${ModuleSettings}
					alignment
					columns {
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
					copy
					gallery {
						image {
							${ImageFields}
						}
					}
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					leftType
					rightType
					title
					video {
						url
						thumbnail {
							${ImageFields}
						}
					}
				}
				... on Project_Postbuilder_Modules_PostCarousel {
					${ModuleSettings}
					posts {
						post {
							... on Post {
								${PostFields}
							}
							... on Page {
								${PageFields}
							}
							... on Showroom {
								${ShowroomFields}
							}
							... on Brand {
								${BrandFields}
							}
							... on SimpleProduct {
								${SimpleProductFields}
							}
							... on VariableProduct {
								${VariableProductFields}
							}
						}
					}
					title
					linkButton {
						target
						title
						url
					}
				}
				... on Project_Postbuilder_Modules_ImageGrid {
					${ModuleSettings}
					copy
					title
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					gallery {
						image {
							${ImageFields}
						}
					}
				}
				... on Project_Postbuilder_Modules_Stores {
					${ModuleSettings}
				}
				... on Project_Postbuilder_Modules_ColumnsBlock {
					${ModuleSettings}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					alignment
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					leftType
					copy
					title
					columns {
						image {
							${ImageFields}
						}
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
				}
			}
		}
		terms: projectCategories {
			edges {
				node {
					slug
					name
					link
				}
			}
		}
	}
`;

export const RELATED_PROJECT_QUERY = gql`
	query RELATED_PROJECT_QUERY($id: ID!) {
		projects(first: 3, where: { notIn: [$id]} ) {
			edges {
				node {
					... on Project {
						${POST_CARD_FRAGMENT}
					}
				}
			}
		}
	}
`;

export const ALL_PROJECTS_CATEGORIES_QUERY = gql`
	query ALL_PROJECTS_CATEGORIES_QUERY {
		projectCategories(where: { hideEmpty: true }) {
			edges {
				node {
					name
					slug
				}
			}
		}
	}
`;

export const PostCard = `
	id
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
	link
	terms {
		edges {
			node {
				slug
				name
			}
		}
	}
`;

export const ALL_PROJECTS_QUERY = gql`
	query ALL_PROJECTS_QUERY {
		projects {
			edges {
				node {
					${PostCard}
				}
			}
		}
	}
`;

export const PROJECT_QUERY = gql`
	${PROJECT_FRAGMENT}
	${STAFF_FRAGMENT}
	query PROJECT_QUERY($id: ID!) {
		project(id: $id, idType: URI) {
			...ProjectPostFields
		}
		showrooms {
			edges {
				node {
					...ShowroomFields
				}
			}
		}
	}
`;

export const PROJECT_DRAFT_QUERY = gql`
	${PROJECT_FRAGMENT}
	${STAFF_FRAGMENT}
	query PROJECT_DRAFT_QUERY($id: ID!) {
		project(id: $id, idType: DATABASE_ID) {
			preview {
				node {
					...ProjectPostFields
				}
			}
		}
		showrooms {
			edges {
				node {
					...ShowroomFields
				}
			}
		}
	}
`;
