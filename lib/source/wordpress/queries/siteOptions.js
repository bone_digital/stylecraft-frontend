import { gql } from '@apollo/client';
import { ImageFields } from './pageBuilder';

export const SITE_OPTIONS_QUERY = gql`
	query SITE_OPTIONS_QUERY {
		acfOptions {
			pageSlug
			siteOptions {
				gaCode
				contactForm
				gravityFormsRegisterFormId
				createAccountIntro
				createAccountTerms
				downloadMessage
				footerCopy
				popupFormHeading
				popupFormImage {
					${ImageFields}
				}
				popupFormId
			}
		}
	}
`;

export const MENU_QUERY = gql`
	query MENU_QUERY($location: MenuLocationEnum!) {
		menus(where: { location: $location }) {
			edges {
				node {
					menuItems {
						edges {
							node {
								id
								path
								label
							}
						}
					}
				}
			}
		}
	}
`;
