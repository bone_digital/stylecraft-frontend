export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const BrandFields = `
	id
	link
	title
	brand {
		description
		excerpt
	}
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
`;

export const PostFields = `
	id
	title
	link
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	categories: postCategories(first: 1) {
		edges {
			node {
				id
				name
			}
		}
	}
`;

export const ProjectFields = `
	id
	title
	link
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	categories: projectCategories(first: 1) {
		edges {
			node {
				id
				name
			}
		}
	}
`;

export const PageFields = `
	id
	link
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
`;

export const StaffFields = `
	id
	link
	title
	staff {
		firstName
		lastName
		role
	}
	featuredImage {
		node {
			${ImageFields}
		}
	}
`;

export const ShowroomFields = `
	id
	link
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
	showroom {
		address {
			city
			country
			countryShort
			latitude
			longitude
			placeId
			postCode
			state
			stateShort
			streetAddress
			streetName
			streetNumber
		}
		email
		fax
		formattedAddress
		phone
	}
`;

export const SimpleProductFields = `
	id
	link
	name
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	product {
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
`;

export const VariableProductFields = `
	id
	link
	name
	databaseId
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	product {
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
`;

export const ModuleSettings = `
	backgroundColor
	fieldGroupName
	anchorId
`;

export const PageBuilderModuleFragments = `
	... on Page_Pagebuilder_Modules_ColumnsBlock {
		${ModuleSettings}
		columnsPerRow
		posts {
			post {
				... on Post {
					${PostFields}
				}
				... on Page {
					${PageFields}
				}
				... on Showroom {
					${ShowroomFields}
				}
				... on Brand {
					${BrandFields}
				}
				... on SimpleProduct {
					${SimpleProductFields}
				}
				... on VariableProduct {
					${VariableProductFields}
				}
			}
		}
		title
		linkButton {
			target
			title
			url
		}
	}
	... on Page_Pagebuilder_Modules_PostCarousel {
		${ModuleSettings}
		posts {
			post {
				... on Post {
					${PostFields}
				}
				... on Page {
					${PageFields}
				}
				... on Showroom {
					${ShowroomFields}
				}
				... on Project {
					${ProjectFields}
				}
				... on Brand {
					${BrandFields}
				}
				... on SimpleProduct {
					${SimpleProductFields}
				}
				... on VariableProduct {
					${VariableProductFields}
				}
			}
		}
		title
		linkButton {
			target
			title
			url
		}
	}
	... on Page_Pagebuilder_Modules_ContactCta {
		${ModuleSettings}
		copy
	}
	... on Page_Pagebuilder_Modules_NewsletterSignup {
		${ModuleSettings}
		copy
	}
	... on Page_Pagebuilder_Modules_OneColumnContent {
		${ModuleSettings}
		copy
	}
	... on Page_Pagebuilder_Modules_TwoColumnContent {
		${ModuleSettings}
        column1Copy
        column2Copy
	}
	... on Page_Pagebuilder_Modules_Hotspot {
		${ModuleSettings}
		title
		copy
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		hotspotGallery {
			image {
				${ImageFields}
			}
			hotspots {
				xPosition
				yPosition
				product {
					... on SimpleProduct {
						${SimpleProductFields}
					}
					... on VariableProduct {
						${VariableProductFields}
					}
				}
			}
		}
	}
	... on Page_Pagebuilder_Modules_LinkList {
		${ModuleSettings}
		brands {
			cardImage: image {
				altText
				sourceUrl(size: CARD)
				thumbSourceUrl:sourceUrl(size: THUMBNAIL)
			}
			brand {
				... on Brand {
					brandId
					link
					title
					contentType {
						node {
							name
						}
					}
					brand {
						description
						excerpt
						locales
					}
					featuredImage {
						node {
							altText
							imageId: id
							sizes
							sourceUrl(size: CARD)
							thumbSourceUrl:sourceUrl(size: THUMBNAIL)
							mediaDetails {
								height
								width
								sizes {
									sourceUrl
									height
									width
									name
								}
							}
						}
					}
				}
			}
			fieldGroupName
		}
	}
	... on Page_Pagebuilder_Modules_BrandShowcase {
		${ModuleSettings}
		copy
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		brand {
			... on Brand {
				${BrandFields}
			}
		}
		image {
			${ImageFields}
		}
		hotspotGallery {
			hotspots {
				xPosition
				yPosition
				product {
					... on SimpleProduct {
						${SimpleProductFields}
					}
					... on VariableProduct {
						${VariableProductFields}
					}
				}
			}
			image {
				${ImageFields}
			}
		}
	}
	... on Page_Pagebuilder_Modules_Stores {
		${ModuleSettings}
	}
	... on Page_Pagebuilder_Modules_ImageVideo {
		${ModuleSettings}
		alignment
		columns {
			copy
			linkButton {
				prefix
				linkInner {
					target
					title
					url
				}
			}
		}
		copy
		gallery {
			image {
				${ImageFields}
			}
		}
		leftImage {
			${ImageFields}
		}
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		leftType
		rightType
		title
		video {
			url
			thumbnail {
				${ImageFields}
			}
		}
	}
	... on Page_Pagebuilder_Modules_ImageContent {
		${ModuleSettings}
		copy
		title
		linkButton {
			prefix
			linkInner {
				target
				title
				url
			}
		}
		fullbleedImage {
			${ImageFields}
		}
		contentImage {
			${ImageFields}
		}
	}
	... on Page_Pagebuilder_Modules_Accordions {
		${ModuleSettings}
		title
		accordions {
			title
			content
		}
	}
`;

export const PageBuilderQuery = `
pageBuilder {
	modules {
		${PageBuilderModuleFragments}
	}
}`;
