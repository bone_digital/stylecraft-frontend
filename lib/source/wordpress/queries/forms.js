import { gql } from '@apollo/client';

export const FORM_QUERY = gql`
	query getForm($formId: ID!) {
		gravityFormsForm(id: $formId, idType: DATABASE_ID) {
			formId
			title
			description
			button {
				text
			}
			confirmations {
				isDefault
				message
			}
			formFields(first: 500) {
				nodes {
					id
					type
					... on AddressField {
						isRequired
						id
						formId
						label
						description
						cssClass
						inputs {
							id
							key
							label
							placeholder
							isHidden
						}
					}
					... on CheckboxField {
						isRequired
						id
						formId
						label
						description
						cssClass
						inputs {
							id
						}
						choices {
							text
							value
						}
					}
					... on DateField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						placeholder
					}
					... on EmailField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						placeholder
					}
					... on MultiSelectField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						choices {
							text
							value
						}
					}
					... on NameField {
						isRequired
						id
						formId
						label
						description
						cssClass
						inputs {
							key
							label
							placeholder
							choices {
								text
								value
							}
						}
					}
					... on PhoneField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						placeholder
					}
					... on RadioField {
						isRequired
						id
						formId
						label
						description
						cssClass
						choices {
							text
							value
						}
					}
					... on SelectField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						defaultValue
						choices {
							text
							value
						}
					}
					... on TextField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						placeholder
					}
					... on TextAreaField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
					}
					... on TimeField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
					}
					... on WebsiteField {
						isRequired
						id
						formId
						label
						description
						cssClass
						isRequired
						placeholder
					}
				}
			}
		}
	}
`;
