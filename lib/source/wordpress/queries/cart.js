import { gql } from '@apollo/client';

import {
	ImageFields,
	VariableProductFields,
	SimpleProductFields
} from './products';

export const CartFields = `
	cart {
		total
		subtotal
		wholesaleTotal
		contents {
			itemCount
			edges {
				node {
					key
					quantity
					product {
						node {
							link
							name
							wholesalePrice
							... on VariableProduct {
								${VariableProductFields}
							}
							... on SimpleProduct {
								${SimpleProductFields}
							}
						}
					}
					variation {
						attributes {
							name
							label
							value
						}
						node {
							name
							databaseId
							id
							link
							name
							featuredImage {
								node {
									${ImageFields}
								}
							}
							status
							stockQuantity
							stockStatus
							contentType {
								node {
									name
								}
							}
						}
					}
					subtotal
					subtotalTax
					total
					tax
				}
			}
		}
	}
`;

/**
 * Basic cart query
 */
export const CART_QUERY = gql`
	query CART_QUERY {
		${CartFields}
	}
`;

/**
 * Basic add to cart mutation
 * @type {DocumentNode | *}
 */
export const UPDATE_CART = gql`
 mutation UPDATE_CART($input: UpdateItemQuantitiesInput!) {
	 updateItemQuantities(input: $input) {
		 cart {
			 total
			 wholesaleTotal
			 subtotal
			 contents {
				 itemCount
				 edges {
				 node {
					 key
					 quantity
					 subtotal
					 total
					 product {
						 node {
							 link
							 name
							 ... on VariableProduct {
								 ${VariableProductFields}
							 }
							 ... on SimpleProduct {
								 ${SimpleProductFields}
							 }
						 }
					 }
					 variation {
						 attributes {
							 name
							 label
							 value
						 }
						 node {
							 name
							 databaseId
							 id
							 link
							 name
							 featuredImage {
								 node {
									 ${ImageFields}
								 }
							 }
							 status
							 stockQuantity
							 stockStatus
							 contentType {
								 node {
									 name
								 }
							 }
						 }
					 }
				 }
			 }
		 }
	 }
	 }
 }
`;
