import { gql } from '@apollo/client';
import { PageBuilderQuery } from './pageBuilder';
const ProductPageBuilderQuery = PageBuilderQuery.toString().replace(/Page_Pagebuilder_/g, 'Product_Pagebuilder_');

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: LARGE)
	thumbSourceUrl: sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const SimpleProductFields = `
	id
	link
	name
	status
	weight
	width
	height
	length
	price
	regularPrice
	salePrice
	status
	stockQuantity
	stockStatus
	onSale
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	product {
		formalPricing
		pricingOptions
		startingPrice
		leadTime
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
`;

export const VariableProductFields = `
	id
	link
	name
	featuredImage {
		node {
			${ImageFields}
		}
	}
	status
	stockQuantity
	stockStatus
	contentType {
		node {
			name
		}
	}
	product {
		formalPricing
		pricingOptions
		designer {
			designerId
			name
		}
		brand {
			... on Brand {
				id
				title
			}
		}
	}
	attributes {
		edges {
			node {
				label
				name
				options
				variation
				id
				... on LocalProductAttribute {
					id
					name
				}
			}
		}
	}
	variations(first: 30) {
		edges {
			node {
				pricingOptions
				leadTime
				hasAttributes
				status
				weight
				width
				height
				length
				metaData {
					key
					value
					id
				}
				image {
					${ImageFields}
				}
				featuredImage {
					node {
						${ImageFields}
					}
				}
				attributes {
					edges {
						node {
							name
							label
							value
						}
					}
				}
				variationId: databaseId
				name
				onSale
				price
				salePrice
				regularPrice
				stockStatus
				stockQuantity
				status
			}
		}
	}
`;

export const BrandFields = `
	id
	brandId
	link
	title
	brand {
		count
		description
		excerpt
		relatedPosts {
			post {
				... on Post {
					slug
					postId
					title
					date
					link
					featuredImage {
						node {
							${ImageFields}
						}
					}
				}
			}
		}
		hotspotGallery {
			image {
				${ImageFields}
			}
			hotspots {
				xPosition
				yPosition
				product {
					... on SimpleProduct {
						${SimpleProductFields}
					}
					... on VariableProduct {
						${VariableProductFields}
					}
				}
			}
		}
	}
	featuredImage {
		node {
			${ImageFields}
		}
	}
`;

export const ProductCard = `
	id
	link
	name
	image {
		${ImageFields}
	}
	product {
		description
		excerpt
		brand {
			... on Brand {
				${BrandFields}
			}
		}
	}
`;

export const PRODUCT_FILTERS_QUERY = gql`
	query PRODUCT_FILTERS_QUERY {
		brands(first: 1000) {
			edges {
				node {
					name: title
					slug
					children: terms {
						edges {
							node {
								name
								slug
								count
								... on Designer {
									designerId
								}
							}
						}
					}
				}
			}
		}
		productCategories {
			edges {
				isPrimary
				node {
					name
					slug
					count
					parentId
					children {
						edges {
							node {
								name
								slug
							}
						}
					}
				}
			}
		}
		leadtimes(first: 1000) {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
		priceRanges(first: 1000) {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
		environmentalCertifications(first: 1000) {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
	}
`;

export const PRODUCT_CATEGORY_FILTERS_QUERY = gql`
	query PRODUCT_CATEGORY_FILTERS_QUERY {
		brands {
			edges {
				node {
					name: title
					slug
					children: terms {
						edges {
							node {
								name
								slug
								count
							}
						}
					}
				}
			}
		}
		productCategories {
			edges {
				isPrimary
				node {
					name
					slug
					count
					parentId
					children {
						edges {
							node {
								name
								slug
							}
						}
					}
				}
			}
		}
		leadtimes {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
		priceRanges {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
		environmentalCertifications {
			edges {
				node {
					name
					slug
					count
				}
			}
		}
	}
`;

export const ALL_PRODUCTS_QUERY = gql`
	query ALL_PRODUCTS_QUERY {
		products {
			edges {
				node {
					id
					description
					link
					slug
					name
					type
					leadtimes {
						edges {
							node {
								name
								slug
							}
						}
					}
					ranges {
						edges {
							node {
								name
								slug
							}
						}
					}
					productCategories {
						edges {
							isPrimary
							node {
								name
								slug
							}
						}
					}
					image {
						${ImageFields}
					}
					galleryImages {
						edges {
							node {
								${ImageFields}
							}
						}
					}
					seo {
						fullHead
					}
					... on VariableProduct {
						${VariableProductFields}
					}
					... on SimpleProduct {
						${SimpleProductFields}
					}
					related {
						edges {
							node {
								name
							}
						}
					}
				}
			}
		}
	}
`;

export const ALL_PRODUCTS_CATEGORIES_QUERY = gql`
	query ALL_PRODUCTS_CATEGORIES_QUERY {
		productCategories(where: { hideEmpty: true }) {
			edges {
				isPrimary
				node {
					id
					slug
					name
				}
			}
		}
	}
`;

export const ALL_PRODUCTS_RANGES_QUERY = gql`
	query ALL_PRODUCTS_RANGES_QUERY {
		ranges(where: { hideEmpty: true }) {
			edges {
				node {
					id
					slug
					name
				}
			}
		}
	}
`;

export const RELATED_PRODUCTS_QUERY = gql`
	query RELATED_PROJECT_QUERY($id: ID!) {
		products(first: 6, where: { notIn: [$id]} ) {
			edges {
				node {
					post {
						__typename
						... on VariableProduct {
							${VariableProductFields}
						}
						... on SimpleProduct {
							${SimpleProductFields}
						}
					}
				}
			}
		}
	}
`;

 // NOTE: This is a simple query to get the page to load faster, but should not be used in PROD.
// export const PRODUCT_QUERY = gql`
// 	query PRODUCT_QUERY($id: ID!, $idType: ProductIdTypeEnum) {
// 		product(id: $id, idType: $idType) {
// 			slug
// 			name
// 			type
// 			link
// 			purchasable
// 			databaseId
// 			product {
// 				locales
// 				backgroundColor
// 				description
// 				excerpt
// 				brand {
// 					... on Brand {
// 						${BrandFields}
// 					}
// 				}
// 				columns {
// 					title
// 					featured
// 					rows {
// 						row
// 					}
// 				}
// 				designer {
// 					designerId
// 					name
// 				}
// 				detailsColumns {
// 					copy
// 				}
// 				downloads {
// 					type
// 					wholesaleExclusive
// 					file {
// 						altText
// 						sourceUrl
// 						title
// 						link
// 						mediaItemUrl
// 					}
// 				}
// 				structuralWarranty
// 				originOfManufacture
// 				wholesalePrice
// 				gallery {
// 					image {
// 						${ImageFields}
// 					}
// 					caption {
// 						copy
// 						prefix
// 					}
// 				}
// 				productGallery {
// 					type
// 					image {
// 						${ImageFields}
// 					}
// 					imageGrid {
// 						image {
// 							${ImageFields}
// 						}
// 					}
// 				}
// 				wholesaleGallery {
// 					type
// 					image {
// 						${ImageFields}
// 					}
// 					imageGrid {
// 						image {
// 							${ImageFields}
// 						}
// 					}
// 				}
// 				relatedProducts {
// 					post {
// 						__typename
// 						... on VariableProduct {
// 							${VariableProductFields}
// 						}
// 						... on SimpleProduct {
// 							${SimpleProductFields}
// 						}
// 					}
// 				}
// 			}
// 		}
// 	}
// `;

export const PRODUCT_QUERY = gql`
	query PRODUCT_QUERY($id: ID!, $idType: ProductIdTypeEnum) {
		product(id: $id, idType: $idType) {
			slug
			name
			type
			link
			purchasable
			databaseId
			${ProductPageBuilderQuery}
			leadtimes {
				edges {
					node {
						name
						slug
					}
				}
			}
			ranges {
				edges {
					node {
						name
						slug
					}
				}
			}
			environmentalCertifications {
				edges {
					node {
						name
						slug
					}
				}
			}
			productCategories {
				edges {
					isPrimary
					node {
						name
						slug
						link
					}
				}
			}
			image {
				${ImageFields}
			}
			galleryImages {
				edges {
					node {
						${ImageFields}
					}
				}
			}
			... on VariableProduct {
			seo {
				fullHead
			}
				${VariableProductFields}
			}
			... on SimpleProduct {
			seo {
				fullHead
			}
				${SimpleProductFields}
			}
			product {
				locales
				backgroundColor
				description
				excerpt
				brand {
					... on Brand {
						${BrandFields}
					}
				}
				columns {
					title
					featured
					rows {
						row
					}
				}
				designer {
					designerId
					name
				}
				detailsColumns {
					copy
				}
				downloads {
					type
					wholesaleExclusive
					file {
						altText
						sourceUrl
						title
						link
						mediaItemUrl
					}
				}
				structuralWarranty
				originOfManufacture
				wholesalePrice
				gallery {
					image {
						${ImageFields}
					}
					caption {
						copy
						prefix
					}
				}
				productGallery {
					type
					image {
						${ImageFields}
					}
					imageGrid {
						image {
							${ImageFields}
						}
					}
				}
				wholesaleGallery {
					type
					image {
						${ImageFields}
					}
					imageGrid {
						image {
							${ImageFields}
						}
					}
				}
				relatedProducts {
					post {
						__typename
						... on VariableProduct {
							${VariableProductFields}
						}
						... on SimpleProduct {
							${SimpleProductFields}
						}
					}
				}
			}
		}
		acfOptions {
			siteOptions {
				globalPdfLabel
				globalPdf {
					link
					mediaItemUrl
					title
				}
			}
		}
	}
`;

export const PRODUCT_BRAND_DEFAULT_RELATED_PRODUCTS_QUERY = gql`
query PRODUCT_QUERY($id: ID!, $idType: ProductIdTypeEnum) {
		product(id: $id, idType: $idType) {
			product {
				locales
				backgroundColor
				description
				excerpt
				brand {
					... on Brand {
						title
						slug
						brand {
							defaultRelatedProducts {
								post {
									__typename
									... on VariableProduct {
										${VariableProductFields}
									}
									... on SimpleProduct {
										${SimpleProductFields}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`;
