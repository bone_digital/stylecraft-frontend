import { gql } from '@apollo/client';
import {
	PageFields,
	ShowroomFields,
	BrandFields,
	SimpleProductFields,
	VariableProductFields
} from './pageBuilder';

export const ImageFields = `
	id
	sizes
	altText
	sourceUrl(size: CARD)
	largeSourceUrl:sourceUrl(size: _2048X2048)
	thumbSourceUrl:sourceUrl(size: THUMBNAIL)
	mediaDetails {
		height
		width
		sizes {
			sourceUrl
			height
			width
			name
		}
	}
`;

export const PostSlide = `
	id
	title
	link
	featuredImage {
		node {
			${ImageFields}
		}
	}
	contentType {
		node {
			name
		}
	}
	categories: postCategories(first: 1) {
		edges {
			node {
				id
				name
			}
		}
	}
`;

const POST_HEADER = `
	postHeader {
		author
		gallery {
			image {
				${ImageFields}
			}
		}
		intro
		backgroundColor
		headingColor
		additionalDetails {
			copy
			subtitle
		}
		columns {
			copy
			linkButton {
				prefix
				linkInner {
					target
					title
					url
				}
			}
		}
	}
`;

export const ALL_POST_CATEGORIES_QUERY = gql`
	query ALL_POST_CATEGORIES_QUERY {
		postCategories(where: { hideEmpty: true }) {
			edges {
				node {
					name
					slug
				}
			}
		}
	}
`;

export const ModuleSettings = `
	backgroundColor
	fieldGroupName
`;

const POST_CARD_FRAGMENT = `
	slug
	postId
	title
	content
	date
	link
	featuredImage {
		node {
			${ImageFields}
		}
	}
	terms {
		edges {
			node {
				slug
				name
				link
			}
		}
	}
`;

const POST_FRAGMENT = `
	fragment PostFields on Post {
		slug
		postId
		title
		content
		date
		seo {
			fullHead
		}
		featuredImage {
			node {
				${ImageFields}
			}
		}
		relatedPosts {
			relatedPosts {
				post {
					... on Post {
						${POST_CARD_FRAGMENT}
					}
				}
			}
		}
		post {
			${POST_HEADER}
		}
		postBuilder {
			modules {
				... on Post_Postbuilder_Modules_ImageVideo {
					${ModuleSettings}
					alignment
					columns {
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
					copy
					gallery {
						image {
							${ImageFields}
						}
					}
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					leftType
					rightType
					title
					video {
						url
						thumbnail {
							${ImageFields}
						}
					}
				}
				... on Post_Postbuilder_Modules_PostCarousel {
					${ModuleSettings}
					posts {
						post {
							... on Post {
								${PostSlide}
							}
							... on Page {
								${PageFields}
							}
							... on Showroom {
								${ShowroomFields}
							}
							... on Brand {
								${BrandFields}
							}
							... on SimpleProduct {
								${SimpleProductFields}
							}
							... on VariableProduct {
								${VariableProductFields}
							}
						}
					}
					title
					linkButton {
						target
						title
						url
					}
				}
				... on Post_Postbuilder_Modules_ImageGrid {
					${ModuleSettings}
					copy
					title
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					gallery {
						image {
							${ImageFields}
						}
					}
				}
				... on Post_Postbuilder_Modules_Stores {
					${ModuleSettings}
				}
				... on Post_Postbuilder_Modules_ColumnsBlock {
					${ModuleSettings}
					linkButton {
						prefix
						linkInner {
							target
							title
							url
						}
					}
					alignment
					leftImage {
						${ImageFields}
					}
					imageCaption {
						prefix
						copy
					}
					leftType
					copy
					title
					columns {
						image {
							${ImageFields}
						}
						copy
						linkButton {
							prefix
							linkInner {
								target
								title
								url
							}
						}
					}
				}
			}
		}
		terms: postCategories {
			edges {
				node {
					link
					slug
					name
				}
			}
		}
	}
`;

export const RELATED_POST_QUERY = gql`
	query RELATED_POST_QUERY($id: ID!) {
		posts(first: 3, where: { notIn: [$id]} ) {
			edges {
				node {
					... on Post {
						${POST_CARD_FRAGMENT}
					}
				}
			}
		}
	}
`;

export const STAFF_FRAGMENT = `
	fragment ShowroomFields on Showroom {
		title
		slug
		showroom {
			address {
				city
				country
				countryShort
				longitude
				latitude
				placeId
				state
				postCode
				stateShort
				streetAddress
				streetNumber
				zoom
				streetName
			}
			formattedAddress
			phone
			fax
			email
			showroomStaff {
				staff {
					... on Staff {
					id
						title
						staff {
							role
							lastName
							firstName
						}
						featuredImage {
							node {
								${ImageFields}
							}
						}
						teams {
							edges {
								node {
									name
									slug
								}
							}
						}
					}
				}
			}
		}
	}
`;

export const PostCard = `
	id
	title
	featuredImage {
		node {
			${ImageFields}
		}
	}
	link
	terms: postCategories {
		edges {
			node {
				slug
				name
			}
		}
	}
`;

export const ALL_POSTS_QUERY = gql`
	query ALL_POSTS_QUERY {
		posts {
			edges {
				node {
					${PostCard}
				}
			}
		}
	}
`;

export const POST_QUERY = gql`
	${POST_FRAGMENT}
	${STAFF_FRAGMENT}
	query POST_QUERY($id: ID!) {
		post(id: $id, idType: SLUG) {
			...PostFields
		}
		showrooms {
			edges {
				node {
					...ShowroomFields
				}
			}
		}
	}
`;

export const POST_DRAFT_QUERY = gql`
	${POST_FRAGMENT}
	${STAFF_FRAGMENT}
	query POST_DRAFT_QUERY($id: ID!) {
		post(id: $id, idType: DATABASE_ID) {
			slug
			status
			preview {
				node {
					...PostFields
				}
			}
		}
		showrooms {
			edges {
				node {
					...ShowroomFields
				}
			}
		}
	}
`;
