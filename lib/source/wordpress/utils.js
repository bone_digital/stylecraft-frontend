/**
 * Loops through page builder module fields to clean the data
 * @param data
 * @returns {*}
 */
export function retrievePageBuilderModules(data) {
	let modules = data?.pageBuilder?.modules?.map((module, i) => {
		/**
		 * Filters can be placed here if need be
		 */
		return module;
	});
	return modules;
}

/**
 * Loops through post builder module fields to clean the data
 * @param data
 * @returns {*}
 */
export function retrievePostBuilderModules(data) {
	let modules = data?.postBuilder?.modules?.map((module, i) => {
		/**
		 * Filters can be placed here if need be
		 */
		return module;
	});
	return modules;
}

export function cleanModuleName(module) {
	if (!module?.fieldGroupName) {
		return null;
	}

	let moduleName = module?.fieldGroupName.replace(
		/Page_Pagebuilder_Modules_|Product_Pagebuilder_Modules_|Brand_Pagebuilder_Modules_|Project_Postbuilder_Modules_|Post_Postbuilder_Modules_|Showroom_Postbuilder_Modules_/g,
		''
	);
	return moduleName;
}

export function cleanModuleId(module) {
	const random = Math.ceil(Math.random() * 100000);
	return `${module.fieldGroupName}${random}`;
}

export function slugify(str) {
	return str
		.toLowerCase()
		.trim()
		.replace(/[^\w\s-]/g, '')
		.replace(/[\s_-]+/g, '-')
		.replace(/^-+|-+$/g, '');
}

/**
 *
 * @param {String} price
 * @return {String}
 */
export function formatPriceWithoutDecimals(price) {
	if (typeof price !== 'string') return '';

	let formatted = price.replace(/\.\d*$/, '');
	return formatted;
}


export function shouldDisableDynamicPathSSG() {
	return process.env.DISABLE_DYNAMIC_PATHS_SSG === 'true';
}
