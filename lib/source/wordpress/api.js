import {
	ALL_POSTS_QUERY,
	POST_QUERY,
	POST_DRAFT_QUERY,
	ALL_POST_CATEGORIES_QUERY,
	RELATED_POST_QUERY
} from './queries/posts';

import {
	ALL_PROJECTS_QUERY,
	PROJECT_QUERY,
	PROJECT_DRAFT_QUERY,
	ALL_PROJECTS_CATEGORIES_QUERY,
	RELATED_PROJECT_QUERY
} from './queries/projects';

import {
	ALL_BRANDS_QUERY,
	BRAND_QUERY,
	BRAND_DRAFT_QUERY,
	RELATED_BRAND_QUERY
} from './queries/brands';

import {
	ALL_SHOWROOMS_QUERY,
	SHOWROOM_QUERY,
	SHOWROOM_DRAFT_QUERY,
	RELATED_SHOWROOM_QUERY
} from './queries/showrooms';

import {
	ALL_PRODUCTS_QUERY,
	ALL_PRODUCTS_CATEGORIES_QUERY,
	ALL_PRODUCTS_RANGES_QUERY,
	PRODUCT_QUERY,
	PRODUCT_DRAFT_QUERY,
	PRODUCT_FILTERS_QUERY, PRODUCT_BRAND_DEFAULT_RELATED_PRODUCTS_QUERY,
	RELATED_PRODUCTS_QUERY
} from "./queries/products";

import { CURRENT_USER_QUERY } from './queries/users';
import { MENU_QUERY, SITE_OPTIONS_QUERY } from './queries/siteOptions';
import {
	ALL_PAGES_QUERY,
	PAGE_QUERY,
	PAGE_DRAFT_QUERY,
	HOME_PAGE_QUERY,
	HOME_DRAFT_PAGE_QUERY,
	PAGE_QUERY_BY_ID
} from './queries/pages';

import { CART_QUERY } from './queries/cart';

import OAuth from 'oauth-1.0a';
import crypto from 'crypto';
import client from './apolloClient';

const API_URL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}`;

/**
 * Processes a query to the WP Rest API
 *
 * @param query
 * @return {Promise<any>}
 */
async function fetchAPI(query, params, previewData = false) {
	const { variables } = params || {};

	const json = await client.query({
		query,
		variables
	});

	if (json.errors) {
		console.log(`message: ${json.errors}`);
		throw new Error('Failed API call');
	}

	return json.data;
}

/**
 * Runs a fetch request and returns the response.
 * @param endpoint WordPress API endpoint excluding /wp-json/
 * @returns {Promise<any>}
 */
async function fetchRESTAPI(endpoint, headers = null, postBody = null) {
	const method = postBody ? 'POST' : 'GET';
	const url = `${API_URL}${endpoint}`;

	const reqBody = postBody ? JSON.stringify(postBody) : null;

	try {
		const data = await fetch(url, {
			headers,
			method,
			body: reqBody
		});

		const json = await data.json();

		return json;
	} catch (error) {
		throw new Error(error.message);
	}
}

/**
 * Returns all posts
 *
 * @return {Promise<*>}
 */
export async function getAllPosts() {
	const query = ALL_POSTS_QUERY;
	const data = await fetchAPI(query);
	return data?.posts;
}

/**
 * Returns all projects
 *
 * @return {Promise<*>}
 */
export async function getAllProjects() {
	const query = ALL_PROJECTS_QUERY;
	const data = await fetchAPI(query);
	return data?.projects;
}

/**
 * Returns all brands
 *
 * @return {Promise<*>}
 */
export async function getAllBrands() {
	const query = ALL_BRANDS_QUERY;
	const data = await fetchAPI(query);
	return data?.brands;
}

/**
 * Returns all brands
 *
 * @return {Promise<*>}
 */
export async function getAllRESTBrands() {
	const endpoint = `stylecraft/v1/fetch-rest-brands`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all products
 *
 * @return {Promise<*>}
 */
export async function getAllRESTProducts() {
	const endpoint = `stylecraft/v1/fetch-all-products`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all showrooms
 *
 * @return {Promise<*>}
 */
export async function getAllShowrooms() {
	const query = ALL_SHOWROOMS_QUERY;
	const data = await fetchAPI(query);
	return data?.showrooms;
}

/**
 * Returns current user
 *
 * @return {Promise<*>}
 */
export async function getUser() {
	const query = CURRENT_USER_QUERY;
	const data = await fetchAPI(query);
	return data?.viewer;
}

/**
 * Returns all products
 *
 * @return {Promise<*>}
 */
export async function getAllProducts() {
	const query = ALL_PRODUCTS_QUERY;
	const data = await fetchAPI(query);

	return data?.products?.edges;
}

/**
 * Returns all product categories
 *
 * @return {Promise<*>}
 */
export async function getAllProductCategories() {
	const query = ALL_PRODUCTS_CATEGORIES_QUERY;
	const data = await fetchAPI(query);

	return data?.productCategories?.edges;
}

/**
 * Returns all product categories
 *
 * @return {Promise<*>}
 */
export async function getAllRESTProductCategories() {
	const endpoint = `stylecraft/v1/fetch-all-product-categories`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all project categories
 *
 * @return {Promise<*>}
 */
export async function getAllRESTProjectCategories() {
	const endpoint = `stylecraft/v1/fetch-all-project-categories`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all product ranges
 *
 * @return {Promise<*>}
 */
export async function getAllRESTProductRange() {
	const endpoint = `stylecraft/v1/fetch-all-product-ranges`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all staff
 *
 * @return {Promise<*>}
 */
export async function getAllRESTStaff() {
	const endpoint = `stylecraft/v1/fetch-all-staff`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all showrooms
 *
 * @return {Promise<*>}
 */
export async function getAllRESTShowroom() {
	const endpoint = `stylecraft/v1/fetch-all-showrooms`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all pages
 *
 * @return {Promise<*>}
 */
export async function getAllRESTPages() {
	const endpoint = `stylecraft/v1/fetch-all-pages`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all posts
 *
 * @return {Promise<*>}
 */
export async function getAllRESTPosts() {
	const endpoint = `stylecraft/v1/fetch-all-posts`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all projects
 *
 * @return {Promise<*>}
 */
export async function getAllRESTProjects() {
	const endpoint = `stylecraft/v1/fetch-all-projects`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns related products for a product
 *
 * @return {Promise<*>}
 */
export async function getRESTRelatedProductCards(id, locale, postCount = 6) {
	const params = new URLSearchParams();
	params.set('product_id', id);
	params.set('locale', locale);
	params.set('post_count', postCount);

	const endpoint = `stylecraft/v1/fetch-related-products?${params.toString()}`;
	const headers = {
		'Content-Type': 'application/json'
	};

	const data = await fetchRESTAPI(endpoint, headers);

	return data;


}

/**
 * Returns all blog categories
 *
 * @return {Promise<*>}
 */
export async function getAllRESTBlogCategories() {
	const endpoint = `stylecraft/v1/fetch-all-blog-categories`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Returns all product ranges
 *
 * @return {Promise<*>}
 */
export async function getAllProductRanges() {
	const query = ALL_PRODUCTS_RANGES_QUERY;
	const data = await fetchAPI(query);

	return data?.ranges?.edges;
}

/**
 * Gets a single post and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getPost(id, preview = false) {
	let query = preview ? POST_DRAFT_QUERY : POST_QUERY;

	const variables = {
		variables: {
			id,
			asPreview: preview
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	const postData = preview
		? {
				post: data?.post?.preview?.node || null,
				showrooms: data?.showrooms || null
		  }
		: data;

	return postData;
}

/**
 * Returns all blog categories
 *
 * @return {Promise<*>}
 */
export async function getAllBlogCategories() {
	const query = ALL_POST_CATEGORIES_QUERY;
	const data = await fetchAPI(query);

	return data?.postCategories?.edges;
}

/**
 * Returns all project categories
 *
 * @return {Promise<*>}
 */
export async function getAllProjectCategories() {
	const query = ALL_PROJECTS_CATEGORIES_QUERY;
	const data = await fetchAPI(query);

	return data?.projectCategories?.edges;
}

/**
 * Gets a single project and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getProject(id, preview = false) {
	let query = preview ? PROJECT_DRAFT_QUERY : PROJECT_QUERY;

	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	const projectData = preview
		? {
				project: data?.project?.preview?.node,
				showrooms: data?.showrooms
		  }
		: data;

	return projectData;
}

/**
 * Gets a single post and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getBrand(id, preview = false) {
	let query = preview ? BRAND_DRAFT_QUERY : BRAND_QUERY;

	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);

	if (data.length <= 0) {
		return [];
	}

	const brandData = preview
		? {
				brand: data?.brand.preview?.node || null,
				showrooms: data?.showrooms || null
		  }
		: data;

	return brandData;
}

/**
 * Gets a single post and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getShowroom(id, preview = false) {
	let query = preview ? SHOWROOM_DRAFT_QUERY : SHOWROOM_QUERY;

	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	const showroomData = preview
		? {
				showroom: data?.showroom?.preview?.node || null,
				staffShowroom: data?.staffShowroom
		  }
		: data;

	return showroomData;
}

/**
 * Gets related posts and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getRelatedPosts(id) {
	let query = RELATED_POST_QUERY;
	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets related brands and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getRelatedBrands(id) {
	let query = RELATED_BRAND_QUERY;
	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets related projects and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getRelatedProjects(id) {
	let query = RELATED_PROJECT_QUERY;
	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets related showrooms and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getRelatedShowrooms(id) {
	let query = RELATED_SHOWROOM_QUERY;
	const variables = {
		variables: {
			id
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets a single product and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getProductBrandData(id, idType = 'SLUG') {
	let query = PRODUCT_BRAND_DEFAULT_RELATED_PRODUCTS_QUERY;

	const variables = {
		variables: {
			id,
			idType
		}
	};

	const data = await fetchAPI(query, variables);

	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets a single product and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getProduct(id, idType = 'SLUG') {
	let query = PRODUCT_QUERY;

	const variables = {
		variables: {
			id,
			idType
		}
	};

	const data = await fetchAPI(query, variables);

	if (data.length <= 0) {
		return [];
	}

	return data;
}

/**
 * Gets related products and returns its data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getRelatedProducts(id, idType = 'SLUG') {
	let query = RELATED_PRODUCTS_QUERY;
	const variables = {
		variables: {
			id,
			idType
		}
	};

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	return data;
}


/**
 * @returns Product Filters
 */
export async function getProductFilters() {
	let query = PRODUCT_FILTERS_QUERY;

	const data = await fetchAPI(query);

	return data;
}

/**
 * Returns all pages
 *
 * @return {Promise<*>}
 */
export async function getAllPages() {
	const query = ALL_PAGES_QUERY;
	const data = await fetchAPI(query);
	return data?.pages;
}

/**
 * Gets a single page's data based on a slug
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getPage(slug, previewData = false) {
	const query = previewData ? PAGE_DRAFT_QUERY : PAGE_QUERY;

	const variables = {
		variables: {
			slug
		}
	};

	// console.log({variables, previewData})

	const data = await fetchAPI(query, variables, previewData);
	// console.log('getPage', previewData, slug, !!data?.page ? 'found page' : 'no page')
	// console.log('getPage', previewData, slug, data)

	return data;
	if (data.length <= 0) {
		return [];
	}

	const pageData = previewData
		? {
				page: data?.page?.preview?.node || null,
				showrooms: data?.showrooms || null
		  }
		: data;

	return pageData;
}

/**
 * Gets a single page's data based on a slug
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getPageById(id, previewData = false) {

	const query = PAGE_QUERY_BY_ID;

	id = parseInt(id);

	const params = {
		variables: {
			id
		}
	};

	// console.log({variables, previewData})

	const data = await fetchAPI(query, params);
	// console.log('getPage', previewData, slug, !!data?.page ? 'found page' : 'no page')
	// console.log('getPageById', !!previewData, id, data)

	return data;
	if (data.length <= 0) {
		return [];
	}

	const pageData = previewData
		? {
				page: data?.page?.preview?.node || null,
				showrooms: data?.showrooms || null
		  }
		: data;

	return pageData;
}

/**
 * Gets a Home page's data
 *
 * @param slug
 * @return {Promise<*>}
 */
export async function getHomePage(previewData = false, id) {
	const query = previewData ? HOME_DRAFT_PAGE_QUERY : HOME_PAGE_QUERY;

	const variables = previewData
		? {
				variables: {
					id: parseInt(id)
				}
		  }
		: null;

	const data = await fetchAPI(query, variables);
	if (data.length <= 0) {
		return [];
	}

	const pageData = previewData
		? {
				page: data?.page?.preview?.node,
				showrooms: data?.showrooms
		  }
		: data;

	return pageData;
}

/**
 * Gets all menus from WP
 * @returns {Promise<*>}
 */
export async function getMenu(location = 'HEADER_NAVIGATION') {
	const query = MENU_QUERY;
	const variables = {
		variables: {
			location: location
		}
	};
	const data = await fetchAPI(query, variables);

	return data?.menus?.edges[0];
}

/**
 * Returns the site options from ACF
 * @returns {Promise<*>}
 */
export async function getSiteOptions() {
	const query = SITE_OPTIONS_QUERY;
	const data = await fetchAPI(query);

	return data?.acfOptions?.siteOptions;
}

function generateGravityFormsHeaders(endpoint) {
	const url = `${API_URL}${endpoint}`;

	const oauth = OAuth({
		consumer: {
			key: process.env.GF_CONSUMER_KEY,
			secret: process.env.GF_CONSUMER_SECRET
		},
		signature_method: 'HMAC-SHA1',
		hash_function(base_string, key) {
			return crypto
				.createHmac('sha1', key)
				.update(base_string)
				.digest('base64');
		}
	});
	const request_data = {
		url: url,
		method: 'GET'
	};

	return oauth.toHeader(oauth.authorize(request_data));
}

/**
 * Gets a form's details using OAuth 1.0
 * @param formID
 * @returns {Promise<string|*>}
 */
export async function getForm(formID = 1) {
	const endpoint = `gf/v2/forms/${formID}`;
	const headers = generateGravityFormsHeaders(endpoint);
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

/**
 * Creats a form submission and returns the result
 * @param formID
 * @param post
 * @returns {Promise<*>}
 */
export async function postForm(formID = 1, post) {
	const endpoint = `gf/v2/forms/${formID}/submissions`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers, post);
	return data;
}

/**
 * Creats a form submission and returns the result
 * @param formID
 * @param post
 * @returns {Promise<*>}
 */
export async function getRestBrand(slug = 'maiori') {
	const endpoint = `stylecraft/v1/fetch-brand-by-slug?slug=${slug}`;
	const headers = {
		'Content-Type': 'application/json'
	};
	const data = await fetchRESTAPI(endpoint, headers);

	return data;
}

// NOTE: looks like this doesn't ever get used.
export async function getCart() {
	const query = CART_QUERY;
	const data = await fetchAPI(query);
	return data;
}
