import { BLOG_PAGE_ID, CATEGORY_PREFIX } from '../config';
import OAuth from 'oauth-1.0a';
import crypto from 'crypto';
import { buildWordPressUrl, extractPaginationFromHeaders, extractPathnameFromWordPressUrl } from './utils';
import { isObject } from 'formik';

// API URL to the backend
const API_URL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}`;

/**
 * Runs a fetch request and returns the response.
 * @param endpoint WordPress API endpoint excluding /wp-json/
 * @returns {Promise<any>}
 */
async function fetchAPI(endpoint, headers = null, postBody = null, credentials = null, hideErrors = false, returnHeaders = false) {
	const method = postBody ? 'post' : 'get';
	const url = `${API_URL}${endpoint}`;
	const reqBody = postBody ? JSON.stringify(postBody) : null;
	const results = {
		success: false,
		data: null,
		headers: [],
		message: null,
		response: null,
	}


	try {
		let options = {
			method,
			mode: 'cors',
		};
		if( headers )
		{
			options.headers = headers;
		}
		if( credentials )
		{
			options.credentials = credentials;
		}
		if( reqBody )
		{
			options.body = reqBody;
		}
		const response = await fetch(url, options);

		results.response = response;

		const data = await response.json();

		results.data = data; // always return the response data

		// only check for client and server errors
		if( response.status >= 400 && response.status <= 599 ) {
			results.success = false;
			results.message = response.statusText || '';
		}
		else {
			results.success = true;
		}

		if( returnHeaders && response?.headers )
		{
			results.headers = Object.fromEntries(response?.headers?.entries());
		}
	}
	catch(error)
	{
		results.success = false;
		results.error = error.message;
	}

	return results;
}

// /**
//  * Get all posts of a given post type
//  *
//  *
//  *
//  * @param {String} postType
//  * @param {Object} queries
//  * @param {Object} headers
//  * @returns {Array}
//  */
// export async function getAll(postType = 'page', queries = {}, headers = {}) {
// 	const endpointBase = getEndpoint(postType);

// 	queries = queries || {};

// 	const requestedPerPage = queries.per_page;
// 	const maxPerPage = 100; // API limit

// 	let queryPerPage = requestedPerPage || 12; // default
// 	// adjust incase request is above the limit, or wants everything
// 	const shouldGetAll = queryPerPage > maxPerPage || queryPerPage === -1;
// 	if (shouldGetAll) {
// 		queryPerPage = maxPerPage;
// 	}

// 	queries.per_page = queryPerPage;

// 	const queryString = new URLSearchParams(queries).toString();
// 	const endpoint = `${endpointBase}/?${queryString}`;

// 	let initialResults = await fetchAPI(endpoint);
// 	if (!initialResults.success) {
// 		return null;
// 	}
// 	// console.log({endpoint}, initialResults.data)

// 	const returnHeaders = Object.fromEntries(initialResults.response.headers?.entries());

// 	const totalPosts = parseInt(returnHeaders['x-wp-total']) || undefined;
// 	const totalPages = parseInt(returnHeaders['x-wp-totalpages']) || 1;

// 	const posts = [...initialResults.data];

// 	// TODO: fallback/error handling
// 	if (shouldGetAll && queryPerPage < totalPosts) {
// 		const pagesRemaining = totalPages - 1;
// 		if (pagesRemaining) {
// 			for (let i = 2; i <= totalPages; i++) {
// 				const searchParams = new URLSearchParams(queries);
// 				searchParams.set('page', i);
// 				const endpoint = `${endpointBase}/?${searchParams.toString()}`;

// 				const results = await fetchAPI(endpoint);
// 				if (results.success) {
// 					results.data?.forEach(post => {
// 						posts.push(post);
// 					})
// 				}
// 			}
// 		}
// 	}

// 	return posts;
// }


/**
 * Gets a single post of a given post type
 *
 *
 * @param {string} postType
 * @param {string} slug
 * @param {Object|null} previewData
 * @returns array|null
 */
export async function getSingle(postType, slug, previewData = false, queries = {}) {
	if (!postType || !slug) {
		// should this throw instead??
		return [];
	}

	const endpoint = getEndpoint(postType);

	let fetchUrl = `${endpoint}/`;
	const searchParams = new URLSearchParams();
	searchParams.set('slug', slug);

	let credentials = null;
	let headers = {};
	queries = queries || {};
	if( previewData ) {
		const token = previewData.token;
		credentials = 'include';
		headers = {
			'Access-Control-Allow-Origin': '*',
			'Authorization' : `Bearer ${token}`
		};
		fetchUrl = `${endpoint}/${previewData.postID}/revisions/${previewData.previewID}`;
	}

	Object.entries(queries)?.forEach(([key, value]) => {
		searchParams.set(key, value);
	})

	if (searchParams) {
		fetchUrl += '?' + searchParams;
	}

	//console.log('getSingle', {fetchUrl})

	let results = await fetchAPI(fetchUrl, headers, null, credentials);


	let pageData = null;
	if (results?.success) {
		// data is usually returned from WP as a single item array
		if (Array.isArray(results?.data) && results.data.length) {
			pageData = results.data[0];
		}
		else if (isObject(results?.data)) {
			pageData = results.data;
		}
	}

	const hasData = !!pageData?.acf;

	if(!hasData) {
		return false;
	}

	return pageData;
}



/**
 * Gets a single post of a given post type
 *
 *
 * @param {String} postType
 * @param {integer} id
 * @param {Object|null} previewData
 * @returns array
 */
 export async function getSingleById(postType, id, previewData = false, queries = {}) {
	console.log('getSingleById', previewData)
	if (!postType || !id) {
		// should this throw instead??
		return [];
	}

	const endpoint = getEndpoint(postType);
	console.log('endpoint', endpoint)

	let fetchUrl = `${endpoint}/${id}`;
	const searchParams = new URLSearchParams();

	let credentials = null;
	let headers = {};
	if( previewData ) {
		const token = previewData.token;
		credentials = 'include';
		headers = {
			'Access-Control-Allow-Origin': '*',
			'Authorization' : `Bearer ${token}`
		};
		fetchUrl = `${endpoint}/${previewData.postID}/revisions/${previewData.previewID}`;
	}

	Object.entries(queries)?.forEach(([key, value]) => {
		searchParams.set(key, value);
	})

	if (searchParams) {
		fetchUrl += '?' + searchParams;
	}

	//console.log('getSingleById', {fetchUrl})

	let results = await fetchAPI(fetchUrl, headers, null, credentials);
	console.log({fetchUrl, headers, credentials, results})

	let pageData = null;
	if (results?.success) {
		// at this endpoint the page data isn't returned in an array - just a single object
		pageData = results.data;
	}

	return pageData;
}


/**
 * Returns all posts
 * @returns {Promise<*>}
 */
export async function getAllPosts() {
	const endpoint = 'wp/v2/posts'

	let results = await fetchAPI(endpoint);
	let posts = null;
	if (results?.success) {
		posts = results.data;
	}

	return posts;
}

// /**
//  * Returns posts with the given query object
//  * @param additionalQueries
//  * @returns {Promise<*>}
//  */
// export async function getPosts(additionalQueries = {}, returnHeaders = false) {
// 	const additionalQueriesFormatted = new URLSearchParams(additionalQueries).toString();
// 	let endpoint = `wp/v2/posts/?${additionalQueriesFormatted}`;
// 	let data = await fetchAPI(endpoint, null, null, null, false, returnHeaders);
// 	if( returnHeaders )
// 	{
// 		const pagination = extractPaginationFromHeaders(data.headers);

// 		data = {
// 			posts: data.data,
// 			headers: data.headers,
// 			pagination: pagination,
// 		};
// 	}
// 	return data;
// }

/**
 * Returns a single post
 * @param slug
 * @param preview
 * @returns {Promise<*>}
 */
export async function getPost(slug, previewData = false) {
	let endpoint = `wp/v2/posts/?slug=${slug}`;
	let credentials = null;
	let headers = {};
	if( previewData )
	{
		const token = previewData.token;
		credentials = 'include';
		headers = {
			'Access-Control-Allow-Origin': '*',
			'Authorization' : `Bearer ${token}`
		};
		endpoint = `wp/v2/posts/${previewData.postID}/revisions/${previewData.previewID}`;
	}
	let data = await fetchAPI(endpoint, headers, null, credentials);
	const seoData = await getSEO(slug);
	if( previewData )
	{
		data = [data];
	}
	if( data && isObject(data[0]) )
	{
		data[0].seo = seoData;
	}
	return data[0];
}

/**
 * Returns all pages
 * @returns {Promise<*>}
 */
export async function getAllPages() {
	let endpoint = 'wp/v2/pages';

	const perPage = 100;
	endpoint += `?per_page=${perPage}`;

	if(BLOG_PAGE_ID)
	{
		endpoint += `&exclude=${BLOG_PAGE_ID}`;
	}

	let results = await fetchAPI(endpoint);

	let pages = null;
	if (results?.success) {
		pages = results.data;
	}

	return pages;
}

/**
 * Returns a single page
 * @param {String || Int || Array} param
 * @returns {Promise<*>}
 */
export async function getPage(param, previewData = false) {

	const initialArg = getInitialArg(param);

	let endpoint = `wp/v2/pages/?${initialArg}`
	// let endpoint = `wp/v2/pages/?slug=${param}`

	let credentials = null;
	let headers = {};
	let results = null;
	if( previewData )
	{
		const token = previewData.token;
		credentials = 'include';
		headers = {
			'Access-Control-Allow-Origin': '*',
			'Authorization' : `Bearer ${token}`
		};
		endpoint = `wp/v2/pages/${previewData.postID}/revisions/${previewData.previewID}`;
	}

	results = await fetchAPI(endpoint, headers, null, credentials);

	let pageData = null;
	if (results?.success) {
		// data is usually returned from WP as a single item array
		if (Array.isArray(results?.data) && results.data.length) {
			pageData = results.data[0];
		}
		else if (isObject(results?.data)) {
			pageData = results.data;
		}
	}

	if (!previewData && pageData?.slug) {
		const extraFetches = await Promise.all([
			getSEO(pageData.link),
			getBreadcrumbs(pageData.link),
		]);

		const [seo, breadcrumbs] = extraFetches;
		// seo.html can return undefined. This causes an error because undefined
		// can't be serialized as JSON
		if (seo?.html === undefined) {
			seo.html = null;
		}

		pageData.seo = seo;
		pageData.breadcrumbs = breadcrumbs;
	}

	// if( !data && previewData )
	// {
	// 	//Data is empty... try a draft
	// 	let endpoint = `wp/v2/pages/?${initialArg}&status=draft`;
	// 	data = await fetchAPI(endpoint, headers, null, credentials);
	// }

	return pageData;
}

/**
 * Returns a single menu
 * @param location
 * @returns {Promise<*>}
 */
export async function getMenu(location = 'primary_navigation') {
	const endpoint = `wp-api-menus/v2/menu-locations/${location}`;
	const data = await fetchAPI(endpoint);
	return data;
}

/**
 * Returns all categories
 * @returns {Promise<*>}
 */
export async function getAllCategories() {
	const endpoint = `wp/v2/categories`;
	const data = await fetchAPI(endpoint);
	return data;
}

/**
 * Gets a single category
 * @param slug
 * @returns {Promise<*>}
 */
export async function getCategory(slug) {
	const endpoint = `wp/v2/categories?slug=${slug}`;
	let data = await fetchAPI(endpoint);
	if( data.length > 0 )
	{
		data = data[0];
	}
	const prefix = CATEGORY_PREFIX.replace("/", '');
	const seoData = await getSEO(`${prefix}/${slug}`); // TODO: arg needs to be a complete URL
	data.seo = seoData;

	return data;
}

/**
 * Returns a list of site options
 * @returns {Promise<*>}
 */
export async function getSiteOptions() {
	const endpoint = `acf/v3/options/options`;
	const data = await fetchAPI(endpoint);
	return data?.acf;
}

/**
 * Creates OAuth headers for Gravity Forms API calls
 * @param endpoint
 * @returns {string|OAuth.Header}
 */
function generateGravityFormsHeaders(endpoint) {
	const url = `${API_URL}${endpoint}`;
	const oauth = OAuth({
		consumer: {
			key: process.env.GF_CONSUMER_KEY,
			secret: process.env.GF_CONSUMER_SECRET,
		},
		signature_method: 'HMAC-SHA1',
		hash_function(base_string, key) {
			return crypto
				.createHmac('sha1', key)
				.update(base_string)
				.digest('base64')
		},
	});
	const request_data = {
		url: url,
		method: 'GET'
	};
	return oauth.toHeader(oauth.authorize(request_data));
}

/**
 * Gets a form's details using OAuth 1.0
 * @param formID
 * @returns {Promise<string|*>}
 */
export async function getForm(formID = 1) {
	const endpoint = `gf/v2/forms/${formID}`;
	const headers = generateGravityFormsHeaders(endpoint);
	const data = await fetchAPI(endpoint, headers);
	return data;
}

/**
 * Creats a form submission and returns the result
 * @param formID
 * @param post
 * @returns {Promise<*>}
 */
export async function postForm(formID = 1, post) {
	const endpoint = `gf/v2/forms/${formID}/submissions`;
	const headers = {
		'Content-Type': 'application/json',
	};
	const data = await fetchAPI(endpoint, headers, post);
	return data;
}

/**
 * Logs a user in and generates a token
 * @param username
 * @param password
 * @returns {Promise<*>}
 */
export async function login(username, password) {
	const endpoint = 'jwt-auth/v1/token';
	const headers = {
		'Content-Type': 'application/json',
	};
	const postData = {
		username,
		password
	};
	const data = await fetchAPI(endpoint, headers, postData, null, true);
	return data;
}

// /**
//  * Searches given a search term
//  * @param searchTerm
//  * @param postTypes
//  * @returns {Promise<*>}
//  */
// export async function search(
// 	searchTerm,
// 	postTypes = null,
// 	page = 1,
// 	returnHeaders = false
// ) {

// 	let queryArgs = {
// 		search: searchTerm,
// 		page: page,
// 		per_page: 5,
// 	};

// 	if(postTypes) {
// 		queryArgs['subtype'] = postTypes;
// 	}

// 	const queriesString = new URLSearchParams(queryArgs)
// 		.toString();


// 	const endpoint = `wp/v2/search/?${queriesString}`;

// 	let response = await fetchAPI(
// 		endpoint, null, null, null, false, returnHeaders
// 	);

// 	if( returnHeaders )
// 	{
// 		const pagination = extractPaginationFromHeaders(
// 			response?.headers
// 		);

// 		response = {
// 			posts: response?.data,
// 			headers: response?.headers,
// 			pagination: pagination,
// 		};
// 	}

// 	return response;
// }


function getEndpoint(postType) {
	const base = 'wp/v2';

	switch(postType) {
		case 'post':
			return `${base}/posts`;
		case 'page':
			return `${base}/pages`;
		case 'event':
			return `${base}/events`;
		case 'resource':
			return `${base}/resources`;
		case 'exhibition':
			return `${base}/exhibitions`;
		default:
			return `${base}/pages`;
	}
}


/**
 * Get Initial Argument from Paramater
 * @param {String || Int || Array} param
 * @returns string
 */
export function getInitialArg(param) {
	let argument;

	if(Array.isArray(param))
	{
		param = param[param.length - 1];
	}

	if(Number.isInteger(param))
	{
		argument = `id=${param}`;
	}
	else
	{
		argument = `slug=${param}`;
	}

	return argument;
}
