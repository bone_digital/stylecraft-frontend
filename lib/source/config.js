export const BRANDS_PREFIX = '/brands';
export const PRODUCTS_PREFIX = '/product';
export const PROJECTS_PREFIX = '/project';
export const SHOWROOMS_PREFIX = '/showroom';
export const STAFF_PREFIX = '/staff';
export const BLOG_PREFIX = '/journal';
