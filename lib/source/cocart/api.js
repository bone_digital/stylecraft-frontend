import Cookies from 'js-cookie';
import { gql, useMutation } from '@apollo/client';
import jwtDecode from 'jwt-decode';

const REFRESH_TOKEN = gql`
	mutation RefreshAuthToken($jwtRefreshToken: String!) {
		refreshJwtAuthToken(input: { jwtRefreshToken: $jwtRefreshToken }) {
			authToken
		}
	}
`;

/**
 * Checks if authToken is valid, returns it or a new one if not.
 * TODO: convert to rest
 *
 * @return {Promise<*>}
 */
export async function getAuthToken(authToken, jwtRefreshToken) {
	const expirationBuffer = 60; // in seconds
	const currentTimeInSeconds = new Date().getTime() / 1000;
	const authTokenExp = jwtDecode(authToken)?.exp;

	const timeLeft = authTokenExp - currentTimeInSeconds - expirationBuffer;

	if (timeLeft < 0) {
		const [refreshToken, { loading, error }] = useMutation(REFRESH_TOKEN, {
			variables: {
				jwtRefreshToken
			},
			onCompleted: (data) => {
				const authToken = data?.refreshJwtAuthToken?.authToken;
				Cookies.set('token', authToken);
				return authToken;
			},
			onError: (error) => {
				console.log({ error: error });
			}
		});
		
		authToken = refreshToken({
			variables: {
				jwtRefreshToken
			}
		});


	} else {
		return authToken;
	}

	return authToken;
}
