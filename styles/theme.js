export const theme = {
	fonts: {
		galanoGrotesque: '"GalanoGrotesque", sans-serif'
	},
	breakpoints: {
		xlDesktop: '(max-width: 1480px)',
		desktop: '(max-width: 1280px)',
		laptop: '(max-width: 1080px)',
		tablet: '(max-width: 768px)',
		mobile: '(max-width: 540px)'
	},
	colors: {
		white: '#fff',
		black: '#000',
		grey: '#F4F4F4',
		greyMedium: '#B3B7BA',
		brand: '#EB008B',
		pink: '#EB008B',
		greyDark: '#A7A9AC',
		green: 'rgba(207, 214, 203, 0.3)',
		greenDark: 'rgba(207, 214, 203, 0.4)',
		overlay: 'rgba(0, 0, 0, 0.2)',
		peach: '#E4D1C9',
		beige: '#ECEBE5',
		red: '#FF2700',
		nude: '#E4D1C9',
		smoke: '#F4F4F4',
		charcoal: '#6B6A6E',
		softPistachio: '#E2E7E0',
		midPink: '#FFBEc2',
		shiraz: '#5E1A33'
	},
	type: {
		display: ['80px', '0.7375'],
		nav: ['40px', '1.25'],
		h1: ['66px', '0.9'],
		h2: ['32px', '1.15'],
		h3: ['24px', '1.2'],
		h4: ['18px', '1.2'],
		h5: ['12px', '1.428'],
		h6: ['12px', '1.08'],
		p: ['15px', '1.333']
	},
	typeMobile: {
		display: ['40px', '1.25'],
		h1: ['40px', '1.03'],
		h2: ['28px', '1.21'],
		nav: ['34px', '1.67']
	},
	transitionSpeed: {
		default: '.2s',
		fast: '.2s',
		medium: '.4s',
		slow: '.8s'
	},
	ease: 'cubic-bezier(0.250, 0.460, 0.450, 0.940)'
};

export const breakpoint = (size) => (props) => {
	return theme.breakpoints[size];
};

export const ease =
	(type = 'all', speed = 'default') =>
	(props) => {
		return `${type} ${theme.transitionSpeed[speed]} ${theme.ease};`;
	};

export const swatch = (color) => (props) => {
	return theme.colors[color] != null
		? theme.colors[color]
		: theme.colors['white'];
};
