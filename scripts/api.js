//Required to allow for .env.local to be read
require('dotenv').config({
	path: '.env.local'
});

const fetch = require('node-fetch');
const API_URL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}`;
const WP_URL = process.env.NEXT_PUBLIC_WP_URL;

/**
 * Processes a query to the WP Rest API
 * @param query
 * @return {Promise<any>}
 */
async function fetchAPI(endpoint) {
	const url = `${API_URL}${endpoint}`;
	const response = await fetch(url, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	});
	const json = await response.json();
	return json;
}

/**
 * Runs a fetch request and returns the response.
 * @param endpoint Woocommerce API endpoint excluding /wp-json/wc/v3/
 * @returns {Promise<any>}
 */
async function fetchWooAPI(endpoint, postBody = null, method = 'POST') {
	const WOO_API_URL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}wc/v3/`;
	const url = `${WOO_API_URL}${endpoint}`;

	const headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Basic ' + Buffer.from(process.env.WOO_CONSUMER_KEY + ":" + process.env.WOO_CONSUMER_SECRET).toString('base64')
	}

	const reqBody = postBody ? JSON.stringify(postBody) : null;

	try {
		const data = await fetch(url, {
			headers,
			method,
			body: reqBody
		});

		const json = await data.json();

		return json;
	} catch (error) {
		throw new Error(error.message);
	}
}

/**
 * Returns a single menu
 * @param location
 * @returns {Promise<*>}
 */
async function getMenu(location = 'header_navigation') {
	const endpoint = `wp-api-menus/v2/menu-locations/${location}`;
	const data = await fetchAPI(endpoint);
	data.map((item) => {
		return (item.url = cleanUrl(item.url));
	});
	return data;
}

/**
 * Gets the site options from ACF
 * @returns {Promise<*>}
 */
async function getSiteOptions() {
	const endpoint = `acf/v3/options/options`;
	const data = await fetchAPI(endpoint);
	return data?.acf;
}

/**
 * Gets the woocommerce system pages
 * @returns {Promise<*>}
 */
async function getSystemPages() {
	const endpoint = `system_status`;

	const data = await fetchWooAPI(endpoint, null, 'GET');

	const pages = data?.pages || [];

	await Promise.all(pages.map(async (page, i) => {
		const slug = await fetchPageSlug(page.page_id);
		pages[i].slug = slug;
	}));

	return pages;
}

/**
 * Gets a slug from a page id
 * @returns {Promise<*>}
 */
async function fetchPageSlug(id) {
	const endpoint = `wp/v2/pages/${id}`;
	const data = await fetchAPI(endpoint);
	return data?.slug;
}

/**
 * Cleas a url to remove the WP prefix so it is a localised url instead
 * @param str
 * @returns {*}
 */
const cleanUrl = (str) => {
	return str.replace(WP_URL, '');
};

//Exports
module.exports = {
	getSiteOptions,
	getSystemPages,
	getMenu
};
