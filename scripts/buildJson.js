/**
 * Builds a json file from data and stores it locally. This allows for SSG to take place for global options such as menus.
 */
const api = require('./api');

/**
 * Builds a list of menus
 * @returns {Promise<void>}
 */
const buildMenus = async () => {
	let menus = {};
	const header = await api.getMenu('header_navigation');
	const main = await api.getMenu('main_navigation');
	const footerA = await api.getMenu('footer_1_navigation');
	const footerB = await api.getMenu('footer_2_navigation');
	const footerC = await api.getMenu('footer_3_navigation');
	const footerD = await api.getMenu('footer_4_navigation');

	menus.header = header;
	menus.main = main;
	menus.footerA = footerA;
	menus.footerB = footerB;
	menus.footerC = footerC;
	menus.footerD = footerD;

	//Build the json file
	writeToJson('menus.json', menus);
};

/**
 * Gets all site options and places them in a json file
 * @returns {Promise<void>}
 */
const buildSiteOptions = async () => {
	let options = await api.getSiteOptions();
	writeToJson('options.json', options);
};

/**
 * Gets all woocomerce system pages and places them in a json file
 * @returns {Promise<void>}
 */
const buildSystemPages = async () => {
	let systemPages = await api.getSystemPages();
	writeToJson('system-pages.json', systemPages);
};

/**
 * Helper function to generate a json file
 * @param file
 * @param data
 */
const writeToJson = (file, data) => {
	const path = 'json';
	const json = JSON.stringify(data);

	const fs = require('fs');
	fs.writeFile(`${path}/${file}`, json, 'utf8', () => {
		console.log(`Wrote ${file} file.`);
	});
};

//Build the menus
buildMenus();
buildSiteOptions();
buildSystemPages();
