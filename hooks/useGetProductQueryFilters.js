import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { getProductQueryFilters } from '../lib/archives';

export default function useGetProductQueryFilters(availableFilters) {
	const router = useRouter();
	const [selectedFilters, setSelectedFilters] = useState(null);

	useEffect(() => {
		if (!router.isReady) {
			return;
		}

		const queryObj = router.query;

		const selections = getProductQueryFilters(availableFilters, queryObj);

		setSelectedFilters(selections);
	}, [router.isReady, router.query]);

	return selectedFilters;
}
