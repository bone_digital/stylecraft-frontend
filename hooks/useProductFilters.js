import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { getProductQueryFilters } from '../lib/archives';

export default function useProductFilters(incomingPage = 1, allFilters) {
	const router = useRouter();
	const [page, setPage] = useState(incomingPage);
	const [brandFilters, setBrandFilters] = useState([]);
	const [designerFilters, setDesignerFilters] = useState([]);
	const [productFilters, setProductFilters] = useState([]);
	const [leadtimeFilters, setLeadtimeFilters] = useState([]);
	const [environmentFilters, setEnvironmentFilters] = useState([]);
	const [rangeFilters, setRangeFilters] = useState([]);
	const [priceFilters, setPriceFilters] = useState([]);
	const [activeFilters, setActiveFilters] = useState([]);
	const [australianMade, setAustralianMade] = useState(null);
	const [newProducts, setNewProducts] = useState(null);
	const [userInteracted, setUserInteracted] = useState(false);
	const [shopFilters, setShopFilters] = useState([]);
	const [shouldUpdateActiveFilters, setShouldUpdateActiveFilters] = useState(false);

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handleShopFilterAdd = (menu) => {
		const slug = menu?.slug;

		setShopFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter?.slug === slug;
			}).length;

			if (filterActive) {
				return [...prevState];
			}

			return [...prevState, menu];
		});
	}

	const handleBrandFilterAdd = (menu) => {
		setBrandFilters([menu]);
		return;
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleBrandFilterRemove = (slug) => {
		setBrandFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handleDesignerFilterAdd = (menu) => {
		const slug = menu?.slug;

		setDesignerFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter?.slug === slug;
			}).length;

			if (filterActive) {
				return [...prevState];
			}

			return [...prevState, menu];
		});
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleDesignerFilterRemove = (slug) => {
		setDesignerFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {object} menu
	 */
	const handleProductFilterAdd = (menu) => {
		const slug = menu?.slug;

		setProductFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter?.slug === slug;
			}).length;

			if (filterActive) {
				return [...prevState];
			}

			return [...prevState, menu];
		});

		return productFilters;
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleProductFilterRemove = (slug) => {
		setProductFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handleLeadtimeFilterAdd = (menu) => {
		setLeadtimeFilters([menu]);
		return;
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleLeadtimeFilterRemove = (slug) => {
		setLeadtimeFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handleEnvironmentFilterAdd = (menu) => {
		const slug = menu?.slug;

		setEnvironmentFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter?.slug === slug;
			}).length;

			if (filterActive) {
				return [...prevState];
			}

			return [...prevState, menu];
		});
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleEnvironmentFilterRemove = (slug) => {
		setEnvironmentFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handlePriceFilterAdd = (menu) => {
		setPriceFilters([menu]);
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handlePriceFilterRemove = (slug) => {
		setPriceFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add filter
	 * @param {string} slug
	 */
	const handleRangeFilterAdd = (menu) => {
		const slug = menu?.slug;

		setRangeFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter?.slug === slug;
			}).length;

			if (filterActive) {
				return [...prevState];
			}

			return [...prevState, menu];
		});
	};

	/**
	 * Remove filter
	 * @param {string} slug
	 */
	const handleRangeFilterRemove = (slug) => {
		setRangeFilters((prevState) => {
			let filterActive = prevState.filter((filter) => {
				return filter === slug;
			}).length;

			if (filterActive) {
				return (filterActive = prevState.filter((filter) => {
					return filter !== slug;
				}));
			}
		});
	};

	/**
	 * Add Australian made
	 */
	const handleAustralianMadeAdd = () => {
		setAustralianMade(true);
	};

	/**
	 * Remove Australian made
	 */
	const handleAustralianMadeRemove = () => {
		setAustralianMade(false);
	};

	/**
	 * Add New product
	 */
	const handleNewProductsAdd = () => {
		setNewProducts(true);
	};

	/**
	 * Remove New product
	 */
	const handleNewProductsRemove = () => {
		setNewProducts(false);
	};

	/**
	 * Active filters
	 */
	const handleActiveFilters = () => {
		let filterArray = [];

		if (brandFilters && brandFilters.length) {
			brandFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (designerFilters && designerFilters.length) {
			designerFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (productFilters && productFilters.length) {
			productFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (leadtimeFilters && leadtimeFilters.length) {
			leadtimeFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (environmentFilters && environmentFilters.length) {
			environmentFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (priceFilters && priceFilters.length) {
			priceFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (rangeFilters && rangeFilters.length) {
			rangeFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (shopFilters && shopFilters.length) {
			shopFilters.forEach((filter) => {
				filterArray.push(filter);
			});
		}

		if (australianMade === true) {
			filterArray.push({
				filter: 'australian-made',
				name: `Australian Made`,
				slug: `australian-made`,
				parent: `australian-made`
			});
		}

		if (newProducts === true) {
			filterArray.push({
				filter: `whats-new`,
				name: `What's New`,
				slug: `whats-new`,
				parent: `whats-new`
			});
		}

		setActiveFilters(filterArray);

		return filterArray;
	};

	/**
	 * @param {object} activeFilter
	 */
	const handleRemoveFilter = (activeFilter) => {
		const type = activeFilter?.parent;
		setUserInteracted(true);

		if (['ranges', 'range'].includes(type)) {
			handleRangeFilterRemove(activeFilter);
		}
		if (['brands', 'brand'].includes(type)) {
			handleBrandFilterRemove(activeFilter);
		}
		if (['designers', 'designer'].includes(type)) {
			handleDesignerFilterRemove(activeFilter);
		}
		if (['productCategories', 'category'].includes(type)) {
			handleProductFilterRemove(activeFilter);
		}
		if (['whatsNew', 'whats-new'].includes(type)) {
			handleNewProductsRemove();
		}
		if (['australianMade', 'australian-made', 'australian_made'].includes(type)) {
			handleAustralianMadeRemove();
		}
		if (['lead_time', 'leadtimes'].includes(type)) {
			handleLeadtimeFilterRemove(activeFilter);
		}
		if (['environmentalCertifications', 'environmental'].includes(type)) {
			handleEnvironmentFilterRemove(activeFilter);
		}
		if (['price', 'priceRanges'].includes(type)) {
			handlePriceFilterRemove(activeFilter);
		}

		setPage(1);

		setShouldUpdateActiveFilters(true);
	};


	/**
	 * Set filter state based on URL query params. Should only run once, at page load.
	 * Due to the way the handle* filters currently work we need to call them with
	 * appropriate args rather than just setting the filters directly.
	 */
	useEffect(() => {
		if (router.isReady && router.query) {
			const parsedFilters = getProductQueryFilters(allFilters, router.query);

			if (!parsedFilters?.length) {
				return;
			}

			const mappings = {
				brand: {
					parent: 'brands',
					handler: handleBrandFilterAdd,
				},
				price: {
					parent: 'priceRanges',
					handler: handlePriceFilterAdd,
				},
				category: {
					parent: 'productCategories',
					handler: handleProductFilterAdd,
				},
				environmental: {
					parent: 'environmental',
					handler: handleEnvironmentFilterAdd,
				},
				lead_time: {
					parent: 'lead_time',
					handler: handleLeadtimeFilterAdd,
				},
				designer: {
					parent: 'designers',
					handler: handleDesignerFilterAdd,
				},
			}

			parsedFilters.forEach(filter => {
				const type = filter.filter;
				const mapping = mappings[type];
				if (mapping) {
					const filterWithParent = Object.assign({}, filter, {parent: mapping.parent});
					mapping.handler(filterWithParent);
				}
				else if ('whats-new' === type) {
					handleNewProductsAdd();
				}
				else if ('australian-made' === type) {
					handleAustralianMadeAdd()
				}
			});

			setShouldUpdateActiveFilters(true);
		}

	}, [router.isReady]);


	/**
	 * Can't update a filter and call handleActiveFilters() in the same run, as handleActiveFilters won't
	 * have the updated stated. Use this workaround to flag that handleActiveFilters() should be called
	 * on the next run.
	 */
	useEffect(() => {
		if (shouldUpdateActiveFilters) {
			handleActiveFilters();
			setShouldUpdateActiveFilters(false);
		}
	}, [shouldUpdateActiveFilters]);


	const resetFilters = () => {
		setUserInteracted(false);
		setBrandFilters([]);
		setDesignerFilters([]);
		setProductFilters([]);
		setLeadtimeFilters([]);
		setEnvironmentFilters([]);
		setRangeFilters([]);
		setPriceFilters([]);
		setActiveFilters([]);
		setAustralianMade([]);
		setNewProducts([]);
		setUserInteracted([]);

		setPage(1);
	};

	return {
		brandFilters,
		designerFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		rangeFilters,
		priceFilters,
		activeFilters,
		australianMade,
		newProducts,
		userInteracted,
		setBrandFilters,
		setDesignerFilters,
		setProductFilters,
		setLeadtimeFilters,
		setEnvironmentFilters,
		setRangeFilters,
		setPriceFilters,
		setActiveFilters,
		setAustralianMade,
		setNewProducts,
		setUserInteracted,
		handleBrandFilterAdd,
		handleBrandFilterRemove,
		handleDesignerFilterAdd,
		handleDesignerFilterRemove,
		handleProductFilterAdd,
		handleProductFilterRemove,
		handleLeadtimeFilterAdd,
		handleLeadtimeFilterRemove,
		handleEnvironmentFilterAdd,
		handleEnvironmentFilterRemove,
		handlePriceFilterAdd,
		handlePriceFilterRemove,
		handleRangeFilterAdd,
		handleRangeFilterRemove,
		handleAustralianMadeAdd,
		handleAustralianMadeRemove,
		handleNewProductsAdd,
		handleNewProductsRemove,
		handleShopFilterAdd,
		handleActiveFilters,
		handleRemoveFilter,
		// selectedFilters: filters,
		resetFilters,
	};
}
