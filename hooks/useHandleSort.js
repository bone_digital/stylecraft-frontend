import { useState } from 'react';
import _ from 'lodash';

export default function useHandleSort(options) {
	const [sortBy, setSortBy] = useState(null);

	const handleSetSortBy = (slug) => {
		let activeFilter = options.find((filter) => {
			return filter?.value === slug;
		});

		setSortBy(activeFilter);
	};

	return {
		handleSetSortBy,
		sortBy,
		setSortBy
	};
}
