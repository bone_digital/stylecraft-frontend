import { theme } from '../styles/theme';

export function useMediaQuery(query) {
	const [matches, setMatches] = useState(false);

	useEffect(() => {
		const media = window.matchMedia(query);
		if (media.matches !== matches) {
			setMatches(media.matches);
		}
		const listener = () => {
			setMatches(media.matches);
		};
		media.addEventListener(listener);
		return () => media.removeEventListener(listener);
	}, [matches, query]);

	return matches;
}

const breakpoints = theme?.breakpoints?.desktop;

export const useIsDesktop = () => useWindowDimensions(breakpoints?.desktop);
export const useIsLaptop = () => useWindowDimensions(breakpoints?.laptop);
export const useIsTablet = () => useWindowDimensions(breakpoints?.tablet);
export const useIsMobile = () => useWindowDimensions(breakpoints?.mobile);
