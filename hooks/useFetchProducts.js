import { useState, useEffect, useRef, useMemo } from 'react';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { paramStringFromUrl } from '../lib/archives';

export default function useFetchProducts(
	posts,
	page = {
		current: 1
	},
	max,
	loadMoreTrigger = false, // DEPRECATED - DO NOT USE
	sortBy = null,
	userInteracted,
	handleActiveFilters,
	designerFilters,
	brandFilters,
	productFilters,
	leadtimeFilters,
	environmentFilters,
	priceFilters,
	rangeFilters,
	australianMade,
	newProducts,
	defaultRange = null,
	archiveType = 'category'
) {
	const maxPages = useRef(max);
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [appending, setAppending] = useState(false);
	const [filteredProducts, setFilteredProducts] = useState([]);
	const [previousFilters, setPreviousFilters] = useState([]);
	const [locale, setLocale] = useState('en-AU');

	useEffect(() => {
		if (posts !== null) {
			setFilteredProducts(posts);
		}
	}, [posts]);

	useEffect(() => {
		if (max !== null) {
			maxPages.current = max;
		}
	}, [max]);

	useEffect(() => {
		setLocale(router.locale);
	}, [router.locale]);

	const userRoles = useSelector(({ user }) => user.roles);
	const isWholeseller = useMemo(() => {
		if (userRoles && userRoles?.length) {
			return (
				userRoles.filter(({ node }) => {
					const roleName = node?.name;

					return (
						roleName === 'default_wholesaler' ||
						roleName === 'administrator' ||
						roleName === 'wholesaler'
					);
				})?.length > 0
			);
		}

		return false;
	}, [userRoles]);

	const getSelectedFilters = () => {
		let activeFilters = [];

		if (brandFilters && brandFilters.length) {
			let brandArray = brandFilters.map((filter) => {
				return filter?.slug;
			});

			if (
				defaultRange &&
				defaultRange !== null &&
				archiveType === 'brand'
			) {
				brandArray.push(defaultRange);
			}

			activeFilters.push({ name: 'brand', value: brandArray });
		}
		else if (defaultRange !== null && archiveType === 'brand') {
			activeFilters.push({ name: 'brand', value: defaultRange });
		}

		if (designerFilters && designerFilters.length) {
			let designerArray = designerFilters.map((filter) => {
				return filter?.slug;
			});

			activeFilters.push({ name: 'designer', value: designerArray });
		}

		if (productFilters && productFilters.length) {
			let productArray = productFilters.map((filter) => {
				return filter?.slug;
			});

			if (
				defaultRange &&
				defaultRange !== null &&
				archiveType === 'category'
			) {
				productArray.push(defaultRange);
			}

			activeFilters.push({ name: 'category', value: productArray });
		} else if (defaultRange !== null && archiveType === 'category') {
			activeFilters.push({ name: 'category', value: defaultRange });
		}

		if (leadtimeFilters && leadtimeFilters.length) {
			let leadtimeArray = leadtimeFilters.map((filter) => {
				return filter?.slug;
			});

			activeFilters.push({ name: 'lead_time', value: leadtimeArray });
		}

		if (environmentFilters && environmentFilters.length) {
			let environmentArray = environmentFilters.map((filter) => {
				return filter?.slug;
			});

			activeFilters.push({
				name: 'environmental',
				value: environmentArray
			});
		}

		if (rangeFilters && rangeFilters.length) {
			let rangeArray = rangeFilters.map((filter) => {
				return filter?.slug;
			});

			if (
				defaultRange &&
				defaultRange !== null &&
				archiveType === 'range'
			) {
				rangeArray.push(defaultRange);
			}

			activeFilters.push({ name: 'range', value: rangeArray });

			queryURL += `${rangeQuery}&`;
		} else if (defaultRange !== null && archiveType === 'range') {
			activeFilters.push({ name: 'range', value: defaultRange });
		}

		if (priceFilters && priceFilters.length) {
			let priceArray = priceFilters.map((filter) => {
				return filter?.slug;
			});

			activeFilters.push({ name: 'price', value: priceArray });
		}

		if (australianMade === true) {
			activeFilters.push({ name: 'australian_made', value: true });
		}
		if (isWholeseller === true) {
			activeFilters.push({ name: 'wholesale', value: true });
		}

		if (locale !== null) {
			activeFilters.push({ name: 'locale', value: locale });
		}

		if (newProducts === true) {
			activeFilters.push({ name: 'new', value: true });
		}

		if (sortBy) {
			activeFilters.push({ name: 'sort', value: sortBy?.slug });
			activeFilters.push({ name: 'order', value: sortBy?.sortby });
		}

		const filtersWithoutPage = activeFilters.filter(item => item.name !== 'page');
		const isSameFilters = JSON.stringify(filtersWithoutPage) === JSON.stringify(previousFilters);

		if ((isSameFilters && page?.current != null) || (previousFilters.length == 0 && page?.current != null)) {
			setPreviousFilters(filtersWithoutPage);
			activeFilters.push({ name: 'page', value: page?.current });
		} else {
			setPreviousFilters(filtersWithoutPage);
			page.current = 1
			activeFilters.push({ name: 'page', value: page?.current });
		}

		return activeFilters;
	};

	/**
	 * Create Query String
	 *
	 * Leaving this here as legacy, but it's really important that the url creation is separated from the
	 * router.push
	 */
	function createFilterUrl(loadMore = false) {
		const url = createFilterUrlOnly(loadMore);
		const urlObj = new URL(url);
		const query = Object.fromEntries(urlObj.searchParams.entries());

		router.push(
			{
				pathname: router.pathname,
				query,
			},
			undefined,
			{ shallow: true }
		);
	}

	/**
	 * Create Query String
	 */
	function createFilterUrlOnly(loadMore = false) {
		const newUrl = new URL(window.location);
		const searchParams = new URLSearchParams(newUrl.search);

		const filters = getSelectedFilters();

		/**
		 * set search param for active filters,
		 * remove params with no active ones
		 */
		filters.forEach((filter) => {
			const name = filter.name || null;
			if (!name) {
				return;
			}

			const value = filter.value || null;
			if (value) {
				searchParams.set(name, value);
			}
		});

		searchParams.forEach((value, key) => {
			const queryHasKey =
				filters.filter(({ name }) => {
					return name === key;
				}).length > 0;

			if (!queryHasKey && key != 'shop') {
				searchParams.delete(key);
			}

			if (!value || value === '' && key != 'shop') {
				searchParams.delete(key);
			}
		});

		// this is necessary so routes like category/[...slug] will have the
		// actual slug value
		if (router.query.slug 
			&& router.query.slug !== ''
			&& router.pathname !== '/brands/collection/[slug]') {
			searchParams.set('slug', router.query.slug);
		}

		newUrl.search = searchParams.toString();
		return newUrl.toString();
	}

	/**
	 * Fetch REST API products
	 * @param {boolean} loadMore
	 */
	async function handleFetchProducts(loadMore = false, filters = null) {
		let rows = [];

		if (loading) {
			return;
		}

		setLoading(true);

		fetch('/api/fetch-products', {
			method: 'POST',
			mode: 'same-origin',
			body: JSON.stringify(
				{filters}
			)
		})
			.then((response) => response.json())
			.then((data) => {
				let posts = data?.posts;
				maxPages.current = data?.max;

				console.log('updated max', maxPages.current)

				if (data?.max === 0) {
					// this happens when we hit the end of the available posts.
					// Should just stop and do nothing here
					return;
				}

				if (data && posts && posts.length) {
					if (!loadMore) {
						const productsHeader = posts.slice(0, 2);

						const productsBody =
							posts.length > 2 ? posts.slice(2) : null;

						rows.push(productsHeader);

						if (productsBody) {
							rows.push(productsBody);
						}

						setFilteredProducts(rows);
					} else {
						setFilteredProducts((prevState) => {
							if (prevState.length === 2) {
								prevState[1].push(...posts);
							}

							return [...prevState];
						});
					}
				} else {
					setFilteredProducts([[null]]);
					return;
				}
			})
			.catch((error) => {
				setFilteredProducts([[null]]);
			})
			.finally(() => {
				setTimeout(() => {
					setLoading(false);
					setAppending(false);
				}, 200);
			});
	}


	/**
	 * Convenience function - wrapper for handleFetchProducts with input from
	 * createFilterUrlOnly().
	 *
	 * @param {boolean} loadMore
	 */
	async function handleFetchProductsFromUrl(loadMore = false) {
		const filterUrl = createFilterUrlOnly(loadMore);
		handleFetchProducts(loadMore, paramStringFromUrl(filterUrl));
	}

	const throttleFetchProductsLoadmore = _.debounce(function () {
		handleFetchProductsFromUrl(true);
	}, 500);

	const throttleReplaceProducts = _.debounce(function () {
		createFilterUrl();
	}, 200);


	const doLoadMore = () => {
		const newCurrent = page.current + 1;
		if (loading || newCurrent > maxPages.current) {
			return;
		}

		page.current = newCurrent;
		setAppending(true);
		throttleFetchProductsLoadmore();
	}


	/**
	 * Handles filter change
	 */
	useEffect(() => {
		if (userInteracted) {
			handleActiveFilters();
			throttleReplaceProducts();
		}
	}, [
		brandFilters,
		designerFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		priceFilters,
		australianMade,
		newProducts,
		userInteracted,
		isWholeseller
	]);

	return {
		loading,
		appending,
		filteredProducts,
		setFilteredProducts,
		handleFetchProducts,
		getSelectedFilters,
		createFilterUrl,
		createFilterUrlOnly,
		fetchProducts: handleFetchProducts,
		fetchProductsFromUrl: handleFetchProductsFromUrl,
		doLoadMore,
	};
}
