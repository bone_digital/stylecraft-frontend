import { useState, useEffect } from 'react';

const useProgressiveImg = (lowQualitySrc, highQualitySrc, stateCallback) => {
	const [src, setSrc] = useState(null);

	useEffect(() => {
		setSrc(lowQualitySrc);

		const img = new Image();

		img.src = highQualitySrc;

		img.onload = () => {
			setSrc(highQualitySrc);
		};

		return () => {
			setSrc(null);
		};
	}, [lowQualitySrc, highQualitySrc, stateCallback]);

	return [src, { blur: src === lowQualitySrc }];
};

export default useProgressiveImg;
