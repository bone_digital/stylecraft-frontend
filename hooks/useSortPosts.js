import { useState, useMemo, useEffect } from 'react';

const sortIntoRow = (posts) => {
	let rows = [];

	if (posts) {
		const header = posts.slice(0, 2);
		const body = posts.length > 2 ? posts.slice(2) : null;

		rows.push(header);

		if (body) {
			rows.push(body);
		}
	}

	return rows;
};

/**
 *
 * @param {Array} posts
 * @param {Object} sortBy
 * @returns {Array}
 */
export default function useSortPosts(posts, setPosts, sortBy, useRows = true) {
	const [sortFilter, setSortFilter] = useState(null);

	const sortedPosts = useMemo(() => {
		let rawPosts = [];

		if (sortBy !== null) {
			setSortFilter(sortBy);

			const orderBy = sortBy?.slug;
			const order = sortBy?.sortby;

			if (useRows) {
				posts.forEach((row) => {
					row.forEach((post) => {
						rawPosts.push(post);
					});
				});
			} else {
				rawPosts = posts;
			}

			if (orderBy === 'title') {
				rawPosts.sort((a, b) => {
					if (order === 'ASC') {
						if (a.title < b.title) {
							return -1;
						}

						if (a.title > b.title) {
							return 1;
						}

						return 0;
					} else {
						if (a.title > b.title) {
							return -1;
						}

						if (a.title < b.title) {
							return 1;
						}

						return 0;
					}
				});
			}

			if (orderBy === 'date') {
				rawPosts.sort((a, b) => {
					return order === 'ASC'
						? new Date(b.date) - new Date(a.date)
						: new Date(a.date) - new Date(b.date);
				});
			}

			if (orderBy === 'featured') {
				rawPosts.sort((a, b) => {
					return b - a;
				});
			}
		}

		let sortedRows = sortIntoRow(rawPosts);

		return useRows ? sortedRows : rawPosts;
	}, [sortBy]);

	useEffect(() => {
		if (sortFilter !== null) {
			setPosts(sortedPosts);
		}
	}, [sortFilter]);

	return sortedPosts;
}
