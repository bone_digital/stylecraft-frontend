import { useEffect, useState } from 'react';

export default function useApplyProductQueryFilters(
	selectedFilters,
	productFilters,
	handleFilters,
	handleActiveFilters,
	createFilterUrl,
	setUserInteracted
) {
	const [filtersAdded, setFiltersAdded] = useState(false);
	const [first, setFirst] = useState(true);

	useEffect(() => {
		if (selectedFilters.length) {
			if (filtersAdded) {
				return;
			}

			selectedFilters.forEach((filterObj) => {
				if (handleFilters.hasOwnProperty(filterObj?.filter)) {
					const applyFilter = handleFilters[filterObj?.filter];

					applyFilter(filterObj);
				}
			});

			if (productFilters.length > 0 || selectedFilters.length > 0 && !first) {
				setFiltersAdded(true);
				setUserInteracted(true);
				handleActiveFilters();
				createFilterUrl();
			} else {
				setFirst(false);
			}
		}
	}, [selectedFilters, productFilters]);

	return productFilters;
}
