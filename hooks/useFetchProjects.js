import { useState, useEffect, useRef } from 'react';
import _ from 'lodash';

export default function useFetchProjects(
	posts,
	max = 1,
	loadMoreTrigger = false,
	sortBy = null
) {
	const [loading, setLoading] = useState(false);
	const [appending, setAppending] = useState(false);
	const [fetchedPosts, setFetchedPosts] = useState([]);

	useEffect(() => {
		if (posts !== null) {
			setFetchedPosts(posts);
		}
	}, [posts]);

	const page = useRef(1);
	const maxPages = useRef(max || 1);

	/**
	 * Fetch REST API posts
	 */
	async function handleFetchPosts() {
		setLoading(true);

		page.current++;

		fetch('/api/fetch-projects', {
			method: 'POST',
			mode: 'same-origin',
			body: JSON.stringify({
				sortBy,
				page: page?.current != null ? page?.current : 1
			})
		})
			.then((response) => response.json())
			.then((data) => {
				if (data) {
					const posts = data?.posts;
					const max = data?.max;

					maxPages.current = max;

					setFetchedPosts((prevState) => {
						return [...prevState, ...posts];
					});
				}
			})
			.catch((error) => {
				setFetchedPosts([]);
			})
			.finally(() => {
				setTimeout(() => {
					setLoading(false);
					setAppending(false);
				}, 200);
			});
	}

	const throttleFetchPosts = _.debounce(handleFetchPosts, 500);

	useEffect(() => {
		if (loadMoreTrigger && page.current < maxPages.current) {
			if (!loading) {
				setAppending(true);
				throttleFetchPosts();
			}
		}
	}, [loadMoreTrigger, maxPages, loading]);

	return {
		loading,
		appending,
		fetchedPosts,
		setFetchedPosts
	};
}
