import { isObject } from "formik";
import { useRouter } from "next/router";

export default function useBlogFilters() {
	const router = useRouter();

	const handleFilterBy = (slug) => {
		if (isObject(slug) && slug.slug) {
			slug = slug.slug;
		}

		if (slug && typeof slug === 'string') {
			router.push(`/journal/category/${slug}`)
		}
		else {
			router.push(`/journal`)
		}
	};

	return {
		handleFilterBy
	};
}
