import { isObject } from "formik";
import { useRouter } from "next/router";

export default function useProjectFilters() {
	const router = useRouter();

	const handleFilterBy = (slug) => {
		if (isObject(slug) && slug.slug) {
			slug = slug.slug;
		}

		if (slug && typeof slug === 'string') {
			router.push(`/project/category/${slug}`)
		}
		else {
			router.push(`/project`)
		}
	};

	return {
		handleFilterBy
	};
}
