import { useInView } from 'react-intersection-observer';
import { useEffect, useState } from 'react';
import _ from 'lodash';

// This doesn't work quite right, use InView wrapper instead.

export default function useLoadMore(element) {
	const [lastScrollTop, setLastScrollTop] = useState(0);
	const [scrollTop, setScrollTop] = useState(0);
	const [innerHeight, setInnerHeight] = useState(element);
	const [threshold, setThreshold] = useState(null);

	const onScroll = ({ target: { documentElement: browser } }) => {
		setScrollTop((prevState) => {
			setLastScrollTop(prevState);

			return browser.scrollTop;
		});
	};

	const getElementHeight = () => {
		if (typeof window !== 'undefined' && element !== null) {
			setInnerHeight(element);
			setThreshold(element * 0.75 - 200); // 200 to account for header size
		}
	};

	useEffect(() => {
		const throttleScroll = _.throttle(onScroll, 500);
		window.addEventListener('scroll', throttleScroll);
		return () => window.removeEventListener('scroll', throttleScroll);
	}, []);

	useEffect(() => {
		if (element !== null) {
			getElementHeight();
		}
	}, [element]);

	if (threshold === null || innerHeight === null || !innerHeight) {
		return false;
	}

	let scrollThreshold = scrollTop >= threshold;

	const loadMore =
		scrollThreshold && scrollTop > lastScrollTop ? scrollTop : false;

	return loadMore;
}
