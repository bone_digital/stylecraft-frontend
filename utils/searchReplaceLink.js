import {
	blogPrefix,
	shopPrefix,
	showroomPrefix,
	projectPrefix,
	brandPrefix
} from '../lib/settings';

export default function searchReplaceLink(link, external = false, type = null) {
	if (link == null || typeof link === 'undefined' || !link) {
		return null;
	}

	let cmsUrl = process.env.NEXT_PUBLIC_CMS_URL;
	let siteUrl = process.env.NEXT_PUBLIC_SITE_URL;
	let sgUrl = process.env.NEXT_PUBLIC_SITE_URL_ASIA;

	if (
		external === false &&
		link.includes('http') &&
		!link.includes('stylecraft.') &&
		!link.includes('stylecraft-asia.') &&
		!link.includes('stylecraftoutlet') &&
		!link.includes('localhost')
	) {
		// If the link includes http or https but does not include stylecraft. then it is an external link
		external = true;
		return link;
	}

	if( link.includes('stylecraftoutlet') )
	{
		return link;
	}

	if (external === false) {
		//Remove https, http, www prefixes
		link = link.replace('https://', '');
		link = link.replace('http://', '');
		link = link.replace('http', '');
		link = link.replace('www', '');
		cmsUrl = cmsUrl.replace('https://', '');
		cmsUrl = cmsUrl.replace('http://', '');
		cmsUrl = cmsUrl.replace('http', '');
		cmsUrl = cmsUrl.replace('www', '');
	}

	// Get only the slug
	link = link.replace(cmsUrl, '');

	if (link === '/shop/') {
		link = '/product/';
	}

	if (link === '/home/') {
		link = '/';
	}

	if (link.includes('/brand/')) {
		link = link.replace('/brand/', '/brands/');
	}

	if (link.includes('/showroom/')) {
		link = link.replace('/showroom/', '/showrooms/');
	}

	if (link.includes('/product-category/')) {
		link = link.replace('/product-category/', '/product/category/');
	}

	if (link.includes('/post-category/')) {
		link = link.replace('/post-category/', '/blog/category/');
	}

	if (link.includes('/project-category/')) {
		link = link.replace('/project-category/', '/project/category/');
	}

	if (link.includes('/blog/')) {
		link = link.replace('/blog/', blogPrefix);
	}

	//Get only the slug - remove the CMS url, remove the public site URLs
	link = link.replace(cmsUrl, '');
	link = link.replace(siteUrl, '');
	link = link.replace(sgUrl, '');

	if (type === 'blog') {
		link = `${blogPrefix}${link}`;
	} else if (type === 'shop') {
		link = `${shopPrefix}${link}`;
	} else if (type === 'showroom') {
		link = `${showroomPrefix}${link}`;
	} else if (type === 'project') {
		link = `${projectPrefix}${link}`;
	} else if (type === 'brand') {
		link = `${brandPrefix}${link}`;
	}

	return link;
}

export function isExternalUrl(link) {
	if (link == null || typeof link === 'undefined' || !link) {
		return null;
	}

	if (
		link.includes('http') &&
		!link.includes('stylecraft.') &&
		!link.includes('localhost')
	) {
		// If the link includes http or https but does not include stylecraft. then it is an external link
		return true;
	}

	return false;
}

function removeTrailingSlash(str) {
	return str.endsWith('/') ? str.slice(0, -1) : str;
}

export function checkUrlsMatch(url1, url2) {
	url1 = removeTrailingSlash(url1);
	url2 = removeTrailingSlash(url2);
	return url1 === url2;
}
