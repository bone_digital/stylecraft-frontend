/**
 * Adds 'week(s)' to the number to create a lead time label.
 * Returns false if empty or 0 value;
 * @param leadTime
 * @returns {boolean}
 * @constructor
 */
export const GenerateLeadTimeLabel = (leadTime) => {
	let label = false;
	if( typeof leadTime === "undefined" || false === leadTime || null === leadTime || 0 === leadTime )
	{
		return false;
	}
	if( leadTime && leadTime > 0 )
	{
		label = `${leadTime} week`;
		if( leadTime > 1 )
		{
			label += `s`;
		}
	}
	else if ( leadTime <= 0 )
	{
		label = 'In Stock';
	}
	return label;
};

/**
 * Takes a string and removes the dollar sign and commas to allow it to be passed as a Number() function
 * @param price
 * @returns {number}
 * @constructor
 */
export const CleanPricingString = (price) => {
	let number = price?.replace("$", "");
	number = number?.replace(",", "");
	return Number(number);
};

/**
 * Gets a category pricing label based on an inputted price
 *
 * @param price
 * @returns {string}
 * @constructor
 */
export const GetWholesalePricingLabel = (price) => {
	let label = 'POA';
	price = Number(price);

	const ranges = [
		7500,
		5000,
		3000,
		2000,
		1500,
		1000,
		750,
		500,
		300,
	];

	const formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD'
	});

	ranges.forEach((value) => {
		if( price <= value && price > 0 )
		{
			const currencyValue = formatter.format(value);
			label = `Under ${currencyValue}`;
		}
	});

	return label;
};
