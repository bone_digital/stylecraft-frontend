/**
 * Redux w/ Redux Toolkit
 * -------------------------
 * By using the toolkit we can split our redux stores into different parts and files.
 * In this example there are two - one for the cart and one for the user.
 * This index file combines the two with some helper functions then exports the store.
 *
 * **NOTE**
 * You need to modify _app.js as well to add the provider
 */
import { configureStore } from '@reduxjs/toolkit';

//Import the 'slices' - see the cartSlice for further info
import userSlice from './userSlice';
import cartSlice from './cartSlice';

/**
 * Combine the 'slices' into one single store - you can only have one store.
 */
const store = configureStore({
	reducer: {
		user: userSlice.reducer,
		cart: cartSlice.reducer,
	}
});

export default store;
