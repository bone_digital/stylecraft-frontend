//Import some helper functions
import { createSlice } from '@reduxjs/toolkit';

//Sets up the initial state, this could be moved inline if you wanted
const initalCartState = {
	itemCount: null,
	items: null,
	tempItems: null,
	total: null,
	wholesaleTotal: null,
	newCartKey: null,
	newCartHash: null,
	newItemCount: null,
	newItems: null,
	newTempItems: null,
	newTotal: null,
	newWholesaleTotal: null,
	newCartIsLoading: false,
	wishlistIsLoading: false,
	wishlistItems: null,
	tempWishlistItems: null,
	wishlistItemCount: 0,
	clientMutationId: null
};

//Create the 'slice' which will  be combined with other 'slices' in the index to create the store
const cartSlice = createSlice({
	name: 'cart',
	initialState: initalCartState,
	reducers: {
		updateNewCartStatus(state, action) {
			const payload = action.payload;
			const isLoading = payload?.isLoading;
			state.newCartIsLoading = isLoading || false;
		},
		updateWishlistStatus(state, action) {
			const payload = action.payload;
			const isLoading = payload?.isLoading;
			state.wishlistIsLoading = isLoading || false;
		},
		addItem(state, action) {
			const payload = action.payload;

			const cartContent = payload?.content;
			const total = payload?.total;
			const wholesaleTotal = payload?.wholesaleTotal;
			const cartCount = cartContent?.itemCount;
			const cartItems = cartContent?.edges;

			state.items = cartItems || [];
			state.tempItems = cartItems || [];
			state.itemCount = cartCount || 0;
			state.total = total || null;
			state.wholesaleTotal = wholesaleTotal || null;
		},
		addWishlistItem(state, action) {
			const payload = action.payload;

			const wishlistContent = payload?.content;

			const wishlistItems = wishlistContent?.edges;
			const wishlistCount = wishlistContent?.itemCount;

			state.wishlistItems = wishlistItems || [];
			state.tempWishlistItems = wishlistItems || [];
			state.wishlistItemCount = wishlistCount || null;
		},
		newUpdateCart(state, action) {
			const payload = action.payload;
			const cartContent = payload?.jsonCart;
			
			state.newCartKey = cartContent?.cart_key;
			state.newCartHash = cartContent?.cart_hash;
			state.newItemCount = cartContent?.item_count || null;
			state.newItems = cartContent?.items;
			state.newTotal = `$${(Math.round(cartContent?.totals?.total * 100) / 10000).toFixed(2)}`;
			state.newWholesaleTotal = `$${(Math.round(cartContent?.totals?.total * 100) / 10000).toFixed(2)}`;
		},
		updateCart(state, action) {
			const payload = action.payload;
			const cartContent = payload?.content;
			const total = payload?.total;
			const wholesaleTotal = payload?.wholesaleTotal;

			const cartCount = cartContent?.itemCount;
			const cartItems = cartContent?.edges;

			state.items = cartItems || [];
			state.tempItems = cartItems;
			state.itemCount = cartCount || 0;
			state.total = total || null;
			state.wholesaleTotal = wholesaleTotal || null;
		},
		updateWishlist(state, action) {
			const payload = action.payload;

			const items = payload?.items;
			const count = payload?.count;

			state.wishlistItems = items || [];
			state.wishlistItemCount = count || 0;
		},
		updateTempCart(state, action) {
			const payload = action.payload;
			const updatedItems = payload?.cart;

			state.tempItems = updatedItems;
		},
		updateTempWishlist(state, action) {
			const payload = action.payload;
			const updatedItems = payload?.cart;

			state.tempWishlistItems = updatedItems;
		},
		logout(state) {
			state.newCartKey = null;
			state.newCartHash = null;
			state.newItemCount = 0;
			state.newItems = null;
			state.newTotal = null;
			state.newWholesaleTotal = null;

			state.wishlistItems = null;
			state.wishlistItemCount = null;
		}
	}
});

//Export the actions (created by redux toolkit) - it makes it easier to match up reducers later on
export const cartActions = cartSlice.actions;

export default cartSlice;
