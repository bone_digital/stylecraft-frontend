import { createSlice } from '@reduxjs/toolkit';

const initialUserState = {
	confirmedStatus: false,
	loggedIn: false,
	userName: null,
	firstName: null,
	lastName: null,
	id: null,
	roles: null
};

const userSlice = createSlice({
	name: 'user',
	initialState: initialUserState,
	reducers: {
		login(state, action) {
			/**
			 * action.payload contains the data passed in to the dispatcher.
			 * See pages/test/redux.js for an example of calling this
			 */

			const user = action.payload?.user;
			state.loggedIn = true;
			state.firstName = user.firstName;
			state.userName = user.username;
			state.lastName = user.lastName;
			state.id = user.userId;
			state.roles = user?.roles?.edges || null;
		},
		confirmStatus(state) {
			state.confirmedStatus = true;
		},
		logout(state) {
			state.loggedIn = false;
			state.firstName = null;
			state.lastName = null;
			state.roles = null;
		}
	}
});

export const userActions = userSlice.actions;

export default userSlice;
