import { createElement, useContext, createContext, useState } from 'react';
import { swatch } from '../styles/theme';

const options = require('../json/options.json')

const UIContext = createContext({
	menuActive: false,
	backgroundColor: null,
	formData: null,
	popupData: null,
	accountActive: false,
	contactActive: false,
	popupActive: false,
	loggedIn: false,
	userRole: null,
	downloadBlocked: null,
	cursorState: 0,
	toggleDownloadBlocked: () => {},
	toggleMenu: () => {},
	toggleAccountMenu: () => {},
	toggleFormMenu: () => {},
	toggleFormPopup: () => {},
	setPageTheme: () => {},
	updateCursor: () => {}
});

export function UIProvider({ children }) {
	const values = useProvideUI();
	return createElement(UIContext.Provider, { value: values }, children);
}

export const useUI = () => useContext(UIContext);

function useProvideUI() {
	const [menuActive, setMenuActive] = useState(false);
	const [accountActive, setAccountActive] = useState(false);
	const [accountActiveAccordionIndex, setAccountActiveAccordionIndex] = useState(null);
	const [contactActive, setContactActive] = useState(false);
	const [popupActive, setPopupActive] = useState(false);
	const [formData, setFormData] = useState(false);
	const [popupData, setPopupData] = useState(false);
	const [backgroundColor, setBackgroundColor] = useState(null);
	const [downloadBlocked, setDownloadBlocked] = useState(null);
	const [cursorState, setCursorState] = useState(0);

	const [loggedIn, setLoggedIn] = useState(false);
	const [userRole, setUserRole] = useState(null);

	const toggleMenu = (value) => {
		setMenuActive(value);
	};

	const updateCursor = () => {
		setCursorState((prevState) => {
			return prevState + 1;
		});
	};

	const toggleAccountMenu = (value) => {
		setAccountActive(value);

		if (!value) {
			setDownloadBlocked(false);
		}
	};

	const toggleFormMenu = (value, data) => {
		setContactActive(value);

		const formData = {
			formId: data?.formId || 1,
			formTitle: data?.formTitle || 'Contact Us',
			formDescription: data?.formDescription || 'We would love to hear from you.<br />Let’s start with your name.',
			formEmailField: data?.formEmailField || '',
			formProductRequestField: data?.formProductRequestField || ''
		}

		setFormData(formData);
	};

	const toggleFormPopup = (value, data) => {
		setPopupActive(value);

		const popupFormData = {
			formId: options?.popup_form_id,
			heading: options?.popup_form_heading,
			image: options?.popup_form_image,
			formEmailField: data?.formEmailField || '',
		}

		setPopupData(popupFormData);
	};

	const setPageTheme = (value) => {
		setBackgroundColor(value);
	};

	const toggleDownloadBlocked = (value) => {
		setDownloadBlocked(value);

		if (value) {
			toggleAccountMenu(true);
		}
	};

	return {
		menuActive,
		cursorState,
		accountActive,
		downloadBlocked,
		contactActive,
		popupActive,
		toggleMenu,
		backgroundColor,
		formData,
		popupData,
		accountActiveAccordionIndex,
		setAccountActiveAccordionIndex,
		toggleAccountMenu,
		toggleFormMenu,
		toggleFormPopup,
		toggleDownloadBlocked,
		setPageTheme,
		updateCursor
	};
}
