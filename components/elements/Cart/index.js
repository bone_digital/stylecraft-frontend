import { motion, AnimatePresence } from 'framer-motion';
import { breakpoint, ease } from '../../../styles/theme';
import { useState } from 'react';
import CartItem from './CartItem';
import Subtotal from './Subtotal';
import styled from 'styled-components';
import Checkout from './Checkout';
import { formatPriceWithoutDecimals } from '../../../lib/source/wordpress/utils';

const Wrapper = styled.div`
	margin-bottom: 100px;
`;

const NavList = styled.ul`
	font-size: ${({ theme }) => theme.type.nav[0]};
	line-height: ${({ theme }) => theme.type.nav[1]};
	opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
	transition: ${ease('all')};

	@media ${breakpoint('xlDesktop')} {
		font-size: ${({ theme }) => theme.typeMobile.nav[0]};
	}

	@media ${breakpoint('tablet')} {
		line-height: ${({ theme }) => theme.typeMobile.nav[1]};
	}
`;

const NavItem = styled(motion.div)`
	transition: ${ease('all')};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};

	&:not(:last-child) {
		margin-bottom: 60px;
	}
`;

const CartList = styled.div`
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	padding-bottom: 15px;
`;

const variants = {
	hidden: {
		opacity: 0,
		transition: {
			duration: 0.2
		}
	},
	visible: {
		opacity: 1,
		transition: {
			duration: 0.2
		}
	}
};

const Cart = ({ cartItems, newCartItems, total, wholesaleTotal, isWholeseller }) => {
	const [disabled, setDisabled] = useState(false);

	const formattedCartTotal = isWholeseller ? wholesaleTotal || total : total;

	return (
		<Wrapper>
			<CartList>
				{newCartItems !== null && (
					<NavList disabled={disabled}>
						<AnimatePresence>
							{newCartItems &&
								newCartItems.map((item) => (
									<NavItem
										key={`cart-${item?.item_key}`}
										variants={variants}
										initial={'hidden'}
										animate={'visible'}
										exit={'hidden'}
									>
										<CartItem
											productInfo={item}
											isWholeseller={isWholeseller}
										/>
									</NavItem>
								))}
						</AnimatePresence>
					</NavList>
				)}
			</CartList>
			<Subtotal total={formattedCartTotal} disabled={disabled} />
			<Checkout setDisabled={setDisabled} cartItems={newCartItems} />
		</Wrapper>
	);
};

export default Cart;
