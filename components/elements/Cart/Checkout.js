import UpdateCart from '../../common/UpdateCart';
import styled, { css, keyframes } from 'styled-components';
import { ease } from '../../../styles/theme';
import jwtDecode from 'jwt-decode';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';

const Button = styled.a`
	padding: 9px 28px;
	color: ${({ theme }) => theme.colors.white};
	display: inline-flex;
	text-align: center;
	transition: ${ease('all')};
	position: relative;
	justify-content: center;
	align-items: center;
	z-index: 10;
	background: ${({ theme }) => theme.colors.brand};
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};

	&:hover {
		background: ${({ theme }) => theme.colors.black};
	}
`;

const Wrapper = styled.div`
	padding: 15px 0 15px 210px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: center;

	> *:first-child {
		margin-right: 30px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-left: 0;
	}
`;

const Checkout = ({ cartItems, setDisabled }) => {
	const router = useRouter();

	let url;

	if (cartItems.length > 0) {
		const localToken = localStorage.getItem('woo-session');
		const cartKey = Cookies.get('cart-key');
		url = `${process.env.NEXT_PUBLIC_CMS_URL}/cart?cocart-load-cart=${cartKey}`;;
		const decoded = jwtDecode(localToken);

		const customerId = decoded?.data?.customer_id;
		if (customerId) {
			//url = `${process.env.NEXT_PUBLIC_CMS_URL}/checkout?session_id=${customerId}`;
//			url = `${process.env.NEXT_PUBLIC_CMS_URL}/checkout?session_id=${customerId}`;
			//url += `&session_id=${customerId}`;
		}
	}

	return cartItems?.length ? (
		<Wrapper>
			<UpdateCart cartItems={cartItems} setDisabled={setDisabled} />

			<Button title="Checkout" href={url}>
				Checkout
			</Button>
		</Wrapper>
	) : null;
};

export default Checkout;
