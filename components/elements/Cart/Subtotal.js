import styled from 'styled-components';
import { ease, breakpoint } from '../../../styles/theme';

const CartSub = styled.div`
	padding: 15px 0 15px 210px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
	transition: ${ease('all')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-left: 0;
	}
`;

const SubTitle = styled.h3``;

const Heading = styled.div`
	margin-right: 30px;
`;

const Price = styled.div``;

const Copy = styled.p``;

const Subtotal = ({ total, disabled }) => {
	return (
		<CartSub disabled={disabled}>
			<Heading>
				<SubTitle>Subtotal</SubTitle>
				<Copy>Excludes Shipping</Copy>
			</Heading>
			{total && (
				<Price>
					<SubTitle>{`A${total}`}</SubTitle>
				</Price>
			)}
		</CartSub>
	);
};

export default Subtotal;
