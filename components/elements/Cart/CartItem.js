import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { cartActions } from '../../../store/cartSlice';
import CartCard from '../../common/CartCard';
import Cookies from 'js-cookie';

const CartItem = ({ productInfo, isWholeseller }) => {
	const dispatch = useDispatch();

	const [updating, setUpdating] = useState(false);

	const userId = useSelector(({ user }) => user.id);

	const updateCartQuantity = async (quantity) => {
		setUpdating(true);

		const cartResponse = await fetch('/api/update-cart', {
			method: 'POST',
			credentials: 'same-origin',
			body: JSON.stringify({
				itemKey: productInfo?.item_key,
				quantity: quantity,
				userId
			})
		})
			.then((res) => res.json())
			.then((json) => {

				dispatch(
					cartActions.newUpdateCart({
						jsonCart: json,
					})
				)

				if (json?.cart_key) {
					Cookies.set('cart-key', json.cart_key, { expires: 10 });
				}

				setUpdating(false);

				return json;
			}).catch((err) => {
				console.log(err);
				setUpdating(false);
			});
	}

	const removeItem = () => {
		updateCartQuantity(0);
	};

	return (
		<CartCard
			productInfo={productInfo}
			removeItem={removeItem}
			updating={updating}
			isWholeseller={isWholeseller}
			setCartQuantity={updateCartQuantity}
		/>
	);
};

export default CartItem;
