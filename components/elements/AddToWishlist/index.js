import React, { useState } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { cartActions } from '../../../store/cartSlice';

const ShareButton = styled.button`
	&:disabled {
		opacity: 0.5;
	}
`;

const AddToWishList = ({ type, variation, product }) => {
	const [buttonText, setButtonText] = useState('+ Add to Wishlist');

	const dispatch = useDispatch();

	const userId = useSelector(({ user }) => user.id);
	const wishlistItems = useSelector(({ cart }) => cart.wishlistItems);

	const productId =
		type === 'VariableProduct'
			? variation?.node?.variationId
			: product?.databaseId;

	const handleAddToWishlist = () => {
		setButtonText('Adding');

		// user is not logged in, so keep in local storage.
		if (!userId && productId) {
			let existingItemIndex = null;

			for (let i = 0; i < wishlistItems?.length; i++) {
				if (wishlistItems[i]?.id == productId) {
					existingItemIndex = i;
					break;
				}
			}

			if (existingItemIndex || existingItemIndex === 0) {
				const newItem = {...wishlistItems[existingItemIndex]};
				newItem.quantity = newItem.quantity + 1;

				const newWishlistItems = [...wishlistItems];

				newWishlistItems[existingItemIndex] = newItem;

				dispatch(
					cartActions.updateWishlist({
						items: newWishlistItems,
						count: newWishlistItems?.length || 0,
					})
				);

				localStorage.setItem('sc-wishlist', JSON.stringify(newWishlistItems));
				localStorage.setItem('sc-wishlist-count', newWishlistItems?.length || 0);

				setButtonText('Added to Wishlist');
				setTimeout(() => {
					setButtonText('+ Add to Wishlist');
				}, 800);
			} else {
				// item is not in wishlist, so fetch to get product card, and add to slice / local storage.
				fetch('/api/update-wishlist', {
					method: 'POST',
					body: JSON.stringify({
						add: productId
					})
				})
					.then((res) => res.json())
					.then((json) => {
						if (json.code === '200') {
							let newWishlistItems = [json?.product]
							if ( wishlistItems != null )
							{
								newWishlistItems = [...wishlistItems, json?.product]
							}

							localStorage.setItem('sc-wishlist', JSON.stringify(newWishlistItems));
							localStorage.setItem('sc-wishlist-count', newWishlistItems?.length || 0);

							dispatch(
								cartActions.updateWishlist({
									items: newWishlistItems,
									count: newWishlistItems?.length || 0,
								})
							);

							setButtonText('Added to Wishlist');
						} else {
							setButtonText('Error, try Again');
							console.log(json?.msg)
						}
						setTimeout(() => {
							setButtonText('+ Add to Wishlist');
						}, 800);
					}).catch((err) => {
						setButtonText('Error, try Again');
						console.log(err);
						setTimeout(() => {
							setButtonText('+ Add to Wishlist');
						}, 800);
					});
			}
		}

		// user logged in, save to backend.
		if (userId && productId) {
			fetch('/api/update-wishlist', {
				method: 'POST',
				body: JSON.stringify({
					userId,
					add: productId
				})
			})
				.then((res) => res.json())
				.then((json) => {
					if (json.code === '200') {
						localStorage.setItem('sc-wishlist', JSON.stringify(json?.wishlist));
						localStorage.setItem('sc-wishlist-count', json?.wishlist?.length || 0);
						dispatch(
							cartActions.updateWishlist({
								items: json?.wishlist,
								count: json?.wishlist?.length || 0,
							})
						);
						setButtonText('Added to Wishlist');
					} else {
						setButtonText('Error, try Again');
						console.log(json?.msg)
					}
					setTimeout(() => {
						setButtonText('+ Add to Wishlist');
					}, 800);
				}).catch((err) => {
					setButtonText('Error, try Again');
					console.log(err);
					setTimeout(() => {
						setButtonText('+ Add to Wishlist');
					}, 800);
				});
		}

	};

	return (
		<ShareButton onClick={handleAddToWishlist} disabled={productId ? false : true}>{buttonText}</ShareButton>
	);
};

export default AddToWishList;
