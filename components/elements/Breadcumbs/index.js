import Link from 'next/link';
import styled from 'styled-components';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import { swatch } from '../../../styles/theme';

const List = styled.ul`
	display: flex;
	flex-wrap: wrap;
`;

const Crumb = styled.li`
	display: flex;

	&:not(:last-child) {
		&::after {
			margin: 0 10px;
			content: '/';
		}
	}
`;

const LinkInner = styled.a`
	transition: color 280ms ease;

	&:hover {
		color: ${swatch('brand')};
	}
`;

const Item = styled.h5`
	text-transform: uppercase;
	letter-spacing: 0.025em;
`;

const Breadcrumbs = ({ title, categories }) => {
	return (
		<List>
			{categories &&
				categories.map(({ node }, index) => {
					if (node?.link) {
						return (
							<Crumb key={index}>
								<Link
									href={`/product?category=${node.slug}`}
									passHref
								>
									<LinkInner title={node?.name}>
										<Item>{node?.name}</Item>
									</LinkInner>
								</Link>
							</Crumb>
						);
					}
				})}
			{title && (
				<Crumb>
					<Item>{title}</Item>
				</Crumb>
			)}
		</List>
	);
};

export default Breadcrumbs;
