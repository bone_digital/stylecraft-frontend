/**
 * This component can be added to preview pages.
 * It shows a simple exit preview which will remove the previewData cookies.
 */
import Link from 'next/link';
import styled from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';

const PreviewBarWrapper = styled.div`
	background: #000;
	padding: 1rem 0;
	color: #fff;
	position: fixed;
	bottom: 0;
	left: 0;
	width: 100%;
	z-index: 10000;

	> * {
		display: flex;
		align-items: center;
		justify-content: space-between;
	}

	a {
		color: #fff;
	}
`;

const PreviewBar = ({ preview }) => {
	if (!preview) {
		//No Preview mode enabled
		return null;
	}

	return (
		<PreviewBarWrapper>
			<InnerWrapper>
				<a href="/api/exit-preview">Exit Preview Mode</a>
			</InnerWrapper>
		</PreviewBarWrapper>
	);
};

export default PreviewBar;
