import styled from 'styled-components';
import { useIsLaptop } from '../../../hooks/useMediaQuery';
import { motion } from 'framer-motion';
import LinkItem from '../../common/LinkHeading';
import SearchFilters from '../SearchFilters';
import ActiveArchiveFilters from '../../elements/ActiveArchiveFilters';
import { useEffect, useState } from 'react';
import { ease } from '../../../styles/theme';

const Header = styled.div`
	padding-top: 160px;
	position: relative;
	z-index: 2;
	margin-bottom: 48px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 24px;
	}
`;

const Title = styled.h1`
	display: block;
	text-indent: -0.05em;
`;

const TitleColumn = styled(motion.div)`
	width: 33.33%;
	display: flex;
	justify-content: flex-end;
	padding: 0 18px;
	flex-direction: column;

	h5 {
		padding-bottom: 4px;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100% !important;
	}
`;

const Row = styled.div`
	display: flex;
	flex-wrap: nowrap;
	margin: 0 -18px;
	justify-content: space-between;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		flex-wrap: wrap;
	}
`;

const Trigger = styled.button`
	position: relative;
	text-align: left;

	&::before {
		content: '';
		display: block;
		width: 7px;
		height: 7px;
		border-radius: 9999px;
		background: ${({ theme }) => theme.colors.black};
		position: absolute;
		top: 50%;
		left: 0%;
		transform: translate(-200%, -50%);
		opacity: ${({ active }) => (active ? 1 : 0)};
		transition: ${ease('all')};
	}

	&:hover {
		&::before {
			opacity: 1;
		}
	}

	&:not(:last-child) {
		margin-right: 48px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			margin: 0 0 1em 0;
		}
	}
`;

const SortByColumn = styled.div`
	padding: 0 18px;
	display: flex;
	align-items: flex-end;

	> div {
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const ArchiveHeader = ({
	title,
	menuActive,
	cachedResults,
	filteredPosts,
	filterByType,
	onClick,
	activeFilters,
	setFilterByType,
	searchingProduct,
	maxProductsCategories
}) => {
	const menuVariant = useIsLaptop
		? {
				open: {
					width: '50%'
				},
				close: {
					width: '33.33%'
				}
		  }
		: {
				open: {
					width: '100%'
				},
				close: {
					width: '100%'
				}
		  };

	const handleTypeChange = (type) => {
		setFilterByType((prevState) => {
			return type === prevState ? null : type;
		});
	};

	const getFilteredProductsLength = () => {
		let posts = [];
		filteredPosts.forEach((row) => {
			return row?.forEach((post) => {
				posts.push(post);
			});
		});

		return posts?.length;
	};

	const resultLength = getFilteredProductsLength();

	return (
		<Header>
			<Row>
				<TitleColumn
					key={'archive-header-title'}
					variants={menuVariant}
					animate={menuActive ? 'open' : 'close'}
					initial={false}
				>
					<LinkItem title={'Search'} />
					<Title>{title}</Title>
				</TitleColumn>

				<ActiveArchiveFilters
					filters={activeFilters}
					menuActive={menuActive}
					onClick={onClick}
					respondTo={'desktop'}
				/>

				<SortByColumn>
					{activeFilters?.length ? (
						<Trigger
							onClick={() => handleTypeChange('product')}
							active={true}
						>
							<LinkItem title={`Product(${resultLength || 0})`} />
						</Trigger>
					) : (
						<SearchFilters
							filtersActive={searchingProduct}
							cachedResults={cachedResults}
							filterByType={filterByType}
							setFilterByType={setFilterByType}
							maxProductsCategories={maxProductsCategories}
						/>
					)}
				</SortByColumn>
			</Row>
		</Header>
	);
};

export default ArchiveHeader;
