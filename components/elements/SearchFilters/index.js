/* eslint-disable func-style */
import { useState, useEffect } from 'react';
import styled from 'styled-components';
import SearchFilterItem from '../SearchFilters/SearchFilterItem';

const Wrapper = styled.div`
	display: flex;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		flex-direction: column;
	}
`;

const SearchFilters = ({
	cachedResults,
	filterByType,
	setFilterByType,
	maxProductsCategories
}) => {
	const [productResults, setProductResults] = useState(0);
	const [articleResults, setArticleResults] = useState(0);
	const [brandResults, setBrandResults] = useState(0);

	useEffect(() => {
		let postsLength = [];
		let productLength = [];
		let brandsLength = [];

		if (cachedResults && cachedResults?.length) {
			cachedResults.forEach((post) => {
				if (post?.type.toLowerCase() == 'post') {
					postsLength.push(post);
				}

				if (post?.type?.toLowerCase() == 'product') {
					productLength.push(post);
				}

				if (post?.type.toLowerCase() == 'brand') {
					brandsLength.push(post);
				}
			});

			setProductResults(productLength);
			setArticleResults(postsLength);
			setBrandResults(brandsLength);
		}

		return () => {
			setProductResults(productLength);
			setArticleResults(postsLength);
			setBrandResults(brandsLength);
		};
	}, [cachedResults]);

	const handleTypeChange = (type) => {
		setFilterByType((prevState) => {
			return type === prevState ? null : type;
		});
	};

	return (
		<Wrapper>
			<SearchFilterItem
				title={'Products'}
				type={'product'}
				filterByType={filterByType}
				onClick={handleTypeChange}
				length={maxProductsCategories?.productMax || 0}
			/>
			<SearchFilterItem
				title={'Articles'}
				type={'post'}
				filterByType={filterByType}
				onClick={handleTypeChange}
				length={maxProductsCategories?.postMax || 0}
			/>
			<SearchFilterItem
				title={'Brands'}
				type={'brand'}
				filterByType={filterByType}
				onClick={handleTypeChange}
				length={maxProductsCategories?.brandMax || 0}
			/>
		</Wrapper>
	);
};

export default SearchFilters;
