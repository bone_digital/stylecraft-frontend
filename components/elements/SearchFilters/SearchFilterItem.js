import styled from 'styled-components';
import { ease } from '../../../styles/theme';
import LinkItem from '../../common/LinkHeading';

const Trigger = styled.button`
	position: relative;
	text-align: left;

	&::before {
		content: '';
		display: block;
		width: 7px;
		height: 7px;
		border-radius: 9999px;
		background: ${({ theme }) => theme.colors.black};
		position: absolute;
		top: 50%;
		left: 0%;
		transform: translate(-200%, -50%);
		opacity: ${({ active }) => (active ? 1 : 0)};
		transition: ${ease('all')};
	}

	&:hover {
		&::before {
			opacity: 1;
		}
	}

	&:not(:last-child) {
		margin-right: 48px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			margin: 0 0 1em 0;
		}
	}
`;

const SearchFilterItem = ({ type, length, onClick, title, filterByType }) => {
	return (
		<Trigger onClick={() => onClick(type)} active={filterByType === type}>
			<LinkItem title={`${title}(${length})`} />
		</Trigger>
	);
};

export default SearchFilterItem;
