import styled from 'styled-components';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useRef } from 'react';
import SwiperCore, { Navigation, A11y, Thumbs } from 'swiper';
import Close from '../../../public/icons/close.svg';
import ImageEl from '../../common/ImageEl/index';
import { motion } from 'framer-motion';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';
import React from 'react';

const GalleryWrapper = styled(motion.div)`
	height: 100%;
	width: 100%;
	display: flex;
	position: absolute;
	align-items: center;
	top: 0;
	left: 0;
	z-index: 100;

	&::before {
		content: '';
		display: block;
		width: 100%;
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		background: rgba(0, 0, 0, 0.8);
		z-index: -1;
	}

	> div {
		width: 100%;
	}
`;

const Wrapper = styled.div`
	position: relative;
	height: 80vh;
	grid-column: 3 / span 8;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		height: 70vh;
		grid-column: 2 / span 4;
	}
`;

const SlideWrapper = styled.div`
	height: 80vh;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		height: 70vh;
	}
`;

const SlideInner = styled.div`
	display: flex;
	overflow: hidden;
	height: 100%;
	width: 100%;
	align-items: center;
	justify-content: center;

	> img {
		object-fit: contain;
		height: 100%;
	}
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;

	&[disabled] {
		cursor: auto;
	}
`;

const CloseTrigger = styled.button`
	width: 44px;
	height: 44px;
	border-radius: 99%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 50%;
	left: 0;
	transform: translate(calc(-100% - 36px), -50%);
	background: ${({ theme }) => theme.colors.white};

	svg {
		width: 8px;
	}
`;

SwiperCore.use([Navigation, A11y, Thumbs]);

const Gallery = ({ setLightboxActive, images, setSwiper, galleryIndex }) => {
	let counter = -1;

	const swiperRef = useRef();

	const sliderVariant = {
		open: {
			opacity: 1,
			pointerEvents: 'all',
			visibility: 'visible',
			transition: {
				type: 'tween'
			}
		},
		close: {
			opacity: 0,
			pointerEvents: 'none',
			visibility: 'hidden',
			transition: {
				type: 'tween'
			}
		}
	};

	return (
		<GalleryWrapper
			variants={sliderVariant}
			animate={'open'}
			initial={'close'}
		>
			<InnerWrapper>
				<Grid>
					<Wrapper>
						<CloseTrigger onClick={() => setLightboxActive(false)}>
							<Close />
						</CloseTrigger>

						{images && images?.length > 1 && (
							<>
								<NavElement
									data-cursor="true"
									className="cursor-link cursor-link--carousel-prev"
									direction="prev"
									onClick={() => swiperRef.current?.slidePrev()}
								/>
								<NavElement
									data-cursor="true"
									className="cursor-link cursor-link--carousel-next"
									direction="next"
									onClick={() => swiperRef.current?.slideNext()}
								/>
							</>
						)}

						<Swiper
							loop={false}
							onSwiper={setSwiper}
							speed={400}
							centeredSlides={false}
							slidesPerView={1}
							spaceBetween={0}
							onBeforeInit={(swiper) => {
								swiperRef.current = swiper;
							}}
						>
							{images.map((slide, index) => {
								counter++;
								const slideCounter = counter;

								const node = slide?.image;
								const type = slide?.type;
								const imageGrid = slide?.imageGrid;

								if (type !== 'grid') {
									return (
										<SwiperSlide key={slideCounter}>
											<SlideWrapper>
												<SlideInner>
													<ImageEl
														alt={node?.alt}
														image={node?.sourceUrl}
														thumbnail={
															node?.thumbSourceUrl
														}
													/>
												</SlideInner>
											</SlideWrapper>
										</SwiperSlide>
									);
								} else {
									let gridCounter = counter;
									return (
										<React.Fragment key={index}>
											{imageGrid?.length &&
												imageGrid.map(
													(slide, gridIndex) => {
														counter =
															gridCounter +
															gridIndex;
														const columnCounter =
															counter;

														return (
															<SwiperSlide
																key={
																	columnCounter
																}
															>
																<SlideWrapper>
																	<SlideInner>
																		<ImageEl
																			alt={
																				slide
																					?.image
																					?.alt
																			}
																			image={
																				slide
																					?.image
																					?.sourceUrl
																			}
																			thumbnail={
																				slide
																					?.image
																					?.thumbSourceUrl
																			}
																		/>
																	</SlideInner>
																</SlideWrapper>
															</SwiperSlide>
														);
													}
												)}
										</React.Fragment>
									);
								}
							})}
						</Swiper>
					</Wrapper>
				</Grid>
			</InnerWrapper>
		</GalleryWrapper>
	);
};

export default Gallery;
