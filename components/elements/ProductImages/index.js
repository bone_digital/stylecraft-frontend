import styled, { css } from 'styled-components';
import ModalPortal from '../../common/ModalPortal';
import { useState, useRef, useEffect } from 'react';
import { AnimatePresence, motion } from 'framer-motion';

import Gallery from './Gallery';
import ImageBg from '../../common/Thumbnail/index';

const ProductGallery = styled.div``;

const ImageWrapper = styled.div`
	position: relative;
	overflow: hidden;
	display: flex;

	&:not(:last-child) {
		margin-bottom: 10px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			margin-bottom: 4px;
		}
	}

	${({ type }) =>
		type === 'grid' &&
		css`
			&:not(:last-child) {
				margin: 0 0;
			}

			@media ${({ theme }) => theme.breakpoints.tablet} {
				grid-column: -1 / 1;
			}
		`};
`;

const LightboxTrigger = styled.button`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 10;
	display: block;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const GridRow = styled.div`
	display: grid;
	width: 100%;
	grid-template-columns: repeat(2, minmax(0, 1fr));
	row-gap: 10px;
	column-gap: 10px;

	&:not(:last-child) {
		margin-bottom: 10px;
	}
`;

const ProductImages = ({ images, hasImages }) => {
	const [lightboxActive, setLightboxActive] = useState(false);
	const [galleryIndex, setGalleryIndex] = useState(0);
	const [modalSwiper, setSwiper] = useState(null);

	let counter = -1;

	const openModal = (index) => {
		setGalleryIndex(index);
		setLightboxActive(true);
	};

	useEffect(() => {
		if (modalSwiper) {
			modalSwiper.slideTo(galleryIndex, 200);
		}
	}, [modalSwiper]);

	return hasImages ? (
		<>
			<ProductGallery>
				{images.map((slide, index) => {
					counter++;

					const slideCounter = counter;

					const type = slide?.type;
					const image = slide?.image;
					const imageGrid = slide?.imageGrid;

					if (type !== 'grid') {
						return (
							<ImageWrapper
								className="cursor-link--view"
								key={`product-gallery-${slideCounter}`}
							>
								<LightboxTrigger
									className="cursor-link--view"
									onClick={() => {
										openModal(slideCounter);
									}}
								/>
								<ImageBg
									alt={image?.alt}
									image={image?.sourceUrl}
									thumbnail={image?.thumbSourceUrl}
								/>
							</ImageWrapper>
						);
					} else {
						let gridCounter = counter;
						return (
							<GridRow>
								{imageGrid?.length &&
									imageGrid.map((slide, gridIndex) => {
										counter = gridCounter + gridIndex;
										const columnCounter = counter;

										return (
											<ImageWrapper
												className="cursor-link--view"
												type="grid"
												key={`grid-gallery-${columnCounter}`}
											>
												<LightboxTrigger
													className="cursor-link--view"
													onClick={() => {
														openModal(
															columnCounter
														);
													}}
												/>
												<ImageBg
													alt={slide?.image?.alt}
													image={
														slide?.image?.sourceUrl
													}
													thumbnail={
														slide?.image
															?.thumbSourceUrl
													}
													ratio={'63.33%'}
												/>
											</ImageWrapper>
										);
									})}
							</GridRow>
						);
					}
				})}
			</ProductGallery>

			<AnimatePresence>
				{lightboxActive && images && images.length && (
					<ModalPortal
						id={'modal'}
						active={lightboxActive}
						key="gallery-modal"
					>
						<Gallery
							images={images}
							setSwiper={setSwiper}
							setLightboxActive={setLightboxActive}
							galleryIndex={galleryIndex}
						/>
					</ModalPortal>
				)}
			</AnimatePresence>
		</>
	) : null;
};

export default ProductImages;
