import styled from "styled-components";
import { ease } from "../../../styles/theme";
import { motion } from "framer-motion";
import Close from '../../../public/icons/close.svg';

export default function ArchiveActiveFiltersList({
	respondTo = 'desktop',
	filters,
	menuActive,
	onDismissFilter,
}) {


	return (
		<FiltersWrapper respondTo={respondTo}>
			<FilterList>
				{!!filters?.length && (
					<FilterList menuActive={menuActive}>
						{filters.map((filter, index) => (
							<FilterPill
								onClick={() => {onDismissFilter(filter)}}
								filter={filter}
								key={`filter-pill-${index}`}
							></FilterPill>
						))}
					</FilterList>
				)}
			</FilterList>
		</FiltersWrapper>
	)
}


const FiltersWrapper = styled.div`
	flex: 1;
	padding: ${({ respondTo }) => respondTo === 'desktop' ? '0 18px' : '0'};
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'flex' : 'none') : 'flex'};
	align-items: flex-end;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
		flex: none;
		margin-top: 16px;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'flex' : 'none') : 'flex'};
	}
`;

const FilterList = styled.ul`
	display: flex;
	width: 100%;
	align-items: flex-end;
	flex-wrap: wrap;
	transition: ${ease('all')};
	opacity: ${({ menuActive }) => (menuActive ? 0 : 1)};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 16px;
	}
`;


function FilterPill({ filter, onClick }) {
	const variants = {
		hidden: { opacity: 0 },
		visible: { opacity: 1 }
	};

	return filter ? (
		<FilterItem
			onClick={onClick}
			key={`pill-${filter?.runtimeId || filter?.slug}`}
			variants={variants}
			animate={'visible'}
			initial={'hidden'}
			exit={'hidden'}
		>
			<FilterButton>
				<FilterLabel dangerouslySetInnerHTML={{ __html: filter.label }} />
				<Close />
			</FilterButton>
		</FilterItem>
	) : null;
};


const FilterButton = styled.button`
	display: flex;
	align-items: center;
	min-width: 130px;
	padding: 0 8px 8px 0;
	border-bottom: 1px solid #929497;
	justify-content: space-between;
	white-space: nowrap;

	svg {
		width: 7px;
		height: 7px;
	}
`;

const FilterLabel = styled.label`
	padding-right: 14px;
	pointer-events: none;
`;

const FilterItem = styled(motion.li)`
	list-style: none;
	padding-left: 0;
	margin: 36px 36px 0 0;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		margin: 16px 16px 0 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin: 0 16px 16px 0;
	}
`;
