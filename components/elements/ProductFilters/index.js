import { AnimatePresence, motion } from 'framer-motion';
import { useMemo, useState, useEffect } from 'react';
import styled from 'styled-components';
import FilterColumn from './FilterColumn';
import FilterShowHide from './FilterShowHide';
import SortBy from '../../common/SortBy';
import { SlideDown } from 'react-slidedown';
import SearchFilters from '../SearchFilters';

const Container = styled(motion.nav)`
	width: 33.33%;
	padding: 0 18px;
	flex-shrink: 0;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'flex' : 'none') : 'flex'};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100% !important;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'flex' : 'none') : 'flex'};
		flex-wrap: wrap;
		margin-bottom: 32px;
	}

	.react-slidedown {
		transition-duration: 200ms;
		transition-timing-function: 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
	}
`;

const MobileFilters = styled.div`
	padding-top: 30px;
`;

const FilterRow = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: flex-start;
	flex-wrap: wrap;
	width: 100%;
	justify-content: space-between;
`;

const ProductFilters = ({
	type = 'product',
	filters,
	activeFilters,
	handleBrandFilterAdd,
	handleProductFilterAdd,
	handleLeadtimeFilterAdd,
	handleEnvironmentFilterAdd,
	handlePriceFilterAdd,
	handleRangeFilterAdd,
	handleAustralianMadeAdd,
	handleDesignerFilterAdd,
	handleNewProductsAdd,
	setMenuActive,
	menuActive,
	options,
	setSortBy,
	handleRemoveFilter,
	setUserInteracted,
	results,
	filterByType,
	setFilterByType
}) => {
	const [showHideActive, setShowHideActive] = useState(null);
	const [showHideLabel, setShowHideLabel] = useState(null);
	const [activeMenu, setActiveMenu] = useState(null);
	const [activeChildMenu, setActiveChildMenu] = useState(null);
	const [activeGrandChildMenu, setActiveGrandChildMenu] = useState(null);
	const [activeGrandGrandChildMenu, setActiveGrandGrandChildMenu] = useState(null);

	const austrlianMadeFilter = {
		slug: 'australianMade',
		title: 'Australian Made'
	};
	// TODO: What's New Filtering. Temp removed for launch. Uncomment when ready
	// const whatsNewFilter = {
	// 	slug: 'whatsNew',
	// 	title: 'What&#8216;s New'
	// };

	// remove grand-children (designers/categories) from brands
	if (filters?.brands?.edges?.length) {
		filters.brands.edges = filters.brands.edges
			.filter(edge => typeof edge.node === "object" && !Array.isArray(edge.node))
			.map(edge => {
				const node = edge.node;
				delete node.children;
			return { node };
		});
	}

	// sort brands alphabetically
	if (filters?.brands?.edges?.length) {
		// Sort the brands array based on the brand name, ignoring case
		filters?.brands?.edges?.sort((a, b) => {
		// Convert the brand name to lowercase for comparison
		const nameA = a?.node?.name.toLowerCase()
		const nameB = b?.node?.name.toLowerCase()
		if (nameA < nameB) {
			return -1
		}
		if (nameA > nameB) {
			return 1
		}
		// If the brand names are equal, return 0
		return 0
		})
	}

	const productFilters = useMemo(() => {
		let filterArray = [];

		if (!filters) {
			return filterArray;
		}

		for (const key of Object.keys(filters)) {
			let title;

			if (key === 'brands') {
				title = 'Brand';
			} else if (key === 'environmentalCertifications') {
				title = 'Environmental';
			} else if (key === 'leadtimes') {
				title = 'Lead Time';
			} else if (key === 'productCategories') {
				title = 'Category';
			} else if (key === 'ranges') {
				title = 'Range';
			} else if (key === 'priceRanges') {
				title = 'Price';
			}

			filterArray.push({
				slug: key,
				children: filters[key],
				title
			});
		}
		// TODO: What's New Filtering. Temp removed for launch. Uncomment when ready
		//filterArray.splice(5, 0, whatsNewFilter);
		filterArray.splice(6, 0, austrlianMadeFilter);

		return filterArray;
	}, [filters]);

	useEffect(() => {
		setActiveChildMenu(null);
		setActiveGrandChildMenu(null);

		if (activeMenu !== null) {
			setMenuActive(true);
		} else {
			setMenuActive(false);
		}
	}, [activeMenu]);

	const handleAddFilter = (menu) => {
		const type = activeMenu?.slug;

		menu.parent = type;

		if (menu?.designerId) {
			menu.parent = 'designers';
		}
		else if (menu.parent === 'ranges') {
			handleRangeFilterAdd(menu);
		}
		else if (menu.parent === 'designers') {
			handleDesignerFilterAdd(menu);
		}
		else if (menu.parent === 'brands') {
			handleBrandFilterAdd(menu);
		}
		else if (menu.parent === 'productCategories') {
			handleProductFilterAdd(menu);
		}
		else if (menu.parent === 'whatsNew') {
			handleNewProductsAdd(true);
		}
		else if (menu.parent === 'australianMade') {
			handleAustralianMadeAdd(true);
		}
		else if (menu.parent === 'leadtimes') {
			handleLeadtimeFilterAdd(menu);
		}
		else if (menu.parent === 'environmentalCertifications') {
			handleEnvironmentFilterAdd(menu);
		}
		else if (menu.parent === 'priceRanges') {
			handlePriceFilterAdd(menu);
		}

		if ((!menu?.children || menu?.children === null) && !menu?.filter) {
			setShowHideLabel(null);
			setUserInteracted(true);
			setActiveMenu(null);
			setActiveChildMenu(null);
			setActiveGrandChildMenu(null);
			setMenuActive(false);
			setShowHideActive(false);
		}
	};

	const handleSetMenu = (filterSlug) => {
		const getMenu = productFilters.find((filter) => {
			return filter?.slug === filterSlug;
		});

		if (!getMenu) {
			return;
		}

		activeMenu?.slug === filterSlug
			? setActiveMenu(null)
			: setActiveMenu(getMenu);

		setShowHideLabel(getMenu?.title || getMenu?.name);

		const itemsWithoutChildren = ['whatsNew', 'australianMade'];

		if (itemsWithoutChildren.includes(getMenu.slug)) {
			getMenu.parent = getMenu.slug;
		}

		if (!activeMenu?.children || activeMenu?.children === null) {
			if (itemsWithoutChildren.includes(filterSlug)) {
				handleAddFilter(getMenu);
			}
		}

		if (!getMenu?.children || getMenu?.children === null) {
			setUserInteracted(true);
			setShowHideLabel(null);

			if (getMenu?.slug === 'whatsNew') {
				if (activeFilters.some(f => f.slug === 'whatsNew')) {
					handleRemoveFilter(getMenu);
				}
				else {
					handleNewProductsAdd(true);
				}
			} else if (getMenu?.slug === 'australianMade') {
				if (activeFilters.some(f => f.slug === 'australianMade')) {
					handleRemoveFilter(getMenu);
				}
				else {
					handleAustralianMadeAdd(true);
				}
			}
		}
	};

	const handleSetChildMenu = (filterSlug) => {
		setUserInteracted(true);
		const menuItems = activeMenu?.children?.edges;
		const getSubmenu = menuItems.find(({ node }) => {
			return node?.slug === filterSlug;
		})?.node;

		const handleRemoveSibling = () => {
			handleRemoveFilter(activeChildMenu);
			setActiveChildMenu(getSubmenu);
		};

		activeChildMenu?.slug === filterSlug
			? setActiveChildMenu(null)
			: handleRemoveSibling();

		activeChildMenu?.slug === filterSlug
			? handleRemoveFilter(getSubmenu)
			: handleAddFilter(getSubmenu);

		if (getSubmenu?.children !== null) {
			setShowHideLabel(getSubmenu?.title || getSubmenu?.name);
		}
	};

	const handleGrandChildMenu = (filterSlug) => {
		const childMenuItems = activeChildMenu?.children?.edges;
		const getGrandChildMenu = childMenuItems.find(({ node }) => {
			return node?.slug === filterSlug;
		})?.node;

		const handleRemoveSibling = () => {
			handleRemoveFilter(activeGrandChildMenu);
			setActiveGrandChildMenu(getGrandChildMenu);
		};

		activeGrandChildMenu?.slug === filterSlug
			? setActiveGrandChildMenu(null)
			: handleRemoveSibling();

		activeGrandChildMenu?.slug === filterSlug
			? handleRemoveFilter(getGrandChildMenu)
			: handleAddFilter(getGrandChildMenu);

		setShowHideLabel(null);
	};

	const handleGrandGrandChildMenu = (filterSlug) => {
		const childMenuItems = activeGrandChildMenu?.children?.edges;
		const getGrandGrandChildMenu = childMenuItems.find(({ node }) => {
			return node?.slug === filterSlug;
		})?.node;

		const handleRemoveSibling = () => {
			handleRemoveFilter(activeGrandGrandChildMenu);
			setActiveGrandGrandChildMenu(getGrandGrandChildMenu);
		};

		activeGrandGrandChildMenu?.slug === filterSlug
			? setActiveGrandGrandChildMenu(null)
			: handleRemoveSibling();

		activeGrandGrandChildMenu?.slug === filterSlug
			? handleRemoveFilter(getGrandGrandChildMenu)
			: handleAddFilter(getGrandGrandChildMenu);

		setShowHideLabel(null);
	};

	const handleExitSubMenu = () => {
		if (!activeMenu) {
			setShowHideLabel(null);

			if (activeGrandChildMenu) {
				setActiveGrandChildMenu(null);
			} else {
				setActiveMenu(null);
				setActiveChildMenu(null);
				setActiveGrandChildMenu(null);
				setShowHideActive(!showHideActive);
			}
		} else {
			if (activeChildMenu) {
				setShowHideLabel(activeMenu?.title);
				setActiveChildMenu(null);
			} else {
				setActiveMenu(null);
				setShowHideLabel(null);
			}
		}

		if (!showHideActive) {
			setActiveMenu(null);
			setActiveChildMenu(null);
			setActiveGrandChildMenu(null);
			setShowHideLabel(null);
		}
	};

	const menuVariantDesktop = {
		open: {
			width: '50%'
		},
		close: {
			width: '33.33%'
		}
	};

	const isActiveMenu =
		activeMenu?.children?.edges && activeMenu?.children?.edges.length;

	const isActiveChildMenu =
		activeChildMenu?.children?.edges &&
		activeChildMenu?.children?.edges.length;

	const isActiveGrandChildMenu =
		activeGrandChildMenu?.children?.edges &&
		activeGrandChildMenu?.children?.edges.length;

	return filters ? (
		<AnimatePresence initial={false}>
			<Container
				respondTo="desktop"
				key={'filter-nav-desktop'}
				variants={menuVariantDesktop}
				animate={menuActive ? 'open' : 'close'}
				initial={false}
			>
				{(type === 'product' || filterByType === 'product') && (
					<>
						<FilterColumn
							id={`menu-deskop`}
							isParent={true}
							active={menuActive}
							filters={productFilters}
							activeFilters={activeFilters}
							menuActive={menuActive}
							activeMenu={activeMenu}
							onClick={handleSetMenu}
						/>

						{isActiveMenu && menuActive && (
							<FilterColumn
								id={`child-menu-${activeMenu?.slug}`}
								isParent={false}
								active={isActiveMenu}
								filters={activeMenu?.children?.edges}
								activeFilters={activeFilters}
								menuActive={menuActive}
								activeMenu={activeChildMenu}
								onClick={handleSetChildMenu}
							/>
						)}

						{isActiveChildMenu && menuActive && (
							<FilterColumn
								id={`grand-child-menu-${activeMenu?.slug}`}
								isParent={false}
								active={isActiveChildMenu}
								filters={activeChildMenu?.children?.edges}
								activeFilters={activeFilters}
								menuActive={menuActive}
								activeMenu={activeGrandChildMenu}
								onClick={handleGrandChildMenu}
							/>
						)}

						{isActiveGrandChildMenu && menuActive && (
							<FilterColumn
								id={`grand-grand-child-menu-${activeMenu?.slug}`}
								isParent={false}
								active={isActiveChildMenu}
								filters={activeGrandChildMenu?.children?.edges}
								activeFilters={activeFilters}
								menuActive={menuActive}
								activeMenu={activeGrandGrandChildMenu}
								onClick={handleGrandGrandChildMenu}
							/>
						)}
					</>
				)}
			</Container>

			<Container
				respondTo="mobile"
				key={'filter-nav-mobile'}
				variants={menuVariantDesktop}
				animate={menuActive ? 'open' : 'close'}
				initial={false}
			>
				{(type === 'product' || filterByType === 'product') && (
					<>
						<FilterRow>
							<FilterShowHide
								onClick={handleExitSubMenu}
								active={showHideActive}
								label={showHideLabel}
							/>

							{type !== 'search' ? (
								<SortBy
									options={options}
									setSortPostsBy={(data) => setSortBy(data)}
								/>
							) : (
								<SearchFilters
									results={results}
									filterByType={filterByType}
									setFilterByType={setFilterByType}
								/>
							)}
						</FilterRow>

						<SlideDown>
							{showHideActive && (
								<MobileFilters>
									{!menuActive && (
										<FilterColumn
											id={`menu-mobile`}
											isParent={true}
											active={menuActive}
											filters={productFilters}
											activeFilters={activeFilters}
											menuActive={menuActive}
											activeMenu={activeMenu}
											onClick={handleSetMenu}
										/>
									)}

									{isActiveMenu &&
										menuActive &&
										activeChildMenu === null && (
											<FilterColumn
												id={`child-menu-${activeMenu?.slug}`}
												isParent={false}
												active={isActiveMenu}
												filters={
													activeMenu?.children?.edges
												}
												activeFilters={activeFilters}
												menuActive={menuActive}
												activeMenu={activeChildMenu}
												onClick={handleSetChildMenu}
											/>
										)}

									{isActiveChildMenu && menuActive && (
										<FilterColumn
											id={`grand-child-menu-${activeMenu?.slug}`}
											isParent={false}
											active={isActiveChildMenu}
											filters={
												activeChildMenu?.children?.edges
											}
											activeFilters={activeFilters}
											menuActive={menuActive}
											activeMenu={activeGrandChildMenu}
											onClick={handleGrandChildMenu}
										/>
									)}
								</MobileFilters>
							)}
						</SlideDown>
					</>
				)}
			</Container>
		</AnimatePresence>
	) : null;
};

export default ProductFilters;
