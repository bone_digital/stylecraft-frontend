import styled from 'styled-components';
import FilterItem from './FilterItem';
import { motion } from 'framer-motion';

const Filters = styled.ul``;

const Column = styled(motion.div)`
	min-width: 120px;
	width: 33.33%;
	padding-right: 16px;
	margin-bottom: 30px;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'block' : 'none') : 'block'};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'block' : 'none') : 'block'};
	}
`;

const FilterColumn = ({
	filters,
	respondTo,
	id,
	active,
	activeFilters,
	activeMenu,
	isParent,
	menuActive,
	onClick
}) => {
	const columnVariants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2,
				ease: [0.83, 0, 0.17, 1]
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2,
				ease: [0.83, 0, 0.17, 1]
			}
		}
	};

	if (!filters) {
		return null;
	}
	return (
		<Column
			key={id}
			respondTo={respondTo}
			variants={!isParent ? columnVariants : null}
			initial={!isParent ? 'hidden' : null}
			animate={active ? 'visible' : 'hidden'}
			exit={!isParent ? 'hidden' : null}
		>
			<Filters>
				{filters.map((filter, index) => {
					const node = filter?.node;
					const nodeTitle = node?.name || node?.title;

					const children = isParent
						? filter?.children
						: node?.children;
					const name = isParent ? filter?.title : nodeTitle;
					const slug = isParent ? filter?.slug : node?.slug;

					const isActive = activeFilters.filter((filter) => {
						if (node?.id) {
							return filter?.id === node.id;
						}
						else {
							return filter?.slug === slug;
						}
					})?.length;

					return (
						<FilterItem
							filter={children}
							title={`${name}`}
							slug={slug}
							onClick={onClick}
							active={isActive}
							menuActive={menuActive}
							key={`${slug}-menu-filter-${index}`}
						/>
					);
				})}
			</Filters>
		</Column>
	);
};

export default FilterColumn;
