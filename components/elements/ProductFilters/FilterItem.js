import styled from 'styled-components';
import { motion } from 'framer-motion';
import { ease } from '../../../styles/theme';

const Item = styled(motion.li)`
	word-wrap: nowrap;
	white-space: nowrap;

	&:not(:last-child) {
		margin-bottom: 1.6em;
	}
`;

const Trigger = styled.button`
	position: relative;
	transition: ${ease('all')};
	word-wrap: nowrap;
	white-space: nowrap;
	color: ${({ active, parent, theme }) =>
		active && parent == false ? theme.colors.brand : theme.colors.black};

	&:hover {
		color: ${({ theme }) => theme.colors.brand};

		&::before {
			background: ${({ theme }) => theme.colors.brand};
		}
	}

	&::before {
		content: '';
		display: block;
		position: absolute;
		top: 50%;
		left: 0;
		transform: translate(calc(-100% - 7px), -60%);
		height: 7px;
		width: 7px;
		border-radius: 50%;
		background: ${({ theme, parent }) =>
			parent == false ? theme.colors.brand : theme.colors.black};
		opacity: ${({ active, menuActive }) => (active && menuActive ? 1 : 0)};
		flex-shrink: 0;
		transition: ${ease('all')};

		@media ${({ theme }) => theme.breakpoints.laptop} {
			display: none;
		}
	}
`;

const FilterItem = ({ title, slug, onClick, filter, active, menuActive }) => {
	const variants = {
		hidden: {
			tranform: 'translateY(10px)',
			opacity: 0
		},
		visible: {
			tranform: 'translateY(0)',
			opacity: 1
		}
	};

	return filter ? (
		<>
			{filter && filter?.edges?.length > 0 && (
				<Item
					variants={variants}
					initial="hidden"
					animate="visible"
					key={`filter-${slug}`}
				>
					<Trigger
						parent={true}
						active={active}
						menuActive={menuActive}
						onClick={() => onClick(slug)}
						dangerouslySetInnerHTML={{ __html: title }}
					/>
				</Item>
			)}
		</>
	) : (
		<Item
			variants={variants}
			initial="hidden"
			animate="visible"
			key={`filter-${slug}`}
		>
			<Trigger
				parent={false}
				active={active}
				menuActive={menuActive}
				onClick={() => onClick(slug)}
				dangerouslySetInnerHTML={{ __html: title }}
			/>
		</Item>
	);
};

export default FilterItem;
