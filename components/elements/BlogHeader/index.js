import React from 'react';
import styled from 'styled-components';
import OverlapHeading from '../../common/OverlapHeading';
import SortBy from '../../common/SortBy';

const PostHeader = styled.nav`
	display: flex;
	justify-content: space-between;
`;

const SortByColumn = styled.div`
	> * {
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const BlogHeader = ({ options, handleFilterBy, title }) => {
	return (
		<PostHeader>
			<OverlapHeading title={title} />
			<SortByColumn>
				<SortBy
					options={options}
					setSortPostsBy={(data) => handleFilterBy(data)}
				/>
			</SortByColumn>
		</PostHeader>
	);
};

export default BlogHeader;
