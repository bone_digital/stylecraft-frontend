import styled, { css } from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import Thumbnail from '../../common/Thumbnail';
import WYSWYG, { Content } from '../../common/WYSWYG';
import LinkButton from '../../common/LinkButton';
import Details from './Details';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const Card = styled.div`
	width: 100%;
	grid-column: ${({ size }) => (size === 'large' ? 'span 6' : 'span 4')};

	${({ size }) =>
		size !== 'large' &&
		css`
			@media ${breakpoint('laptop')} {
				grid-column: -1 / 1;
			}
		`};

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}

	&:hover {
		&&& img {
			transform: scale(1.1) translate(-50%, -50%);
		}
	}
`;

const Title = styled.h3``;

const ThumbnailWrap = styled.a`
	display: block;
	margin-bottom: 20px;
	position: relative;

	@media ${breakpoint('laptop')} {
		margin-bottom: 32px;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 16px;
	}
`;

const Body = styled(Content)`
	display: flex;
	flex-direction: column;

	> a {
		width: 100%;
	}

	&:not(:last-child) {
		margin-bottom: 1.333em;
	}
`;

const ShowroomCard = ({ size, post }) => {
	const ratio = size === 'large' ? '49.45%' : '75.63%';

	const ratioMobile = '75.63%';

	const featuredImage = post?.featuredImage?.node;

	return (
		<Card size={size}>
			<Link href={searchReplaceLink(post?.link)} passHref>
				<ThumbnailWrap title={post?.title}>
					<Thumbnail
						ratio={ratio}
						ratioMobile={ratioMobile}
						image={featuredImage?.sourceUrl}
						thumbnail={featuredImage?.thumbSourceUrl}
						alt={featuredImage?.alt}
					/>
				</ThumbnailWrap>
			</Link>

			<Body>
				<Title>{post?.title}</Title>
				<Details size={size} showroom={post?.showroom} />
			</Body>

			<Body>
				<LinkButton title={'Showroom'} href={post?.link} />
			</Body>
		</Card>
	);
};

export default ShowroomCard;
