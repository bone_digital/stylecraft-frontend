import styled, { css } from 'styled-components';
import Link from 'next/link';

const CardLink = styled.a`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;

	${({ size }) =>
		size === 'small' &&
		css`
			@media ${({ theme }) => theme.breakpoints.xlDesktop} {
				br {
					display: none;
				}
			}

			@media ${({ theme }) => theme.breakpoints.desktop} {
				br {
					display: inline;
				}
			}
		`};
`;

const CardAddress = styled.div`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`;

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
`;

const Column = styled.div`
	width: ${({ size }) =>
		size !== 'small' ? 'calc(33.33% - 16px)' : 'calc(50% - 16px)'};
	display: flex;
	flex-direction: column;

	&:last-child {
		width: ${({ size }) =>
			size !== 'small' ? 'calc(66.66% - 16px)' : 'calc(50% - 16px)'};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		font-size: ${({ theme }) => theme.type.h5[0]};
		line-height: ${({ theme }) => theme.type.h5[1]};
	}

	${({ size }) =>
		size === 'small' &&
		css`
			@media ${({ theme }) => theme.breakpoints.desktop} {
				width: 100%;

				&:first-child {
					margin-bottom: 1.33em;
				}

				&:last-child {
					width: 100%;
				}
			}

			@media ${({ theme }) => theme.breakpoints.laptop} {
				width: calc(41.66% - 8px);

				&:first-child {
					margin-bottom: 0;
				}

				&:last-child {
					width: calc(58.33% - 8px);
				}
			}
		`};
`;

const Details = ({ size, showroom }) => {
	const { email, address, formattedAddress, phone, fax } = showroom || {};

	return (
		<Row>
			<Column size={size}>
				{address && formattedAddress && (
					<CardAddress
						size={size}
						dangerouslySetInnerHTML={{ __html: formattedAddress }}
					/>
				)}
			</Column>
			<Column size={size}>
				{phone && (
					<CardLink
						href={`tel:${phone.split(' ').join('')}`}
						title="Call Showroom"
						target="_blank"
					>
						{`${phone}`}
					</CardLink>
				)}
				{fax && (
					<CardLink
						href={`tel:${fax.split(' ').join('')}`}
						title="Fax Showroom"
						target="_blank"
					>
						{`${fax}`}
					</CardLink>
				)}
				{email && (
					<CardLink
						href={email}
						title="Email Showroom"
						target="_blank"
					>
						{`${email}`}
					</CardLink>
				)}
			</Column>
		</Row>
	);
};

export default Details;
