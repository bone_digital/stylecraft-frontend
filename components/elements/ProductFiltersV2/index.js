import { AnimatePresence, motion } from 'framer-motion';
import { useState, useEffect } from 'react';
import styled from 'styled-components';
import FilterItem from './FilterItem';
import { SlideDown } from 'react-slidedown';
import FilterShowHide from './FilterShowHide';
import SortBy from '../../common/SortBy';

const Container = styled(motion.nav)`
	width: 100%;
	flex-shrink: 0;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'flex' : 'none') : 'flex'};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100% !important;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'flex' : 'none') : 'flex'};
		flex-wrap: wrap;
		margin-bottom: 32px;
	}

	.react-slidedown {
		transition-duration: 200ms;
		transition-timing-function: 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
	}
`;

const MobileFilters = styled.div`
	padding-top: 30px;
`;

const FilterRow = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: flex-start;
	flex-wrap: wrap;
	width: 100%;
	justify-content: space-between;
`;

const Filters = styled.ul``;

const Column = styled(motion.div)`
	min-width: 120px;
	width: 33.33%;
	padding-right: 16px;
	margin-bottom: 30px;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'block' : 'none') : 'block'};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'block' : 'none') : 'block'};
	}
`;


/**
 * TODO!! Split out the mobile logic / UI somehow. The UI in particular
 * is actually quite different
 *
 *
 */
const ProductFiltersV2 = ({
	filters,
	filterGroups,
	onSelectFilter,
	// onDeselectFilter,
	setMenuActive,
	menuActive,
	sortOptions,
	setSortBy,
	categoryId,
}) => {
	const [activeMenu, setActiveMenu] = useState(null);
	const [activeChildMenu, setActiveChildMenu] = useState(null);
	const [activeGrandChildMenu, setActiveGrandChildMenu] = useState(null);
	const [mobileFiltersActive, setMobileFiltersActive] = useState(false);
	const [mobileFiltersLabel, setMobileFiltersLabel] = useState(null);

	function resetState() {
		setActiveMenu(null);
		setActiveChildMenu(null);
		setActiveGrandChildMenu(null);
	}

	useEffect(() => {
		if (!menuActive) {
			resetState();
		}
	}, [menuActive]);

	if (!filters?.length) return null;

	if (!filterGroups?.length) {
		return (
			<div>
				Error: can't get filters
			</div>
		)
	}

	function handleFirstColClick(filter) {
		if (filter.isParent) {
			if (activeMenu !== filter.value) {
				setActiveMenu(filter.value);
				setMenuActive(true);
				setMobileFiltersLabel(filter.label);
			}
			else { // close menu
				setMenuActive(false);
				setMobileFiltersActive(false);
			}
		}
		else {
			if (typeof onSelectFilter === 'function' && filter.filter) {
				onSelectFilter(filter.filter);
			}

			setMenuActive(false);
			setMobileFiltersActive(false);
			setMobileFiltersLabel(null);
		}
	}

	function handleSecondColClick(filter) {
		if (typeof onSelectFilter === 'function' && filter) {
			onSelectFilter(filter);
		}

		if (activeChildMenu !== filter.value) {
			setActiveChildMenu(filter.value);
			setMobileFiltersLabel(filter.label);
		}
		else {
			setActiveChildMenu(null);
		}

		const hasChildren = filters.some(f => f.parentId === filter.id);
		if (!hasChildren) {
			setMenuActive(false);
			setMobileFiltersActive(false);
			setMobileFiltersLabel(null);
		}
	}

	function handleThirdColClick(filter) {
		if (typeof onSelectFilter === 'function' && filter) {
			onSelectFilter(filter);
			setActiveGrandChildMenu(filter.value);
			setMobileFiltersLabel(filter.label);

			const hasChildren = filters.some(f => f.parentId === filter.id);
			if (!hasChildren) {
				setMenuActive(false);
				setMobileFiltersActive(false);
				setMobileFiltersLabel(null);
			}
			
		}
	}

	function handleForthColClick(filter) {
		if (typeof onSelectFilter === 'function' && filter) {
			onSelectFilter(filter);
			setMenuActive(false);
			setMobileFiltersActive(false);
			setMobileFiltersLabel(null);
		}
	}

	function handleToggleMobileMenu() {
		if (mobileFiltersActive) {
			setMenuActive(true);
			setActiveMenu(null);
			setActiveChildMenu(null);
			setActiveGrandChildMenu(null);
			setMobileFiltersActive(false);
			setMobileFiltersLabel(null);
		}
		else {
			setMenuActive(true);
			setMobileFiltersActive(true);
			setMobileFiltersLabel(activeMenu);
			setActiveChildMenu(null);
			setActiveGrandChildMenu(null);
			setActiveMenu(null);
		}
	}

	const firstColumn = [];
	for (const group of filterGroups) {
		const {type, label, isToggle, rewriteTag} = group;

		const groupFilters = filters.filter(f => f.type === type || f.type === rewriteTag);
		const hasFilters = groupFilters.length >= 1;

		if (isToggle) {
			const filter = hasFilters ? groupFilters[0] : null;
			firstColumn.push({
				isParent: false,
				label: filter.label,
				value: filter.value,
				isSelected: false,
				filter: filter,
			});
		}
		else if (hasFilters) {
			firstColumn.push({
				isParent: true,
				label,
				value: rewriteTag || type,
				isSelected: false,
			})
		}
	}

	let secondColumn = [];

	if (activeMenu) {
		const group = filterGroups?.find(f => f.type === activeMenu);
		const parentId = group?.baseParentId || 0;

		let activeMenuFilters;

		if (categoryId && activeMenu === 'category') {
			activeMenuFilters = filters.filter(
				f => f.type === activeMenu && f.parentId == categoryId
			);
		} else {
			activeMenuFilters = filters.filter(
				f => f.type === activeMenu && f.parentId === parentId,
			);
		}

		secondColumn = activeMenuFilters;
	}

	let thirdColumn = [];
	if (activeChildMenu) {
		const parent = filters.find(f => f.type === activeMenu && f.value === activeChildMenu);
		const activeMenuFilters = filters.filter(
			f => f.type === activeMenu && f.parentId === parent?.id,
		);

		thirdColumn = activeMenuFilters;
	}

	let forthColumn = [];
	if (activeGrandChildMenu) {
		const parent = filters.find(f => f.type === activeMenu && f.value === activeGrandChildMenu);
		const activeMenuFilters = filters.filter(
			f => f.type === activeMenu && f.parentId === parent?.id,
		);

		forthColumn = activeMenuFilters;
	}

	const showSecondColumn = !!(activeMenu && secondColumn.length);
	const showThirdColumn = !!(showSecondColumn && activeChildMenu && thirdColumn.length);
	const showForthColumn = !!(showSecondColumn && activeChildMenu && showThirdColumn && activeGrandChildMenu && forthColumn.length);


	const mobileShowFirstColumn = !activeMenu && !activeChildMenu;
	const mobileShowSecondColumn = activeMenu && !activeChildMenu;
	const mobileShowThirdColumn = activeMenu && activeChildMenu && !activeGrandChildMenu;
	const mobileShowForthColumn = activeMenu && activeChildMenu && activeGrandChildMenu;

	return (
		<AnimatePresence initial={false}>
			<Container
				key={'filter-nav-desktop'}
				respondTo="desktop"
			>
				<Column
					key={'filter-first-col'}
					respondTo={'desktop'}
					variants={null}
					initial={'visible'}
					animate={'visible'}
					exit={null}
				>
					<Filters>
						{firstColumn?.map((filter, index) => {
							return (
								<FilterItem
									key={`${filter.value}-menu-filter-${index}`}
									filter={filter}
									title={filter.label}
									slug={filter.value}
									onClick={() => handleFirstColClick(filter)}
									active={false}
									menuActive={menuActive}
								/>
							);
						})}
					</Filters>
				</Column>

				{menuActive && (
					<>
						{showSecondColumn && (
							<Column
								key={'filter-second-col'}
								respondTo={'desktop'}
								variants={null}
								initial={'visible'}
								animate={'visible'}
								exit={null}
							>
								<Filters>
									{secondColumn.map((filter, index) => {
										return (
											<FilterItem
												key={`${filter.value}-menu-filter-${index}`}
												filter={filter}
												title={filter.label}
												slug={filter.value}
												onClick={() => handleSecondColClick(filter)}
												active={filter.isSelected}
												menuActive={menuActive}
											/>
										);
									})}
								</Filters>
							</Column>
						)}

						{showThirdColumn && (
							<Column
								key={'filter-third-col'}
								respondTo={'desktop'}
								variants={null}
								initial={'visible'}
								animate={'visible'}
								exit={null}
							>
								<Filters>
									{thirdColumn.map((filter, index) => {
										return (
											<FilterItem
												key={`${filter.value}-menu-filter-${index}`}
												filter={filter}
												title={filter.label}
												slug={filter.value}
												onClick={() => handleThirdColClick(filter)}
												active={filter.isSelected}
												menuActive={menuActive}
											/>
										);
									})}
								</Filters>
							</Column>
						)}

						{showForthColumn && (
							<Column
								key={'filter-forth-col'}
								respondTo={'desktop'}
								variants={null}
								initial={'visible'}
								animate={'visible'}
								exit={null}
							>
								<Filters>
									{forthColumn.map((filter, index) => {
										return (
											<FilterItem
												key={`${filter.value}-menu-filter-${index}`}
												filter={filter}
												title={filter.label}
												slug={filter.value}
												onClick={() => handleForthColClick(filter)}
												active={filter.isSelected}
												menuActive={menuActive}
											/>
										);
									})}
								</Filters>
							</Column>
						)}
					</>
				)}
			</Container>

			<Container
				key={'filter-nav-mobile'}
				respondTo="mobile"
			>
				<FilterRow>
					<FilterShowHide
						onClick={handleToggleMobileMenu}
						active={mobileFiltersActive}
						label={mobileFiltersLabel}
					/>

					<SortBy
						options={sortOptions}
						setSortPostsBy={setSortBy}
					/>
				</FilterRow>
				<SlideDown>
					{mobileFiltersActive && (
						<MobileFilters>
							{menuActive && (
								<>
									{mobileShowFirstColumn && (
										<Column
											key={'filter-first-col'}
											respondTo={'mobile'}
											variants={null}
											initial={'visible'}
											animate={'visible'}
											exit={null}
										>
											<Filters>
												{firstColumn?.map((filter, index) => {
													return (
														<FilterItem
															key={`${filter.value}-menu-filter-${index}`}
															filter={filter}
															title={filter.label}
															slug={filter.value}
															onClick={() => handleFirstColClick(filter)}
															active={false}
															menuActive={menuActive}
														/>
													);
												})}
											</Filters>
										</Column>
									)}

									{mobileShowSecondColumn && (
										<Column
											key={'filter-second-col'}
											respondTo={'mobile'}
											variants={null}
											initial={'visible'}
											animate={'visible'}
											exit={null}
										>
											<Filters>
												{secondColumn.map((filter, index) => {
													return (
														<FilterItem
															key={`${filter.value}-menu-filter-${index}`}
															filter={filter}
															title={filter.label}
															slug={filter.value}
															onClick={() => handleSecondColClick(filter)}
															active={filter.isSelected}
															menuActive={menuActive}
														/>
													);
												})}
											</Filters>
										</Column>
									)}

									{mobileShowThirdColumn && (
										<Column
											key={'filter-third-col'}
											respondTo={'mobile'}
											variants={null}
											initial={'visible'}
											animate={'visible'}
											exit={null}
										>
											<Filters>
												{thirdColumn.map((filter, index) => {
													return (
														<FilterItem
															key={`${filter.value}-menu-filter-${index}`}
															filter={filter}
															title={filter.label}
															slug={filter.value}
															onClick={() => handleThirdColClick(filter)}
															active={filter.isSelected}
															menuActive={menuActive}
														/>
													);
												})}
											</Filters>
										</Column>
									)}

									{mobileShowForthColumn && (
										<Column
											key={'filter-forth-col'}
											respondTo={'mobile'}
											variants={null}
											initial={'visible'}
											animate={'visible'}
											exit={null}
										>
											<Filters>
												{forthColumn.map((filter, index) => {
													return (
														<FilterItem
															key={`${filter.value}-menu-filter-${index}`}
															filter={filter}
															title={filter.label}
															slug={filter.value}
															onClick={() => handleForthColClick(filter)}
															active={filter.isSelected}
															menuActive={menuActive}
														/>
													);
												})}
											</Filters>
										</Column>
									)}
								</>
							)}
						</MobileFilters>
					)}
				</SlideDown>

			</Container>
		</AnimatePresence>
	)
};

export default ProductFiltersV2;
