import styled, { css } from 'styled-components';
import Rarr from '../../../public/icons/rarr.svg';
import { ease } from '../../../styles/theme';

const Button = styled.button`
	display: flex;
	align-items: center;
	max-width: ${({ active }) => (active ? '160px' : '85px')};
	width: 100%;
	justify-content: ${({ menuOpen }) =>
		menuOpen ? 'flex-start' : 'space-between'};
	padding-bottom: 6px;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	transition: ${ease('all')};

	svg {
		width: 4px;
		height: 8px;
	}

	${({ menuOpen }) =>
		menuOpen &&
		css`
			svg {
				transform: rotate(180deg);
			}
		`};

	> *:first-child {
		margin-right: 20px;
	}
`;

const Label = styled.p``;

const FilterShowHide = ({ onClick, active, label }) => {
	return (
		<Button onClick={onClick} active={active} menuOpen={label !== null}>
			{label === null ? (
				<>
					<Label>{label ? label : 'Filter By'}</Label>
					<Rarr />
				</>
			) : (
				<>
					<Rarr />
					<Label>{label ? label : 'Filter By'}</Label>
				</>
			)}
		</Button>
	);
};

export default FilterShowHide;
