import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import cookie from 'js-cookie';
import { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import { Formik, Form } from 'formik';
import TextField from '../../common/TextField';
import PasswordField from '../../common/PasswordField';
import * as Yup from 'yup';
import { ease } from '../../../styles/theme';
import { userActions } from '../../../store/userSlice';
import { cartActions } from '../../../store/cartSlice';
import { useUI } from '../../../context/UIProvider';
import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';

const Wrapper = styled.div`
	margin-bottom: 235px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 64px;
	}
`;

const FormWrapper = styled(Form)`
	display: grid;
	width: 100%;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 16px;
	column-gap: 36px;
	grid-auto-rows: initial;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 16px;
		column-gap: 36px;
	}
`;

const Column = styled.div`
	grid-column: span 6;
	display: flex;
	justify-content: space-between;
	align-items: start;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 3;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const LoginButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	margin-left: 30px;
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
	display: ${({ respondTo }) => (respondTo === 'desktop' ? 'block' : 'none')};
	transition: ${ease('all')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-left: 0;
		grid-column: -1 / 1;
		text-align: start;
		display: ${({ respondTo }) =>
			respondTo !== 'desktop' ? 'block' : 'none'};
	}
`;

const ResetPasswordButton = styled.button`
	margin-top: 30px;
	color: ${({ theme }) => theme.colors.black};
	transition: color 280ms ease;
	font-size: ${({ theme }) => theme.type.h6[0]};

	&:hover {
		color: ${({ theme }) => theme.colors.pink};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		text-align: start;
	}
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${LoginButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const ErrorMessage = styled.div`
	padding-bottom: 2em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};

	strong {
		font-weight: normal;
	}
`;

const Login = ({ handleTogglePassword }) => {
	const LOGIN_USER = gql`
		mutation Login($username: String!, $password: String!) {
			login(input: { password: $password, username: $username }) {
				authToken
				refreshToken
				user {
					userId
					username
					firstName
					lastName
					roles {
						edges {
							node {
								id
								name
								capabilities
							}
						}
					}
				}
			}
		}
	`;

	const dispatch = useDispatch();

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [formActions, setFormActions] = useState(null);
	const [formError, setFormError] = useState(null);

	const [userLogin, { loading, error }] = useMutation(LOGIN_USER, {
		variables: {
			username,
			password
		},
		onCompleted: (data) => {
			formActions?.setSubmitting(false);

			const authToken = data?.login?.authToken;
			const refreshToken = data?.login?.refreshToken;
			const user = data?.login?.user;
			const userId = user?.userId;

			setLoginCookie(username, password);

			cookie.set('token', authToken);
			cookie.set('refresh-token', refreshToken);
			cookie.set('ussr-data', btoa(JSON.stringify(data)));

			dispatch(userActions.login({ user }));

			const getWishlistAndCart = async () => {
				dispatch(
					cartActions.updateNewCartStatus({ isLoading: true })
				);
				dispatch(
					cartActions.updateWishlistStatus({ isLoading: true })
				);
				const wishlistResponse = await fetch('/api/get-wishlist', {
					method: 'POST',
					body: JSON.stringify({
						userId
					})
				})
					.then((res) => res.json())
					.then((json) => {
						dispatch(
							cartActions.updateWishlist({
								items: json?.wishlist,
								count: json?.wishlist?.length || 0,
							})
						);
						dispatch(
							cartActions.updateWishlistStatus({ isLoading: false })
						);
						return json;
					}).catch((err) => {
						console.log(err);
					});
				
				const cartResponse = await fetch('/api/get-cart', {
					method: 'POST',
					body: JSON.stringify({
						userId
					})
				})
					.then((res) => res.json())
					.then((json) => {
						dispatch(
							cartActions.newUpdateCart({
								jsonCart: json,
							})
						)
						dispatch(
							cartActions.updateNewCartStatus({ isLoading: false })
						);
												
						if (json?.cart_key) {
							Cookies.set('cart-key', json.cart_key, { expires: 10 });
						}
						return json;
					}).catch((err) => {
						console.log(err);
					});
			}
			getWishlistAndCart();
		},
		onError: (error) => {
			console.log({ error: error });
			if (
				error?.message === 'incorrect_password' ||
				error?.message === 'invalid_email'
			) {
				setFormError('Email or password is incorrect');
			} else {
				setFormError(error?.message);
			}

			// formActions?.setSubmitting(false);
			// Reset the disabled status here
			setFormActions((prevActions) => {
				if (prevActions) {
				  prevActions.setSubmitting(false);
				  //prevActions.setErrors({});
				  prevActions.setStatus(undefined);
				  prevActions.setTouched({});
				}
				return prevActions;
			});
		}
	});

	const LoginSchema = Yup.object().shape({
		username: Yup.string()
			.email('Please enter a valid email')
			.required('Email is required')
			.nullable(),
		password: Yup.string().required('Password is required').nullable()
	});

	const usernameField = {
		id: 'username',
		key: 'username',
		formId: 'loginForm',
		type: 'text',
		label: 'Username',
		cssClass: null,
		isRequired: true,
		placeholder: 'Email',
		fieldErrors: {}
	};

	const passwordField = {
		id: 'password',
		key: 'password',
		formId: 'loginForm',
		type: 'password',
		label: 'Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'Password',
		fieldErrors: {}
	};

	const setLoginCookie = async (username, password) => {
		await fetch(
			`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/login?username=${username}&password=${password}`,
			{
				credentials: 'include'
			}
		)
			.then((response) => response.json())
			.then((json) => {
				const cookies = JSON.parse(json);

				if (cookies) {
					for (const [key, value] of Object.entries(cookies)) {
						Cookies.set(key, value, {
							path: '',
							domain: 'stylecraft.bonestaging.com.au'
						});
					}
				}
			})
			.catch((err) => {
				console.log({ error: err });
			});
	};

	return (
		<Wrapper>
			{formError && (
				<ErrorMessage dangerouslySetInnerHTML={{ __html: formError }} />
			)}

			<Formik
				initialValues={{
					username,
					password
				}}
				validationSchema={LoginSchema}
				onSubmit={(values, actions) => {
					setFormActions(actions);

					setUsername(values.username);
					setPassword(values.password);
					actions.setSubmitting(true);
					userLogin({
						variables: {
							username: values.username,
							password: values.password
						}
					});
				}}
			>
				{({
					values,
					errors,
					touched,
					isSubmitting,
					handleBlur,
					handleChange
				}) => (
					<FormWrapper noValidate>
						<Column>
							<TextField
								field={usernameField}
								error={errors.username}
								value={values.username}
								disabled={isSubmitting}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
						</Column>
						<Column>
							<PasswordField
								field={passwordField}
								error={errors.password}
								value={values.password}
								disabled={isSubmitting}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<LoginButton
								type="submit"
								title="Login"
								respondTo="desktop"
								disabled={isSubmitting}
							>
								<Underline>Login</Underline>
							</LoginButton>
						</Column>
						<LoginButton
							type="submit"
							title="Login"
							respondTo="mobile"
							disabled={isSubmitting}
						>
							<Underline>Login</Underline>
						</LoginButton>
					</FormWrapper>
				)}
			</Formik>
			<ResetPasswordButton onClick={handleTogglePassword}>
				Forgot password
			</ResetPasswordButton>
		</Wrapper>
	);
};

export default Login;
