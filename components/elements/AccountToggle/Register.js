import styled from 'styled-components';
import IframeGravityForm from '../../common/IframeGravityForm';

const options = require('../../../json/options.json');

const Wrapper = styled.div`
	margin-bottom: 64px;
`;

const Register = ({ introCopy }) => {
	const gravityFormsFormId = options?.gravity_forms_register_form_id;

	if (!gravityFormsFormId) {
		return null;
	}

	return (
		<Wrapper>
			{introCopy && (
				<Intro
					dangerouslySetInnerHTML={{
						__html: introCopy
					}}
				/>
			)}
			<IframeGravityForm gravityFormsFormId={gravityFormsFormId} />
		</Wrapper>
	);
};

const Intro = styled.div`
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};
	margin-bottom: 20px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		font-size: ${({ theme }) => theme.type.p[0]};
		line-height: ${({ theme }) => theme.type.p[1]};
	}

	p {
		all: inherit;
	}
`;

export default Register;
