import styled from 'styled-components';
import { useState } from 'react';
import { Formik, Form } from 'formik';
import TextField from '../../common/TextField';
import * as Yup from 'yup';
import { ease } from '../../../styles/theme';

const Wrapper = styled.div`
	margin-bottom: 235px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 64px;
	}
`;

const FormWrapper = styled(Form)`
	display: grid;
	width: 100%;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 16px;
	column-gap: 36px;
	grid-auto-rows: initial;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 16px;
		column-gap: 36px;
	}
`;

const Column = styled.div`
	grid-column: span 6;
	display: flex;
	justify-content: space-between;
	align-items: start;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 3;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const LoginButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	margin-left: 30px;
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
	display: ${({ respondTo }) => (respondTo === 'desktop' ? 'block' : 'none')};
	transition: ${ease('all')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-left: 0;
		grid-column: -1 / 1;
		text-align: start;
		display: ${({ respondTo }) =>
			respondTo !== 'desktop' ? 'block' : 'none'};
	}
`;

const ResetPasswordButton = styled.button`
	margin-top: 30px;
	color: ${({ theme }) => theme.colors.black};
	transition: color 280ms ease;
	font-size: ${({ theme }) => theme.type.h6[0]};

	&:hover {
		color: ${({ theme }) => theme.colors.pink};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		text-align: start;
	}
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${LoginButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const ErrorMessage = styled.div`
	padding-bottom: 2em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};

	strong {
		font-weight: normal;
	}
`;

const Message = styled.div`
	padding-bottom: 2em;

	strong {
		font-weight: normal;
	}
`;

const ResetPassword = ({ handleTogglePassword }) => {
	const [email, setEmail] = useState('');
	const [formError, setFormError] = useState(null);
	const [showConfirmation, setShowConfirmation] = useState(false);

	const emailField = {
		id: 'email',
		key: 'email',
		formId: 'passwordResetForm',
		type: 'text',
		label: 'email',
		cssClass: null,
		isRequired: true,
		placeholder: 'Email',
		fieldErrors: {}
	};

	const resetPasswordSchema = Yup.object().shape({
		email: Yup.string().required('Email is required'),
	});

	const resetPassword = async (values, actions) => {
		try {
			const data = {
				email: values?.email,
			};

			const response = await fetch('/api/reset-password', {
				method: 'POST',
				mode: 'same-origin',
				body: JSON.stringify(data)
			});

			const resetPassword = await response.json();

			if (resetPassword.code === 400) {
				setFormError(resetPassword.msg);
			} else {
				setShowConfirmation(true);
			}

			actions.setSubmitting(false);
		} catch (error) {
			const decodeHtmlCharCodes = (str) => {
				let element = document.createElement('textarea');
				element.innerHTML = str;

				return element.value;
			};

			if (error) {
				setFormError(decodeHtmlCharCodes(String(error?.message)));
			}
		} finally {
			actions.setSubmitting(false);
		}
	}

	return (
		<Wrapper>
			{showConfirmation ? (
				<Message>
					If a user with this email exists, we have sent a password reset email.
				</Message>
			) : (
				<>
					{formError && (
						<ErrorMessage dangerouslySetInnerHTML={{ __html: formError }} />
					)}
					<Message>
						Reset your password:
					</Message>
					<Formik
						initialValues={{
							email,
						}}
						validationSchema={resetPasswordSchema}
						onSubmit={(values, actions) => {
							resetPassword(values, actions);
						}}
					>
						{({
							values,
							errors,
							touched,
							isSubmitting,
							handleBlur,
							handleChange
						}) => (
							<FormWrapper noValidate>
								<Column>
									<TextField
										field={emailField}
										error={errors.email}
										value={values.email}
										disabled={isSubmitting}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<LoginButton
										type="submit"
										title="Login"
										respondTo="desktop"
										disabled={isSubmitting}
									>
										<Underline>Reset</Underline>
									</LoginButton>
								</Column>
								<LoginButton
									type="submit"
									title="Login"
									respondTo="mobile"
									disabled={isSubmitting}
								>
									<Underline>Reset</Underline>
								</LoginButton>
							</FormWrapper>
						)}
					</Formik>
				</>
			)}

			<ResetPasswordButton onClick={handleTogglePassword}>
				Return to login
			</ResetPasswordButton>
		</Wrapper>
	);
};

export default ResetPassword;
