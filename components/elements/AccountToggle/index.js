import styled from 'styled-components';
import { ease } from '../../../styles/theme';
import { useState } from 'react';
import Grid from '../../common/Grid';
import Login from './Login';
import ResetPassword from './ResetPassword';
import Register from './Register';
import { motion, AnimatePresence } from 'framer-motion';
import { useUI } from '../../../context/UIProvider';

const Wrapper = styled.div`
	//
`;

const Header = styled.nav`
	margin-bottom: 60px;
	display: block;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 24px;
	}
`;

const Toggle = styled.button`
	font-size: ${({ theme }) => theme.type.h3[0]};
	line-height: ${({ theme }) => theme.type.h3[1]};
	padding-bottom: 12px;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	grid-column: span 6;
	opacity: ${({ active }) => (active ? 1 : 0.3)};
	transition: ${ease('all')};
	text-align: left;

	&:hover {
		opacity: 1;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 3;
	}
`;

const Body = styled(motion.div)``;

const DownloadCopy = styled.h3`
	display: block;
	margin-bottom: 100px;

	p {
		display: block;
		font-size: inherit;
		line-height: inherit;

		&:not(:last-child) {
			margin-bottom: 1.2em;
		}
	}
`;

const AccountToggle = ({ introCopy, termsCopy, downloadCopy }) => {
	const [hasAccount, setHasAccount] = useState(true);
	const [reserPassword, setReserPassword] = useState(false);

	const handleToggleAccount = (value) => {
		setHasAccount(value);
	};

	const handleTogglePassword = () => {
		setReserPassword(!reserPassword);
	};

	const { downloadBlocked } = useUI();

	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<Wrapper>
			{downloadBlocked && downloadCopy !== null && (
				<DownloadCopy
					dangerouslySetInnerHTML={{ __html: downloadCopy }}
				/>
			)}

			<Header>
				<Grid>
					<Toggle
						active={hasAccount}
						onClick={() => handleToggleAccount(true)}
					>
						Login
					</Toggle>
					<Toggle
						active={!hasAccount}
						onClick={() => handleToggleAccount(false)}
					>
						Create Account
					</Toggle>
				</Grid>
			</Header>
			<AnimatePresence exitBeforeEnter={true}>
				{hasAccount ? (
					reserPassword ? (
						<Body
							key={'account-reset'}
							variants={variants}
							initial={'hidden'}
							animate={'visible'}
							exit={'hidden'}
						>
							<ResetPassword handleTogglePassword={handleTogglePassword} />
						</Body>
					) : (
						<Body
							key={'account-login'}
							variants={variants}
							initial={'hidden'}
							animate={'visible'}
							exit={'hidden'}
						>
							<Login downloadCopy={downloadCopy} handleTogglePassword={handleTogglePassword} />
						</Body>
					)
				) : (
					<Body
						key={'account-register'}
						variants={variants}
						initial={'hidden'}
						animate={'visible'}
						exit={'hidden'}
					>
						<Register
							introCopy={introCopy}
						/>
					</Body>
				)}
			</AnimatePresence>
		</Wrapper>
	);
};

export default AccountToggle;
