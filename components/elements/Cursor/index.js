/**
 * NOTE - Alex, 30/05/2023
 *
 * This global-style cursor component is very cumbersome, and very difficult to
 * tailor for specific use cases. For instance, on carousels Swiper will disable
 * prev/next buttons when that action can't be performed, which makes using
 * event listeners much more difficult.
 *
 * Some better approaches would be:
 *
 * A) keep the global cursor and convert it to a context, so its state can be
 * set directly by the calling component (rather than using listeners)
 *
 * B) convert this to a reusable component that the calling component could own
 * and set state directly.
 *
 *
 *
 */
import { useState, useEffect } from 'react';
import styled, { css } from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';
import { useRouter } from 'next/router';
import { useMousePosition } from '../../../hooks/useMousePosition';
import Close from '../../../public/icons/close.svg';
import Rarr from '../../../public/icons/rarr.svg';
import Open from '../../../public/icons/open.svg';
import { useUI } from '../../../context/UIProvider';
import { throttle } from 'lodash';

const CursorWrapper = styled.div`
	height: var(--cursor-height);
	width: var(--cursor-width);
	z-index: 1000;
	position: fixed;
	display: ${({ isOnDevice }) => (isOnDevice ? 'none' : 'block')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const CursorRing = styled(motion.div)`
	position: fixed;
	display: flex;
	align-items: center;
	justify-content: center;
	opacity: ${({ isActive }) => (isActive ? 1 : 0)};
	top: ${({ isMouseDownOnCarousel, isDragging, isHoveringLink, isActive }) =>
		!isActive
			? 'calc(var(--cursor-height) * -0.25)'
			: isMouseDownOnCarousel
			? 'calc(var(--cursor-down) * -0.5)'
			: isDragging
			? 'calc(var(--cursor-height) * -0.5)'
			: isHoveringLink
			? 'calc(var(--cursor-hover) * -0.5)'
			: 'calc(var(--cursor-height) * -0.5)'};
	left: ${({ isMouseDownOnCarousel, isDragging, isHoveringLink, isActive }) =>
		!isActive
			? 'calc(var(--cursor-width) * -0.25)'
			: isMouseDownOnCarousel
			? 'calc(var(--cursor-down) * -0.5)'
			: isDragging
			? 'calc(var(--cursor-width) * -0.5)'
			: isHoveringLink
			? 'calc(var(--cursor-hover) * -0.5)'
			: 'calc(var(--cursor-width) * -0.5)'};
	height: ${({
		isMouseDownOnCarousel,
		isDragging,
		isHoveringLink,
		isActive
	}) =>
		!isActive
			? 'calc(var(--cursor-height) * 0.5)'
			: isMouseDownOnCarousel
			? 'var(--cursor-down)'
			: isDragging
			? 'var(--cursor-height)'
			: isHoveringLink
			? 'var(--cursor-hover)'
			: 'var(--cursor-height)'};
	width: ${({
		isMouseDownOnCarousel,
		isDragging,
		isHoveringLink,
		isActive
	}) =>
		!isActive
			? 'calc(var(--cursor-width) * 0.5)'
			: isMouseDownOnCarousel
			? 'var(--cursor-down)'
			: isDragging
			? 'var(--cursor-drag)'
			: isHoveringLink
			? 'var(--cursor-hover)'
			: 'var(--cursor-width)'};
	background: ${({ backgroundColor, theme }) =>
		backgroundColor === 'grey' || backgroundColor === 'greyMid'
			? theme.colors.white
			: theme.colors.grey};
	border-radius: 999px;
	pointer-events: none;
	text-align: center;
	z-index: 1000;
	transition: height 200ms ease, width 200ms ease, opacity 200ms ease,
		background 200ms ease, top 200ms ease, left 200ms ease,
		border-radius 200ms ease;
`;

const CursorInner = styled(motion.div)`
	height: 8px;

	svg {
		height: 8px;
		fill: ${({ theme }) => theme.colors.black};
	}

	${({ direction }) =>
		direction &&
		direction === 'prev' &&
		css`
			svg {
				transform: scaleX(-1);
			}
		`};
`;

const Cursor = ({ cursorRefresh, handleCursorRefresh }) => {
	const [isNextButton, setIsNextButton] = useState(false);
	const [isPrevButton, setIsPrevButton] = useState(false);
	const [isViewButton, setIsViewButton] = useState(false);
	const [isCloseButton, setIsCloseButton] = useState(false);
	const [isHoveringLink, setIsHoveringLink] = useState(false);
	const [isDragging, setIsDragging] = useState(false);
	const [isMouseDownOnCarousel, setIsMouseDownOnCarousel] = useState(false);

	const [cursorActive, setCursorActive] = useState(false);

	const [isOnDevice, setIsOnDevice] = useState('');
	const position = useMousePosition();
	const router = useRouter();

	let mouseXPosition = position.x;
	let mouseYPosition = position.y;

	const variantsWrapper = {
		visible: {
			x: mouseXPosition,
			y: mouseYPosition,
			transition: {
				type: 'spring',
				mass: 0.1,
				stiffness: 800,
				damping: 20,
				ease: 'linear'
			}
		}
	};

	const variantsIcon = {
		visible: {
			transform: 'scale(1)'
		},
		hidden: {
			transform: 'scale(0)'
		}
	};

	const clearCursor = () => {
		setIsHoveringLink(false);
		setIsViewButton(false);
		setIsDragging(false);
		setIsCloseButton(false);
		setIsPrevButton(false);
		setIsNextButton(false);
		setIsMouseDownOnCarousel(false);
		setIsOnDevice(false);
	};

	const { backgroundColor, cursorState } = useUI() || 'white';

	useEffect(() => {
		clearCursor();

		const viewStyleLinks = document.querySelectorAll('.cursor-link--view');
		const closeStyleLinks = document.querySelectorAll(
			'.cursor-link--close'
		);

		// These are now handled by the delegate on the body. Once that approach is confirmed good
		// these should be safe to delete.
		const carouselNextStyleLinks = document.querySelectorAll(
			'.cursor-link--carousel-next'
		);
		const carouselPrevStyleLinks = document.querySelectorAll(
			'.cursor-link--carousel-prev'
		);

		viewStyleLinks.forEach((link) => {
			link.addEventListener('mouseenter', throttle(() => {
				setIsViewButton(true);
			}, 15));
			link.addEventListener('mouseleave', throttle(() => {
				setIsViewButton(false);
			}, 15));
		});

		closeStyleLinks.forEach((link) => {
			link.addEventListener('mouseenter', throttle(() => {
				setIsCloseButton(true);
			}, 15));
			link.addEventListener('mouseleave', throttle(() => {
				setIsCloseButton(false);
			}, 15));

			link.addEventListener('click', () => {
				handleCursorRefresh();
			});
		});

		// checking if on a device
		const ua = navigator.userAgent;
		if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
			setIsOnDevice(true);
		} else if (
			/Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
				ua
			)
		) {
			setIsOnDevice(true);
		}

		return () => {
			clearCursor();
			// todo: clear listeners to avoid duplicates??
		};
	}, [cursorRefresh, cursorState]);

	useEffect(() => {
		document.body.addEventListener('mouseenter', (e) => {

			if (e.target.classList.contains('cursor-link--carousel-next')) {
				const link = e.target;
				if (link.hasAttribute('disabled')) return;

				setIsNextButton(true);
				link.addEventListener('mouseleave', (e) => setIsNextButton(false), {once: true});
			}
			else if (e.target.classList.contains('cursor-link--carousel-prev')) {
				const link = e.target;
				if (link.hasAttribute('disabled')) return;

				setIsPrevButton(true);
				link.addEventListener('mouseleave', (e) => setIsPrevButton(false), {once: true});
			}

			else { // we know it's not a prev or next button
				setIsPrevButton(false);
				setIsNextButton(false);
			}
		}, {capture: true});
	}, [])

	// reset cursor on page change
	useEffect(() => {
		clearCursor();
	}, [router.pathname, router.asPath, router.query.slug]);

	useEffect(() => {
		handleCursorRefresh();

		return () => {
			handleCursorRefresh();
		};
	}, []);

	return (
		<CursorWrapper isHoveringLink={isHoveringLink} isOnDevice={isOnDevice}>
			<CursorRing
				isHoveringLink={isHoveringLink}
				isMouseDownOnCarousel={isMouseDownOnCarousel}
				isDragging={isDragging}
				isActive={
					isNextButton ||
					isPrevButton ||
					isViewButton ||
					isCloseButton ||
					isHoveringLink ||
					isDragging ||
					isMouseDownOnCarousel
				}
				variants={variantsWrapper}
				animate="visible"
				backgroundColor={backgroundColor}
			>
				<AnimatePresence exitBeforeEnter initial={true}>
					{isViewButton && (
						<CursorInner
							key="view"
							initial={'hidden'}
							exit={'hidden'}
							animate={isViewButton ? 'visible' : 'hidden'}
							variants={variantsIcon}
						>
							<Open />
						</CursorInner>
					)}
					{isCloseButton && (
						<CursorInner
							key="close"
							initial={'hidden'}
							exit={'hidden'}
							animate={isCloseButton ? 'visible' : 'hidden'}
							variants={variantsIcon}
						>
							<Close />
						</CursorInner>
					)}
					{isNextButton && (
						<CursorInner
							key="next"
							initial={'hidden'}
							exit={'hidden'}
							animate={isNextButton ? 'visible' : 'hidden'}
							variants={variantsIcon}
						>
							<Rarr />
						</CursorInner>
					)}
					{isPrevButton && (
						<CursorInner
							key="prev"
							initial={'hidden'}
							exit={'hidden'}
							animate={isPrevButton ? 'visible' : 'hidden'}
							variants={variantsIcon}
							direction={'prev'}
						>
							<Rarr />
						</CursorInner>
					)}
				</AnimatePresence>
			</CursorRing>
		</CursorWrapper>
	);
};

export default Cursor;
