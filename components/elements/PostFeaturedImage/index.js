import styled from 'styled-components';
import ImageBg from '../../common/Thumbnail/index';
import MobileModuleHeading from '../../common/MobileModuleHeading';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useState, useRef } from 'react';
import SwiperCore, { Navigation, A11y } from 'swiper';

const Wrapper = styled.div``;

const Gallery = styled.div`
	position: relative;
`;

const ImageWrapper = styled.div`
	div::before {
		content: '';
		width: 100%;
		display: block;
		padding-top: 51.75%;

		@media ${({ theme }) => theme.breakpoints.desktop} {
			padding-top: 60%;
		}

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 51.75%;
		}
	}

	img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;
`;

SwiperCore.use([Navigation, A11y]);

const PostFeaturedImage = ({ gallery, title }) => {
	const prevRef = useRef();
	const nextRef = useRef();

	return gallery && gallery.length ? (
		<Wrapper>
			<MobileModuleHeading title={title} />
			<Gallery>
				{gallery && gallery?.length > 1 && (
					<>
						<NavElement
							data-cursor="true"
							className="cursor-link--carousel-prev"
							direction="prev"
							ref={prevRef}
						/>
						<NavElement
							data-cursor="true"
							className="cursor-link--carousel-next"
							direction="next"
							ref={nextRef}
						/>
					</>
				)}
				<Swiper
					loop={gallery?.length > 1 ? true : false}
					allowTouchMove={gallery?.length > 1 ? true : false}
					preventInteractionOnTransition={true}
					speed={400}
					centeredSlides={false}
					slidesPerView={1}
					spaceBetween={0}
					onInit={(swiper) => {
						swiper.params.navigation.prevEl = prevRef.current;
						swiper.params.navigation.nextEl = nextRef.current;

						swiper.navigation.init();
						swiper.navigation.update();
					}}
				>
					{gallery.map(({ image }, index) => (
						<SwiperSlide key={index}>
							<ImageWrapper>
								<ImageBg
									image={image?.sourceUrl}
									thumbnail={image?.thumbSourceUrl}
									alt={image?.alt}
								/>
							</ImageWrapper>
						</SwiperSlide>
					))}
				</Swiper>
			</Gallery>
		</Wrapper>
	) : null;
};

export default PostFeaturedImage;
