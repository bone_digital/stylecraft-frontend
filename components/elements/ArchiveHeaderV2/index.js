import styled from 'styled-components';
import SortBy from '../../common/SortBy';
import { useIsLaptop } from '../../../hooks/useMediaQuery';
import { motion } from 'framer-motion';
import ArchiveActiveFiltersList from '../ArchiveActiveFiltersList';

const Header = styled.div`
	padding-top: 160px;
	position: relative;
	z-index: 2;
	margin-bottom: 48px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 24px;
	}
`;

const Title = styled.h1`
	display: block;
`;

const TitleColumn = styled(motion.div)`
	width: 33.33%;
	display: flex;
	align-items: flex-end;
	padding: 0 18px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100% !important;
	}
`;

const Row = styled.div`
	display: flex;
	flex-wrap: nowrap;
	margin: 0 -18px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		flex-wrap: wrap;
	}
`;

const SortByColumn = styled.div`
	padding: 0 18px;
	display: flex;
	align-items: flex-end;

	> div {
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;


const ArchiveHeaderV2 = ({
	title,
	filters,
	onDismissFilter,
	sortBy,
	sortOptions,
	menuActive,
	setSortBy
}) => {
	const menuVariant = useIsLaptop
		? {
				open: {
					width: '50%'
				},
				close: {
					width: '33.33%'
				}
		  }
		: {
				open: {
					width: '100%'
				},
				close: {
					width: '100%'
				}
		  };

	return (
		<Header>
			<Row>
				<TitleColumn
					key={'archive-header-title'}
					variants={menuVariant}
					animate={menuActive ? 'open' : 'close'}
					initial={false}
				>
					<Title dangerouslySetInnerHTML={{ __html: title }} />
				</TitleColumn>

				<ArchiveActiveFiltersList
					respondTo={'desktop'}
					filters={filters}
					menuActive={menuActive}
					onDismissFilter={onDismissFilter}
				/>

				<SortByColumn>
					<SortBy
						options={sortOptions}
						setSortPostsBy={setSortBy}
					/>
				</SortByColumn>
			</Row>
		</Header>
	);
};


export default ArchiveHeaderV2;
