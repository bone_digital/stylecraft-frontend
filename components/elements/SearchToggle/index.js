import styled from 'styled-components';
import { swatch, ease, breakpoint } from '../../../styles/theme';
import SearchIcon from '../../../public/icons/search.svg';
import RarrIcon from '../../../public/icons/rarr.svg';
import { motion, AnimatePresence } from 'framer-motion';
import { Formik, Form, Field, withFormik } from "formik";
import { useEffect, useRef, useState } from "react";
import { useRouter } from 'next/router';

const SearchWrapper = styled.div`
	position: relative;
	z-index: 10;
	display: ${({ type }) => (type === 'mobile' ? 'none' : 'block')};

	@media ${breakpoint('laptop')} {
		display: ${({ type }) => (type === 'mobile' ? 'block' : 'none')};
	}

	@media ${breakpoint('tablet')} {
		position: unset;
	}
`;

const Wrapper = styled.div`
	position: static;
	z-index: 10;
`;

const FormWrap = styled(Form)`
	display: flex;
	overflow: hidden;
	white-space: nowrap;
	align-items: center;
	width: 100%;
	z-index: 0;
	justify-content: space-between;
	white-space: nowrap;
	pointer-events: all;
`;

const ErrorMessage = styled.div``;

const Toggle = styled.button`
	width: 44px;
	height: 44px;
	border-radius: 999px;
	display: flex;
	align-items: center;
	justify-content: center;
	margin-right: 30px;
	background: ${swatch('grey')};
	z-index: 6;
	position: relative;

	@media ${breakpoint('tablet')} {
		margin-right: 20px;
	}

	@media ${breakpoint('mobile')} {
		margin-right: 16px;
	}

	&:hover {
		svg {
			transform: scale(0.9);
		}
	}

	svg {
		width: 16px;
		transition: ${ease('transform')};
	}
`;

const InputWrap = styled.div`
	position: absolute;
	left: -50px;
	top: 50%;
	transform: translateY(-50%);
	height: 44px;
	z-index: 0;
	display: flex;
	margin-left: 54px;
	width: 448px;
	pointer-events: none;

	@media ${breakpoint('tablet')} {
		left: 40px;
		max-width: calc(100% - 54px - 40px);
		margin-left: 38px;
	}
`;

const InputWrapInner = styled.div`
	position: absolute;
	left: 0;
	top: 0;
	height: 100%;
	display: flex;
	overflow: hidden;
	white-space: nowrap;
	align-items: center;
	width: 100%;
	z-index: 0;
	background: ${swatch('grey')};
	border-radius: 999px;
	justify-content: space-between;
	white-space: nowrap;
	pointer-events: all;
`;

const MotionInputInner = motion(InputWrapInner);

const SearchInput = styled(Field)`
	width: 100%;
	padding-left: 50px;

	&::placeholder {
		opacity: 0.4;
	}
`;

const SubmitButton = styled.button`
	display: flex;
	align-items: center;
	flex-shrink: 0;
	padding: 0 24px;

	&:hover {
		svg {
			transform: translateX(2px);
		}
	}

	svg {
		width: 4px;
		transition: ${ease('transform')};
	}
`;

const SubmitInner = styled.div`
	&:first-child {
		margin-right: 18px;
	}
`;

const SearchToggle = ({ type }) => {
	const router = useRouter();
	const [searchActive, setSearchActive] = useState(false);
	const searchInputRef = useRef(null);

	const handleClick = () => {
		setSearchActive(!searchActive);
	};

	useEffect(() => {
		if(true === searchActive)
		{
			//alert(78);
			searchInputRef?.current?.focus();
		}
	}, [searchActive])

	/**
	 *
	 * @param {string} searchTerm
	 * @param {boolean} setSubmitting
	 */
	const handleSearch = (searchTerm, setSubmitting) => {
		setSearchActive(false);
		const params = new URLSearchParams({
			search: searchTerm
		});

		const searchUrl = `/search/?${params.toString()}`;

		setSubmitting(false);

		const isShallow = router.pathname.indexOf('search') > 0 ? true : false;

		router.push(searchUrl, undefined, { shallow: isShallow });
	};

	return (
		<SearchWrapper type={type}>
			<Wrapper>
				<Toggle onClick={handleClick}>
					<SearchIcon />
				</Toggle>

				<InputWrap>
					<AnimatePresence initial={false} exitBeforeEnter>
						{searchActive && (
							<MotionInputInner
								key={'active'}
								duration={'0.6'}
								initial={{
									opacity: 0,
									maxWidth: '0%'
								}}
								animate={{
									opacity: 1,
									maxWidth: '100%'
								}}
								exit={{
									opacity: 0,
									maxWidth: '0%'
								}}
								transition={{ duration: 0.3 }}
							>
								<Formik
									initialValues={{
										searchTerm: ''
									}}
									onSubmit={(values, actions) => {
										const { setSubmitting } = actions;
										const { searchTerm } = values;

										setSubmitting(true);
										handleSearch(searchTerm, setSubmitting);
									}}
								>
									{({
										values,
										errors,
										touched,
										isSubmitting
									}) => (
										<FormWrap>
											<SearchInput
												innerRef={searchInputRef}
												type="text"
												name="searchTerm"
												required={false}
												disabled={isSubmitting}
												error={
													errors.searchTerm &&
													touched.searchTerm
												}
												placeholder="Enter search"
											/>

											<SubmitButton type="submit">
												<SubmitInner>Search</SubmitInner>
												<SubmitInner>
													<RarrIcon />
												</SubmitInner>
											</SubmitButton>
										</FormWrap>
									)}
								</Formik>
							</MotionInputInner>
						)}
					</AnimatePresence>
				</InputWrap>
			</Wrapper>
		</SearchWrapper>
	);
};

export default SearchToggle;
