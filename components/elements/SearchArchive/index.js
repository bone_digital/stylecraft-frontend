import styled, { css } from 'styled-components';
import { Container } from '../../common/Grid';
import PostCard from '../../common/PostCard';
import BrandCard from '../../common/BrandCard';
import { AnimatePresence, motion } from 'framer-motion';
import { ease } from '../../../styles/theme';

const Row = styled(Container)`
	align-items: ${({ header }) => (header ? 'flex-end' : 'flex-start')};
	row-gap: 145px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		row-gap: 80px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		row-gap: 54px;
	}
`;

const CardWrapper = styled(motion.div)`
	flex-shrink: 0;
	grid-column: span 4;
	transition: ${ease('all')};

	&:nth-child(1) {
		grid-column: span 8;
	}

	&:nth-child(2) {
		align-self: flex-end;

		h3 {
			display: -webkit-box;
			-webkit-line-clamp: 1;
			-webkit-box-orient: vertical;
			overflow: hidden;
		}
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: span 6;

		&:nth-child(1) {
			grid-column: -1 / 1;
		}

		&:nth-child(2) {
			align-self: start;

			h3 {
				display: inline-block;
				-webkit-line-clamp: initial;
				overflow: initial;
			}
		}
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
	}

	${({ isLoading }) =>
		isLoading &&
		css`
			& > div {
				transition: ${ease('all')};
				opacity: 0.5;
			}
		`};
`;

const SearchArchive = ({ posts, loading, sortBy }) => {
	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<AnimatePresence>
			{posts && posts.length ? (
				<Row key={`search-body-${sortBy?.slug}`}>
					{posts.map((post, index) => {
						const type = post?.type;

						return (
							<CardWrapper
								key={`search-${post?.id}`}
								isLoading={loading ? true : false}
								variants={variants}
								initial={'hidden'}
								animate={'visible'}
								exit={'hidden'}
							>
								{type === 'Brand' ? (
									<BrandCard post={post} type={type} />
								) : (
									<PostCard post={post} type={type} />
								)}
							</CardWrapper>
						);
					})}
				</Row>
			) : null}
		</AnimatePresence>
	);
};

export default SearchArchive;
