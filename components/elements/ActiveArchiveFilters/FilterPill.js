import styled from 'styled-components';
import { motion } from 'framer-motion';
import Close from '../../../public/icons/close.svg';

const Button = styled.button`
	display: flex;
	align-items: center;
	min-width: 130px;
	padding: 0 8px 8px 0;
	border-bottom: 1px solid #929497;
	justify-content: space-between;
	white-space: nowrap;

	svg {
		width: 7px;
		height: 7px;
	}
`;

const Label = styled.label`
	padding-right: 14px;
	pointer-events: none;
`;

const FilterItem = styled(motion.li)`
	list-style: none;
	padding-left: 0;
	margin: 36px 36px 0 0;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		margin: 16px 16px 0 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin: 0 16px 16px 0;
	}
`;

const FilterPill = ({ filter, onClick }) => {
	const variants = {
		hidden: { opacity: 0 },
		visible: { opacity: 1 }
	};

	return filter ? (
		<FilterItem
			onClick={() => onClick(filter)}
			key={`pill-${filter?.slug}`}
			variants={variants}
			animate={'visible'}
			initial={'hidden'}
			exit={'hidden'}
		>
			<Button>
				<Label
					dangerouslySetInnerHTML={{ __html: filter?.label || filter?.name }}
				></Label>
				<Close />
			</Button>
		</FilterItem>
	) : null;
};

export default FilterPill;
