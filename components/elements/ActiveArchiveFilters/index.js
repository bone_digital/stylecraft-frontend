import styled from 'styled-components';
import { ease } from '../../../styles/theme';
import FilterPill from './FilterPill';

const Wrapper = styled.div`
	flex: 1;
	padding: 0 18px;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'flex' : 'none') : 'flex'};
	align-items: flex-end;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
		flex: none;
		margin-top: 16px;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'flex' : 'none') : 'flex'};
	}
`;

const FilterList = styled.ul`
	display: flex;
	width: 100%;
	align-items: flex-end;
	flex-wrap: wrap;
	transition: ${ease('all')};
	opacity: ${({ menuActive }) => (menuActive ? 0 : 1)};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 16px;
	}
`;

const ActiveArchiveFilters = ({ filters, onClick, menuActive, respondTo }) => {
	return (
		<Wrapper respondTo={respondTo}>
			{filters && filters.length > 0 && (
				<FilterList menuActive={menuActive}>
					{filters.map((filter, index) => (
						<FilterPill
							onClick={onClick}
							filter={filter}
							key={`filter-pill-${index}`}
						></FilterPill>
					))}
				</FilterList>
			)}
		</Wrapper>
	);
};

export default ActiveArchiveFilters;
