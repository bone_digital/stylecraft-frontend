import styled from 'styled-components';
import { useInView } from 'react-intersection-observer';
import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import InnerWrapper from '../../common/InnerWrapper';
import Grid from '../../common/Grid';
import { ease } from '../../../styles/theme';

const Heading = styled(motion.h1)`
	text-indent: -0.05em;
	grid-column: ${({ active }) => (active ? 'span 8' : '5 / span 8')};
	margin-bottom: -0.4em;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const Wrapper = styled.div`
	position: sticky;
	top: -1px;
	margin-top: ${({ post }) => (post ? '51px' : '150px')};
	padding-top: 51px;
	z-index: 20;
	display: block;
	opacity: ${({ sectionActive }) => (sectionActive ? 1 : 0)};
	transform: ${({ sectionActive }) =>
		sectionActive ? 'translate3d(0, 0, 0)' : 'translate3d(0, 30px, 0)'};
	transition: ${ease('all', 'slow')};
`;

const TranslatingHeading = ({ title, post }) => {
	const [headingFixed, setHeadingFixed] = useState(false);
	const [sectionLoaded, setSectionLoaded] = useState(false);

	const { ref, inView, entry } = useInView({
		triggerOnce: false,
		rootMargin: '0px 0px 50% 0px',
		threshold: 1
	});

	useEffect(() => {
		if (entry != null) {
			if (entry?.intersectionRatio < 1) {
				setHeadingFixed(true);
			} else {
				setHeadingFixed(false);
			}
		}

		if (inView) {
			setSectionLoaded(true);
		}
	}, [inView]);

	useEffect(() => {
		async function loadPolyfills() {
			if (
				typeof window !== 'undefined' &&
				typeof window.IntersectionObserver === 'undefined'
			) {
				await import('intersection-observer');
			}
		}

		loadPolyfills();
	}, []);

	return (
		<Wrapper
			active={headingFixed}
			ref={ref}
			sectionActive={sectionLoaded}
			post={post}
		>
			<InnerWrapper>
				<Grid>
					<Heading active={headingFixed} layout post={post}>
						{title}
					</Heading>
				</Grid>
			</InnerWrapper>
		</Wrapper>
	);
};

export default TranslatingHeading;
