import styled from 'styled-components';
import { Content } from '../../common/WYSWYG';
import LinkButton from '../../common/LinkButton';

const Wrapper = styled.div`
	max-width: 400px;

	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.desktop} {
		margin-bottom: 80px;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: ${({ flipMobile }) => (flipMobile ? 0 : '82px')};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: ${({ flipMobile }) => (flipMobile ? 0 : '32px')};
		max-width: 100%;

		> a {
			margin-top: 20px;
		}
	}
`;

const TextColumn = styled(Content)``;

const IntroCopy = ({ copy, flipMobile, linkButton }) => {
	return copy ? (
		<Wrapper flipMobile={flipMobile}>
			<TextColumn dangerouslySetInnerHTML={{ __html: copy }} />
			{linkButton && linkButton?.linkInner && (
				<LinkButton
					prefix={linkButton?.prefix}
					href={linkButton?.linkInner?.url}
					title={linkButton?.linkInner?.title}
					target={linkButton?.linkInner?.target}
				/>
			)}
		</Wrapper>
	) : null;
};

export default IntroCopy;
