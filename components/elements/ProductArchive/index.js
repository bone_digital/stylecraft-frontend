import styled, { css } from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';
import { ease } from '../../../styles/theme';
import ProductFilters from '../ProductFilters';
import PostCard from '../../common/PostCard';
import NoResultsCard from '../../common/NoResultsCard';
import LoadingCard from '../../common/LoadingCard';
import ActiveArchiveFilters from '../ActiveArchiveFilters';
import React from 'react';

const Wrapper = styled.div``;

const Body = styled(motion.div)`
	margin-bottom: -64px;
	position: relative;
	z-index: 2;
	min-height: 200px;
	display: ${({ respondTo }) => (respondTo === 'desktop' ? 'block' : 'none')};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: ${({ respondTo }) =>
			respondTo !== 'desktop' ? 'block' : 'none'};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: -80px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		margin-bottom: -54px;
	}
`;

const Row = styled(motion.div)`
	display: flex;
	flex-wrap: nowrap;
	margin: 0 -18px;

	&:nth-child(2) {
		flex-wrap: wrap;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		flex-wrap: wrap;
	}
`;

const CardWrapper = styled(motion.div)`
	flex-shrink: 0;
	width: 33.33%;
	padding: 0 18px;
	margin-bottom: 64px;
	transition: ${ease('all')};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 50%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		width: 100%;
		margin-bottom: 80px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		margin-bottom: 54px;
	}

	${({ isLoading }) =>
		isLoading &&
		css`
			& > div {
				transition: ${ease('all')};
				opacity: 0.5;
			}
		`};
`;

const ProductArchive = ({
	type = 'product',
	products,
	filters,
	handleDesignerFilterAdd,
	handleBrandFilterAdd,
	handleProductFilterAdd,
	handleLeadtimeFilterAdd,
	handleEnvironmentFilterAdd,
	handlePriceFilterAdd,
	handleRangeFilterAdd,
	handleAustralianMadeAdd,
	handleNewProductsAdd,
	setMenuActive,
	menuActive,
	activeFilters,
	loading,
	handleRemoveFilter,
	options,
	setSortBy,
	setUserInteracted,
	sortBy,
	filterByType,
	setFilterByType
}) => {
	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	const wrapperKey = products?.length
		? `archive-body-wrapper-${products?.length}`
		: `archive-body-wrapper`;

	return (
		<Wrapper key={wrapperKey}>
			<Body key={'body-desktop'} respondTo={'desktop'}>
				{products.map((row, rowIndex) => {
					const sortBySlug =
						sortBy?.slug && sortBy?.slug !== null
							? sortBy?.slug
							: rowIndex;

					let rowKey =
						rowIndex === 0
							? `archive-row-${rowIndex}`
							: `archive-row-${sortBySlug}`;

					return (
						<Row
							key={rowKey}
							variants={variants}
							animate={'visible'}
							initial={'hidden'}
							exit={'hidden'}
						>
							{rowIndex === 0 && (
								<ProductFilters
									type={type}
									filters={filters}
									setUserInteracted={setUserInteracted}
									handleBrandFilterAdd={handleBrandFilterAdd}
									handleProductFilterAdd={
										handleProductFilterAdd
									}
									handleLeadtimeFilterAdd={
										handleLeadtimeFilterAdd
									}
									handleEnvironmentFilterAdd={
										handleEnvironmentFilterAdd
									}
									handlePriceFilterAdd={handlePriceFilterAdd}
									handleRangeFilterAdd={handleRangeFilterAdd}
									handleAustralianMadeAdd={
										handleAustralianMadeAdd
									}
									handleNewProductsAdd={handleNewProductsAdd}
									handleDesignerFilterAdd={
										handleDesignerFilterAdd
									}
									handleRemoveFilter={handleRemoveFilter}
									setMenuActive={setMenuActive}
									menuActive={menuActive}
									activeFilters={activeFilters}
									key={'filter-products-desktop'}
									results={products}
									filterByType={filterByType}
									setFilterByType={setFilterByType}
								/>
							)}

							<AnimatePresence>
								{row.length &&
									row.map((product, postIndex) => {
										const type = product?.type;

										let productKey = `${rowIndex}-loading-${postIndex}`;

										if (product === null) {
											productKey = `product-${postIndex}-${rowIndex}`;
										} else if (
											product &&
											product !== null
										) {
											productKey = `${postIndex}-${product?.id}-${rowKey}`;
										}

										return (
											<CardWrapper
												menuActive={menuActive}
												key={productKey}
												isLoading={
													loading ? true : false
												}
												variants={variants}
												initial={'hidden'}
												animate={'visible'}
												exit={'hidden'}
											>
												{product && product !== null ? (
													<PostCard
														ratio={'100%'}
														ratioMobile={'71.42%'}
														post={product}
														type={type}
													/>
												) : product == null ? (
													<NoResultsCard />
												) : !product ? (
													<LoadingCard />
												) : null}
											</CardWrapper>
										);
									})}
							</AnimatePresence>
						</Row>
					);
				})}
			</Body>

			<Body key={'body-mobile'} respondTo={'mobile'}>
				<Row key="row-mobile-wrap">
					{products.map((row, rowIndex) => (
						<React.Fragment key={`mobile-row-${rowIndex}`}>
							{rowIndex === 0 && (
								<>
									<ProductFilters
										type={type}
										filters={filters}
										setUserInteracted={setUserInteracted}
										handleBrandFilterAdd={
											handleBrandFilterAdd
										}
										handleProductFilterAdd={
											handleProductFilterAdd
										}
										handleLeadtimeFilterAdd={
											handleLeadtimeFilterAdd
										}
										handleEnvironmentFilterAdd={
											handleEnvironmentFilterAdd
										}
										handlePriceFilterAdd={
											handlePriceFilterAdd
										}
										handleRangeFilterAdd={
											handleRangeFilterAdd
										}
										handleAustralianMadeAdd={
											handleAustralianMadeAdd
										}
										handleNewProductsAdd={
											handleNewProductsAdd
										}
										handleDesignerFilterAdd={
											handleDesignerFilterAdd
										}
										handleRemoveFilter={handleRemoveFilter}
										setMenuActive={setMenuActive}
										menuActive={menuActive}
										activeFilters={activeFilters}
										key={'filter-products-mobile'}
										options={options}
										setSortBy={setSortBy}
										results={products}
										filterByType={filterByType}
										setFilterByType={setFilterByType}
									/>
									<ActiveArchiveFilters
										filters={activeFilters}
										menuActive={menuActive}
										onClick={handleRemoveFilter}
										respondTo={'mobile'}
										key={'mobile-active-filters'}
									/>
								</>
							)}

							{row.length &&
								row.map((product, postIndex) => {
									const type = product?.type;

									const sortBySlug =
										sortBy?.slug && sortBy?.slug !== null
											? sortBy?.slug
											: '';

									let productKey = `mobile-${rowIndex}-loading-${postIndex}`;

									if (product === null) {
										productKey = `mobile-product-${postIndex}-${rowIndex}`;
									} else if (product && product !== null) {
										productKey = `mobile-${postIndex}-${product?.id}-${sortBySlug}`;
									}

									return (
										<CardWrapper
											menuActive={menuActive}
											key={productKey}
											isLoading={loading ? true : false}
											variants={variants}
											initial={'hidden'}
											animate={'visible'}
											exit={'hidden'}
										>
											{product !== null ? (
												<PostCard
													ratio={'100%'}
													ratioMobile={'71.42%'}
													post={product}
													type={type}
												/>
											) : (
												<NoResultsCard />
											)}
										</CardWrapper>
									);
								})}
						</React.Fragment>
					))}
				</Row>
			</Body>
		</Wrapper>
	);
};

export default ProductArchive;
