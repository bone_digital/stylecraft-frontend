import { motion, AnimatePresence } from 'framer-motion';
import { breakpoint, ease } from '../../../styles/theme';
import { useState, useEffect } from 'react';
import styled from 'styled-components';
import WishlistItem from './WishlistItem';
import Footer from './Footer';
import { useSelector } from 'react-redux';

const Wrapper = styled.div`
	margin-bottom: 100px;
`;

const NavList = styled.ul`
	font-size: ${({ theme }) => theme.type.nav[0]};
	line-height: ${({ theme }) => theme.type.nav[1]};
	opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
	transition: ${ease('all')};

	@media ${breakpoint('xlDesktop')} {
		font-size: ${({ theme }) => theme.typeMobile.nav[0]};
	}

	@media ${breakpoint('tablet')} {
		line-height: ${({ theme }) => theme.typeMobile.nav[1]};
	}
`;

const NavItem = styled(motion.div)`
	transition: ${ease('all')};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};

	&:not(:last-child) {
		margin-bottom: 60px;
	}
`;

const List = styled.div`
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	padding-bottom: 15px;
`;

const variants = {
	hidden: {
		opacity: 0,
		transition: {
			duration: 0.2
		}
	},
	visible: {
		opacity: 1,
		transition: {
			duration: 0.2
		}
	}
};

const Wishlist = ({ userId, setWishlist }) => {
	const [disabled, setDisabled] = useState(false);
	const wishlistItems = useSelector(({ cart }) => cart.wishlistItems);
	const wishlistItemCount = useSelector(({ cart }) => cart.wishlistItemCount);

	
	if (!wishlistItems || !wishlistItems?.length) {
		return null;
	}
	
	return (
		<Wrapper count={wishlistItemCount}>
			<List>
				{wishlistItems !== null && (
					<NavList disabled={disabled}>
						<AnimatePresence>
							{wishlistItems && !!wishlistItems?.length &&
								wishlistItems?.map((wishlistItem) => {
									return (
										<NavItem
											key={`wishlist-${wishlistItem?.id}`}
											variants={variants}
											initial={'hidden'}
											animate={'visible'}
											exit={'hidden'}
										>
											<WishlistItem 
												wishlistItem={wishlistItem}
												setWishlist={setWishlist}
												userId={userId}
												/>
										</NavItem>
									);
								})}
						</AnimatePresence>
					</NavList>
				)}
			</List>
			<Footer setDisabled={setDisabled} cartItems={wishlistItems} />
		</Wrapper>
	);
};

export default Wishlist;
