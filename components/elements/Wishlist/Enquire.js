import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import TextAreaField from '../../common/TextAreaField';
import * as Yup from 'yup';
import styled from 'styled-components';
import Button from '../../common/Button';
import { ease } from '../../../styles/theme';
import GravityForm from '../../common/GravityForm';
import { useSelector } from "react-redux";

const Wrapper = styled.div`
	width: 100%;

	&&& {
		margin-right: 0;
	}

	label {
		height: auto;
		visibility: visible;
		opacity: 1;
		margin-bottom: 12px;
	}

	&&& textarea {
		height: 10em;
		max-height: 10em;
		border: 1px solid;
		padding: 1em;
	}

	form * {
		grid-column: -1 / 1;
	}
`;

const Column = styled.div`
	> *:not(:last-child) {
		margin-right: 24px;
	}
`;

const Row = styled.div`
	display: flex;
	justify-content: flex-end;
	padding-top: 30px;
`;

const Enquire = ({ setActive, active, cartItems }) => {
	const [isSubmitting, setSubmitting] = useState(false);
	let wishlistItems = '';
	cartItems?.forEach((item) => {
		const productId = item?.id;
		const productTitle = item?.name;
		const qty = item?.quantity;
		wishlistItems += `${qty}x ${productTitle} | ${productId}`;
		wishlistItems += '\n';
	});

	const handleClose = () => {
		setActive(false);
	};

	return (
		<Wrapper>
			<GravityForm wishlistItems={wishlistItems} formID={3} buttonType={'button'}>
				<Button
					type={'button'}
					color={'greyDark'}
					title={'Close'}
					onClick={handleClose}
				/>
			</GravityForm>
		</Wrapper>
	);
};

export default Enquire;
