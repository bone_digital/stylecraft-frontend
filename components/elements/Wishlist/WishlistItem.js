import { useSelector, useDispatch } from 'react-redux';
import { useState, useEffect } from 'react';
import { cartActions } from '../../../store/cartSlice';
import WishlistCartCard from '../../common/WishlistCartCard';
import React from 'react';

const WishlistItem = ({ wishlistItem, userId }) => {
	const [updating, setUpdating] = useState(false);
	const dispatch = useDispatch();

	const {
		featuredImage,
		name, 
		product,
		quantity,
		link,
		id
	} = wishlistItem;

	const designer = product?.designer;

	const increaseQuantity = () => {
		if (userId && id) {
			setUpdating(true);
			fetch('/api/update-wishlist', {
				method: 'POST',
				body: JSON.stringify({
					userId,
					add: id
				})
			})
				.then((res) => res.json())
				.then((json) => {
					if (json.code === '200') {
						localStorage.setItem('sc-wishlist', JSON.stringify(json?.wishlist));
						localStorage.setItem('sc-wishlist-count', json?.wishlist?.length || 0);
						dispatch(
							cartActions.updateWishlist({
								items: json?.wishlist,
								count: json?.wishlist?.length || 0,
							})
						);
					} else {
						console.log(json?.msg)
					}
					setUpdating(false);
				}).catch((err) => {
					console.log(err);
					setUpdating(false);
				});
		}
	}

	const decreaseQuantity = () => {
		if (userId && id) {
			setUpdating(true);
			fetch('/api/update-wishlist', {
				method: 'POST',
				body: JSON.stringify({
					userId,
					remove: id
				})
			})
				.then((res) => res.json())
				.then((json) => {
					if (json.code === '200') {
						localStorage.setItem('sc-wishlist', JSON.stringify(json?.wishlist));
						localStorage.setItem('sc-wishlist-count', json?.wishlist?.length || 0);
						dispatch(
							cartActions.updateWishlist({
								items: json?.wishlist,
								count: json?.wishlist?.length || 0,
							})
						);
					} else {
						console.log(json?.msg)
					}
					setUpdating(false);
				}).catch((err) => {
					console.log(err);
					setUpdating(false);
				});
		}
	}

	return (
		<WishlistCartCard
			isWishlist={true}
			updating={updating}
			image={featuredImage?.node?.sourceUrl || ''}
			featuredImage={featuredImage}
			name={name}
			link={link}
			designer={designer}
			quantity={quantity}
			decreaseQuantity={decreaseQuantity}
			increaseQuantity={increaseQuantity}

		/>
	);
};

export default WishlistItem;
