import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ease } from '../../../styles/theme';
import styled, { css, keyframes } from 'styled-components';
import LinkButton from '../../common/LinkButton';
import Button from '../../common/Button';
import { cartActions } from '../../../store/cartSlice';
import Enquire from './Enquire';
import { motion, AnimatePresence } from 'framer-motion';

const Wrapper = styled(motion.div)`
	padding: ${({ form }) => (form ? '15px 0' : '15px 0 15px 210px')};
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: center;

	> *:first-child {
		margin-right: 30px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-left: 0;
	}
`;

const Column = styled.div`
	> *:not(:last-child) {
		margin-right: 24px;
	}
`;

const Footer = ({ setDisabled, disabled, cartItems }) => {
	const [active, setActive] = useState(false);

	const updateWishlist = () => {
		setDisabled(true);

		setTimeout(() => {
			setDisabled(false);
		}, 600);
	};

	const handleEnquire = () => {
		setActive(true);
	};

	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<AnimatePresence exitBeforeEnter>
			{!active ? (
				<Wrapper
					key={'enquire-close'}
					variants={variants}
					initial={'hidden'}
					animate={'visible'}
					exit={'hidden'}
				>
					<Column>
						<LinkButton
							type={'full'}
							title={'Update Wishlist'}
							uppercase={false}
							onClick={updateWishlist}
						/>
					</Column>
					<Column>
						<Button
							color={'brand'}
							title={'Enquire'}
							disabled={disabled}
							onClick={handleEnquire}
						/>
					</Column>
				</Wrapper>
			) : (
				<Wrapper
					key={'enquire-open'}
					variants={variants}
					form={'true'}
					initial={'hidden'}
					animate={'visible'}
					exit={'hidden'}
				>
					<Enquire setActive={setActive} active={active} cartItems={cartItems} />
				</Wrapper>
			)}
		</AnimatePresence>
	);
};

export default Footer;
