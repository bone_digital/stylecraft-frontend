import React from 'react';
import styled from 'styled-components';
import Thumbnail from '../../../common/Thumbnail';
import { ease } from '../../../../styles/theme';

const Card = styled.div`
	max-width: 740px;
	display: flex;
	align-items: flex-start;
	font-size: ${({ theme }) => theme.type.p[0]};
	line-height: 28px;
	transition: ${ease('all')};
	flex-wrap: wrap;
	padding-bottom: 32px;

	&:last-child {
		padding-bottom: 0;
	}
`;

const Title = styled.h3`
	margin-bottom: 8px;
`;

const ProductDetails = styled.div`
	padding-right: 30px;
	max-width: 300px;
`;

const LinkInner = styled.div`
	display: flex;
	width: 100%;
`;

const Header = styled.div`
	margin-bottom: 1.333em;
`;

const ThumbnailWrapper = styled.div`
	position: relative;
	display: flex;
	max-width: 150px;
	width: 100%;
	margin-right: 60px;
	flex-shrink: 0;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		max-width: 100%;
		margin-bottom: 1.333em;
	}
`;

const Row = styled.div`
	display: flex;
	justify-content: flex-start;
`;

const Subtitle = styled.div`
	margin-right: 1.333em;
`;

const Body = styled.div`
	flex: 1;
`;

const ItemPrice = styled.h3`
	margin-left: auto;
	margin-top: 0.333em;
	font-size: ${({ theme }) => theme.type.p[0]};
`;

const SingleItem = ({
	item,
}) => {


	return (
		<Card>
			<ThumbnailWrapper>
				<LinkInner title={item?.name}>
					{
						<Thumbnail
						ratio={'100%'}
						ratioMobile={'100%'}
						image={item?.image?.src}
						thumbnail={item?.image?.src}
					/>
					}
				</LinkInner>
			</ThumbnailWrapper>

			<ProductDetails>
				<Header>
					<Title> {item?.name}</Title>
				</Header>

				<Row>
					<Subtitle>Qty</Subtitle>
					<Body>{item?.quantity}</Body>
				</Row>

				<ItemPrice>{`$${item?.subtotal}`}</ItemPrice>
			</ProductDetails>
		</Card>
	);
};

export default SingleItem;
