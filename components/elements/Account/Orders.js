import styled, { css } from 'styled-components';
import { useEffect, useState } from 'react';
import Loading from '../../common/Loading';
import Grid from '../../common/Grid';
import Accordian from '../../common/Accordian';
import { swatch } from '../../../styles/theme';
import SingleOrder from './SingleOrder';

const Orders = ({userId}) => {

	const [orders, setOrders] = useState(null);
	const [loading, setLoading] = useState(false);

	useEffect(() => {

		if (userId) {
			setLoading(true);
			fetch('/api/fetch-orders', {
				method: 'POST',
				body: JSON.stringify({
					userId
				})
			})
				.then((res) => res.json())
				.then((json) => {
					setOrders(json || null);
					setLoading(false);
				});
		}

	},[userId]);

	return (
		<>
			<HeadingWrapper>
				<Heading>
					Orders
				</Heading>
			</HeadingWrapper>
			{loading ? (
				<LoadingWrapper>
					<Loading />
				</LoadingWrapper>
			) : (
				<>
					{orders && !!orders.length ? (
						<>
							<OrderHeadingWrapper>
								<Grid
									rowGap={'0'}
									rowGapMobile={'10px'}
									columns={13}
									columnsMobile={13}
									>
									<OrderHeading>
										Order Number
									</OrderHeading>
									<OrderHeading>
										Date
									</OrderHeading>
									<OrderHeading className='desktop-only'>
										Status
									</OrderHeading>
									<OrderHeading className='desktop-only'>
										Total
									</OrderHeading>
								</Grid>
							</OrderHeadingWrapper>
							<AccordianWrapper>
								<Accordian>
									{orders.map((order, i) => {
										let orderDate = new Date(order.date_created);
										orderDate = orderDate.toLocaleDateString('en-AU');
										const orderTitle = (
											<OrderRow title={orderTitle}>
												<Grid
													rowGap={'0'}
													rowGapMobile={'10px'}
													columns={13}
													columnsMobile={13}
													>
														<OrderItem>{order.number}</OrderItem>
														<OrderItem>{orderDate}</OrderItem>
														<OrderItem className='desktop-only'>{order.status}</OrderItem>
														<OrderItem className='desktop-only'>${order.total}</OrderItem>
														<OrderView>view</OrderView>

												</Grid>
											</OrderRow>
										)
										return (
												<OrderRow title={orderTitle} key={`${i}`}>
													<SingleOrder order={order} />
												</OrderRow>
										)
									})}
								</Accordian>
							</AccordianWrapper>
						</>
					):(
						<OrderHeadingWrapper>
							You have no previous orders.
						</OrderHeadingWrapper>
					)
					}
				</>
			)}
		</>
	);
};

export default Orders;

const LoadingWrapper = styled.div`
	grid-column: 1 / -1;
	position: relative;

	& > div {
		opacity: 1;
	}
`;

const HeadingWrapper = styled.div`
	grid-column: 1 / -1;
	padding-bottom: 15px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: flex-end;
	margin-bottom: 64px;
	margin-top: 64px;
`;

const Heading = styled.h3``;

const OrderHeadingWrapper = styled.div`
	grid-column: 1 / -1;
`;

const OrderHeading = styled.h4`
	grid-column: span 3;
	margin-bottom: 15px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 6;

		&.desktop-only {
			display: none;
		}
	}
`;

const OrderRow = styled.div`
	grid-column: 1 / -1;
	padding: 15px 0;
`;

const OrderItem = styled.div`
	grid-column: span 3;
	text-align: left;
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 6;

		&.desktop-only {
			display: none;
		}
	}
`;

const OrderView = styled.div`
	grid-column: span 1;
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};
	transition: color 280ms ease;
	justify-self: flex-end;

	&:hover {
		color: ${swatch('brand')};
	}
`;


const AccordianWrapper = styled.div`
	grid-column: 1 / -1;
	width: 100%;

	div {
		&:not(:last-child) {
			margin-bottom: 0;
		}
	}

	button {
		display: block;
		padding: 15px 0;

		svg {
			display: none;
		}
	}
`;