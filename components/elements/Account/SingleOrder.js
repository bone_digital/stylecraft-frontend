import styled, { css } from 'styled-components';
import Grid from '../../common/Grid';
import { swatch } from '../../../styles/theme';
import SingleItem from './SingleItem';

const SingleOrder = ({order}) => {
	let orderDate = new Date(order.date_created);
	orderDate = orderDate.toLocaleDateString('en-AU');

	const billingAddress = order.billing;
	const shippingAddress = order.shipping;

	return (
		<>
			{order &&
				<Grid
					rowGap={'0'}
					rowGapMobile={'10px'}
					columns={13}
					columnsMobile={13}
					>
						<OrderDetails>
							<OrderItemHeading>
								Date
							</OrderItemHeading>
							<OrderItem>
								{orderDate}
							</OrderItem>
						</OrderDetails>
						<OrderDetails>
							<OrderItemHeading>
								Status
							</OrderItemHeading>
							<OrderItem>
								{order.status}
							</OrderItem>
						</OrderDetails>
						<AddressWrapper>
								<AddressHeading>
									Billing Address
								</AddressHeading>
								{billingAddress?.first_name} {billingAddress?.last_name} {billingAddress?.first_name && <br />}
								{billingAddress?.company} {billingAddress?.company && <br /> }
								{billingAddress?.address_1} {billingAddress?.address_1 && <br />}
								{billingAddress?.address_2} {billingAddress?.address_2 && <br />}
								{billingAddress?.city}, {billingAddress?.state}, {billingAddress?.country} {(billingAddress?.city || billingAddress?.state || billingAddress?.country) && <br />}
								{billingAddress?.postcode}
							</AddressWrapper>
							<AddressWrapper>
								<AddressHeading>
									Shipping Address
								</AddressHeading>
								{shippingAddress?.first_name} {shippingAddress?.last_name}  {shippingAddress?.first_name && <br />}
								{shippingAddress?.company} {shippingAddress?.company && <br /> }
								{shippingAddress?.address_1} {shippingAddress?.address_1 && <br />}
								{shippingAddress?.address_2} {shippingAddress?.address_2 && <br />}
								{shippingAddress?.city}, {shippingAddress?.state}, {shippingAddress?.country} {(shippingAddress?.city || shippingAddress?.state || shippingAddress?.country) && <br />}
								{shippingAddress?.postcode}
						</AddressWrapper>
						<ItemsHeading>
								Items
						</ItemsHeading>
						<ItemsWrapper>
							{order.line_items?.length && order.line_items.map((item, i) => {
								return (
									<SingleItem key={`${i}-item`} item={item} />
								)
							})}
						</ItemsWrapper>
						<TotalWrapper>
							<OrderItemHeading>
								Total
							</OrderItemHeading>
							<Total>
								${order.total}
							</Total>
						</TotalWrapper>
				</Grid>
			}
		</>
	);
};

export default SingleOrder;

const ItemsWrapper = styled.div`
	grid-column: 1 / -1;
`;

const OrderDetails = styled.div`
	grid-column: 1 / span all;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: 1 / span all;
	}
`

const OrderItemHeading = styled.h4`
	margin-bottom: 15px;
`;

const OrderItem = styled.div`
	text-align: left;
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};
	padding-bottom: 32px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
	}
`;

const ItemsHeading = styled.h4`
	grid-column: 1 / -1;
	margin-bottom: 15px;
`;


const AddressWrapper = styled.div`
	grid-column: span 5;
	margin-bottom: 15px;
	line-height: 1.3;
	padding-bottom: 32px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: 1 / span all;
	}
`;

const AddressHeading = styled.h4`
	margin-bottom: 15px;
`;

const TotalWrapper = styled.div`
	grid-column: 1 / -1;
	padding-top: 32px;
`

const Total = styled.h3`
`