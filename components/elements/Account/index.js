import styled, { css } from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';
import Grid from '../../common/Grid';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import AccountDetails from './AccountDetails';
import Orders from './Orders';

const Account = (props) => {

	const userId = useSelector(({ user }) => user.id);

	return (
		<Wrapper>
			<InnerWrapper>
				<Grid
					rowGap={'0'}
					rowGapMobile={'0'}
					columns={13}
					columnsMobile={5}
				>
					<AccountDetails userId={userId} />
					<Orders userId={userId} />

				</Grid>
			</InnerWrapper>
		</Wrapper>
	);
};

export default Account;


const Wrapper = styled.div`
	padding-top: 132px;
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-top: 100px;
	}
`

const HeadingWrapper = styled.div`
	grid-column: 1 / -1;
	padding-bottom: 15px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: flex-end;
	margin-bottom: 64px;
	margin-top: 64px;
`;

const Heading = styled.h3``;
