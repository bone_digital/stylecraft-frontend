import styled, { css } from 'styled-components';
import { useEffect, useState } from 'react';
import Loading from '../../common/Loading';
import { swatch } from '../../../styles/theme';
import UnstyledShowHide from '../../common/UnstyledShowHide';
import PasswordReset from './PasswordReset';
import AddressReset from './AddressReset';
import { useUI } from '../../../context/UIProvider';

const AccountDetails = ({userId}) => {

	const [customer, setCustomer] = useState(null);
	const [loading, setLoading] = useState(false);
	const [togglePassword, setTogglePassword] = useState(false);
	const [toggleBillingAddresses, setToggleBillingAddresses] = useState(false);
	const [toggleShippingAddresses, setToggleShippingAddresses] = useState(false);

	const {
		toggleAccountMenu,
		toggleMenu,
		toggleFormMenu,
		accountActive
	} = useUI() || {};

	const handleAccountOpen = () => {
		const isOpen = !accountActive;
		toggleAccountMenu(isOpen);
		toggleMenu(false);
		toggleFormMenu(false);
	};

	const updateCustomer = () => {
		setLoading(true);
		fetch('/api/fetch-customer', {
			method: 'POST',
			body: JSON.stringify({
				userId
			})
		})
			.then((res) => res.json())
			.then((json) => {
				setCustomer(json);
				setToggleBillingAddresses(false);
				setToggleShippingAddresses(false);
				setLoading(false);
			});
	}

	useEffect(() => {

		if (userId) {
			updateCustomer();
		}

	},[userId]);

	const firstName = customer?.first_name || null;
	const lastName = customer?.last_name || null;
	const email = customer?.email || null;
	const billingAddress = customer?.billing || null;
	const shippingAddress = customer?.shipping || null;

	return (
		<>
			<HeadingWrapper>
				<Heading>
					Account Details
				</Heading>
			</HeadingWrapper>
			{loading ? (
				<LoadingWrapper>
					<Loading />
				</LoadingWrapper>
			) : (
				<>
					{email ? (
						<>
							{(firstName || lastName) &&
								<NameWrapper>
									Welcome {firstName} {lastName},
								</NameWrapper>
							}
							<EmailWrapper>
								{email}
							</EmailWrapper>
							<PasswordWrapper>
								<UnstyledShowHide
									title={togglePassword ? 'Close' : 'Reset Password'}
									active={togglePassword}
									handleToggle={() => setTogglePassword(!togglePassword)}
									>
									<PasswordReset userId={userId} />
								</UnstyledShowHide>
							</PasswordWrapper>
							{billingAddress &&
								<AddressWrapper>
									<AddressHeading>
										Billing Address
									</AddressHeading>
									{billingAddress?.first_name} {billingAddress?.last_name} {billingAddress?.first_name && <br />}
									{billingAddress?.company} {billingAddress?.company && <br /> }
									{billingAddress?.address_1} {billingAddress?.address_1 && <br />}
									{billingAddress?.address_2} {billingAddress?.address_2 && <br />}
									{billingAddress?.city}, {billingAddress?.state}, {billingAddress?.country} {(billingAddress?.city || billingAddress?.state || billingAddress?.country) && <br />}
									{billingAddress?.postcode}
									<EditAddress>
										<UnstyledShowHide
											title={toggleBillingAddresses ? 'Close' : 'edit'}
											active={toggleBillingAddresses}
											handleToggle={() => setToggleBillingAddresses(!toggleBillingAddresses)}
											>
											<AddressReset userId={userId} type={'billing'} address={billingAddress} updateCustomer={updateCustomer} />
										</UnstyledShowHide>
									</EditAddress>
								</AddressWrapper>
							}
							{shippingAddress &&
								<AddressWrapper>
									<AddressHeading>
										Shipping Address
									</AddressHeading>
									{shippingAddress?.first_name} {shippingAddress?.last_name}  {shippingAddress?.first_name && <br />}
									{shippingAddress?.company} {shippingAddress?.company && <br /> }
									{shippingAddress?.address_1} {shippingAddress?.address_1 && <br />}
									{shippingAddress?.address_2} {shippingAddress?.address_2 && <br />}
									{shippingAddress?.city}, {shippingAddress?.state}, {shippingAddress?.country} {(shippingAddress?.city || shippingAddress?.state || shippingAddress?.country) && <br />}
									{shippingAddress?.postcode}
									<EditAddress>
										<UnstyledShowHide
											title={toggleShippingAddresses ? 'Close' : 'edit'}
											active={toggleShippingAddresses}
											handleToggle={() => setToggleShippingAddresses(!toggleShippingAddresses)}
											>
											<AddressReset userId={userId} type={'shipping'} address={shippingAddress} updateCustomer={updateCustomer} />
										</UnstyledShowHide>
									</EditAddress>
								</AddressWrapper>
							}
						</>
					): (
						<AddressWrapper>
							<AccountTrigger onClick={handleAccountOpen}>
								Login to view your account details.
							</AccountTrigger>
						</AddressWrapper>
					)}
				</>
			)}
		</>
	);
};

export default AccountDetails;

const HeadingWrapper = styled.div`
	grid-column: 1 / -1;
	padding-bottom: 15px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: flex-end;
	margin-bottom: 64px;
	margin-top: 64px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 32px;
		margin-top: 32px;
	}
`;

const Heading = styled.h3``;

const LoadingWrapper = styled.div`
	grid-column: 1 / -1;
	position: relative;

	& > div {
		opacity: 1;
	}
`;

const NameWrapper = styled.h3`
	grid-column: 1 / -1;
`;

const EmailWrapper = styled.div`
	grid-column: 1 / -1;
	margin-bottom: 5px;
`;

const PasswordWrapper = styled.h5`
	grid-column: 1 / -1;
	margin-bottom: 64px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 32px;
	}
`;

const AddressWrapper = styled.div`
	grid-column: span 5;
	margin-bottom: 15px;
	line-height: 1.3;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 32px;
	}
`;

const AddressHeading = styled.h4`
	margin-bottom: 15px;
`;

const EditAddress = styled.h5`
	grid-column: 1 / -1;
	margin-top: 15px;
`;


const ToggleButton = styled.button`
	transition: color 280ms ease;
	justify-self: flex-end;

	&:hover {
		color: ${swatch('brand')};
	}
`;

const AccountTrigger = styled.button`
	transition: color 280ms ease;
	justify-self: flex-end;

	&:hover {
		color: ${swatch('brand')};
	}
`;