import styled from 'styled-components';
import { useState } from 'react';
import { Formik, Form } from 'formik';
import TextField from '../../common/TextField';
import * as Yup from 'yup';
import { ease } from '../../../styles/theme';

const Wrapper = styled.div`
	margin-bottom: 64px;
`;

const FormWrapper = styled(Form)`
	width: 100%;
`;

const Grid = styled.div`
	display: grid;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 60px;
	column-gap: 36px;
	grid-auto-rows: initial;

	&:first-child {
		margin-bottom: 30px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 30px;
		column-gap: 24px;
	}
`;

const Column = styled.div`
	grid-column: span 8;
	display: flex;
	justify-content: space-between;
	align-items: start;
	flex-wrap: wrap;
	gap: 20px;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const RegisterButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	transition: ${ease('all')};
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${RegisterButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const ErrorMessage = styled.div`
	padding-bottom: 2em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};

	strong {
		font-weight: normal;
	}
`;

const AddressReset = ({ userId, type, address, updateCustomer }) => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [address1, setAddress1] = useState('');
	const [company, setCompany] = useState('');
	const [city, setCity] = useState('');
	const [state, setState] = useState('');
	const [country, setCountry] = useState('');
	const [postcode, setPostcode] = useState('');

	const [formError, setFormError] = useState(null);
	const [showConfirmation, setShowConfirmation] = useState(false);

	const firstNameField = {
		id: 'firstName',
		key: 'firstName',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'First Name',
		cssClass: null,
		isRequired: true,
		placeholder: 'First Name',
		fieldErrors: {}
	};

	const lastNameField = {
		id: 'lastName',
		key: 'lastName',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'Last Name',
		cssClass: null,
		isRequired: true,
		placeholder: 'Last Name',
		fieldErrors: {}
	};

	const address1Field = {
		id: 'address1',
		key: 'address1',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'Street Address',
		cssClass: null,
		isRequired: true,
		placeholder: 'Street Address',
		fieldErrors: {}
	};

	const companyField = {
		id: 'company',
		key: 'company',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'Company',
		cssClass: null,
		isRequired: true,
		placeholder: 'Company',
		fieldErrors: {}
	};

	const cityField = {
		id: 'city',
		key: 'city',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'City',
		cssClass: null,
		isRequired: true,
		placeholder: 'City',
		fieldErrors: {}
	};

	const stateField = {
		id: 'state',
		key: 'state',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'state',
		cssClass: null,
		isRequired: true,
		placeholder: 'State',
		fieldErrors: {}
	};

	const countryField = {
		id: 'country',
		key: 'country',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'country',
		cssClass: null,
		isRequired: true,
		placeholder: 'Country',
		fieldErrors: {}
	};

	const postcodeField = {
		id: 'postcode',
		key: 'postcode',
		formId: 'addressChangeForm',
		type: 'text',
		label: 'postcode',
		cssClass: null,
		isRequired: true,
		placeholder: 'Postcode',
		fieldErrors: {}
	};

	const registerSchema = Yup.object().shape({
		address1: Yup.string().required('Street Address is required'),
		city: Yup.string().required('City is required'),
		state: Yup.string().required('State is required'),
		country: Yup.string().required('Country is required'),
	});

	const updateAddress = async (values, actions) => {

		try {
			const data = {
				firstName: values?.firstName,
				lastName: values?.lastName,
				address1: values?.address1,
				company: values?.company,
				city: values?.city,
				state: values?.state,
				country: values?.country,
				postcode: values?.postcode,
				userId: userId,
				type: type,
			};

			const response = await fetch('/api/change-address', {
				method: 'POST',
				mode: 'same-origin',
				body: JSON.stringify(data)
			});

			const changeAddress = await response.json();

			// handleToggle(true);
			if (changeAddress.code === 400) {
				setFormError(changeAddress.msg);
			} else {
				setShowConfirmation(true);
				updateCustomer();
			}

			actions.setSubmitting(false);
		} catch (error) {
			const decodeHtmlCharCodes = (str) => {
				let element = document.createElement('textarea');
				element.innerHTML = str;

				return element.value;
			};

			if (error) {
				setFormError(decodeHtmlCharCodes(String(error?.message)));
			}
		} finally {
			actions.setSubmitting(false);
		}
	};

	return (
		<Wrapper>
			{showConfirmation ? (
				<>
					Address changed successfully.
				</>
			) : (
				<>
					{formError && (
						<ErrorMessage dangerouslySetInnerHTML={{ __html: formError }} />
					)}

					<Formik
						initialValues={{
							firstName,
							lastName,
							address1,
							company,
							city,
							state,
							country,
							postcode,
						}}
						validationSchema={registerSchema}
						onSubmit={(values, actions) => {
							updateAddress(values, actions);
						}}
					>
						{({
							values,
							errors,
							touched,
							isSubmitting,
							handleChange,
							handleBlur
						}) => (
							<FormWrapper noValidate>
								<Grid>
									<Column>
										<TextField
											field={firstNameField}
											error={touched.firstName && errors.firstName}
											value={values.firstName}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={lastNameField}
											error={touched.lastName && errors.lastName}
											value={values.lastName}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={address1Field}
											error={touched.address1 && errors.address1}
											value={values.address1}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={companyField}
											error={touched.company && errors.company}
											value={values.company}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={cityField}
											error={touched.city && errors.city}
											value={values.city}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={stateField}
											error={touched.state && errors.state}
											value={values.state}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={countryField}
											error={touched.country && errors.country}
											value={values.country}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<TextField
											field={postcodeField}
											error={touched.postcode && errors.postcode}
											value={values.postcode}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
									</Column>
								</Grid>

								<Grid>
									<Column>
										<RegisterButton
											type="submit"
											title="Submit"
											disabled={isSubmitting}
										>
											<Underline>Submit</Underline>
										</RegisterButton>
									</Column>
								</Grid>
							</FormWrapper>
						)}
					</Formik>
				</>
			)}
		</Wrapper>
	);
};

export default AddressReset;
