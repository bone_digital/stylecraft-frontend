import styled from 'styled-components';
import { useState } from 'react';
import { Formik, Form } from 'formik';
import PasswordField from '../../common/PasswordField';
import * as Yup from 'yup';
import { ease } from '../../../styles/theme';

const Wrapper = styled.div`
	margin-bottom: 64px;
`;

const FormWrapper = styled(Form)`
	width: 100%;
`;

const Grid = styled.div`
	display: grid;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 60px;
	column-gap: 36px;
	grid-auto-rows: initial;

	&:first-child {
		margin-bottom: 30px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 30px;
		column-gap: 24px;
	}
`;

const Column = styled.div`
	grid-column: span 3;
	display: flex;
	justify-content: space-between;
	align-items: start;
	flex-wrap: wrap;
	gap: 20px;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const RegisterButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	transition: ${ease('all')};
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${RegisterButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const ErrorMessage = styled.div`
	padding-bottom: 2em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};

	strong {
		font-weight: normal;
	}
`;

const PasswordReset = ({ handleToggle, userId }) => {
	const [currentPassword, setCurrentPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [newPasswordRepeated, setNewPasswordRepeated] = useState('');
	const [formError, setFormError] = useState(null);
	const [showConfirmation, setShowConfirmation] = useState(false);

	const currentPasswordField = {
		id: 'currentPassword',
		key: 'currentPassword',
		formId: 'registerForm',
		type: 'text',
		label: 'Current Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'Current Password',
		fieldErrors: {}
	};

	const newPasswordField = {
		id: 'newPassword',
		key: 'newPassword',
		formId: 'registerForm',
		type: 'text',
		label: 'New Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'New password',
		fieldErrors: {}
	};

	const newPasswordRepeatedField = {
		id: 'newPasswordRepeated',
		key: 'newPasswordRepeated',
		formId: 'registerForm',
		type: 'text',
		label: 'Repeat New Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'Repeat New Password',
		fieldErrors: {}
	};

	const registerSchema = Yup.object().shape({
		currentPassword: Yup.string().required('Current Password is required'),
		newPassword: Yup.string().required('New Password is required'),
		newPasswordRepeated: Yup.string()
			.required('New Password is required')
			.matches(newPassword, 'Passwords must match'),
	});

	const updatePassword = async (values, actions) => {

		if (values?.newPassword !== values?.newPasswordRepeated) {
			setFormError('Passwords must match');
			actions.setSubmitting(false);
			return;
		}

		try {
			const data = {
				currentPassword: values?.currentPassword,
				newPassword: values?.newPassword,
				newPasswordRepeated: values?.newPasswordRepeated,
				userId: userId,
			};

			const response = await fetch('/api/change-password', {
				method: 'POST',
				mode: 'same-origin',
				body: JSON.stringify(data)
			});

			const changePassword = await response.json();

			// handleToggle(true);
			if (changePassword.code === 400) {
				setFormError(changePassword.msg);
			} else {
				setShowConfirmation(true);
			}

			actions.setSubmitting(false);
		} catch (error) {
			const decodeHtmlCharCodes = (str) => {
				let element = document.createElement('textarea');
				element.innerHTML = str;

				return element.value;
			};

			if (error) {
				setFormError(decodeHtmlCharCodes(String(error?.message)));
			}
		} finally {
			actions.setSubmitting(false);
		}
	};

	return (
		<Wrapper>
			{showConfirmation ? (
				<>
					Password changed successfully.
				</>
			) : (
				<>
					{formError && (
						<ErrorMessage dangerouslySetInnerHTML={{ __html: formError }} />
					)}

					<Formik
						initialValues={{
							currentPassword,
							newPassword,
							newPasswordRepeated,
						}}
						validationSchema={registerSchema}
						onSubmit={(values, actions) => {
							updatePassword(values, actions);
						}}
					>
						{({
							values,
							errors,
							touched,
							isSubmitting,
							handleChange,
							handleBlur
						}) => (
							<FormWrapper noValidate>
								<Grid>
									<Column>
										<PasswordField
											field={currentPasswordField}
											error={touched.currentPassword && errors.currentPassword}
											value={values.currentPassword}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<PasswordField
											field={newPasswordField}
											error={touched.newPassword && errors.newPassword}
											value={values.newPassword}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<PasswordField
											field={newPasswordRepeatedField}
											error={touched.newPasswordRepeated && errors.newPasswordRepeated}
											value={values.newPasswordRepeated}
											disabled={isSubmitting}
											onChange={handleChange}
											onBlur={handleBlur}
										/>
									</Column>
								</Grid>

								<Grid>
									<Column>
										<RegisterButton
											type="submit"
											title="Submit"
											disabled={isSubmitting}
										>
											<Underline>Submit</Underline>
										</RegisterButton>
									</Column>
								</Grid>
							</FormWrapper>
						)}
					</Formik>
				</>
			)}
		</Wrapper>
	);
};

export default PasswordReset;
