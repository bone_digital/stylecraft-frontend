import searchReplaceLink, {
	checkUrlsMatch
} from '../../../utils/searchReplaceLink';
import Link from 'next/link';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { breakpoint, ease, swatch } from '../../../styles/theme';

const NavList = styled.ul`
	display: flex;
	align-items: center;

	 @media ${breakpoint('laptop')} {
		display: none;
	} 
`;

const NavLink = styled.a``;

const NavItem = styled.li`
	&:not(:last-child) {
		margin-right: 40px;
	}

	a,
	button {
		color: ${({ active }) => (active ? swatch('brand') : swatch('black'))};
		transition: ${ease('color')};
		position: relative;

		&:hover {
			color: ${swatch('brand')};
		}
	}
`;

export default function Navigation({ menu, ...props }) {
	const router = useRouter();
	const currentPath = router.asPath;

	return menu ? (
		<NavList {...props}>
			{menu.map((node, index) => {
				const link = searchReplaceLink(node.url);
				const isActive = checkUrlsMatch(link, currentPath);

				return (
					<NavItem key={`${node?.id}-${index}`}>
						<Link href={`${link}`} passHref>
							<NavLink title={node.title} active={isActive}>
								{node.title}
							</NavLink>
						</Link>
					</NavItem>
				);
			})}
		</NavList>
	) : null;
}
