import styled from 'styled-components';
import Navigation from '../Navigation';
import MenuTrigger from '../MenuTrigger';
import AccountTrigger from '../AccountTrigger';
import { Wrapper } from '../../common/InnerWrapper';
import { breakpoint, ease, swatch } from '../../../styles/theme';
import SearchToggle from '../../elements/SearchToggle';
import Link from 'next/link';
import BrandMark from '../../../public/icons/brandmark.svg';
import { useState, useEffect } from 'react';
import _ from 'lodash';
import { useUI } from '../../../context/UIProvider';
import CartTrigger from '../CartTrigger';
import WishlistTrigger from '../WishlistTrigger';

const HeaderWrapper = styled.header`
	padding: ${({ hasScrolled }) =>
		hasScrolled ? '24px 0 24px 10px' : '40px 0 40px 10px'};
	z-index: 50;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	background: ${({ theme, hasScrolled, backgroundColor }) =>
		hasScrolled ? theme.colors[backgroundColor] : 'transparent'};
	transition: ${ease('all')};
	transform: ${({ hasScrolled, scrollingUp }) =>
		hasScrolled && !scrollingUp ? 'translateY(-100%)' : 'translateY(0)'};

	@media ${breakpoint('tablet')} {
		padding: 16px 0;
	}
`;

const Row = styled.div`
	display: flex;
	align-items: center;
	position: relative;
`;

const NavList = styled.ul`
	display: flex;
	align-items: center;

	@media ${breakpoint('laptop')} {
		display: none;
	}
`;

const NavLink = styled.a``;

const NavItem = styled.li`
	a {
		color: ${({ active }) => (active ? swatch('brand') : swatch('black'))};
	}

	a,
	button {
		transition: ${ease('color')};
		position: relative;

		&:hover {
			color: ${swatch('brand')};
		}
	}
`;

const ColumnInner = styled.div`
	display: flex;
	align-items: center;
`;

const Column = styled(Wrapper)`
	width: 50%;
`;

const Logo = styled.div`
	height: 48px;
	margin-left: auto;
	color: ${({ theme, backgroundColor, hasScrolled }) =>
		backgroundColor === 'pink' || backgroundColor === 'brand'
			? hasScrolled
				? theme.colors.brand
				: theme.colors.white
			: theme.colors.brand};

	@media ${breakpoint('mobile')} {
		height: 32px;
	}

	svg {
		fill: currentColor;
	}
`;

export default function Header({ menu }) {
	const [hasScrolled, setHasScrolled] = useState(false);
	const [scrollingUp, setScrollingUp] = useState(false);
	const [lastScrollTop, setLastScrollTop] = useState(0);

	const { backgroundColor } = useUI() || {};

	const handleScroll = () => {
		let currentScroll = window.pageYOffset;

		setHasScrolled(currentScroll > 20);
		setScrollingUp(hasScrolled && currentScroll < lastScrollTop);
		setLastScrollTop(currentScroll);
	};

	useEffect(() => {
		const throttledHandleScroll = _.throttle(handleScroll, 100);

		window.addEventListener('scroll', throttledHandleScroll);

		return () => {
			window.removeEventListener('scroll', throttledHandleScroll);
		};
	}, [lastScrollTop, hasScrolled]);

	return (
		<HeaderWrapper
			hasScrolled={hasScrolled}
			scrollingUp={scrollingUp}
			backgroundColor={backgroundColor}
		>
			<Row>
				<Column>
					<ColumnInner>
						<MenuTrigger />
						<MenuTrigger type="mobile" />
						<SearchToggle type="mobile" />
						<CartTrigger type="mobile" />
						<WishlistTrigger type="mobile" />
						<SearchToggle type="desktop" />
						{menu && <Navigation menu={menu} />}

					</ColumnInner>
				</Column>

				<Column>
					<ColumnInner>
						<NavList>
							<NavItem>
								<CartTrigger type="desktop" />
							</NavItem>
							<NavItem>
								<WishlistTrigger type="desktop" />
							</NavItem>
						</NavList>

						<Logo
							hasScrolled={hasScrolled}
							backgroundColor={backgroundColor}
						>
							<Link href="/" passHref>
								<NavLink
									title="Return home"
									aria-label="Return home"
								>
									<BrandMark />
								</NavLink>
							</Link>
						</Logo>
					</ColumnInner>
				</Column>
			</Row>
		</HeaderWrapper>
	);
}
