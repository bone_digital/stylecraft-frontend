import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const Trigger = styled.button`
	display: ${({ type }) => type === 'mobile' ? 'none' : 'block'};
	color: ${({ active }) => active ? swatch('brand') : swatch('black')};
	position: relative;

	@media ${breakpoint('desktop')} {
		margin-bottom: 1.25em;
	}

	@media ${breakpoint('laptop')} {
		display: ${({ type }) => type === 'mobile' ? 'block' : 'none'};
		line-height: 1.67;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 0;
	}
`;

const Counter = styled.span`
	position: absolute;
	top: 0;
	right: 0;
	transform: translate(100%, -75%);
	background: ${({ theme, backgroundColor, hasScrolled }) =>
		backgroundColor === 'pink' || backgroundColor === 'brand'
			? hasScrolled
				? theme.colors.brand
				: theme.colors.white
			: theme.colors.brand};
	border-radius: 999px;
	width: 20px;
	height: 20px;
	color: ${({ theme, backgroundColor, hasScrolled }) =>
		backgroundColor === 'pink' || backgroundColor === 'brand'
			? hasScrolled
				? theme.colors.white
				: theme.colors.brand
			: theme.colors.white};
	text-align: center;
	white-space: nowrap;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 10px;

	@media ${breakpoint('tablet')} {
		transform: translate(150%, 0);
	}

`;

export default function AccountTrigger( { hasScrolled, type }) {
	const {
		toggleAccountMenu,
		toggleMenu,
		toggleFormMenu,
		accountActive,
	} = useUI() || {};

	const handleClick = () => {
		const isOpen = !accountActive;
		toggleAccountMenu(isOpen);
		toggleMenu(false);
		toggleFormMenu(false);
	};

	return (
		<Trigger onClick={handleClick} active={accountActive} type={type}>
			Account
		</Trigger>
	);
}
