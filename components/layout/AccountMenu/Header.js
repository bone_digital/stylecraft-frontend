import cookie from 'js-cookie';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { userActions } from '../../../store/userSlice';
import { cartActions } from '../../../store/cartSlice';
import { ease } from '../../../styles/theme';
import Link from 'next/link';

const systemPages = require('../../../json/system-pages.json');
const myAccountSystemPage = systemPages?.find((page) => page?.shortcode === '[woocommerce_my_account]');

const Wrapper = styled.div`
	padding-bottom: 15px;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	align-items: flex-end;
	margin-bottom: 64px;
`;

const Name = styled.h3``;

const Menu = styled.div`
`;

const Button = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	margin-left: 30px;
	transition: ${ease('all')};
`;

const Anchor = styled.a`
	flex-shrink: 0;
	flex-grow: 0;
	margin-left: 30px;
	transition: ${ease('all')};
`;

const Header = ({ firstName, userName, handleClose }) => {
	const dispatch = useDispatch();

	const removeLoginCookie = async () => {
		await fetch(
			`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/logout`,
			{
				credentials: 'include'
			}
		).then((response) => response);
	};

	const handleLogout = () => {
		const cookies = cookie.get();

		if (cookies) {
			for (const [key, value] of Object.entries(cookies)) {
				if (key.startsWith('wordpress_logged_in_')) {
					cookie.remove(key);
				}
			}
		}

		cookie.remove('token');
		cookie.remove('refresh-token');
		cookie.remove('cart-key');
		cookie.remove('ussr-data');

		dispatch(userActions.logout());
		dispatch(cartActions.logout());
		removeLoginCookie();
	};

	return (
		<Wrapper>
			<Name>{firstName || userName}</Name>
			<Menu>
				<Link href={`/${myAccountSystemPage?.slug}`} passHref>
					<Anchor onClick={handleClose}>Account</Anchor>
				</Link>
				<Button onClick={handleLogout}>Logout</Button>
			</Menu>
		</Wrapper>
	);
};

export default Header;
