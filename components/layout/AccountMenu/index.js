import { useSelector } from 'react-redux';
import SlideOutMenu from '../../common/SlideOutMenu';
import styled from 'styled-components';
import { swatch, ease, breakpoint } from '../../../styles/theme';
import Accordian from '../../common/Accordian';
import { useUI } from '../../../context/UIProvider';
import { AnimatePresence, motion } from 'framer-motion';
import Close from '../../../public/icons/close.svg';
import Cart from '../../elements/Cart';
import Wishlist from '../../elements/Wishlist';
import AccountToggle from '../../elements/AccountToggle';
import Header from './Header';
import { useMemo, useEffect, useState } from 'react';
import Loading from '../../common/Loading';
import { cartActions } from '../../../store/cartSlice';
import { useDispatch } from 'react-redux';
import Cookies from 'js-cookie';

const Inner = styled.nav`
	padding: 215px 0 45px;
	display: flex;
	width: 100%;
	flex-direction: column;
	min-height: 100%;
	height: auto;
	box-sizing: content-box;

	@media ${breakpoint('xlDesktop')} {
		padding-top: 140px;
	}

	@media ${breakpoint('tablet')} {
		padding-bottom: 40px;
	}
`;

const MenuClose = styled.button`
	width: 44px;
	height: 44px;
	border-radius: 99%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 50%;
	left: -36px;
	transform: translate(-100%);
	background: ${swatch('white')};

	@media ${breakpoint('tablet')} {
		display: none;
	}

	svg {
		width: 8px;
	}
`;

const ActiveMenu = styled(motion.div)``;

const Content = styled.div``;

const LoadingWrapper = styled.div`
	width: 100%;
	position: relative;

	& > div {
		opacity: 1;
	}
`;

const AccountMenu = ({ introCopy, termsCopy, downloadCopy }) => {
	const [loading, setLoading] = useState(false);
	const { toggleAccountMenu, 
		accountActive, 
		accountActiveAccordionIndex,
		setAccountActiveAccordionIndex,
	} = useUI() || {};

	const dispatch = useDispatch();

	const handleClose = () => {
		toggleAccountMenu(false);
		setAccountActiveAccordionIndex(null);
	};

	const cartItems = useSelector(({ cart }) => cart?.items);
	const newCartItems = useSelector(({ cart }) => cart?.newItems); // TODO: swap for one above.
	const cartCount = useSelector(({ cart }) => cart.newItemCount);
	const cartTotals = useSelector(({ cart }) => cart.newTotal);
	const cartWholesaleTotal = useSelector(({ cart }) => cart.newWholesaleTotal);

	const wishlistItemCount = useSelector(({ cart }) => cart.wishlistItemCount);

	const userId = useSelector(({ user }) => user.id);
	const loggedIn = useSelector(({ user }) => user.loggedIn);
	const firstName = useSelector(({ user }) => user.firstName);
	const userName = useSelector(({ user }) => user.userName);
	const userRoles = useSelector(({ user }) => user.roles);
	const userStatuConfirmed = useSelector(({ user }) => user.confirmedStatus);

	const wishlistIsLoading = useSelector(({ cart }) => cart.wishlistIsLoading);
	const cartIsLoading = useSelector(({ cart }) => cart.newCartIsLoading);

	const isWholeseller = useMemo(() => {
		if (userRoles && userRoles?.length) {
			return (
				userRoles.filter(({ node }) => {
					const roleName = node?.name;

					return (
						roleName === 'default_wholesaler' ||
						roleName === 'administrator' ||
						roleName === 'wholesaler'
					);
				})?.length > 0
			);
		}

		return false;
	}, [userRoles]);

	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<SlideOutMenu active={accountActive} onClick={handleClose}>
			<MenuClose onClick={handleClose}>
				<Close />
			</MenuClose>

			<Inner>
				<Content>
					<AnimatePresence exitBeforeEnter={true}>
						{loggedIn ? (
							<ActiveMenu
								key={'account-header'}
								variants={variants}
								initial={'hidden'}
								animate={'visible'}
								exit={'hidden'}
							>
								<Header
									firstName={firstName}
									userName={userName}
									handleClose={handleClose}
								/>
							</ActiveMenu>
						) : (
							userStatuConfirmed ? (
								<ActiveMenu
									key={'account-toggle'}
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
								>
									<AccountToggle
										introCopy={introCopy}
										termsCopy={termsCopy}
										downloadCopy={downloadCopy}
									/>
								</ActiveMenu>
							) : (
								<LoadingWrapper>
									<Loading />
								</LoadingWrapper>
							)
						)}
					</AnimatePresence>

					{cartIsLoading || wishlistIsLoading ? (
						<>
							<LoadingWrapper>
								<Loading />
							</LoadingWrapper>
						</>
					) : (
						<Accordian align={'start'} activeIndex={accountActiveAccordionIndex}>
							{newCartItems && newCartItems.length > 0 && (
								<Cart
									title={`Cart (${cartCount})`}
									cartItems={cartItems}
									newCartItems={newCartItems}
									total={cartTotals}
									isWholeseller={isWholeseller}
									wholesaleTotal={cartWholesaleTotal}
								/>
							)}
							{wishlistItemCount > 0 &&
								<Wishlist
									title={`Wishlist (${wishlistItemCount})`}
									userId={userId}
								/>
							}
						</Accordian>
					)}
				</Content>
			</Inner>
		</SlideOutMenu>
	);
};

export default AccountMenu;
