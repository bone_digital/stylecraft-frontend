import styled from 'styled-components';
import LinkHeading from '../../../common/LinkHeading';
import { breakpoint, swatch } from '../../../../styles/theme';
import GravityForm from '../../../common/GravityForm';
import { useUI } from '../../../../context/UIProvider';
import Rarr from '../../../../public/icons/rarr.svg';
import { useState } from 'react';
import EmailField from '../../../common/EmailField';

const Wrapper = styled.div`
	max-width: 200px;
	padding-bottom: 40px;

	form input {
		text-transform: uppercase;
		letter-spacing: 0.25em;
		font-size: ${({ theme }) => theme.type.h5[0]};
		line-height: ${({ theme }) => theme.type.h5[1]};
	}

	> h5 {
		line-height: 2.5;
	}

	form {
		position: relative;
	}

	@media ${breakpoint('desktop')} {
		max-width: 100%;
		width: 100%;
	}
`;

const FooterSubmit = styled.button`
	position: static;
	flex-shrink: 0;
	margin-left: 0.5rem;

	&::before {
		content: '';
		display: block;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 24px;
		height: 24px;
	}

	&:hover {
		svg {
			transform: translateX(2px);
		}
	}

	svg {
		width: 4px;
		transition: 280ms transform ease;
	}
`;

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	cursor: pointer;
`;

const Input = styled.input`
	padding: 0 0 6px 0;
	border-bottom: 1px solid black;
	transition: all 280ms ease;
	display: block;
	width: 100%;

	&::placeholder {
		opacity: 1;
		color: black;
		font-size: 12px;
	}
`;

const SignupForm = () => {
	const [emailInput, setEmailInput] = useState('');
	const { toggleFormPopup } = useUI() || {};

	const formData = {
		formEmailField: emailInput,
	}

	const openMenu = () => {
		toggleFormPopup(true, formData);
	};

	return (
		<Wrapper>
			<LinkHeading title="Newsletter" />
			<FieldWrap>
				<Input
					name="newsletter-email"
					id="newsletter-email"
					placeholder="EMAIL"
					value={emailInput}
					onChange={(e) => setEmailInput(e.target.value)}
				/>
				<FooterSubmit onClick={openMenu}>
					<Rarr/>
				</FooterSubmit>
			</FieldWrap>
		</Wrapper>
	);
};

export default SignupForm;
