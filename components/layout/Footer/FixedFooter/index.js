import styled from 'styled-components';
import { useState, useEffect } from 'react';
import _ from 'lodash';
import { breakpoint, swatch } from '../../../../styles/theme';
import InnerWrapper from '../../../common/InnerWrapper';
import Grid from '../../../common/Grid';
import Link from 'next/link';
import BrandMark from '../../../../public/icons/brandmark.svg';
import React from 'react';

const Logo = styled.div`
	grid-column: span 4;
	margin-top: auto;

	> a {
		max-width: 160px;
		display: flex;
	}

	@media ${breakpoint('desktop')} {
		order: 2;
		grid-column: -1 / 1;
	}

	@media ${breakpoint('tablet')} {
		> a {
			width: 100%;
			max-width: 100%;
		}
	}
`;

const FooterLink = styled.a``;

const Wrapper = styled.footer`
	position: relative;
`;

const Footer = styled.div`
	position: sticky;
	bottom: 0;
	left: 0;
	padding: ${({ expanded }) => (expanded ? '60px 0 18px' : '9px')}; 
	width: 100%;
	/* min-height: 100vh; */
	background-color: ${swatch('brand')};
	color: ${swatch('white')};

	svg {
		fill: currentColor;
	}

	@media ${breakpoint('desktop')} {
		padding-top: ${({ expanded }) => (expanded ? '36px' : '9px')};
	}

	div {
		align-items: bottom;
	}
`;

const Content = styled.div`
	font-size: ${({ theme }) => theme.type.h4[0]};
	line-height: ${({ theme }) => theme.type.h4[1]};
	grid-column: span 6;

	p {
		font-size: inherit;
		line-height: inherit;
	}

	p > a,
	> a {
		text-decoration: underline;
	}

	@media ${breakpoint('xlDesktop')} {
		grid-column: span 8;
	}

	@media ${breakpoint('desktop')} {
		grid-column: -1 / 1;
		order: 0;
		margin-bottom: 124px;
	}

	@media ${breakpoint('tablet')} {
		font-size: ${({ theme }) => theme.type.h4[0]};
		line-height: ${({ theme }) => theme.type.h4[1]};
		margin-bottom: 32px;
	}
`;

const FixedFooter = React.forwardRef((props, ref) => {
	let copy = props?.copy;

	const [scrollBottom, setScrollBottom] = useState(false);
	const [firstBottom, setFirstBottom] = useState(false);

	const handleScroll = (e) => {
		const clientHeight = e?.target?.scrollingElement?.clientHeight;
		const scrollHeight = e?.target?.scrollingElement?.scrollHeight;
		const scrollTop = e.target.scrollingElement?.scrollTop;
		const isAtFooter = scrollHeight - scrollTop - ref.current.clientHeight + 18 <= clientHeight;

		setScrollBottom(scrollHeight - scrollTop === clientHeight);

		if (isAtFooter) {
			setTimeout(() => {
				setFirstBottom(true);
			}, 1000)
		} else {
			setFirstBottom(false);
		}
	};

	useEffect(() => {
		const debouncedHandleScroll = _.debounce(handleScroll, 160);
		window.addEventListener('scroll', debouncedHandleScroll);
		return () =>
			window.removeEventListener('scroll', debouncedHandleScroll);
	}, []);

	return (
		<Footer ref={ref} active={scrollBottom} expanded={firstBottom}>
			{firstBottom &&
				<Wrapper>
					<InnerWrapper>
						<Grid rowGap="0">
							<Logo>
								<Link href="/">
									<FooterLink
										title="Return home"
										aria-label="Return home"
									>
										<BrandMark />
									</FooterLink>
								</Link>
							</Logo>

							<Content dangerouslySetInnerHTML={{ __html: copy }} />
						</Grid>
					</InnerWrapper>
				</Wrapper>
			}
		</Footer>
	);
});

export default FixedFooter;
