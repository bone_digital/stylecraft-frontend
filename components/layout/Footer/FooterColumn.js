import styled from 'styled-components';
import Link from 'next/link';
import LinkHeading from '../../common/LinkHeading';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import { ease } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const NavList = styled.ul`
	grid-column: span 2;
	display: flex;
	flex-direction: column;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: span 4;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: span 4;
	}
`;

const FooterItem = styled.li`
	text-transform: uppercase;
	display: flex;
	padding-right: 1em;
	transition: ${ease('color')};

	> * > * {
		line-height: 2.5;
	}

	&:hover {
		color: ${({ theme }) => theme.colors.brand};
	}
`;

const FooterLink = styled.a``;

const FooterColumn = ({ menu }) => {
	
	const { toggleFormMenu } = useUI() || {};

	const openMenu = () => {
		toggleFormMenu(true);
	};
	
	return menu ? (
		<NavList>
			{menu &&
				menu?.map((node, index) => {
					const { id, title, url } = node || null;

					let isExternal = true;
					const siteUrl = process.env.NEXT_PUBLIC_SITE_URL;
					const sgUrl = process.env.NEXT_PUBLIC_SITE_URL_ASIA;

					if (url.indexOf('/') === 0 || 
						url.indexOf(sgUrl) > -1 || 
						url.indexOf(siteUrl) > -1) {
							isExternal = false;
					}
					
					let href = searchReplaceLink(url, isExternal);

					return (
						<FooterItem key={`${id}-${index}`}>
							{title === 'Contact Us' ? (
							<button onClick={openMenu}>
								<LinkHeading title={title} />
							</button>
							) : isExternal ? (
							<a href={href} target="_blank" title={title}>
								<LinkHeading title={title} />
							</a>
							) : (
							<Link href={href} passHref>
								<FooterLink title={title}>
								<LinkHeading title={title} />
								</FooterLink>
							</Link>
							)}
						</FooterItem>
					);
				})}
		</NavList>
	) : null;
};

export default FooterColumn;
