import styled from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';
import Grid from '../../common/Grid';
import { breakpoint, ease } from '../../../styles/theme';
import SignupForm from './Form';
import _ from 'lodash';
import { useRef } from 'react';
import FixedFooter from './FixedFooter';
import FooterColumn from './FooterColumn';
import { useUI } from '../../../context/UIProvider';

const Wrapper = styled.footer`
	position: relative;
	z-index: 1;
	transition: ${ease('background')};
	background: ${({ theme, backgroundColor }) =>
		backgroundColor ? theme.colors[backgroundColor] : theme.colors.white};
`;

const FooterDefault = styled.div`
	padding: 85px 0 24px 0;
	position: relative;
	z-index: 1;

	@media ${breakpoint('tablet')} {
		padding-bottom: 125px;
	}

	@media ${breakpoint('mobile')} {
		padding: 95px 0 20px 0;
	}

	&&& h5 {
		justify-content: flex-start;
	}
`;

const Hr = styled.div`
	content: '';
	display: block;
	width: 100%;
`;

const Column = styled.div`
	grid-column: span 4;
	display: flex;
	flex-wrap: wrap;
	align-items: flex-end;

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}
`;

const SubGrid = styled.div`
	display: grid;
	grid-template-columns: repeat(8, minmax(0, 1fr));
	column-gap: 32px;
	row-gap: 36px;
	grid-column: span 8;

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}
`;

const Footer = ({ footerA, footerB, footerC, footerD, copy }) => {
	const footerWrap = useRef(null);
	const { backgroundColor } = useUI() || {};

	return (
		<>
			<Wrapper backgroundColor={backgroundColor}>
				<FooterDefault>
					<InnerWrapper>
						<Grid rowGap={'42px'}>
							<Column>
								<SignupForm />
							</Column>
							<SubGrid>
								<FooterColumn menu={footerA} />
								<FooterColumn menu={footerB} />
								<FooterColumn menu={footerC} />
								<FooterColumn menu={footerD} />
							</SubGrid>
						</Grid>
					</InnerWrapper>
				</FooterDefault>
				<Hr />
			</Wrapper>
			<FixedFooter ref={footerWrap} copy={copy} />
		</>
	);
};

export default Footer;
