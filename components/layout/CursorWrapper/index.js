import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import Cursor from '../../elements/Cursor';

const Wrapper = styled.div`
	position: relative;
`;

const CursorWrapper = ({ children, cursorRefresh, handleCursorRefresh }) => {
	return (
		<React.Fragment>
			<Wrapper>{children}</Wrapper>
			<Cursor
				cursorRefresh={cursorRefresh}
				handleCursorRefresh={handleCursorRefresh}
			/>
		</React.Fragment>
	);
};

export default CursorWrapper;
