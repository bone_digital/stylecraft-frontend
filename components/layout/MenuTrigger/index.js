import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';
import { useState } from 'react';
import MenuIcon from '../../../public/icons/menu.svg';


const Toggle = styled.button`
	min-width: 44px;
	height: 44px;
	border-radius: 999px;
	display: flex;
	align-items: center;
	justify-content: center;
	margin-right: 20px;
	background: ${swatch('grey')};
	z-index: 6;
	position: relative;
	display: ${({ type }) => (type === 'mobile' ? 'none' : 'flex')};
	color: ${({ active }) => (active ? swatch('brand') : swatch('black'))};

	@media ${breakpoint('laptop')} {
		display: ${({ type }) => (type === 'mobile' ? 'flex' : 'none')};
	}

	@media ${breakpoint('tablet')} {
		margin-right: 20px;
	}

	@media ${breakpoint('mobile')} {
		margin-right: 16px;
	}

	&:hover {
		svg {
			transform: scale(0.9);
		}
	}

	svg {
		width: 16px;
	}
`;


export default function MenuTrigger({ type }) {
	const { toggleMenu, toggleAccountMenu, toggleFormMenu, menuActive } =
		useUI() || {};

	const handleClick = () => {
		const isOpen = !menuActive;
		toggleMenu(isOpen);
		toggleAccountMenu(false);
		toggleFormMenu(false);
	};

	return (
		<Toggle onClick={handleClick} type={type} active={menuActive}>
			<MenuIcon />
		</Toggle>
	);
}
