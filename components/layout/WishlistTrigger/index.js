import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';
import { useState } from 'react';
import WishlistIcon from '../../../public/icons/wishlist.svg';
import { useSelector } from 'react-redux';

const Toggle = styled.button`
	min-width: 44px;
	height: 44px;
	border-radius: 999px;
	display: flex;
	align-items: center;
	justify-content: center;
	margin-right: 20px;
	background: ${swatch('grey')};
	z-index: 6;
	position: relative;
	display: ${({ type }) => (type === 'mobile' ? 'none' : 'flex')};
	color: ${({ active }) => (active ? swatch('brand') : swatch('black'))};

	@media ${breakpoint('laptop')} {
		display: ${({ type }) => (type === 'mobile' ? 'flex' : 'none')};
	}

	@media ${breakpoint('tablet')} {
		margin-right: 20px;
	}

	@media ${breakpoint('mobile')} {
		margin-right: 16px;
	}

	&:hover {
		svg {
			transform: scale(0.9);
		}
	}

	svg {
		width: 24px;
	}
`;

const Counter = styled.span`
	position: absolute;
	top: 0;
	right: 0;
	transform: translate(20%, -30%);
	background: ${({ theme, backgroundColor, hasScrolled }) =>
		backgroundColor === 'pink' || backgroundColor === 'brand'
			? hasScrolled
				? theme.colors.brand
				: theme.colors.white
			: theme.colors.brand};
	border-radius: 999px;
	width: 20px;
	height: 20px;
	color: ${({ theme, backgroundColor, hasScrolled }) =>
		backgroundColor === 'pink' || backgroundColor === 'brand'
			? hasScrolled
				? theme.colors.white
				: theme.colors.brand
			: theme.colors.white};
	text-align: center;
	white-space: nowrap;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 10px;
`;


export default function WishlistTrigger({ type }) {
	const { 
		toggleMenu, 
		toggleAccountMenu, 
		toggleFormMenu, 
		menuActive, 
		backgroundColor,
		setAccountActiveAccordionIndex
	} =
		useUI() || {};
	
	const wishlistCount = useSelector((state) => state.cart.wishlistItemCount);

	const handleClick = () => {
		toggleAccountMenu(true);
		toggleFormMenu(false);
		setAccountActiveAccordionIndex(1);
	};

	return (
		<>
			{wishlistCount !== null && (
				<Toggle onClick={handleClick} type={type} active={menuActive}>
					<WishlistIcon />
						<Counter
							backgroundColor={backgroundColor}
							active={wishlistCount > 0}
						>
							{wishlistCount}
						</Counter>
				</Toggle>
			)}
		</>
	);
}
