import SlideOutMenu from '../../common/SlideOutMenu';
import styled from 'styled-components';
import Link from 'next/link';
import { swatch, ease, breakpoint } from '../../../styles/theme';
import FormField from '../../common/FormField';
import { useUI } from '../../../context/UIProvider';
import { motion } from 'framer-motion';
import Close from '../../../public/icons/close.svg';
import searchReplaceLink, { isExternalUrl } from "../../../utils/searchReplaceLink";
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import AccountTrigger from '../AccountTrigger';
import Navigation from '../Navigation';

// const menusJSON = require('../../../json/menus.json');

const MainMenu = ({ menu, mobileMenu }) =>
{
	const { toggleMenu, menuActive } = useUI() || {};
	const [site, setSite] = useState(null);
	const router = useRouter();

	const locale = router.locale;

	const siteOptions =
		locale === 'en-AU'
			? [
				{
					value: process.env.NEXT_PUBLIC_SITE_REDIRECT,
					label: 'Australia'
				},
				{
					value: process.env.NEXT_PUBLIC_SITE_REDIRECT_ASIA,
					label: 'Singapore'
				}
			]
			: [
				{
					value: process.env.NEXT_PUBLIC_SITE_REDIRECT_ASIA,
					label: 'Singapore'
				},
				{
					value: process.env.NEXT_PUBLIC_SITE_REDIRECT,
					label: 'Australia'
				}
			];

	const handleSelect = (e) =>
	{
		setSite(e.currentTarget.value);
	};

	const handleClose = () =>
	{
		toggleMenu(false);
	};

	useEffect(() =>
	{
		if (site !== null)
		{
			router.push(site, undefined, { shallow: true });
		}
	}, [site]);

	return (
		<SlideOutMenu active={menuActive} onClick={handleClose}>
			<MenuClose onClick={handleClose}>
				<Close />
			</MenuClose>

			<Inner>

				<NavList>
					<>
						<MobileAdditionalItems>
							<AnimatedItem
								key={`main-menu-mobile`}
								variants={{
									open: {
										opacity: 1,
										transform: 'translateY(0%)',
										transition: {
											delay: 0.6
										}
									},
									close: {
										opacity: 0,
										transform: 'translateY(20px)',
										transition: {
											delay: 0.6
										}
									}
								}}
								animate={menuActive ? 'open' : 'close'}
								initial={'close'}
							>
								{mobileMenu && mobileMenu.length > 0 && <MobileSubNav menu={mobileMenu} />}
								<AccountTrigger type="mobile" />
							</AnimatedItem>

						</MobileAdditionalItems>
						<AnimatedItem
							variants={{
								open: {
									opacity: 1,
									transform: 'translateY(0%)',
									transition: {
										delay: 0.6
									}
								},
								close: {
									opacity: 0,
									transform: 'translateY(20px)',
									transition: {
										delay: 0.6
									}
								}
							}}
							animate={menuActive ? 'open' : 'close'}
							initial={'close'}
						>
							<AccountTrigger type="desktop" />
						</AnimatedItem>
						{menu &&
							menu?.map((node, index) =>
							{
								const { ID, title, url } = node || null;
								const cleanUrl = searchReplaceLink(url);
								const isExternal = isExternalUrl(url);

								return (
									<AnimatedItem
										key={`main-menu-${ ID || index }`}
										variants={{
											open: {
												opacity: 1,
												transform: 'translateY(0%)',
												transition: {
													delay: 0.6
												}
											},
											close: {
												opacity: 0,
												transform: 'translateY(20px)',
												transition: {
													delay: 0.6
												}
											}
										}}
										animate={menuActive ? 'open' : 'close'}
										initial={'close'}
									>
										{!isExternal && <Link
											href={cleanUrl}
											passHref
										>
											<NavLink title={title}>{title}</NavLink>
										</Link>}
										{isExternal &&
											<NavLink title={title} href={cleanUrl}>{title}</NavLink>
										}
									</AnimatedItem>
								);
							})}

					</>
				</NavList>

				<SelectWrap>
					<FormField
						type="select"
						name="country"
						label="Country"
						options={siteOptions}
						onChange={handleSelect}
					/>
				</SelectWrap>
			</Inner>
		</SlideOutMenu>
	);
};

const CloseArea = styled(motion.button)`
	width: 100%;
	position: absolute;
	height: 100%;
	left: ${ ({ direction }) => (direction === 'rigth' ? 0 : 'auto') };
	right: ${ ({ direction }) => (direction === 'left' ? 0 : 'auto') };
	top: 0;
	cursor: default;
`;

const Inner = styled.nav`
	padding: 215px 0 45px;
	display: flex;
	width: 100%;
	flex-direction: column;
	justify-content: space-between;

	@media ${ breakpoint('xlDesktop') } {
		padding-top: 140px;
	}

	@media ${ breakpoint('tablet') } {
		min-height: 100%;
		padding-bottom: 40px;
	}
`;

const NavList = styled.ul`
	font-size: ${ ({ theme }) => theme.type.nav[0] };
	line-height: ${ ({ theme }) => theme.type.nav[1] };
	margin-bottom: 45px;

	@media ${ breakpoint('xlDesktop') } {
		font-size: ${ ({ theme }) => theme.typeMobile.nav[0] };
	}

	@media ${ breakpoint('tablet') } {
		line-height: ${ ({ theme }) => theme.typeMobile.nav[1] };
		margin-bottom: 40px;
	}
`;

const NavItem = styled.li`
	&:not(:last-child) {
		margin-bottom: ${ ({ theme }) => `${ theme.type.nav[1] }em` };

		@media ${ breakpoint('tablet') } {
			margin-bottom: 0;
		}
	}
`;

const AnimatedItem = motion(NavItem);

const NavLink = styled.a`
	color: ${ ({ active }) => (active ? swatch('brand') : swatch('black')) };
	transition: ${ ease('color') };

	&:hover {
		color: ${ swatch('brand') };
	}
`;

const SelectWrap = styled.div`
	margin-top: auto;
	max-width: 160px;
	opacity: 0.4;
	transition: ${ ease('opacity') };

	&:hover {
		opacity: 1;
	}
`;

const MenuClose = styled.button`
	width: 44px;
	height: 44px;
	border-radius: 99%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 50%;
	left: -36px;
	transform: translate(-100%);
	background: ${ swatch('white') };

	@media ${ breakpoint('tablet') } {
		display: none;
	}

	svg {
		width: 8px;
	}
`;

const MobileAdditionalItems = styled.div`
	display: none;

	@media ${ breakpoint('laptop') } {
		display: block;
	}
`;

const MobileSubNav = styled(Navigation)`

	@media ${ breakpoint('laptop') } {
		display: block;

		li {
			margin-bottom: 1.25em;
		}
	}

	@media ${ breakpoint('tablet') } {
		display: block;

		li {
			margin-bottom: 0;
		}
	}
`



export default MainMenu;
