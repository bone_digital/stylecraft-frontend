import SlideOutMenu from '../../common/SlideOutMenu';
import styled from 'styled-components';
import 'babel-polyfill';
import { swatch, breakpoint } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';
import Close from '../../../public/icons/close.svg';
import GravityForm from '../../common/GravityForm';
import { useState } from 'react';

const Inner = styled.nav`
	display: flex;
	width: 100%;
	flex-direction: column;
	justify-content: space-between;
	min-height: 100%;
	height: auto;
`;

const MenuClose = styled.button`
	width: 44px;
	height: 44px;
	border-radius: 99%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 50%;
	right: -36px;
	transform: translate(100%);
	background: ${swatch('white')};

	@media ${breakpoint('tablet')} {
		display: none;
	}

	svg {
		width: 8px;
	}
`;

const Header = styled.h3`
	display: block;
	padding-bottom: 12px;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	margin-bottom: 230px;

	@media ${breakpoint('xlDesktop')} {
		margin-bottom: 100px;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 40px;
	}
`;

const FormBody = styled.div`
	padding: 215px 0 45px;

	@media ${breakpoint('xlDesktop')} {
		padding-top: 140px;
	}

	@media ${breakpoint('tablet')} {
		padding-bottom: 40px;
	}
`;

const FormWraper = styled.div``;

const Intro = styled.h4`
	margin-bottom: 40px;
	font-weight: normal;
`;

const FormMenu = () => {
	const { toggleFormMenu, contactActive, formData } = useUI() || {};
	const [wasSubmitted, setWasSubmitted] = useState(false);

	const handleClose = () => {
		toggleFormMenu(false);
	};

	const handleAfterSubmit = () => {
		if (!wasSubmitted) {
			setWasSubmitted(true);
		}
	}

	const showIntro = formData?.formDescription && !wasSubmitted; // hide after the first submission

	return (
		<SlideOutMenu
			active={contactActive}
			onClick={handleClose}
			direction={'left'}
		>
			<MenuClose onClick={handleClose}>
				<Close />
			</MenuClose>

			<Inner>
				{formData && (
					<FormBody>
						<Header>{formData?.formTitle}</Header>
						<FormWraper>
							{showIntro && (
								<Intro
									dangerouslySetInnerHTML={{
										__html: formData.formDescription
									}}
								/>
							)}
							{formData?.formId && (
								<GravityForm
									formID={parseInt(formData?.formId)}
									formEmailField={formData?.formEmailField}
									formProductRequestField={
										formData?.formProductRequestField
									}
									parentHandleAfterSubmit={handleAfterSubmit}
								/>
							)}
						</FormWraper>
					</FormBody>
				)}
			</Inner>
		</SlideOutMenu>
	);
};

export default FormMenu;
