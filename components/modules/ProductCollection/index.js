import styled from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';
import Grid from '../../common/Grid/index';
import LinkButton from '../../common/LinkButton/index';
import ImageBg from '../../common/Thumbnail/index';
import ViewMore from '../../common/ViewMore';
import OverlapHeading from '../../common/OverlapHeading/index';
import PostCard from '../../common/PostCard';
import InviewWrapper from '../../common/InviewWrapper/index';
import { brandPrefix } from '../../../lib/settings';

const Wrapper = styled.div``;

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const Column = styled.div`
	width: 50%;
	padding: 235px 0 45px;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		padding-top: 100px;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
	}
`;

const IntroColumn = styled(Column)`
	background: ${({ theme }) => theme.colors.grey};
`;

const GridColumn = styled(Column)``;

const IntroBody = styled.div`
	font-size: ${({ theme }) => theme.type.h2[0]};
	grid-column: span 10;
	position: relative;
	z-index: 1;
	margin-bottom: calc((17px + 24px + 4.8125em) * -1);

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		margin-bottom: calc((17px + 24px + 3.85em) * -1);
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: calc((17px + 24px + 1.925em) * -1);
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: -1 / 1;
	}

	> a {
		margin-top: 24px;
	}
`;

const Intro = styled.h2`
	* {
		all: inherit;
	}
`;

const ImageColumn = styled.div`
	grid-column: 6 / span 8;
	z-index: 0;
	position: relative;
	padding: 0 40px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		padding: 0 0;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
	}
`;

const ProductCollection = ({ brand, brandData, anchorId }) => {
	let brandFields = brand?.brand;
	const brandFieldsOriginal = brandFields;
	if( null !== brandData )
	{
		brandFields = brandData?.brand;
	}
	const { title, featuredImage, link, slug } = brand || {};

	const image = featuredImage?.node || null;

	const { excerpt, description } =
	brandFieldsOriginal || {};

	const { relatedProducts } =
		brandFields || {};
	let defaultRelatedProducts = brandFields?.defaultRelatedProducts;

	const relatedPosts =
		relatedProducts && relatedProducts.length
			? relatedProducts
			: defaultRelatedProducts;

	const collectionLink = slug ? `${ brandPrefix }collection/${ slug }` : false;
	const viewMoreLink = brandData?.slug ? `/product/?brand=${brandData?.slug}` : false;

	return (
		<InviewWrapper id={anchorId} sectionTheme={'white'}>
			<Wrapper>
				<Row>
					<IntroColumn>
						<InnerWrapper>
							<Grid
								rowGap={'0'}
								rowGapMobile={'40px'}
								columns={13}
								columnsMobile={13}
							>
								<IntroBody>
									{excerpt && (
										<Intro
											dangerouslySetInnerHTML={{
												__html: excerpt
											}}
										/>
									)}
									<LinkButton
										title={'Collections'}
										href={link}
									/>
								</IntroBody>

								<ImageColumn>
									{image && (
										<ImageBg
											image={image?.sourceUrl}
											thumbnail={image?.thumbSourceUrl}
											alt={image?.altText}
											ratio={'130%'}
										/>
									)}
								</ImageColumn>
							</Grid>
						</InnerWrapper>
					</IntroColumn>
					<GridColumn>
						<InnerWrapper>
							<OverlapHeading
								title={`More from ${title}`}
								size="small"
							/>
							{relatedPosts && relatedPosts.length && (
								<Row>
									<ViewMore
										title={'View More'}
										url={viewMoreLink || link}
									/>
									<Grid rowGap={'50px'} rowGapMobile={'50px'}>
										{relatedPosts.map((brand, index) => {
											const post = brand?.post;
											const type = post?.__typename;

											return (
												<PostCard
													size="large"
													post={post}
													type={type}
													key={index}
												/>
											);
										})}
									</Grid>
								</Row>
							)}
						</InnerWrapper>
					</GridColumn>
				</Row>
			</Wrapper>
		</InviewWrapper>
	);
};

export default ProductCollection;
