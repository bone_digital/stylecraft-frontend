import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';
import InviewWrapper from '../../common/InviewWrapper';
import LinkButton from '../../common/LinkButton';
import ImageBg from '../../common/Thumbnail';
import { Content } from '../../common/WYSWYG';

const Row = styled.section`
	display: flex;
	flex-wrap: wrap;
`;

const Column = styled.div`
	width: 50%;
	position: relative;
	display: flex;
	align-items: flex-end;

	> * {
		width: 100%;
	}

	&:last-child {
		padding-bottom: 32px;
	}

	@media ${breakpoint('laptop')} {
		width: 100%;

		&:last-child {
			padding-top: 40px;
			padding-bottom: 0;
		}
	}

	@media ${breakpoint('tablet')} {
		&:last-child {
			padding-top: 0;
		}
	}
`;

const ImageColumn = styled.div`
	grid-column: span 7;
	padding-right: 36px;

	@media ${breakpoint('laptop')} {
		padding-right: 0;
		grid-column: span 6;
	}

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const TextColumn = styled.div`
	grid-column: span 6;

	@media ${breakpoint('laptop')} {
		grid-column: 8 / span 5;
	}

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const TextInner = styled(Content)`
	margin-bottom: 25px;
`;

const Title = styled.h1`
	grid-column: -1 / 1;
	max-width: 475px;
	margin-bottom: -0.4em;
	z-index: 2;
	position: relative;

	@media ${breakpoint('laptop')} {
		margin-bottom: 0;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 0;
		margin-top: -0.6em;
		max-width: 350px;
	}
`;

export default function index({
	fullbleedImage,
	backgroundColor,
	anchorId,
	link, // DEPRECATED - seems not to be used???
	title,
	linkButton,
	contentImage,
	copy
}) {
	let hasLink = !!linkButton?.linkInner;

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Row>
				<Column>
					{fullbleedImage && (
						<ImageBg
							ratio={'112.5%'}
							ratioMobile={'84%'}
							alt={fullbleedImage?.alt}
							image={fullbleedImage?.sourceUrl}
							thumbnail={fullbleedImage?.thumbSourceUrl}
						/>
					)}
				</Column>

				<Column>
					<InnerWrapper>
						<Grid
							rowGap={'0'}
							rowGapMobile={'40px'}
							columns={13}
							columnsMobile={12}
						>
							{title && <Title>{title}</Title>}

							{contentImage && (
								<ImageColumn>
									<ImageBg
										ratio={'146.5%'}
										ratioMobile={'113%'}
										alt={contentImage?.alt}
										image={contentImage?.sourceUrl}
										thumbnail={contentImage?.thumbSourceUrl}
									/>
								</ImageColumn>
							)}
							<TextColumn>
								<TextInner
									dangerouslySetInnerHTML={{ __html: copy }}
								/>
								{hasLink && <LinkButton prefix={linkButton?.prefix} title={linkButton?.linkInner?.title} href={linkButton?.linkInner?.url} />}
							</TextColumn>
						</Grid>
					</InnerWrapper>
				</Column>
			</Row>
		</InviewWrapper>
	);
}
