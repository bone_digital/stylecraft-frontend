import styled from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import Filters from './Filters';
import StaffResults from './StaffResults';
import { motion, AnimatePresence } from 'framer-motion';
import { swatch, ease } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const StaffWrapper = styled.div`
	> div > div > div {
		align-items: start;

		> *:first-child {
			position: sticky;
			top: 100px;

			@media ${({ theme }) => theme.breakpoints.desktop} {
				position: static;
				top: auto;
			}
		}
	}
`;

const FilterWrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;

	li
	{
		margin-bottom: 16px;
	}

	> *:first-child {
		margin-bottom: 100px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			margin-bottom: 40px;
		}
	}
`;

const ContactMessage = styled.h3`
	position: relative;
	font-weight: 400;
	text-align: left;
	max-width: 350px;
`;

const ContactMessageHover = styled.span`
	position: relative; 
	&::before {
		content: '';
		position: absolute;
		bottom: -4px;
		left: 0;
		height: 1px;		
		width: 100%;
		background: ${swatch('black')};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}
	&:hover {
		&::before {
		transform: scaleX(0);
		transform-origin: right;
		}
	}
`;

const Dot = styled.div`
	width: 16px;
	height: 16px;
	border-radius: 50%;
	display: block;
	background: ${({ theme }) => theme.colors.brand};
	margin-right: 22px;
	margin-top: 0.4em;
	flex-shrink: 0;
`;

const Trigger = styled.button`
	position: relative;
	text-align: left;
	display: flex;
	align-items: flex-start;
`;

const SectionInner = ({
	posts,
	locationFilters,
	activeShowRoom,
	setActiveShowroom
}) => {
	const { toggleFormMenu } = useUI();

	const openMenu = () => {
		toggleFormMenu(true);
	};

	return (
		<AnimatePresence initial={false} exitBeforeEnter={true}>
			<StaffWrapper id="post-body">
				<ThirdsColumn
					leftColumn={
						<PostFilters
							locationFilters={locationFilters}
							activeShowRoom={activeShowRoom}
							setActiveShowroom={setActiveShowroom}
							openMenu={openMenu}
						/>
					}
					rightColumn={
						<PostBody
							activeShowRoom={activeShowRoom}
							posts={posts}
						/>
					}
				/>
			</StaffWrapper>
		</AnimatePresence>
	);
};

const PostFilters = ({
	locationFilters,
	setActiveShowroom,
	activeShowRoom,
	openMenu
}) => (
	<FilterWrapper>
		<Filters
			filters={locationFilters}
			setActive={setActiveShowroom}
			active={activeShowRoom}
		/>
		<Trigger onClick={openMenu}>
			<Dot />
			<ContactMessage>
				Ready to chat?
				<br />
				Get in <ContactMessageHover>contact</ContactMessageHover>.
			</ContactMessage>
		</Trigger>
	</FilterWrapper>
);

const PostBody = ({ activeShowRoom, posts }) => (
	<motion.div
		key={`${activeShowRoom?.slug}`}
		initial={{ opacity: 0 }}
		animate={{ opacity: 1 }}
		exit={{ opacity: 0 }}
		transition={{
			duration: 0.4
		}}
	>
		<StaffResults posts={posts} />
	</motion.div>
);

export default SectionInner;
