import styled from 'styled-components';
import { useState, useMemo } from 'react';
import SortBy from '../../common/SortBy';
import Card from './Card';
import { AnimatePresence } from 'framer-motion';

const Wrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: flex-end;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		justify-content: flex-start;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const Row = styled.div`
	display: ${({ respondTo }) => (respondTo === 'desktop' ? 'grid' : 'none')};
	grid-template-columns: repeat(6, minmax(0, 1fr));
	column-gap: 20px;
	row-gap: 60px;
	width: 100%;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: ${({ respondTo }) =>
			respondTo !== 'desktop' ? 'grid' : 'none'};
	}

	> div {
		grid-column: span 2;

		@media ${({ theme }) => theme.breakpoints.laptop} {
			grid-column: span 3;
		}

		@media ${({ theme }) => theme.breakpoints.tablet} {
			grid-column: -1 / 1;
			display: block;
		}
	}
`;

const StaffResults = ({ posts }) => {
	const [sortPostsBy, setSortPostsBy] = useState(null);

	/**
	 * @param {object} filter
	 * @param {array} filterArray
	 * @returns Boolean
	 */
	const isDuplicateFilter = (filter, filterArray) => {
		if (Array.isArray(filterArray)) {
			return (
				filterArray.filter((filterObj) => {
					return filter?.slug === filterObj?.slug;
				}).length > 0
			);
		}

		return true;
	};

	// Grab available filters from posts
	const sortPostsFilters = useMemo(() => {
		let filterList = [
			{
				label: 'All',
				slug: '*',
				value: '*'
			}
		];

		if (posts && posts.length) {
			posts.forEach((postObj) => {
				let teamArray = postObj?.staff?.teams?.edges;

				let team =
					teamArray && teamArray.length ? teamArray[0]?.node : null;

				if (team) {
					if (!isDuplicateFilter(team, filterList)) {
						let filterTeam = team;

						filterTeam.value = team?.slug;
						filterTeam.slug = team?.slug;
						filterTeam.label = team?.name;

						filterList.push(filterTeam);
					}
				}
			});
		}

		return filterList;
	}, [posts, sortPostsBy]);

	const filteredPosts = useMemo(() => {
		if (posts && posts.length && sortPostsBy !== null) {
			return posts.filter(({ staff }) => {
				const staffMemberTeams = staff?.teams?.edges || [];
				const sortPostsBySlug = sortPostsBy?.slug ? sortPostsBy.slug : '*';

				let isActiveStaff = staffMemberTeams.filter(({ node }) => {
					return sortPostsBySlug === '*' ||
						!sortPostsBySlug ||
					sortPostsBySlug === null
						? node
						: node?.slug === sortPostsBySlug;
				}).length;

				return isActiveStaff;
			});
		}

		return posts;
	}, [sortPostsBy]);

	return posts && posts.length ? (
		<>
			<Wrapper>
				<SortBy
					options={sortPostsFilters}
					setSortPostsBy={(data) => setSortPostsBy(data)}
					respondTo="desktop"
				/>

				<Row respondTo="desktop">
					<AnimatePresence initial={false} exitBeforeEnter={true}>
						{filteredPosts.map((post, index) => (
							<Card
								post={post}
								sortFilter={sortPostsBy}
								index={index}
								key={`${sortPostsBy}-${index}`}
							/>
						))}
					</AnimatePresence>
				</Row>
			</Wrapper>

			<Row respondTo="mobile">
				<AnimatePresence initial={false} exitBeforeEnter={true}>
					{posts.map((post, index) => (
						<Card
							post={post}
							sortFilter={sortPostsBy}
							index={index}
							key={`mobile-${sortPostsBy}-${index}`}
						/>
					))}
				</AnimatePresence>
			</Row>
		</>
	) : null;
};

export default StaffResults;
