import styled from 'styled-components';
import SectionInner from './SectionInner';
import { useState, useMemo, useEffect } from 'react';
import { useRouter } from "next/router";

const Wrapper = styled.div``;

const Content = ({ showrooms }) => {
	const [activeShowRoom, setActiveShowroom] = useState(null);
	const router = useRouter();

	const viewAllFilter = {
		title: 'View All',
		slug: '*'
	};

	const activePosts = useMemo(() => {
		if (activeShowRoom?.slug === '*') {
			let allStaff = [];

			showrooms.forEach((showroom) => {
				const showroomStaff = showroom?.node?.showroom?.showroomStaff;

				if (showroomStaff && showroomStaff.length) {
					allStaff.push(...showroomStaff);
				}
			});

			allStaff?.sort((a, b) => a?.staff?.title?.localeCompare(b?.staff?.title))

			return allStaff;
		}

		const getActiveShowroom = showrooms
			.filter((showroom) => {
				if (showroom?.node?.slug === activeShowRoom?.slug) {
					return showroom;
				}
			})
			.find((showroom) => {
				return showroom;
			});

		const currentStaff = getActiveShowroom?.node?.showroom?.showroomStaff;

		if (currentStaff) {
			currentStaff?.sort((a, b) => a?.staff?.title?.localeCompare(b?.staff?.title))
		}

		return currentStaff;
	}, [showrooms, activeShowRoom]);

	useEffect(() => {
		if (showrooms && showrooms.length) {
			let defaultFilter = viewAllFilter;
			if ( router?.query?.team )
			{
				const defaultSlug = router.query.team;
				const showroomFromSlug = showrooms.filter((showroom) => {
					if(showroom?.node?.slug === defaultSlug)
					{
						return showroom;
					}
				});
				if( showroomFromSlug )
				{
					defaultFilter = {
						slug: showroomFromSlug[0].node.slug,
						title: showroomFromSlug[0].node.slug,
					};
				}
			}
			setActiveShowroom(defaultFilter);
		}
	}, []);

	const locationFilters = useMemo(() => {
		const filters = showrooms.map(({ node }) => {
			const showroomStaff = node?.showroom?.showroomStaff;

			if (showroomStaff && showroomStaff.length) {
				return node;
			}
		});

		filters.unshift(viewAllFilter);

		return filters;
	}, [showrooms]);

	return (
		<Wrapper>
			<SectionInner
				activeShowRoom={activeShowRoom}
				locationFilters={locationFilters}
				setActiveShowroom={(data) => setActiveShowroom(data)}
				posts={activePosts}
			/>
		</Wrapper>
	);
};

export default Content;
