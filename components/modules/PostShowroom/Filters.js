import styled, { css } from 'styled-components';
import { ease, swatch, breakpoint } from '../../../styles/theme';
import Rarr from '../../../public/icons/rarr.svg';
import { AnimatePresence, motion } from 'framer-motion';
import { useState } from 'react';

const List = styled.ul`
	margin-bottom: 2px;
	display: grid;
	grid-template-columns: 1fr 1fr;
	gap: 16px;
	row-gap: 0;
	
	li:first-child
	{
		grid-column: span 2;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 8px;
	}
`;

const DesktopList = styled(List)`
	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const MobileFilters = styled.div`
	display: none;
	width: 100%;
	margin-bottom: 54px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: block;
	}
`;

const MobileBody = styled(motion.ul)`
	padding-top: 32px;
	transition: ${ease('all')};
	display: grid;
	grid-template-columns: repeat(6, minmax(0, 1fr));
	row-gap: 30px;
	column-gap: 16px;
	margin-bottom: 2px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 8px;
	}
`;

const Item = styled.li`
	margin-bottom: 30px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 24px;
	}
`;

const MobileItem = styled(motion.li)`
	grid-column: span 3;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: span 2;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: span 3;
	}
`;

const Trigger = styled.button`
	transition: ${ease('all')};
	color: ${({ active, theme }) =>
		active ? theme.colors.brand : theme.colors.black};
	position: relative;

	&:hover {
		color: ${({ theme }) => theme.colors.brand};
	}

	&::before {
		content: '';
		display: inline-block;
		width: 8px;
		height: 8px;
		background: ${swatch('brand')};
		position: absolute;
		top: 0.3em;
		left: -10px;
		border-radius: 50%;
		opacity: 0;
		transform: ${({ active }) =>
			active
				? 'translate(-100%, 0) scale(1)'
				: 'translate(calc(-100% + 10px), 0) scale(1)'};
		opacity: ${({ active }) => (active ? 1 : 0)};
		transition: ${ease('all', 'fast')};

		@media ${breakpoint('tablet')} {
			display: none;
		}
	}
`;

const FilterToggle = styled.button`
	display: flex;
	align-items: center;
	padding-bottom: 6px;
	position: relative;

	&::before {
		content: '';
		display: block;
		width: 100%;
		height: 1px;
		position: absolute;
		bottom: 0;
		right: 0;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	&:hover::before {
		transform: scaleX(0);
		transform-origin: right;
	}

	svg {
		width: 4px;
		transform: none;
		transition: ${ease('transform', 'fast')};
	}

	${({ active }) =>
		active &&
		css`
			svg {
				transform: rotate(90deg);
			}
		`};
`;

const Label = styled.label`
	margin-right: 20px;
`;

const Filters = ({ filters, setActive, active }) => {
	const [listOpen, setListOpen] = useState(true);

	/**
	 *
	 * @param {Object} filter;
	 * @param {Array} filterActive
	 */
	const toggleActive = (filter, filterActive) => {
		if (!filterActive) {
			setActive(filter);
		}
	};

	const scrollToPostBodyTop = () => {
		const postBodyElement = document.getElementById('post-body');
		if (postBodyElement) {
			postBodyElement.scrollIntoView({behavior: 'smooth', block: 'start'}); 
		}
	}

	const handleTriggerClick = (filter, filterActive) => {
		toggleActive(filter, filterActive);
		scrollToPostBodyTop();
	};

	const handleListToggle = () => {
		setListOpen(!listOpen);
	};

	const parentVariant = {
		open: {
			opacity: 1,
			maxHeight: '20em',
			transition: {
				delayChildren: 0.4,
				staggerChildren: 0.1
			}
		},
		close: {
			opacity: 0,
			maxHeight: '0em',
			transition: {
				type: 'tween',
				delayChildren: 0.4,
				staggerChildren: 0.1,
				staggerDirection: -1,
				when: 'afterChildren'
			}
		}
	};

	const childrenVariant = {
		open: {
			opacity: 1,
			transform: 'translateY(0%)',
			transition: {
				duration: 0.4
			}
		},
		close: {
			opacity: 0,
			transform: 'translateY(20px)',
			transition: {
				duration: 0.4
			}
		}
	};

	return filters && filters.length ? (
		<>
			<DesktopList>
				{filters.map((filter, index) => {
					/**
					 * Return active filter or first filter
					 */

					let filterActive =
						active !== null
							? active?.slug === filter?.slug
							: filters[0]?.slug === filter?.slug;
					if( typeof filter == "undefined" )
					{
						return;
					}
					return (
						<Item key={index}>
							<Trigger
								active={filterActive}
								onClick={() =>
									handleTriggerClick(filter, filterActive)
								}
							>
								{filter?.title || 'Filter'}
							</Trigger>
						</Item>
					);
				})}
			</DesktopList>

			<MobileFilters>
				<FilterToggle onClick={handleListToggle} active={listOpen}>
					<Label>Filter By</Label>
					<Rarr></Rarr>
				</FilterToggle>

				<AnimatePresence initial={false} exitBeforeEnter={true}>
					{listOpen && (
						<MobileBody
							variants={parentVariant}
							animate={listOpen ? 'open' : 'close'}
							initial={'close'}
							exit={'close'}
							key={'motion-body'}
						>
							{
								/**
								 * Return active filter or first filter
								 */
								filters.map((filter, index) => {
									let filterActive =
										active !== null
											? active?.slug === filter?.slug
											: filters[0]?.slug === filter?.slug;

									if( typeof filter == "undefined" )
									{
										return;
									}

									return (
										<MobileItem
											variants={childrenVariant}
											animate={
												listOpen ? 'open' : 'close'
											}
											initial={'close'}
											exit={'close'}
											key={`list-${index}`}
										>
											<Trigger
												active={filterActive}
												onClick={() => {
													toggleActive(
														filter,
														filterActive
													);
													scrollToPostBodyTop();
												}}
											>
												{filter?.title || 'Filter'}
											</Trigger>
										</MobileItem>
									);
								})
							}
						</MobileBody>
					)}
				</AnimatePresence>
			</MobileFilters>
		</>
	) : null;
};

export default Filters;
