import styled from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import Filters from './Filters';
import StaffResults from './StaffResults';
import Content from './Content';
import SectionInner from './SectionInner';
import InviewWrapper from '../../common/InviewWrapper/index';
import { useMemo } from 'react';

const PostShowroom = (props) => {
	const { backgroundColor, showrooms, anchorId } = props || {};

	if (!showrooms || showrooms === null) {
		return null;
	}

	const showroomPosts = showrooms?.edges;

	return showroomPosts && showroomPosts.length ? (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Content showrooms={showroomPosts} />
		</InviewWrapper>
	) : null;
};

export default PostShowroom;
