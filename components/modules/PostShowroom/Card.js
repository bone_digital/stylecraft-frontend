import styled from 'styled-components';
import ImageEl from '../../common/ImageEl/index';
import WYSWYG from '../../common/WYSWYG/index';
import ImageBg from '../../common/Thumbnail/index';
import { motion } from 'framer-motion';

const ShowroomCard = styled(motion.div)`
	grid-column: span 2;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: span 3;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
		display: block;
	}
`;

const ImageWrapper = styled.div`
	margin-bottom: 12px;
`;

const Body = styled.div``;

const Name = styled.h3`
	margin-bottom: 2px;
`;

const Role = styled.h5`
	opacity: 0.5;
	text-transform: uppercase;
`;

const Card = ({ post, index, sortFilter }) => {
	const staffMember = post?.staff;
	const displayPicture = staffMember?.featuredImage?.node;

	const staffFields = staffMember?.staff;

	return post ? (
		<ShowroomCard
			key={`${sortFilter}-${index}`}
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			exit={{ opacity: 0 }}
			transition={{
				duration: 0.2
			}}
		>
			<ImageWrapper>
				<ImageBg
					image={displayPicture?.sourceUrl}
					thumbnail={displayPicture?.thumbSourceUrl}
					alt={displayPicture?.alt}
					ratio="125%"
				/>
			</ImageWrapper>
			<Body>
				{staffMember?.title && (
					<Name>{staffMember?.title}</Name>
				)}
				{staffFields?.role && <Role>{staffFields?.role}</Role>}
			</Body>
		</ShowroomCard>
	) : null;
};

export default Card;
