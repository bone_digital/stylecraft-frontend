import styled from 'styled-components';
import { breakpoint, ease, swatch } from '../../../styles/theme';
import Grid from '../../common/Grid';
import Hotspot from '../../common/Hotspot';
import InnerWrapper from '../../common/InnerWrapper';
import { Content } from '../../common/WYSWYG/index';
import LinkButton from '../../common/LinkButton';
import InviewWrapper from '../../common/InviewWrapper';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const TextColumn = styled.div`
	grid-column: span 4;
	position: relative;

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
		margin-bottom: 50px;
	}

	> div {
		& + a {
			margin-top: 12px;
		}

		p {
			max-width: 320px;
		}
	}
`;

const TextInner = styled(Content)``;

const HotspotColumn = styled.div`
	grid-column: span 8;

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}

	ul {
		padding-top: 36px;
	}
`;

const Title = styled.h1``;

const BodyCopy = styled.div``;

export default function index({
	title,
	copy,
	linkButton,
	hotspotGallery,
	backgroundColor,
	anchorId,
}) {
	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<InnerWrapper>
				<Grid>
					<TextColumn>
						<TextInner>
							{title && <Title>{title}</Title>}
							{
								<BodyCopy
									dangerouslySetInnerHTML={{ __html: copy }}
								/>
							}
						</TextInner>

						{linkButton && (
							<LinkButton
								prefix={linkButton?.prefix}
								title={linkButton?.linkInner?.title}
								href={linkButton?.linkInner?.url}
								target={linkButton?.linkInner?.target}
							/>
						)}
					</TextColumn>
					<HotspotColumn>
						{hotspotGallery && (
							<Hotspot
								ratio={'64%'}
								ratioMobile={'64%'}
								content={hotspotGallery}
							/>
						)}
					</HotspotColumn>
				</Grid>
			</InnerWrapper>
		</InviewWrapper>
	);
}


const LinkInner = styled.a`
	display: inline-flex;
	position: relative;
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	transition: color 280ms ease;

	&:hover {
		color: ${swatch('brand')};
	}

	&::before {
		content: '';
		position: absolute;
		bottom: 0px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${swatch('black')};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${LinkInner}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const LinkButtonTitle = styled.h5`
	text-transform: ${({ uppercase }) =>
		uppercase ? 'uppercase' : 'capitalize'};
	letter-spacing: 0.025em;
`;



