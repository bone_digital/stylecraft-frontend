import styled, { css } from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper/index';
import Grid from '../../common/Grid/index';
import ShowroomCard from '../../elements/ShowroomCard';
import BrandCard from '../../common/BrandCard';
import PostCard from '../../common/PostCard';
import OverlapHeading from '../../common/OverlapHeading';
import ViewMore from '../../common/ViewMore';
import InviewWrapper from '../../common/InviewWrapper';

const Row = styled.div`
	position: relative;
`;

export default function index(props) {
	const { backgroundColor, columnsPerRow, linkButton, title, posts, anchorId } =
		props || {};

	const columnSize = columnsPerRow === 3 ? 'small' : 'large';

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<InnerWrapper>
				{title && <OverlapHeading title={title} />}

				<Row>
					{linkButton?.url && (
						<ViewMore
							title={linkButton?.title}
							url={linkButton?.url}
						/>
					)}

					<Grid rowGap={'82px'} rowGapMobile={'45px'}>
						{posts &&
							posts.map((post, index) => {
								const data = post?.post;
								const type = `${data?.__typename}`;

								if (type === 'Showroom') {
									return (
										<ShowroomCard
											size={columnSize}
											post={data}
											key={index}
										/>
									);
								} else if (type === 'Brand') {
									return (
										<BrandCard
											size={posts?.length === 2 && index === 1 ? 'large' : 'small'}
											columnsPerRow={columnsPerRow}
											post={data}
											key={index}
										/>
									);
								} else {
									return (
										<PostCard
											size={columnSize}
											type={type}
											post={data}
											key={index}
										/>
									);
								}
							})}
					</Grid>
				</Row>
			</InnerWrapper>
		</InviewWrapper>
	);
}
