import styled from 'styled-components';
import OverlapHeading from '../../common/OverlapHeading';
import InnerWrapper from '../../common/InnerWrapper';
import SwiperCore, { A11y, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import ViewMore from '../../common/ViewMore';
import BrandCard from '../../common/BrandCard';
import ShowroomCard from '../../elements/ShowroomCard';
import PostCard from '../../common/PostCard';
import { useEffect, useRef } from 'react';
import InviewWrapper from '../../common/InviewWrapper';
import { useState } from 'react';

const defaultNavHeight = '100%';

const Row = styled.div`
	position: relative;
`;

const Gallery = styled.div`
	position: relative;
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 10%;
	height: ${({ height }) => height || defaultNavHeight};
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;
`;

SwiperCore.use([Navigation, A11y]);

const PostCarousel = (props) => {
	const { backgroundColor, posts, title, linkButton, anchorId } = props;

	const prevRef = useRef();
	const nextRef = useRef();

	const [navHeight, setNavHeight] = useState(defaultNavHeight);

	// set navHeight based on the first slide
	const onSliderResize = (swiper) => {
		const firstSlide = swiper?.slides[0];
		if (firstSlide?.clientWidth) {
			// TODO: find a way to reference the thumbnail element or aspect ratio directly
			const ratio = 0.7563; // 'small' ratio - taken from various card components
			const height = firstSlide.clientWidth * ratio;

			setNavHeight(height + 'px');
		}
	};

	const [render, setRender] = useState(false);

	useEffect(() => {
		setRender(true);
	});

	return posts && posts.length ? (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<InnerWrapper>
				{title && <OverlapHeading title={title} />}

				<Row>
					{linkButton?.url && (
						<ViewMore
							title={linkButton?.title}
							url={linkButton?.url}
							target={linkButton?.target}
						/>
					)}

					<Gallery>
						{posts && posts?.length > 1 && (
							<>
								<NavElement
									data-cursor="true"
									className="cursor-link cursor-link--carousel-prev"
									direction="prev"
									ref={prevRef}
									height={navHeight}
								/>
								<NavElement
									data-cursor="true"
									className="cursor-link cursor-link--carousel-next"
									direction="next"
									ref={nextRef}
									height={navHeight}
								/>
							</>
						)}
						{render && (
							<Swiper
								enabled={posts?.length > 1}
								preventInteractionOnTransition={true}
								speed={400}
								loop={false}
								centeredSlides={false}
								spaceBetween={16}
								slidesPerView={1.21}
								onResize={onSliderResize}
								onAfterInit={onSliderResize}
								onUpdate={onSliderResize} // not 100% sure this is necessary
								breakpoints={{
									540: {
										slidesPerView:
											posts?.length > 1 ? 1.21 : 1,
										enabled: posts?.length > 1,
										spaceBetween: 16
									},
									768: {
										slidesPerView:
											posts?.length > 1 ? 1.21 : 1,
										enabled: posts?.length > 1,
										spaceBetween: 32
									},
									992: {
										slidesPerView: posts?.length > 2 ? 3 : 2,
										spaceBetween: 32,
										enabled: posts?.length > 2
									}
								}}
								onInit={(swiper) => {
									swiper.params.navigation.prevEl =
										prevRef.current;
									swiper.params.navigation.nextEl =
										nextRef.current;

									swiper.navigation.init();
									swiper.navigation.update();
								}}
							>
								{posts.map(({ post }, index) => {
									const type = `${post?.__typename}`;

									if (type === 'Showroom') {
										return (
											<SwiperSlide key={`post-${index}`}>
												<ShowroomCard
													size={'small'}
													post={post}
													key={index}
												/>
											</SwiperSlide>
										);
									} else if (type === 'Brand') {
										return (
											<SwiperSlide key={`post-${index}`}>
												<BrandCard
													size={'small'}
													post={post}
													key={index}
												/>
											</SwiperSlide>
										);
									} else {
										return (
											<SwiperSlide key={`post-${index}`}>
												<PostCard
													size={'small'}
													type={type}
													post={post}
													key={index}
												/>
											</SwiperSlide>
										);
									}
								})}
							</Swiper>
						)}
					</Gallery>
				</Row>
			</InnerWrapper>
		</InviewWrapper>
	) : null;
};

export default PostCarousel;
