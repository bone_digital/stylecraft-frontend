import styled from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import HalvesColumn from '../../common/HalvesColumn';
import InviewWrapper from '../../common/InviewWrapper/index';
import Grid from '../../common/Grid';
import PostFeaturedImage from '../../elements/PostFeaturedImage';
import ShowroomDetails from './ShowroomDetails';
import InnerWrapper from '../../common/InnerWrapper';
import OverlapHeading from '../../common/OverlapHeading';

const ShowroomHeader = ({ showroom, title }) => {
	const { backgroundColor, headingColor, gallery, columns } = showroom || {};

	const DetailsWrap = () => (
		<ShowroomDetails details={showroom} sectionTheme={backgroundColor} />
	);

	const FeaturedImageWrap = () => (
		<PostFeaturedImage gallery={gallery} title={title} />
	);

	return showroom ? (
		<Row>
			<Title headingColor={headingColor}>
				<InnerWrapper>
					<Grid>
						<TitleInner>
							<OverlapHeading title={title} post={false} />
						</TitleInner>
					</Grid>
				</InnerWrapper>
			</Title>
			<InviewWrapper sectionTheme={backgroundColor}>
				<Wrapper>
					<Body>
						<ThirdsColumn
							flipMobile={true}
							leftColumn={<DetailsWrap />}
							rightColumn={<FeaturedImageWrap />}
						/>

						{columns && columns.length && (
							<HalvesColumn columns={columns} />
						)}
					</Body>
				</Wrapper>
			</InviewWrapper>
		</Row>
	) : null;
};

const Wrapper = styled.div`
	padding-bottom: 100px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 65px;
	}
`;

const Title = styled.div`
	margin-bottom: 0;
	position: relative;
	z-index: 10;
	color: ${({ headingColor, theme }) =>
		headingColor ? theme.colors[headingColor] : theme.colors.black};

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const Row = styled.div`
	padding-top: 200px;
`;

const TitleInner = styled.div`
	grid-column: 5 / span 8;

	> div {
		max-width: 100%;
	}
`;

const Body = styled.div`
	> *:not(:last-child) {
		margin-bottom: 30px;
	}
`;

export default ShowroomHeader;
