import styled from 'styled-components';
import LinkButton from '../../common/LinkButton/index';
import WYSWYG, { Content } from '../../common/WYSWYG/index';
import { swatch, ease } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const PostIntro = styled.div`
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
	flex-direction: column;
	height: 100%;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		padding-bottom: 40px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 24px;
	}
`;

const Details = styled.div`
	margin-bottom: 40px;

	> *:not(:last-child) {
		margin-bottom: 1.333em;
	}
`;

const ContactMessage = styled.h3`
	position: relative;
	font-weight: 400;
	text-align: left;
	max-width: 350px;

	&::before {
		content: '';
		position: absolute;
		bottom: -4px;
		left: 0;
		height: 1px;
		max-width: 6rem;
		width: 100%;
		background: currentColor;
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	&:hover {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const Dot = styled.div`
	width: 16px;
	height: 16px;
	border-radius: 50%;
	display: block;
	background: ${({ theme, sectionTheme }) =>
		sectionTheme === 'pink' || sectionTheme === 'brand'
			? theme.colors.black
			: theme.colors.brand};
	margin-right: 22px;
	margin-top: 0.4em;
	flex-shrink: 0;
`;

const Text = styled.p`
	> a {
		text-decoration: none;
	}
`;

const Trigger = styled.a`
	position: relative;
	text-align: left;
	display: flex;
	align-items: flex-start;
`;

const BodyCopy = styled(Content)``;

const ShowroomDetails = ({ details, sectionTheme }) => {
	const {
		address,
		formattedAddress,
		openingHours,
		phone,
		fax,
		email,
		linkButton
	} = details || {};

	const removeSymbols = (string) => {
		if (!string) {
			return string;
		}
		return string.replace(/\(0\)|\s+/g, '');
	};

	const removeSpace = (string) => {
		if (!string) {
			return string;
		}
		return string.replace(' ', '');
	};

	const convertMapLink = (string) => {
		const mapString = string.replace(' ', '+');
		return `https://www.google.com/maps/search/?api=1&query=${mapString}`;
	};

	const { toggleFormMenu } = useUI();

	const openMenu = () => {
		toggleFormMenu(true);
	};

	return (
		<PostIntro>
			<Details>
				{formattedAddress && address && (
					<BodyCopy>
						<Trigger
							dangerouslySetInnerHTML={{
								__html: formattedAddress
							}}
							href={convertMapLink(address?.streetAddress)}
							target="blank"
						/>
					</BodyCopy>
				)}

				<BodyCopy>
					<Text>
						{phone && (
							<Trigger
								href={`tel:${removeSymbols(phone)}`}
								title={'Phone us'}
							>
								{phone}
							</Trigger>
						)}
						{fax && (
							<Trigger
								href={`fax:${removeSpace(fax)}`}
								title={'Fax us'}
							>
								{fax}
							</Trigger>
						)}
						{email && (
							<Trigger
								href={`mailto:${removeSpace(email)}`}
								title={'Email us'}
							>
								{email}
							</Trigger>
						)}
					</Text>
				</BodyCopy>

				{openingHours && (
					<BodyCopy
						dangerouslySetInnerHTML={{
							__html: `Opening Hours<br/>${openingHours}`
						}}
					/>
				)}

				{linkButton && linkButton?.linkInner && (
					<LinkButton
						prefix={linkButton?.prefix}
						href={linkButton?.linkInner?.url}
						title={linkButton?.linkInner?.title}
						target={linkButton?.linkInner?.target}
					/>
				)}
			</Details>

			<Trigger onClick={openMenu}>
				<Dot sectionTheme={sectionTheme} />
				<ContactMessage>

						Contact our showroom

				</ContactMessage>
			</Trigger>
		</PostIntro>
	);
};

export default ShowroomDetails;
