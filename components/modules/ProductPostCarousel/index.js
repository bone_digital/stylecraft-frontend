import PostCarousel from '../PostCarousel';

const ProductPostCarousel = ({ relatedProducts }) => {

	const posts = relatedProducts?.map((post) => {
		return {
			__typename: post.type,
			post: post
		};
	})

	return posts ? (
		<PostCarousel posts={posts} title={'Related Products'} />
	) : null;
};

export default ProductPostCarousel;
