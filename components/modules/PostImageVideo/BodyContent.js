import styled from 'styled-components';
import ReactPlayer from 'react-player';
import Grid from '../../common/Grid';
import HalvesColumn from '../../common/HalvesColumn';
import { Content } from '../../common/WYSWYG';
import LinkButton from '../../common/LinkButton';
import PostFeaturedImage from '../../elements/PostFeaturedImage/index';
import OverlapHeading from '../../common/OverlapHeading/index';
import VideoPlayer from '../../common/VideoPlayer/index';

const Body = styled.div`
	direction: ltr;
`;

const GridColumn = styled.div`
	grid-column: span 4;
	display: block;
	border: 0px solid transparent;

	> a,
	> button {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;

		> a {
			margin-top: 20px;
		}

		&:first-child {
			margin-bottom: 36px;
		}
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		&:first-child {
			margin-bottom: 18px;
		}
	}
`;

const ImageVideoWrapper = styled.div`
	margin-bottom: ${({ hasColumns }) => (hasColumns ? '40px' : '0')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: ${({ hasColumns }) => (hasColumns ? '30px' : '0')};
	}
`;

const Row = styled.div`
	grid-column: -1 / 1;
`;

const Heading = styled.div`
	display: block;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const VideoWrapper = styled.div`
	> div::before {
		padding-top: 51.75%;

		@media ${({ theme }) => theme.breakpoints.desktop} {
			padding-top: 60%;
		}

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 51.75%;
		}
	}
`;

const Copy = styled(Content)``;

const BodyContent = ({ title, type, gallery, video, columns, flipMobile }) => {
	return (
		<Body flipMobile={flipMobile}>
			<ImageVideoWrapper hasColumns={columns && columns.length}>
				{type === 'video' ? (
					<VideoWrapper>
						<VideoPlayer video={video} title={title} />
					</VideoWrapper>
				) : (
					<PostFeaturedImage gallery={gallery} title={title} />
				)}
			</ImageVideoWrapper>

			{columns && columns.length && (
				<Row>
					<Grid columns={8} rowGapMobile={0}>
						{columns.map(({ copy, linkButton }, index) => (
							<GridColumn key={index}>
								{copy && (
									<Copy
										dangerouslySetInnerHTML={{
											__html: copy
										}}
									/>
								)}
								{linkButton && linkButton?.linkInner && (
									<LinkButton
										prefix={linkButton?.prefix}
										href={linkButton?.linkInner?.url}
										title={linkButton?.linkInner?.title}
										target={linkButton?.linkInner?.target}
									/>
								)}
							</GridColumn>
						))}
					</Grid>
				</Row>
			)}
		</Body>
	);
};

export default BodyContent;
