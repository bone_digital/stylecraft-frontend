import styled, { css } from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import PortraitImage from '../../common/PortraitImage';
import BodyContent from './BodyContent';
import IntroCopy from '../../elements/IntroCopy';
import InviewWrapper from '../../common/InviewWrapper/index';
import InnerWrapper from '../../common/InnerWrapper/index';
import Grid from '../../common/Grid/index';
import OverlapHeading from '../../common/OverlapHeading/index';
import ImageCaption from '../../common/ImageCaption';

const HeadingRow = styled.div`
	display: block;
	z-index: 5;
	position: relative;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const Heading = styled.div`
	grid-column: 5 / span 8;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: -1 / 1;
	}
`;

const IntroCopyWrap = styled.div`
	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		> a {
			margin-top: 20px;
		}
	}
`;

const ImageWrapper = styled.div`
	display: flex;
	width: 100%;
`;

const PostImageVideo = (props) => {
	const {
		alignment,
		backgroundColor,
		anchorId,
		copy,
		columns,
		gallery,
		leftType,
		leftImage,
		imageCaption,
		linkButton,
		rightType,
		title,
		video
	} = props || {};

	const LeftColumn = () => (
		<>
			{leftType === 'image' ? (
				<ImageWrapper>
					{leftImage && (
						<PortraitImage
							alignment={alignment}
							image={leftImage}
							caption={imageCaption}
						/>
					)}
				</ImageWrapper>
			) : (
				<IntroCopyWrap>
					{copy && (
						<IntroCopy
							copy={copy}
							flipMobile={leftType === 'image'}
							linkButton={linkButton}
						/>
					)}
				</IntroCopyWrap>
			)}
		</>
	);

	const RightColumn = () => (
		<BodyContent
			type={rightType}
			gallery={gallery}
			video={video}
			columns={columns}
			title={title}
			flipMobile={leftType === 'image'}
		/>
	);

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			{title && (
				<HeadingRow flipMobile={leftType === 'image'}>
					<InnerWrapper>
						<Grid>
							<Heading>
								<OverlapHeading title={title} />
							</Heading>
						</Grid>
					</InnerWrapper>
				</HeadingRow>
			)}

			<ThirdsColumn
				flipMobile={leftType === 'image'}
				leftColumn={<LeftColumn />}
				rightColumn={<RightColumn />}
			/>
		</InviewWrapper>
	);
};

export default PostImageVideo;
