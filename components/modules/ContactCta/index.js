import styled from 'styled-components';
import InviewWrapper from '../../common/InviewWrapper';
import InnerWrapper from '../../common/InnerWrapper/index';
import Grid from '../../common/Grid';
import { breakpoint, swatch, ease } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const ContactMessage = styled.h3`
	position: relative;
	font-weight: 400;
	text-align: left;
`;

const ContactMessageHover = styled.span`
	position: relative;
	&::before {
		content: '';
		position: absolute;
		bottom: -4px;
		left: 0;
		height: 1px;
		width: 100%;
		background: ${swatch('black')};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}
	&:hover {
		&::before {
		transform: scaleX(0);
		transform-origin: right;
		}
	}
`;

const Dot = styled.div`
	width: 16px;
	height: 16px;
	border-radius: 50%;
	display: block;
	background: ${swatch('brand')};
	margin-right: 22px;
	margin-top: 0.4em;
	flex-shrink: 0;
`;

const Trigger = styled.button`
	position: relative;
	grid-column: span 6;
	text-align: left;
	display: flex;
	align-items: flex-start;

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

export default function ContactCta({ backgroundColor, copy }) {
	const { toggleFormMenu, anchorId } = useUI() || {};

	const openMenu = () => {
		toggleFormMenu(true);
	};

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<InnerWrapper>
				<Grid>
					<Trigger onClick={openMenu}>
						<Dot />
						<ContactMessage>
							{copy ? (
								<span
									dangerouslySetInnerHTML={{ __html: copy }}
								/>
							) : (
								<span>
									Cant find what you are looking for?
									<br />
									Get in <ContactMessageHover>contact.</ContactMessageHover>
								</span>
							)}
						</ContactMessage>
					</Trigger>
				</Grid>
			</InnerWrapper>
		</InviewWrapper>
	);
}
