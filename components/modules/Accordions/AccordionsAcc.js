import styled from 'styled-components';
import { breakpoint, swatch, theme } from '../../../styles/theme';
import { Content } from '../../common/WYSWYG';
import { useState } from 'react';
import { motion } from 'framer-motion';
import { slugify } from '../../../lib/source/wordpress/utils';
import Darr from '../../../public/icons/darr.svg';

const variants = {
	hidden: {
		opacity: 0,
		height: 0
	},
	visible: {
		opacity: 1,
		height: 'auto'
	}
};

export default function AccordionsAcc({ acc }) {
	const [open, setOpen] = useState();
	const id = slugify(acc.title);

	return (
		<>
			<Button
				type="button"
				onClick={() => setOpen(!open)}
				ariaExpanded={open}
				ariaControls={id}
				id={`${id}-title`}
			>
				{acc.title}
				<Arrow $open={open} />
			</Button>
			<ContentWrap
				variants={variants}
				animate={open ? 'visible' : 'hidden'}
				ariaExpanded={open}
				id={id}
				aria-labelledby={`${id}-title`}
			>
				<ContentInner>
					<Content
						dangerouslySetInnerHTML={{ __html: acc.content }}
					/>
				</ContentInner>
			</ContentWrap>
		</>
	);
}

const Button = styled.button`
	font-size: ${({ theme }) => theme.type.h3[0]};
	line-height: ${({ theme }) => theme.type.h3[1]};
	display: flex;
	align-items: center;
	text-align: left;
	justify-content: space-between;
	width: 100%;
`;

const ContentWrap = styled(motion.div)`
	overflow: hidden;
`;

const ContentInner = styled.div`
	padding-top: 1rem;
`;

const Arrow = styled(Darr)`
	display: block;
	width: 1.25rem;
	height: auto;
	color: ${swatch('pink')};
	margin-left: 1rem;
	flex-shrink: 0;
	${({ $open }) => $open && `transform: rotate(0.5turn);`}
	transition: transform 0.25s;

	@media ${breakpoint('tablet')} {
		width: 1rem;
	}
`;
