import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';
import InviewWrapper from '../../common/InviewWrapper';
import AccordionsAcc from './AccordionsAcc';

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const Column = styled.div`
	width: 100%;
`;

const TextColumn = styled.div`
	grid-column: span 12;

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const Title = styled.h1`
	margin-bottom: 3rem;

	@media ${breakpoint('tablet')} {
		margin-bottom: 1.5rem;
	}
`;

const AccordionItem = styled.li`
	border-bottom: 1px solid ${swatch('grey')};
	padding-block: 1rem;
`;

export default function Accordions({
	backgroundColor,
	anchorId,
	title,
	accordions
}) {
	const accordionsLength = accordions.length;

	if (!accordionsLength) {
		return null;
	}

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Row>
				<Column>
					<InnerWrapper>
						<Grid
							rowGap={'0'}
							rowGapMobile={'40px'}
							columns={13}
							columnsMobile={12}
						>
							<TextColumn>
								<Title>{title}</Title>
								<ul>
									{accordions.map((acc, i) =>
										<AccordionItem key={i}>
											<AccordionsAcc acc={acc} />
										</AccordionItem>
									)}
								</ul>
							</TextColumn>
						</Grid>
					</InnerWrapper>
				</Column>
			</Row>
		</InviewWrapper>
	);
}
