import styled from 'styled-components';
import { breakpoint, ease, swatch } from '../../../styles/theme';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const Item = styled.div`
	display: flex;
`;

const ItemMobile = styled(Item)`
	&&& {
		display: none;

		@media ${breakpoint('laptop')} {
			display: flex;
		}
	}
`;

const ItemDesktop = styled(Item)`
	&&& {
		display: flex;

		@media ${breakpoint('laptop')} {
			display: none;
		}
	}
`;

const LinkInner = styled.a`
	position: relative;
	color: ${({ active }) => (active ? swatch('brand') : swatch('black'))};
	transition: ${ease('color', 'fast')};

	&:hover {
		color: ${swatch('brand')};
	}

	@media ${breakpoint('tablet')} {
		color: ${({ active }) => (active ? swatch('black') : swatch('black'))};
	}

	&::before {
		content: '';
		display: inline-block;
		width: 8px;
		height: 8px;
		background: ${swatch('brand')};
		position: absolute;
		top: 50%;
		left: -10px;
		transform: ${({ active }) =>
			active
				? 'translate(-100%, -50%) scale(1)'
				: 'translate(calc(-100% + 10px), -50%) scale(0)'};
		border-radius: 50%;
		opacity: ${({ active }) => (active ? 1 : 0)};
		transition: ${ease('all', 'fast')};

		@media ${breakpoint('tablet')} {
			display: none;
		}
	}
`;

const Title = styled.h3`
	line-height: inherit;
	line-height: 2.08em;
`;

const LinkItem = ({ onHover, active, brand }) => {
	return brand ? (
		<>
			<ItemMobile>
				<Link href={searchReplaceLink(brand?.link)} passHref>
					<LinkInner active={active} title={brand?.title}>
						<Title>{brand?.title}</Title>
					</LinkInner>
				</Link>
			</ItemMobile>
			<ItemDesktop>
				<Link href={searchReplaceLink(brand?.link)} passHref>
					<LinkInner
						active={active}
						onMouseEnter={onHover}
						title={brand?.title}
					>
						<Title>{brand?.title}</Title>
					</LinkInner>
				</Link>
			</ItemDesktop>
		</>
	) : null;
};

export default LinkItem;
