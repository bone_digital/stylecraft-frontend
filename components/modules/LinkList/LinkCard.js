import styled from 'styled-components';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import LinkButton from '../../common/LinkButton';
import ImageBg from '../../common/Thumbnail';
import { useState } from 'react';
import { ease } from '../../../styles/theme';
import { useEffect } from 'react';

const ColumnInner = styled.div`
	width: 100%;
	grid-column: span 4;
	position: relative;
	opacity: ${({ isLoaded }) => (isLoaded ? 1 : 0)};
	transition: ${ease('all', '200ms')};
`;

const Card = styled.div`
	width: 100%;
`;

const Thumbnail = styled.div`
	margin-bottom: 36px;
`;

const Content = styled.h3`
	margin-bottom: 20px;
`;

const LinkWrapper = styled.a``;

const LinkCard = ({ brand, active }) => {
	const [isLoaded, setIsLoaded] = useState(false);

	const { brand: post, cardImage } = brand || {};
	const featuredImage = brand?.brand?.featuredImage?.node || null;

	const image = cardImage !== null ? cardImage : featuredImage;

	useEffect(() => {
		return () => setIsLoaded(false);
	}, []);

	return brand && image && active ? (
		<ColumnInner isLoaded={image && active ? isLoaded : true}>
			<Card>
				{image && (
					<Thumbnail>
						<ImageBg
							image={image?.sourceUrl}
							thumbnail={image?.thumbSourceUrl}
							alt={image?.altText}
							setStateCallback={setIsLoaded}
						/>
					</Thumbnail>
				)}

				{post?.brand?.excerpt && (
					<Link href={searchReplaceLink(post?.link, false)} passHref>
						<LinkWrapper>
							<Content
								dangerouslySetInnerHTML={{
									__html: post?.brand?.excerpt
								}}
							/>
						</LinkWrapper>
					</Link>
				)}

				<LinkButton title="More" href={post?.link} />
			</Card>
		</ColumnInner>
	) : null;
};

export default LinkCard;
