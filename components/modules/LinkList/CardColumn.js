import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';
import LinkCard from './LinkCard';
import { AnimatePresence, motion } from 'framer-motion';
import React from 'react';

const CardWrap = styled.div`
	position: relative;
	display: block;
	grid-column: span 4;

	@media ${breakpoint('laptop')} {
		display: none;
	}
`;

const MotionCard = styled(motion.div)`
	width: 100%;
`;

const CardColumn = ({ active, brands }) => {
	const cardVariants = {
		open: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		},
		close: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<CardWrap>
			<AnimatePresence initial={true}>
				{brands &&
					brands?.length &&
					brands.map((brand, index) => {
						return active === index ? (
							<MotionCard
								key={`link-card-${index}`}
								variants={cardVariants}
								animate={'open'}
								initial={'close'}
								exit={'close'}
							>
								<LinkCard
									active={active === index}
									index={index}
									brand={brand}
								/>
							</MotionCard>
						) : null;
					})}
			</AnimatePresence>
		</CardWrap>
	);
};

export default CardColumn;
