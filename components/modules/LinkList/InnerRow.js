import styled, { props } from 'styled-components';
import { breakpoint } from '../../../styles/theme';
import InnerWrapper from '../../common/InnerWrapper';
import Grid, { Container } from '../../common/Grid';
import ListColumn from './List';
import CardColumn from './CardColumn';
import { useState } from 'react';
import { motion } from 'framer-motion';

const ListWrap = styled(Container)`
	grid-column: span 8;
	grid-auto-flow: column;
	grid-template-rows: repeat(
		${(props) => Math.ceil(props?.children?.props?.brands?.length / 2)},
		minmax(0, 1fr)
	);

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}
`;

const Row = styled.div`
	ul,
	div {
		align-items: start;
	}
`;

const MotionList = motion(ListWrap);

const InnerRow = ({ brands }) => {
	const [activeIndex, setActiveIndex] = useState(0);

	return (
		<Row>
			<InnerWrapper>
				<Grid autoRows={false}>
					<MotionList as="ul">
						<ListColumn
							onHover={setActiveIndex}
							active={activeIndex}
							sectionActive={true}
							brands={brands}
						/>
					</MotionList>

					<CardColumn brands={brands} active={activeIndex} />
				</Grid>
			</InnerWrapper>
		</Row>
	);
};

export default InnerRow;
