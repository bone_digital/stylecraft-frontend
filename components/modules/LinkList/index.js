import InviewWrapper from '../../common/InviewWrapper';
import InnerRow from './InnerRow';

const LinkList = ({ brands, anchorId }) => {
	return (
		<InviewWrapper id={anchorId}>
			<InnerRow brands={brands} />
		</InviewWrapper>
	);
};

export default LinkList;
