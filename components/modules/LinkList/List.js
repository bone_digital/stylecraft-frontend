import styled from 'styled-components';
import { motion } from 'framer-motion';
import LinkItem from './LinkItem';
import { breakpoint } from '../../../styles/theme';
import { useRouter } from 'next/router';

const ListItemWrap = styled.li`
	display: block;
	grid-column: span 6;

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const MotionItem = motion(ListItemWrap);

const ListColumn = ({ onHover, active, sectionActive, brands }) => {
	const router = useRouter();
	const locale = router.locale;

	const variants = {
		visible: (index) => ({
			opacity: 1,
			transform: 'translateY(0%)',
			transition: {
				delay: 0.1 + index * 0.05
			}
		}),
		hidden: {
			opacity: 0,
			transform: 'translateY(20px)'
		}
	};

	const hasBrands = !!brands?.length;

	return (
		<>
			{
				hasBrands &&
				brands.map(({ brand }, index) => {
					return (
						localesMatch(brand?.brand?.locales, locale) &&
							<MotionItem
								key={index}
								custom={index}
								animate={sectionActive ? 'visible' : 'hidden'}
								initial="hidden"
								variants={variants}
							>
								<LinkItem
									onHover={() => onHover(index)}
									brand={brand}
									active={active === index}
								/>
							</MotionItem>
					)
				})}
		</>
	);
};

export default ListColumn;


const localesMatch = (productLocale, currentLocale) => {
	if (
		productLocale === 'all' ||
		productLocale === 'au' && currentLocale === 'en-AU' ||
		productLocale === 'sg' && currentLocale === 'en-SG'
		) {
		return true
	} else {
		return false
	}
}
