import styled from 'styled-components';
import Darr from '../../../public/icons/darr.svg';

const Qty = styled.div`
	flex: 1;
	display: flex;
	flex-wrap: wrap;
`;

const QtySelect = styled.input`
	width: 40px;
	appearance: none;
	-moz-appearance: textfield;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	padding: 0 0 2px 0;

	&::-webkit-outer-spin-button,
	&::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
`;

const Available = styled.div`
	display: inline-flex;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		width: 100%;
		margin-top: 8px;
	}
`;

const InputWrapper = styled.div`
	display: flex;
	align-items: center;
	margin-right: 40px;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		margin-right: 0;
	}
`;

const QtyIcon = styled.button`
	padding: 5px 10px 10px 5px;

	&:first-child {
		padding: 10px 10px 5px 5px;

		> * {
			transform: rotate(180deg);
		}
	}

	&:last-child {
		padding-top: 5px;
	}
`;

const ButtonWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding-bottom: 1px;
	margin-left: 7px;

	svg {
		width: 100%;
		height: auto;
	}
`;

const QuantityPicker = ({ 
	quantity, 
	setQuantity = () => {}, 
	stock, 
	showStock = true, 
	increaseQuantity = () => {}, 
	decreaseQuantity = () => {}, 
}) => {
	const addQuantity = () => {
		increaseQuantity();
		setQuantity(quantity + 1);
	};

	const reduceQuantity = () => {
		decreaseQuantity();
		setQuantity(quantity - 1 <= 0 ? 1 : quantity - 1);
	};

	const handleQuantity = (value) => {
		if( value <= 0 )
		{
			value = 1;
		}
		setQuantity(value);
	};

	return (
		<Qty>
			<InputWrapper>
				<QtySelect
					type="number"
					min="1"
					max={stock || 10}
					value={quantity}
					onChange={(e) => {
						handleQuantity(e.target.value);
					}}
				/>
				<ButtonWrapper>
					<QtyIcon onClick={addQuantity}>
						<Darr />
					</QtyIcon>

					<QtyIcon onClick={reduceQuantity}>
						<Darr />
					</QtyIcon>
				</ButtonWrapper>
			</InputWrapper>
			{stock && showStock && (
				<Available>{`${stock} Available`} </Available>
			)}
		</Qty>
	);
};

export default QuantityPicker;
