import styled from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';
import Grid from '../../common/Grid';
import { Content } from '../../common/WYSWYG/index';
import LinkButton from '../../common/LinkButton/index';
import { useState, useEffect, useMemo } from 'react';
import QuantityPicker from './QuantityPicker';
import VariationPicker from './VariationPicker';
import ProductShowHide from './ProductShowHide';
import AddToCartButton from '../../common/AddToCartButton';
import AddToWishList from '../../elements/AddToWishlist';
import { useUI } from '../../../context/UIProvider';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import { shopPrefix } from '../../../lib/settings';
import Button from '../../common/Button';
import { useRouter } from 'next/router';
import { CleanPricingString, GenerateLeadTimeLabel, GetWholesalePricingLabel } from "../../../utils/product";
import { swatch } from '../../../styles/theme';
import { formatPriceWithoutDecimals } from '../../../lib/source/wordpress/utils';

const Details = ({ product, isWholeseller, sectionTheme }) => {
	const [quantity, setQuantity] = useState(1);
	const [shared, setShared] = useState(false);
	const [descriptionActive, setDescriptionActive] = useState(false);
	const [variationTitles, setVariationTitles] = useState(null);
	const [productVariations, setProductVariations] = useState(null);
	const [variationOptions, setVariationOptions] = useState(null);
	const [selectedVariations, setSelectedVariations] = useState({});
	const [activeVariation, setActiveVariation] = useState(null);
	const [showPrice, setShowPrice] = useState(true);
	const [enquireOnly, setEnquireOnly] = useState(false);
	const [showVariations, setShowVariations] = useState(true);

	const router = useRouter();
	const {
		name: productName,
		product: productFields,
		__typename: type,
		variations,
		purchasable,
		attributes,
		stockStatus,
		ranges,
		leadtimes,
		wholesalePrice,
		link: shareLink
	} = product || {};

	const productAttributes = attributes?.edges;
	const productRanges = ranges?.edges;
	const productLeadtimes = leadtimes?.edges;
	const locale = productFields?.locale;
	const wholesalePricing =
		wholesalePrice && wholesalePrice !== null
			? JSON.parse(wholesalePrice)
			: null;

	const {
		designer,
		brand,
		description,
		excerpt,
		pricingOptions,
		startingPrice,
	} = productFields || {};

	const handleToggle = () => {
		setDescriptionActive(!descriptionActive);
	};

	const updateVariation = (key, value) => {
		// get current variation values.
		const selections = Object.assign({}, selectedVariations);

		// update with new value, remove product property.
		selections[key] = value;
		setSelectedVariations(selections);

		// search productVariations to find one that matches new values.
		// look for full match first, then partial match.
		const selected =
			productVariations.find((product) => {
				const foundProduct = Object.keys(product).every(function(k) {
					if (k === 'node' || product[k] === selections[k]) {
						// 'node' is the product object, so it's skipped.
						return true
					} else {
						return false
					}
				})
				return foundProduct
			})
			||
			productVariations.find((product) => {
				const foundProduct = Object.keys(product).every(function(k) {
					if (k === 'node' || product[k] === selections[k] || product[k] === '') {
						// 'node' is the product object, so it's skipped.
						return true
					} else {
						return false
					}
				})
				return foundProduct
			}
		)

		setActiveVariation(selected);
	}

	// Some products have yet to select value, default to standard
	const pricingType = pricingOptions !== null ? pricingOptions : 'standard';
	const wholesalePriceCategory = GetWholesalePricingLabel(productFields?.wholesalePrice);

	// Get the pricing label incase its needed
	let priceForRetailPricingLabel = CleanPricingString(product?.regularPrice) || 0;

	if( variations )
	{
		// Check if it's a variable product and if so find the lowest price
		variations?.edges?.forEach(item => {
			const newPrice = CleanPricingString(item?.node?.regularPrice);
			if( newPrice > priceForRetailPricingLabel || priceForRetailPricingLabel <= 0 )
			{
				priceForRetailPricingLabel = newPrice;
			}
		});
	}
	const retailPriceCategory = GetWholesalePricingLabel(priceForRetailPricingLabel);
	let leadTimeLabel = GenerateLeadTimeLabel(productFields?.leadTime);
	if( !leadTimeLabel && productLeadtimes && productLeadtimes[0]?.node?.name )
	{
		leadTimeLabel = productLeadtimes[0].node.name;
	}

	useEffect(() => {
		if (
			(pricingType === 'wholesale' && isWholeseller) ||
			(pricingType === 'non-wholesale' && !isWholeseller) ||
			pricingType === 'enquire-only'
		) {
			setShowPrice(false);
			setShowVariations(false);
			setEnquireOnly(true);
		} else {
			setShowPrice(true);
			setShowVariations(true);
			setEnquireOnly(false);
		}

		if (pricingType === 'standard' && activeVariation !== null) {
			const variationPricingOpt = activeVariation?.node?.pricingOptions;

			if (showPrice) {
				setShowVariations(true);
			}

			if (variationPricingOpt) {
				if (variationPricingOpt === 'wholesale' && isWholeseller) {
					setEnquireOnly(true);
				} else if (
					variationPricingOpt === 'non-wholesale' &&
					!isWholeseller
				) {
					setEnquireOnly(true);
				} else if (variationPricingOpt === 'enquire-only') {
					setEnquireOnly(true);
				} else {
					setEnquireOnly(false);
				}
			}
		}
		// TODO: short term fix for golive. need to refactor this.
		if (pricingType === 'wholesale') {
			setEnquireOnly(true);
		}
		else if (pricingType === 'standard') {
			if (showPrice) {
				setShowVariations(true);
			}
		}

		if (router.locale !== 'en-AU' || isWholeseller) {
			setEnquireOnly(true);
		}

	}, [activeVariation, isWholeseller, pricingType]);

	useEffect(() => {
		if (product !== null) {
			if (variations?.edges !== null && variations?.edges?.length) {
				const availableVariations = variations?.edges?.map(singleVariation => {
					const currentVar = {}
					singleVariation?.node?.attributes?.edges?.forEach(option => {
						currentVar[option?.node?.label] = option?.node?.value;
					})
					currentVar.node = singleVariation?.node;
					return currentVar;
				});

				const productOptions = {};

				availableVariations?.forEach(singleAvailableVariation => {
					for (const [key, value] of Object.entries(singleAvailableVariation)) {
						if (productOptions[key] === undefined && value) {
							productOptions[key] = [value]
						} else {
							if (productOptions[key]?.indexOf(value) < 0 && value) {
								productOptions[key].push(value);
							}
						}
					}
				})

				setVariationOptions(productOptions);
				setActiveVariation(availableVariations[0]);
				setSelectedVariations(availableVariations[0]);
				setProductVariations(availableVariations);
			}

			if (productAttributes !== null && productAttributes?.length) {
				const attributes = productAttributes.map(attribute => attribute?.node?.name);
				setVariationTitles(attributes);
			}
		}
	}, [product]);

	const stock = useMemo(() => {
		return type === 'VariableProduct'
			? activeVariation?.node?.stockQuantity
			: product?.stockQuantity;
	}, [type, activeVariation]);

	const price = useMemo(() => {
		var formatter = new Intl.NumberFormat('en-US', {
			style: 'currency',
			currency: 'USD'
		});

		if (type === 'VariableProduct') {
			const isOnSale = activeVariation?.node?.onSale || false;

			const variationId = activeVariation?.node?.variationId;

			if (
				isWholeseller &&
				wholesalePricing &&
				wholesalePricing[variationId] !== null
			) {
				const wholesalePrice =
					wholesalePricing[variationId]?.wholesaleprice;
				return formatter.format(wholesalePrice);
			}

			return isOnSale
				? activeVariation?.node?.salePrice
				: activeVariation?.node?.regularPrice;
		} else {
			if (isWholeseller && wholesalePricing?.wholesale_price) {
				return formatter.format(wholesalePrice);
			}

			return product?.onSale ? product?.salePrice : product?.regularPrice;
		}
	}, [type, activeVariation, isWholeseller]);

	const { toggleFormMenu } = useUI() || {};

	const formData = {
		formProductRequestField: product?.name,
	}

	const openMenu = () => {
		toggleFormMenu(true, formData);
	};

	const handleShare = () => {
		navigator.clipboard.writeText(shareLink);
		setShared(true);

		setTimeout(() => {
			setShared(false);
		}, 1000);
	};

	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return (
		<AnimatePresence exitBeforeEnter initial={false}>
			<DetailsRow>
				<Grid columns={9} rowGap={'48px'} rowGapMobile={'48px'}>
					<LeftColumn>
						{excerpt && (
							<ProductCopy
								dangerouslySetInnerHTML={{ __html: excerpt }}
							/>
						)}

						{description && (
							<ProductShowHide
								handleToggle={handleToggle}
								active={descriptionActive}
							>
								<ProductCopy
									dangerouslySetInnerHTML={{
										__html: description
									}}
								/>
							</ProductShowHide>
						)}
					</LeftColumn>

					<RightColumn>
						<ProductDetails>
							{brand && (
								<Row>
									<Subtitle>Brand</Subtitle>
									<Body>
										<Link
											passHref
											href={searchReplaceLink(
												brand?.link
											)}
										>
											<LinkInner title={brand?.title}>
												{brand?.title}
											</LinkInner>
										</Link>
									</Body>
								</Row>
							)}

							{designer && (
								<Row>
									<Subtitle>Designer</Subtitle>
									<Body>{designer?.name}</Body>
								</Row>
							)}

							{productRanges && (
								<Row>
									<Subtitle>Range</Subtitle>
									<Body>
										<Link
											passHref
											href={`${shopPrefix}range/${productRanges[0]?.node?.slug}`}
										>
											<LinkInner
												title={
													productRanges[0]?.node?.name
												}
											>
												{productRanges[0]?.node?.name}
											</LinkInner>
										</Link>
									</Body>
								</Row>
							)}
							{showVariations ? (
								<>
									{
										(productRanges &&
										activeVariation &&
										activeVariation?.node?.leadTime)
											? (
												<Row>
													<Subtitle>Lead time</Subtitle>
													<Body>
														{
															GenerateLeadTimeLabel(activeVariation?.node?.leadTime)
														}
													</Body>
												</Row>
											)
											: (
												<Row>
													<Subtitle>Lead Time</Subtitle>
													<Body>
														{leadTimeLabel}
													</Body>
												</Row>
											)
									}
									{productVariations !== null &&
										productVariations?.length &&
											Object.entries(variationOptions).map(([label, values], i) => {

												if (label === 'node') {
													return null
												}

												return (
													<Row key={`variation-${label}`}>
														<Subtitle>
														{label
															.replace(/-/g, ' ')
															.split(' ')
															.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
														}
														</Subtitle>

														<VariationPicker
															options={{label: label, values: values}}
															// length -2 because we aren't including the 'node' property.
															variation={i === Object.entries(variationOptions).length - 2 && activeVariation}
															updateVariation={
																// setActiveVariation
																updateVariation
															}
															productName={productName}
														/>
													</Row>
												)
											})}
								</>
							) : (
								<>
									{leadTimeLabel && (
										<Row>
											<Subtitle>Lead Time</Subtitle>
											<Body>
												{leadTimeLabel}
											</Body>
										</Row>
									)}
								</>
							)}

							{showVariations && purchasable && !enquireOnly && (
								<Row
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
									key="qty"
								>
									<Subtitle>Qty</Subtitle>
									<QuantityPicker
										quantity={quantity}
										setQuantity={setQuantity}
										stock={stock}
									/>
								</Row>
							)}

							{showPrice && price && !enquireOnly && (
								<Row
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
									key="price"
								>
									<Subtitle>
										Price <small>(AUD)</small>
									</Subtitle>
									<Body>{formatPriceWithoutDecimals(price)}</Body>
								</Row>
							)}

							{wholesalePriceCategory && isWholeseller && enquireOnly && (
								<Row
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
									key="price"
								>
									<Subtitle>
										Price <small>(AUD)</small>
									</Subtitle>
									<Body>{wholesalePriceCategory}</Body>
								</Row>
							)}

							{showPrice && startingPrice && !enquireOnly && (
								<Row
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
									key="starting-price"
								>
									<Subtitle>
										Starting Price <small>(AUD)</small>
									</Subtitle>
									<Body>{`RRP $${formatPriceWithoutDecimals(startingPrice)}`}</Body>
								</Row>
							)}

							{retailPriceCategory && enquireOnly && !isWholeseller && (
								<Row
									variants={variants}
									initial={'hidden'}
									animate={'visible'}
									exit={'hidden'}
									key="price"
								>
									<Subtitle>
										Price <small>(AUD)</small>
									</Subtitle>
									<Body>{retailPriceCategory}</Body>
								</Row>
							)}
						</ProductDetails>

						{locale && router.locale !== 'en-AU' ? (
							<ExclusiveBtnWrapper>
								<p>
									This product is only available in Australia
								</p>

								<Button
									title="Product unavailable"
									type="button"
									color="black"
								/>
							</ExclusiveBtnWrapper>
						) : stockStatus !== 'IN_STOCK' || !showPrice ? (
							<LinkButton
								title="Request Pricing"
								onClick={openMenu}
								type="full"
							/>
						) : (
							<>
								{enquireOnly || (!activeVariation && type == 'VariableProduct') ? (
									<BtnWrapper
										variants={variants}
										initial={'hidden'}
										animate={'visible'}
										exit={'hidden'}
										key="enquire-only-variation"
									>
										<Button
											title="Enquire only"
											type="button"
											color="black"
											onClick={openMenu}
										/>
									</BtnWrapper>
								) : (
									<BtnWrapper
										variants={variants}
										initial={'hidden'}
										animate={'visible'}
										exit={'hidden'}
										key="addd-to-cart"
									>
										<AddToCartButton
											type={type}
											variation={activeVariation}
											product={product}
											quantity={quantity}
											disabled={!purchasable}
											sectionTheme={sectionTheme}
										/>
									</BtnWrapper>
								)}
							</>
						)}

						<ShareWrapper>
							<AddToWishList
								type={type}
								variation={activeVariation}
								product={product}
								quantity={quantity}
							/>
							<ShareButton onClick={handleShare}>
								{shared ? 'Link Copied!' : 'Share'}
							</ShareButton>
						</ShareWrapper>
					</RightColumn>
				</Grid>
			</DetailsRow>
		</AnimatePresence>
	);
};

export default Details;

const BtnWrapper = styled(motion.span)`
	width: 100%;
	display: block;
	margin-top: 48px;

	> p {
		margin-bottom: 1em;
	}

	a,
	button {
		padding: 9px 24px;
		font-size: 100%;
		line-height: 1.15;
		width: 100%;
	}
`;

const ExclusiveBtnWrapper = styled(BtnWrapper)`
	a,
	button {
		opacity: 0.5;
		pointer-events: none;
	}
`;

const DetailsRow = styled.div`
	display: flex;
	padding-bottom: 60px;
`;

const LeftColumn = styled.div`
	grid-column: span 5;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: -1 / 1;
	}
`;

const RightColumn = styled.div`
	grid-column: span 4;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: -1 / 1;
	}

	> a {
		margin-top: 30px;
	}

	> button {
		margin-top: 48px;
	}
`;

const LinkInner = styled.a`
	transition: color 280ms ease;

	&:hover {
		color: ${swatch('brand')};
	}
`;

const Row = styled(motion.div)`
	display: flex;

	&:not(:last-child) {
		margin-bottom: 16px;
	}
`;

const Subtitle = styled.div`
	width: 38.95%;
	padding-right: 8px;
`;

const Body = styled.div`
	flex: 1;
`;

const ProductDetails = styled.div``;

const ShareWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	padding-top: 20px;
	border-top: 1px solid ${({ theme }) => theme.colors.black};
	margin-top: 32px;
`;

const ShareButton = styled.button``;

const ProductCopy = styled(Content)``;
