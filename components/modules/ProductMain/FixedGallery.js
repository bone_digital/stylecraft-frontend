import styled, { css } from 'styled-components';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, A11y } from 'swiper';
import { useState, useRef } from 'react';
import Rarr from '../../../public/icons/rarr.svg';
import ImageEl from '../../common/ImageEl';
import { ease } from '../../../styles/theme';
import InnerWrapper from '../../common/InnerWrapper';

const Gallery = styled.div`
	width: 100%;
	height: 100vh;
	position: sticky;
	top: 0;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		height: auto;
		position: relative;
	}
`;

const Wrapper = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: calc(100% - 10vw);
	height: 100vh;
	z-index: 0;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		position: static;
		top: auto;
		left: auto;
		width: 100%;
		height: auto;
	}
`;

const ImageWrapper = styled.div`
	position: relative;
	height: 100vh;
	overflow: hidden;

	> img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		height: auto;

		&::before {
			content: '';
			display: block;
			width: 100%;
			padding-top: 68%;
			transition: ${ease('all', 'slow')};
		}
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		&::before {
			padding-top: 62%; // roughly 1730 x 1080 ratio 
		}
	}
`;

const Nav = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: flex;
	justify-content: start;
	align-items: end;
	padding: 16px;
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;

	svg {
		width: 4px;
		transition: ${ease('transform')};
	}

	${({ direction }) =>
		direction === 'prev' &&
		css`
			svg {
				transform: rotate(180deg);
			}
		`};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: flex;
		width: 30px;
		height: 30px;
		position: relative;
		opacity: 1;
		top: auto;
		right: auto;
		bottom: auto;
		left: auto;
		border-radius: 999px;
		background: ${({ theme }) => theme.colors.white};
		align-items: center;
		justify-content: center;
		z-index: 20;

		&:first-child {
			margin-right: 16px;
		}
	}
`;

const Caption = styled.div`
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	z-index: 10;
	padding: 30px 0;
	color: ${({ theme }) => theme.colors.white};
	text-transform: uppercase;
	opacity: ${({ galleryActive }) => (galleryActive ? 1 : 0)};
	transition: ${ease('opacity', 'medium')};

	> div {
		display: flex;
		flex-wrap: wrap;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		padding: 24px 0;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const CaptionColumn = styled.h6`
	letter-spacing: 0.025em;

	&:not(:last-child) {
		margin-right: 16px;
	}

	&:only-child {
		margin-right: 0;
	}

	p {
		font-size: inherit;
	}
`;

const CaptionBlock = styled.span`
	display: flex;
	flex-wrap: wrap;

	p {
		font-size: inherit;
		line-height: inherit;
	}

	&:not(:last-child) {
		margin-right: 90px;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;

		&:not(:last-child) {
			margin: 0 0 0.5em 0;
		}
	}
`;

SwiperCore.use([Navigation, A11y]);

const FixedGallery = ({ galleryActive, images }) => {
	const prevRef = useRef();
	const nextRef = useRef();

	return images && images?.length ? (
		<Wrapper>
			<Gallery active={galleryActive}>
				{images && images?.length > 1 && (
					<Nav>
						<NavElement
							data-cursor="true"
							className="cursor-link cursor-link--carousel-prev"
							direction="prev"
							ref={prevRef}
						>
							<Rarr />
						</NavElement>
						<NavElement
							data-cursor="true"
							className="cursor-link cursor-link--carousel-next"
							direction="next"
							ref={nextRef}
						>
							<Rarr />
						</NavElement>
					</Nav>
				)}

				<Swiper
					loop={images?.length > 1 ? true : false}
					allowTouchMove={images?.length > 1 ? true : false}
					preventInteractionOnTransition={true}
					speed={400}
					centeredSlides={false}
					slidesPerView={1}
					spaceBetween={0}
					onInit={(swiper) => {
						swiper.params.navigation.prevEl = prevRef.current;
						swiper.params.navigation.nextEl = nextRef.current;

						swiper.navigation.init();
						swiper.navigation.update();
					}}
				>
					{images.map(({ image, caption }, index) => (
						<SwiperSlide key={`fixed-gallery-${index}`}>
							<ImageWrapper active={galleryActive}>
								<ImageEl
									image={image?.largeSourceUrl}
									thumbnail={image?.thumbSourceUrl}
									alt={image?.alt}
								/>

								{caption && caption?.length > 0 && (
									<Caption galleryActive={galleryActive}>
										<InnerWrapper>
											{caption.map((column) => {
												const { prefix, copy } = column;

												return (
													<CaptionBlock>
														{prefix && (
															<CaptionColumn>
																{prefix}
															</CaptionColumn>
														)}
														{copy && (
															<CaptionColumn
																dangerouslySetInnerHTML={{
																	__html: copy
																}}
															/>
														)}
													</CaptionBlock>
												);
											})}
										</InnerWrapper>
									</Caption>
								)}
							</ImageWrapper>
						</SwiperSlide>
					))}
				</Swiper>
			</Gallery>
		</Wrapper>
	) : null;
};

export default FixedGallery;
