import styled from 'styled-components';
import Darr from '../../../public/icons/darr.svg';
import { breakpoint, swatch, ease } from '../../../styles/theme';
import { useState } from 'react';
import ImageEl from '../../common/ImageEl/index';

const Wrapper = styled.div`
	flex: 1;
`;

const SelectWrapper = styled.div`
	display: block;
	position: relative;
	width: 100%;
	padding: 0 0 2px 0;
	border-bottom: 1px solid ${({ theme }) => theme.colors.black};
	margin-bottom: 10px;

	svg {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translate(0, calc(-50% - 3px));
		width: 8px;
		fill: currentColor;
		transition: ${ease('all')};
		pointer-events: none;
	}
`;

const Select = styled.select`
	appearance: none;
	width: 100%;
	padding: 0 0;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	width: 100%;
`;

const Option = styled.option``;

const Label = styled.small`
	font-size: 10px;
	transition: ${ease('all', 'fast')};
	opacity: ${({ expanded }) => (expanded ? '0' : '1')};
	visibility: ${({ expanded }) => (expanded ? 'hidden' : 'visible')};
	margin-left: 12px;
`;

const CloseLabel = styled.small`
	font-size: 10px;
	transition: ${ease('all', 'fast')};
	position: absolute;
	top: 0%;
	right: 0;
	opacity: ${({ expanded }) => (!expanded ? '0' : '1')};
	visibility: ${({ expanded }) => (!expanded ? 'hidden' : 'visible')};
`;

const SwatchButton = styled.button`
	position: relative;
	width: 100%;
	display: flex;
	align-items: center;
`;

const Swatch = styled.div`
	border-radius: 999px;
	height: ${({ expanded }) => (expanded ? '110px' : '30px')};
	width: ${({ expanded }) => (expanded ? '110px' : '30px')};
	transition: ${ease('all', 'medium')};
	position: relative;
	overflow: hidden;

	@media ${({ theme }) => theme.breakpoints.xlDesktop} {
		height: ${({ expanded }) => (expanded ? '80px' : '30px')};
		width: ${({ expanded }) => (expanded ? '80px' : '30px')};
	}

	> img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
`;

const VariationPicker = ({ options, variation, updateVariation, productName, image }) => {
	const [expanded, setExpanded] = useState(false);

	const handleChange = (key, value) => {
		updateVariation(key, value);
	};

	const toggleExpand = () => {
		setExpanded((prevState) => !prevState);
	};

	const activeVariation = variation?.node;

	return (
		<Wrapper>
			{options && (
				<SelectWrapper>
					<Select
						onChange={(e) => {
							handleChange(options?.label, e.target.value);
						}}
					>
						{options?.values?.map((values, index) => {

							return (
								<Option value={values} key={index}>
									{values}
								</Option>
							);
						})}
					</Select>
					<Darr />
				</SelectWrapper>
			)}

			{activeVariation?.image && (
				<SwatchButton onClick={toggleExpand} expanded={expanded}>
					<Swatch expanded={expanded}>
						<ImageEl
							image={activeVariation?.image?.sourceUrl}
							thumbnail={activeVariation?.image?.thumbSourceUrl}
							alt={activeVariation?.image?.alt}
							ratio={'100%'}
						/>
					</Swatch>

					<Label expanded={expanded}>Expand</Label>
					<CloseLabel expanded={expanded}>Close</CloseLabel>
				</SwatchButton>
			)}
		</Wrapper>
	);
};

export default VariationPicker;
