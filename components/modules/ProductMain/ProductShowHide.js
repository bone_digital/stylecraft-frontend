import styled, { css } from 'styled-components';
import { SlideDown } from 'react-slidedown';
import 'react-slidedown/lib/slidedown.css';
import LinkButton from '../../common/LinkButton/index';
import { ease } from '../../../styles/theme';

const Row = styled.div`
	width: 100%;
	transition: ${ease('all')};

	.react-slidedown {
		transition-duration: 200ms;
		transition-timing-function: 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
	}

	> button {
		margin-top: 24px;

		@media ${({ theme }) => theme.breakpoints.laptop} {
			margin-top: 20px;
		}
	}
`;

const Body = styled.div`
	padding-top: 1.33em;
`;

const ProductShowHide = ({ children, active, handleToggle }) => {
	return (
		<Row active={active}>
			<SlideDown>{active ? <Body>{children}</Body> : null}</SlideDown>

			<LinkButton
				title={active ? 'View Less' : 'View More'}
				onClick={handleToggle}
				type="full"
			/>
		</Row>
	);
};

export default ProductShowHide;
