import styled from 'styled-components';
import Breadcrumbs from '../../elements/Breadcumbs';
import Details from './Details';
import FixedGallery from './FixedGallery';
import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import InviewWrapper from '../../common/InviewWrapper/index';
import { useUI } from '../../../context/UIProvider';
import WholesaleMain from '../WholesaleMain';
import WholesaleDetails from '../WholesaleMain/Details';
import { useInView } from 'react-intersection-observer';

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const StickyRow = styled(Row)`
	position: relative;
	justify-content: flex-end;
`;

const Column = styled.div`
	width: 50%;
	background: ${({ theme, backgroundColor }) =>
		backgroundColor ? theme.colors[backgroundColor] : 'transparent'};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
	}
`;

const StickyColumn = styled(Column)`
	position: sticky;
	top: 0;
	z-index: 5;
	pointer-events: ${({ galleryActive }) => (galleryActive ? 'none' : 'all')};

	&&& > div {
		@media ${({ theme }) => theme.breakpoints.tablet} {
			transform: none !important;
		}
	}
`;

const StickyWholesaleColumn = styled(Column)`
	display: ${({ device }) => (device !== 'mobile' ? 'flex' : 'none')};
	position: sticky;
	top: 0;
	margin-top: 100vh;
	width: 100%;
	z-index: 5;
	background: ${({ theme }) => theme.colors.grey};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: ${({ device }) => (device === 'mobile' ? 'flex' : 'none')};
		margin-top: 0;
		min-height: auto;
		position: relative;
		order: 2;
	}
`;

const MainInner = styled.div`
	overflow: hidden;
	pointer-events: none;
`;

const StickyInner = styled(motion.div)`
	margin: 0 auto;
	display: block;
	padding-left: 32px;
	padding-right: 32px;
	padding-top: 132px;
	pointer-events: all;
	background: ${({ theme, sectionTheme }) => {
		return sectionTheme ? theme.colors[sectionTheme] : theme.colors.white;
	}};
	min-height: calc(100vh + 1px);

	@media ${({ theme }) => theme.breakpoints.laptop} {
		padding-top: 32px;
		padding-left: 16px;
		padding-right: 16px;
		transform: none !important;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		min-height: auto;
	}
`;

const Wrapper = styled.div`
	width: 100%;

	&&& {
		padding-top: 0;

		@media ${({ theme }) => theme.breakpoints.mobile} {
			padding-top: 0;
		}
	}
`;

const Title = styled.h1`
	margin-bottom: 32px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 20px;
	}
`;

const OpenTrigger = styled.button`
	position: absolute;
	top: 0;
	left: 0;
	width: 50%;
	height: 100vh;
	z-index: 10;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const CloseTrigger = styled.button`
	position: absolute;
	top: 0;
	right: 0;
	width: 10vw;
	height: 100%;
	z-index: 10;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const BreadcrumbWrapper = styled.div`
	margin-bottom: 54px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 20px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 54px;
	}
`;

const ProductMain = ({ product, anchorId, siteOptions, isWholeseller = false }) => {
	const [hasInteracted, setHasInteracted] = useState(false);
	const [galleryActive, setGalleryActive] = useState(false);
	const { updateCursor } = useUI();


	const { ref, inView, entry } = useInView({
		triggerOnce: false,
		rootMargin: '0px 0px -50% 0px',
		threshold: .1,
	});

	useEffect(() => {
		if (galleryActive) {
			closeGallery();
		}
	}, [inView]);

	const { productCategories, product: productFields } = product || {};

	const backgroundColor = productFields?.backgroundColor || 'white';

	let productCrumbs = [];

	if (
		productCategories?.edges !== null &&
		typeof productCategories?.edges === 'object'
	) {
		const productCategoryEdges = [...productCategories?.edges];

		productCrumbs = productCategoryEdges?.sort(
			(a, b) => a.node.link.length - b.node.link.length
		);
	}

	const openGallery = () => {
		setGalleryActive(true);
		setHasInteracted(true);
		updateCursor();
	};

	const closeGallery = () => {
		setGalleryActive(false);
		updateCursor();
	};

	const mainInnerVariants = {
		hidden: {
			transform: 'translateX(calc(100% - 10vw))',
			opacity: hasInteracted ? 1 : 0,
			transition: {
				ease: 'easeIn',
				duration: 0.8
			}
		},
		visible: {
			opacity: 1,
			transform: 'translateX(calc(0% - 0vw))',
			transition: {
				transition: {
					ease: 'easeIn',
					duration: 0.8,
					ease: 'easeIn'
				}
			}
		}
	};

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor} hasSibling={true}>
			<Wrapper>
				<StickyRow>
					{productFields?.gallery?.length && (
						<>
							{!galleryActive ? (
								<OpenTrigger
									key="open"
									id="open"
									data-cursor="true"
									className="cursor-link--view"
									onClick={openGallery}
								/>
							) : (
								<CloseTrigger
									id="div"
									key="close"
									data-cursor="true"
									className="cursor-link--close"
									onClick={closeGallery}
								/>
							)}
						</>
					)}

					<Column backgroundColor="grey">
						{productFields?.gallery && (
							<FixedGallery
								galleryActive={galleryActive}
								setGalleryActive={setGalleryActive}
								images={productFields?.gallery}
								key="fixed-gallery"
							/>
						)}
						<StickyWholesaleColumn device="desktop" ref={ref}>
							<WholesaleDetails
								siteOptions={siteOptions}
								product={product}
								isWholeseller={isWholeseller}
							/>
						</StickyWholesaleColumn>
					</Column>

					<StickyColumn galleryActive={galleryActive}>
						<MainInner sectionTheme={backgroundColor}>
							<StickyInner
								variants={mainInnerVariants}
								initial="hidden"
								animate={galleryActive ? 'hidden' : 'visible'}
							>
								<BreadcrumbWrapper>
									<Breadcrumbs
										categories={productCrumbs}
										title={product?.name}
									/>
								</BreadcrumbWrapper>

								<Title>{product?.name}</Title>

								<Details
									product={product}
									isWholeseller={isWholeseller}
									sectionTheme={backgroundColor}
								/>

								<WholesaleMain
									product={product}
									siteOptions={siteOptions}
									isWholeseller={isWholeseller}
								/>
							</StickyInner>
						</MainInner>
						<StickyWholesaleColumn device="mobile">
							<WholesaleDetails
								siteOptions={siteOptions}
								product={product}
								isWholeseller={isWholeseller}
							/>
						</StickyWholesaleColumn>
					</StickyColumn>
				</StickyRow>
			</Wrapper>
		</InviewWrapper>
	);
};

export default ProductMain;
