import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';
import InviewWrapper from '../../common/InviewWrapper';
import { Content } from '../../common/WYSWYG';

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const Column = styled.div`
	width: 100%;
	position: relative;
	display: flex;
	align-items: flex-end;

	> * {
		width: 100%;
	}

	&:last-child {
		padding-bottom: 32px;
	}

	@media ${breakpoint('laptop')} {
		width: 100%;

		&:last-child {
			padding-top: 40px;
			padding-bottom: 0;
		}
	}

	@media ${breakpoint('tablet')} {
		&:last-child {
			padding-top: 0;
		}
	}
`;

const TextColumn = styled.div`
	grid-column: span 12;

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const TextInner = styled(Content)`
	margin-bottom: 25px;
`;

export default function OneColumnContent({
	backgroundColor,
	anchorId,
	copy
}) {

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Row>
				<Column>
					<InnerWrapper>
						<Grid
							rowGap={'0'}
							rowGapMobile={'40px'}
							columns={13}
							columnsMobile={12}
						>
							<TextColumn>
								<TextInner
									dangerouslySetInnerHTML={{ __html: copy }}
								/>
							</TextColumn>
						</Grid>
					</InnerWrapper>
				</Column>
			</Row>
		</InviewWrapper>
	);
}
