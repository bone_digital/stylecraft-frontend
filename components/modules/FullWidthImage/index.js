import Image from 'next/image';

export default function FullWidthImage({ image }) {
	const imageSizes = image?.mediaDetails?.sizes;

	let selectedImageSize = null;
	imageSizes.map((size) => {
		if (size.name == 'card') {
			selectedImageSize = size;
		}
	});

	if (!selectedImageSize) {
		selectedImageSize = imageSizes[imageSizes.length - 1];
	}

	return selectedImageSize?.sourceUrl ? (
		<div>
			<Image
				alt={image.altText}
				src={selectedImageSize?.sourceUrl}
				width={selectedImageSize.width}
				height={selectedImageSize.height}
			></Image>
		</div>
	) : null;
}
