import PostCarousel from '../PostCarousel';

const SinglePostCarousel = ({ relatedPostsOverwrite, relatedPosts, type }) => {
	const getRelatedPosts = relatedPosts?.edges;

	const defaultRelatedPosts = getRelatedPosts?.length
		? getRelatedPosts.map(({ node, __typename }) => {
				return {
					__typename,
					post: node
				};
		  })
		: null;

	const posts =
		relatedPostsOverwrite && relatedPostsOverwrite?.length
			? relatedPostsOverwrite
			: defaultRelatedPosts;

	let title = 'Related Posts';

	if (type == 'project') {
		title = 'Related Projects';
	}

	if (type == 'showroom') {
		title = 'Related Showrooms';
	}

	return posts ? <PostCarousel posts={posts} title={title} /> : null;
};

export default SinglePostCarousel;
