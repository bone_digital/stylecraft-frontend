import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import Grid from '../../common/Grid';
import Hotspot from '../../common/Hotspot';
import InnerWrapper from '../../common/InnerWrapper';
import LinkButton from '../../common/LinkButton';
import ImageBg from '../../common/Thumbnail';
// import LinkItem from '../../common/LinkHeading/index';
import InviewWrapper from '../../common/InviewWrapper';
import Link from 'next/link';

export default function index(props) {
	const { backgroundColor, copy, linkButton, brand, image, hotspotGallery, anchorId } =
		props || {};

	const brandFields = brand?.brand;

	return brand ? (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Row>
				<Column>
					<InnerWrapper>
						<Title>{`Meet ${brand?.title}`}</Title>
						<Grid>
							<HotspotWrapper>
								<Hotspot
									content={hotspotGallery}
									ratio={'116%'}
									section={false}
								/>
							</HotspotWrapper>
						</Grid>
					</InnerWrapper>
				</Column>

				<BackgroundColumn>
					<InnerWrapper>
						<Subheading>
							{brand?.title && (
								<BrandLink>
									<BrandCategory>
										<Link
											href='/brands'
											passHref
										>
											<LinkTitle>Brands</LinkTitle>
										</Link>
									</BrandCategory> /{' '}

									<BrandTitle> {brand?.title}</BrandTitle>
								</BrandLink>
							)}
						</Subheading>
						<Grid
							rowGap={'40px'}
							rowGapMobile={'40px'}
							columns={13}
						>
							{brandFields?.excerpt && (
								<Intro
									dangerouslySetInnerHTML={{
										__html: brandFields?.excerpt
									}}
								/>
							)}

							<ImageColumn>
								<ImageBg
									image={image?.sourceUrl}
									thumbnail={image?.thumbSourceUrl}
									alt={image?.alt}
									ratio={'146.5%'}
								/>
							</ImageColumn>

							<TextColumn>
								<TextInner>
									<P2
										dangerouslySetInnerHTML={{
											__html: copy
										}}
									/>
									{linkButton?.linkInner?.url && (
										<LinkButton
											prefix={linkButton?.prefix}
											title={linkButton?.linkInner?.title}
											href={linkButton?.linkInner?.url}
											target={
												linkButton?.linkInner?.target
											}
										/>
									)}
								</TextInner>
							</TextColumn>
						</Grid>
					</InnerWrapper>
				</BackgroundColumn>
			</Row>
		</InviewWrapper>
	) : null;
}

const LinkTitle = styled.a`
	transition: color 280ms ease;

	&:hover {
		color: ${swatch('brand')};
	}
`

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
	position: relative;
	padding: 100px 0;

	@media ${breakpoint('laptop')} {
		padding-bottom: 0;
	}

	@media ${breakpoint('tablet')} {
		padding-top: 65px;
	}
`;

const BrandLink = styled.h5`
	text-transform: uppercase;
	display: flex;
	justify-content: flex-end;
	gap: 10px;
`;

const BrandCategory = styled.span`
	font-weight: bold;
`;

const BrandTitle = styled.span``;

const P2 = styled.div`
	p {
		all: inherit;

		&:last-child {
			margin-bottom: 0;
		}
	}

	@media ${breakpoint('laptop')} {
		p {
			font-size: ${({ theme }) => theme.type.h4[0]};
			line-height: ${({ theme }) => theme.type.h4[1]};
		}
	}
`;

const Column = styled.div`
	width: 50%;

	&:last-child {
		padding-bottom: 32px;
	}

	@media ${breakpoint('laptop')} {
		width: 100%;

		&:last-child {
			padding-bottom: 0;
		}
	}

	@media ${breakpoint('laptop')} {
		&:last-child {
			padding-bottom: 65px;
		}
	}
`;

const BackgroundColumn = styled(Column)`
	padding-top: 32px;
	background: ${swatch('greenDark')};

	@media ${breakpoint('laptop')} {
		padding-top: 30px;
	}
`;

const Intro = styled.h2`
	grid-column: span 10;
	margin-bottom: -3.153em;
	position: relative;
	z-index: 1;

	@media ${breakpoint('xlDesktop')} {
		grid-column: -1 / 1;
		margin-bottom: 0;
	}

	> * {
		font-size: inherit;
		line-height: inherit;
	}
`;

const ImageColumn = styled.div`
	grid-column: span 7;
	z-index: 0;
	position: relative;
	padding-left: 32px;

	@media ${breakpoint('xlDesktop')} {
		padding-left: 0;
		grid-column: -1 / 1;

		> div::before {
			padding-top: 76%;
		}
	}

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}
`;

const TextColumn = styled.div`
	grid-column: span 6;
	padding: 120px 0;
	display: flex;
	align-items: flex-end;

	@media ${breakpoint('xlDesktop')} {
		padding-top: 0;
		padding-bottom: 0;
		grid-column: -1 / 1;
	}

	@media ${breakpoint('laptop')} {
		grid-column: -1 / 1;
	}
`;

const TextInner = styled.div`
	> div {
		margin-bottom: 25px;
	}
`;

const Subheading = styled.div`
	text-align: right;
	width: 100%;
	margin-bottom: 36px;

	@media ${breakpoint('laptop')} {
		display: none;
	}
`;

const Title = styled.h1`
	margin-top: -0.05em;
	margin-bottom: 48px;
`;

const HotspotWrapper = styled.div`
	grid-column: span 8;

	@media ${breakpoint('xlDesktop')} {
		grid-column: -1 / 1;
	}

	ul {
		padding: 40px 0 36px 0;

		@media ${breakpoint('laptop')} {
			padding: 16px 0 45px 0;
		}
	}
`;
