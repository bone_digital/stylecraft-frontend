import styled from 'styled-components';
import InviewWrapper from '../../common/InviewWrapper';
import InnerWrapper from '../../common/InnerWrapper/index';
import Grid from '../../common/Grid';
import { breakpoint, swatch, ease } from '../../../styles/theme';
import ImageEl from '../../common/ImageEl';
import GravityForm from '../../common/GravityForm'
import { useUI } from '../../../context/UIProvider';

const options = require('../../../json/options.json');

const Row = styled.section`
	display: flex;
	flex-wrap: wrap;
	align-items: center;

	&& {
		padding: 0;
	}
`;

const ImageWrapper = styled.div`
	position: relative;
	flex: 0.5;
	width: 100%;
	background-color: ${({ theme }) => theme.colors.brand};
	padding-left: 8px;
	overflow: hidden;
	grid-column: span 6;
	min-height: calc(100vh - 132px);
	
	@media ${breakpoint('tablet')} {
		grid-column: span 12;
		min-height: 100vw;
		display: none;
	}
`

const ImageLockup = styled.div`
	position: relative;
	height: 100%;
	width: 100%;

	img {
		position: absolute;
		height: 100%;
		width: 100%;
		object-fit: cover;
		object-position: center;
	}
`

const FormWrapper = styled.div`
	position: relative;
	flex: 0.5;
	padding: 0;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: stretch;
	width: 100%;
	grid-column: 7 / span 6;

	& > * {
		display: flex;
		width: 100%;
		flex-direction: column;
	}

	& div[type="general"] {
		display: flex;
		flex-direction: column;
	}

	& fieldset {
		& legend {
			visibility: visible;
			width: 100%;
			margin-bottom: 24px;
		}
	}
	
	@media ${breakpoint('tablet')} {
		grid-column: span 12;
		padding-top: 50px;
	}
`
const FormLockup = styled(GravityForm)``

const HeadingLockup = styled.h3`
	margin-bottom: 24px;
`

const Content = styled.div`
margin-top: 24px;
`;

export default function NewsletterSignup({ backgroundColor, copy }) {
	const { anchorId } = useUI() || {};
	const formId = options?.popup_form_id;
	const heading = options?.popup_form_heading;
	const image = options?.popup_form_image;

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<Row>
				<InnerWrapper>
					<Grid>
						<ImageWrapper>
							<ImageLockup>
								<ImageEl
									image={image?.sizes?.medium}
									thumbnail={image?.sizes?.thumbnail}
									alt={image?.alt}
								/>
							</ImageLockup>
						</ImageWrapper>
						<FormWrapper>
							{heading &&
								<HeadingLockup>
									{heading}
								</HeadingLockup>
							}
							<FormLockup 
								formID={parseInt(formId)}
								buttonType={'button'}
							/>
							{copy &&
								<Content>
									<p>{copy}</p>
								</Content>
							}
						</FormWrapper>
					</Grid>
				</InnerWrapper>			
			</Row>
		</InviewWrapper>
	);
}
