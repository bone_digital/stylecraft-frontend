import styled from 'styled-components';

const Dimensions = ({ content }) => {
	function transpose(a) {
		if( typeof a != 'object' || typeof a[0] != 'object')
		{
			return null;
		}

		return Object.keys(a[0]).map(function (c) {
			return a.map(function (r) {
				if (r) {
					return r[c];
				}
			});
		});
	}

	let hasFeatured = false;

	const getColumns = content?.map((row) => {
		const featured = row.featured;

		if (featured) {
			hasFeatured = true;
		}

		return row?.rows?.map((content) => {
			return {
				featured: featured,
				content: content?.row
			};
		});
	});

	const columnHeaders = content?.map((row) => {
		const featured = row.featured;
		const title = row.title;

		return {
			title,
			featured
		};
	});

	const rowsToColumns = transpose(getColumns);
	let showColumns = false;
	if( rowsToColumns || columnHeaders )
	{
		showColumns = true;
	}

	return showColumns ? (
		<>
			{columnHeaders && (
				<Row>
					{columnHeaders.map(({ title, featured }, index) => (
						<DimensionsColumn
							columns={content?.length || 0}
							key={index}
							hasFeatured={hasFeatured}
							featured={featured}
						>
							<Subheading>{title}</Subheading>
						</DimensionsColumn>
					))}
				</Row>
			)}

			{rowsToColumns &&
				rowsToColumns.map((row, index) => (
					<Row key={`row-${index}`}>
						{row.map(
							(
								content,
								columnIndex
							) => (
								<DimensionsColumn
									columns={row?.length || 0}
									key={columnIndex}
									hasFeatured={hasFeatured}
									featured={content?.featured}
								>
									<p>{content?.content}</p>
								</DimensionsColumn>
							)
						)}
					</Row>
				))}
		</>
	) : null;
};

const Row = styled.div`
	display: flex;
	margin: 0 -8px;
	overflow: hidden;
	flex-wrap: wrap;

	:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const DimensionsColumn = styled.div`
	width: ${({ columns, featured, hasFeatured }) =>
		hasFeatured
			? featured
				? `${(100 / columns) * 1.5}%`
				: `${(100 - (100 / columns) * 1.5) / (columns - 1)}%`
			: `${100 / columns}%`};
	padding: 0 8px;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		width: ${({ columns, featured, hasFeatured }) =>
			hasFeatured
				? featured
					? `${(100 / columns) * 1.25}%`
					: `${(100 - (100 / columns) * 1.25) / (columns - 1)}%`
				: `${100 / columns}%`}
`;

const Subheading = styled.p`
	font-weight: 700;
`;

export default Dimensions;
