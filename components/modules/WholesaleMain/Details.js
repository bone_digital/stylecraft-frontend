import styled from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';
import Accordian from '../../common/Accordian';
import Dimensions from './Dimensions';
import Downloads from './Downloads';
import GeneralInfo from './GeneralInfo';

const Title = styled.h1`
	font-size: ${({ theme }) => theme.type.nav[0]};
	line-height: ${({ theme }) => theme.type.nav[1]};
	margin: 100px 0;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const Wrapper = styled.div`
	display: flex;
	width: 100%;

	> div {
		display: flex;
		width: 100%;
	}
`;

const Body = styled.div`
	display: flex;
	width: 100%;
	flex-direction: column;
	justify-content: space-between;
	padding-bottom: 42px;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		padding-top: 36px;
	}
`;

const WholesaleDetails = ({ product, isWholeseller, siteOptions }) => {
	const {
		name: productName,
		product: productFields,
		__typename: type,
		variations,
		environmentalCertifications,
		attributes
	} = product || {};

	const globalPdf = {
		file: siteOptions?.globalPdf,
		label: siteOptions?.globalPdfLabel
	};

	const productOptions = variations?.edges || [];
	const productAttributes = attributes?.edges || [];

	const {
		columns,
		detailsColumns,
		structuralWarranty,
		originOfManufacture,
		downloads
	} = productFields || {};

	const hasDetails =
		detailsColumns ||
		structuralWarranty ||
		originOfManufacture ||
		environmentalCertifications ||
		globalPdf;

	const variationTitle =
		productAttributes !== null && productAttributes?.length
			? productAttributes[0]?.node?.name
			: false;

	return (
		<Wrapper>
			<InnerWrapper>
				<Body>
					<Title>{product?.name}</Title>

					<Accordian>
						{columns && (
							<Dimensions
								title={'Dimensions'}
								content={columns}
							/>
						)}
						{hasDetails && (
							<GeneralInfo
								title={'Details'}
								structuralWarranty={structuralWarranty}
								originOfManufacture={originOfManufacture}
								environmentalCertifications={environmentalCertifications}
								content={detailsColumns}
							/>
						)}
						{downloads && (
							<Downloads
								title={'Downloads'}
								isWholeseller={isWholeseller}
								content={downloads}
								globalPdf={globalPdf}
							/>
						)}
					</Accordian>
				</Body>
			</InnerWrapper>
		</Wrapper>
	);
};

export default WholesaleDetails;
