import styled, { css } from 'styled-components';
import ProductImages from '../../elements/ProductImages';
import { useState, useRef } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { ease } from '../../../styles/theme';
import SwiperCore, { Navigation, A11y } from 'swiper';
import ImageEl from '../../common/ImageEl';
import InviewWrapper from '../../common/InviewWrapper/index';
import React from 'react';
import Rarr from '../../../public/icons/rarr.svg';

const Nav = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: flex;
	justify-content: start;
	align-items: start;
	padding: 16px;
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;

	svg {
		width: 4px;
		transition: ${ease('transform')};
	}

	${({ direction }) =>
		direction === 'prev' &&
		css`
			svg {
				transform: rotate(180deg);
			}
		`};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: flex;
		width: 30px;
		height: 30px;
		position: relative;
		opacity: 1;
		top: auto;
		right: auto;
		bottom: auto;
		left: auto;
		border-radius: 999px;
		background: ${({ theme }) => theme.colors.white};
		align-items: center;
		justify-content: center;
		z-index: 20;

		&:first-child {
			margin-right: 16px;
		}
	}
`;

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const StickyRow = styled(Row)`
	position: relative;
	justify-content: flex-end;
	align-items: flex-start;
`;

const Column = styled.div`
	width: 50%;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 100%;
		order: 1;
	}
`;

const StickyColumn = styled(Column)`
	position: sticky;
	top: 0;
	z-index: 5;
	background: ${({ theme }) => theme.colors.grey};
	min-height: 100vh;
	display: flex;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		min-height: auto;
		position: relative;
		order: 2;
	}
`;

const Wrapper = styled.div`
	&&& {
		padding-top: 0;

		@media ${({ theme }) => theme.breakpoints.mobile} {
			padding-top: 0;
		}
	}
`;

const ProductGallery = styled.div`
	display: ${({ device }) => (device !== 'mobile' ? 'block' : 'none')};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-top: 54px;
		display: ${({ device }) => (device === 'mobile' ? 'block' : 'none')};
	}
`;

const Gallery = styled.div`
	height: auto;
	position: relative;
	padding-bottom: 54px;
`;

const ImageWrapper = styled.div`
	position: relative;
	overflow: hidden;
	height: auto;

	> img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
`;

// const NavElement = styled.button`
// 	position: absolute;
// 	top: 0;
// 	width: 33.33%;
// 	height: 100%;
// 	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
// 	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
// 	z-index: 10;
// 	opacity: 0;
// `;

SwiperCore.use([Navigation, A11y]);

const WholesaleMain = ({ product, isWholeseller, siteOptions }) => {
	const prevRef = useRef();
	const nextRef = useRef();

	const productImages = product?.product?.productGallery || [];
	const wholesaleImages = product?.product?.wholesaleGallery || [];

	const hasImages = isWholeseller
		? wholesaleImages?.length + productImages?.length > 0
		: productImages?.length > 0;

	const loop = isWholeseller
		? wholesaleImages?.length + productImages?.length > 1
		: productImages?.length > 1;

	const gallery = !isWholeseller
		? [...productImages, ...wholesaleImages]
		: productImages;

	return product ? (
		<>
			<ProductGallery device="desktop">
				<ProductImages
					hasImages={hasImages}
					images={gallery}
					key="product-images"
				/>
			</ProductGallery>

			<ProductGallery device="mobile">
				<Gallery>
					{hasImages && (
						<>
						<Nav>
							<NavElement
								data-cursor="true"
								className="cursor-link cursor-link--carousel-prev"
								direction="prev"
								ref={prevRef}
							>
								<Rarr />
							</NavElement>
							<NavElement
								data-cursor="true"
								className="cursor-link cursor-link--carousel-next"
								direction="next"
								ref={nextRef}
							>
								<Rarr />
							</NavElement>
						</Nav>
						
						<Swiper
							loop={loop}
							allowTouchMove={loop}
							preventInteractionOnTransition={true}
							speed={400}
							centeredSlides={false}
							slidesPerView={1}
							spaceBetween={0}
							onInit={(swiper) => {
								swiper.params.navigation.prevEl =
									prevRef.current;
								swiper.params.navigation.nextEl =
									nextRef.current;

								swiper.navigation.init();
								swiper.navigation.update();
							}}
						>
							{gallery.map((slide, index) => {
								const node = slide?.image;
								const type = slide?.type;
								const imageGrid = slide?.imageGrid;

								if (type !== 'grid') {
									return (
										<SwiperSlide
											key={`mobile-product-gallery-${index}`}
										>
											<ImageWrapper>
												<ImageEl
													alt={node?.alt}
													image={node?.sourceUrl}
													thumbnail={
														node?.thumbSourceUrl
													}
												/>
											</ImageWrapper>
										</SwiperSlide>
									);
								} else {
									return (
										<React.Fragment
											key={`mobile-product-gallery-${index}`}
										>
											{imageGrid?.length &&
												imageGrid.map(
													(slide, gridIndex) => {
														const node =
															slide?.image;

														return (
															<SwiperSlide
																key={`mobile-product-grid-${gridIndex}`}
															>
																<ImageWrapper>
																	<ImageEl
																		alt={
																			node?.alt
																		}
																		image={
																			node?.sourceUrl
																		}
																		thumbnail={
																			node?.thumbSourceUrl
																		}
																	/>
																</ImageWrapper>
															</SwiperSlide>
														);
													}
												)}
										</React.Fragment>
									);
								}
							})}
						</Swiper>

						</>
					)}
				</Gallery>
			</ProductGallery>
		</>
	) : null;
};

export default WholesaleMain;
