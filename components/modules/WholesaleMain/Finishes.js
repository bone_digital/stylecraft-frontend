import styled from 'styled-components';
import ShowHide from '../../common/ShowHide';
import { Container } from '../../common/Grid';
import { Content } from '../../common/WYSWYG';

const Row = styled(Container)`
	display: grid;

	&:not(:last-child) {
		margin-bottom: 1.33em;
	}
`;

const Column = styled.div`
	grid-column: span 3;
	display: flex;
	align-items: flex-start;

	> *:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const CopyWrapper = styled(Content)``;

const Subtitle = styled.p`
	font-weight: 500;
`;

const Finishes = ({ variations, name, title }) => {
	return (
		<Row
			columns={6}
			rowGapMobile={'30px'}
			columnGap={'16px'}
			columnGapMobile={'16px'}
		>
			{variations &&
				variations.map(({ node }, index) => {
					const variationName = node?.name.replace(`${name} -`, '');

					return (
						<Column key={index}>
							<CopyWrapper>
								<Subtitle>{variationName}</Subtitle>
							</CopyWrapper>
						</Column>
					);
				})}
		</Row>
	);
};

export default Finishes;
