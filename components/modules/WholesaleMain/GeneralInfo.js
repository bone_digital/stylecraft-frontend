import styled from 'styled-components';
import { Container } from '../../common/Grid';
import { Content } from '../../common/WYSWYG';
import React from 'react';

const GeneralInfo = ({
	content,
	structuralWarranty,
	originOfManufacture,
	environmentalCertifications,
	globalPdf
}) => {
	if ( !structuralWarranty )
	{
		structuralWarranty = 1;
	}

	if( !originOfManufacture || originOfManufacture.length <= 0 )
	{
		originOfManufacture = false;
	}
	else if(originOfManufacture.length > 1)
	{
		originOfManufacture = originOfManufacture[1];
	}

	let warranty = structuralWarranty;
	warranty += ' year';
	if (warranty > 1)
	{
		warranty += 's';
	}

	return (
		<Row
			columns={6}
			rowGapMobile={'30px'}
			columnGap={'16px'}
			columnGapMobile={'16px'}
		>
			{structuralWarranty && (
				<Column>
					<CopyWrapper>
						<strong>Structural warranty</strong>
						<p>{warranty}</p>
					</CopyWrapper>
				</Column>
			)}
			{originOfManufacture && (
				<Column>
					<CopyWrapper>
						<strong>Origin of manufacture</strong>
						<p>
							{originOfManufacture
								? originOfManufacture
								: 'Australia'}
						</p>
					</CopyWrapper>
				</Column>
			)}
			{environmentalCertifications && environmentalCertifications?.edges.length > 0 && (
				<Column>
					<CopyWrapper>
						<strong>Certifications</strong>
						<p>
							{environmentalCertifications?.edges?.map((child, index) => {
								const title = child?.node?.name;

								return (
									<span key={`cert-${index}`}>
										{title}<br/>
									</span>
								);
							})}
						</p>
					</CopyWrapper>
				</Column>
			)}
			{content &&
				content.map(({ copy }, index) => (
					<Column key={index}>
						<CopyWrapper
							dangerouslySetInnerHTML={{ __html: copy }}
						/>
					</Column>
				))}
		</Row>
	);
};

const Row = styled(Container)`
	display: grid;

	&:not(:last-child) {
		margin-bottom: 1.33em;
	}
`;

const Column = styled.div`
	grid-column: span 3;
	display: flex;
	align-items: flex-start;

	> *:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const CopyWrapper = styled(Content)``;

export default GeneralInfo;
