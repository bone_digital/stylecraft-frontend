import styled from 'styled-components';
import Download from '../../../public/icons/download.svg';
import { Container } from '../../common/Grid';
import { useUI } from '../../../context/UIProvider';
import { countBy } from 'lodash';
import Link from 'next/link';

const Row = styled(Container)`
	display: grid;

	&:not(:last-child) {
		margin-bottom: 1.33em;
	}
`;

const DownloadsColumn = styled.div`
	grid-column: span 3;
	display: flex;
	align-items: flex-start;

	> *:not(:last-child) {
		margin-bottom: 1em;
	}

	svg {
		min-width: 20px;
		width: 20px;
		margin-right: 16px;
	}
`;

const FileTrigger = styled.a`
	display: flex;
	align-items: center;
`;

const Subheading = styled.p`
	font-weight: 500;
`;

const Copy = styled.p`
	text-align: left;
`;

const Downloads = ({ content, isWholeseller, globalPdf }) => {
	const { toggleDownloadBlocked } = useUI();

	const handleAccount = () => {
		toggleDownloadBlocked(true);
	};

	const rawGlobalPdf = globalPdf?.file?.mediaItemUrl
		?.split('/')
		.slice('-1')
		.join();

	return content ? (
		<>
			{content.map((download, index) => {
				const type = download?.type;
				const wholesaleOnly = download?.wholesaleExclusive || false;
				const file = download?.file;

				const fileUrl = file?.mediaItemUrl;

				const rawFile = fileUrl
					? fileUrl.split('/').slice('-1').join()
					: null;

				return (
					rawFile && (
						<Row
							columns={6}
							rowGapMobile={'30px'}
							columnGap={'16px'}
							columnGapMobile={'16px'}
							key={index}
						>
							<DownloadsColumn>
								<Subheading>{type}</Subheading>
							</DownloadsColumn>
							<DownloadsColumn>
								{(isWholeseller && wholesaleOnly) || !wholesaleOnly
									? (
									<FileTrigger
										href={file?.mediaItemUrl}
										title={file?.title}
										target="_blank"
										download
									>
										<Download />
										<Copy>
											{rawFile ? rawFile : file?.title}
										</Copy>
									</FileTrigger>
								) : (
									<FileTrigger
										title="Login"
										as="button"
										onClick={handleAccount}
									>
										<Download />
										<Copy>
											{rawFile ? rawFile : file?.title}
										</Copy>
									</FileTrigger>
								)}
							</DownloadsColumn>
						</Row>
					)
				);
			})}
			{globalPdf && (
				<Row
					columns={6}
					rowGapMobile={'30px'}
					columnGap={'16px'}
					columnGapMobile={'16px'}
				>
					<DownloadsColumn>
						<Subheading>
							{globalPdf?.label || 'Product Information'}
						</Subheading>
					</DownloadsColumn>
					<DownloadsColumn>
						<FileTrigger
							href={globalPdf?.file?.link || '#'}
							title="Click here to download"
							target="_blank"
							download
						>
							<Download />
							<Copy>
								{rawGlobalPdf
									? rawGlobalPdf
									: globalPdf?.file.title}
							</Copy>
						</FileTrigger>
					</DownloadsColumn>
				</Row>
			)}
		</>
	) : null;
};

export default Downloads;
