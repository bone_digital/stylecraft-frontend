import styled from 'styled-components';
import Grid from '../../common/Grid/index';
import ImageEl from '../../common/ImageEl/index';
import { Content } from '../../common/WYSWYG/index';
import LinkButton from '../../common/LinkButton/index';
import MobileModuleHeading from '../../common/MobileModuleHeading';

const Row = styled.div``;

const Column = styled.div`
	grid-column: span 6;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: -1 / 1;
	}
`;

const ImageWrapper = styled.div`
	display: flex;

	& + * {
		margin-top: 36px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			margin-top: 30px;
		}
	}
`;

const Body = styled.div`
	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: ${({ combineColumns }) => (combineColumns ? 'none' : 'block')};

		${Column}:first-child & {
			padding-bottom: 60px;
		}

		> a {
			margin-top: 20px;
		}
	}
`;

const MobileContent = styled.span`
	display: none;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: block;
	}
`;

const Copy = styled(Content)``;

const Columns = ({ title, columns, combineColumns = false }) => {
	return (
		<Row>
			<MobileModuleHeading title={title} />
			<Grid rowGap={'20px'} rowGapMobile={'20px'}>
				{columns &&
					columns.length &&
					columns.map(({ copy, image, linkButton }, index) => (
						<Column key={index}>
							<ImageWrapper>
								<ImageEl
									image={image?.sourceUrl}
									thumbnail={image?.thumbSourceUrl}
									alt={image?.alt}
								/>
							</ImageWrapper>

							<Body combineColumns={false}>
								{copy && (
									<Copy
										dangerouslySetInnerHTML={{
											__html: copy
										}}
									/>
								)}
								{linkButton && linkButton?.linkInner && (
									<LinkButton
										prefix={linkButton?.prefix}
										href={linkButton?.linkInner?.url}
										title={linkButton?.linkInner?.title}
										target={linkButton?.linkInner?.target}
									/>
								)}
							</Body>
						</Column>
					))}
			</Grid>
		</Row>
	);
};

export default Columns;
