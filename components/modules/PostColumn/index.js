import styled from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import IntroCopy from '../../elements/IntroCopy';
import InviewWrapper from '../../common/InviewWrapper/index';
import InnerWrapper from '../../common/InnerWrapper/index';
import PortraitImage from '../../common/PortraitImage/index';
import Grid from '../../common/Grid/index';
import OverlapHeading from '../../common/OverlapHeading/index';
import Columns from './Columns';

const Wrapper = styled.div``;

const HeadingRow = styled.div`
	display: block;
	position: relative;
	z-index: 1;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const Heading = styled.div`
	grid-column: 5 / span 8;
`;

const ImageWrapper = styled.div`
	@media ${({ theme }) => theme.breakpoints.desktop} {
		margin-bottom: 110px;
	}
`;

const IntroCopyWrap = styled.div`
	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		> a {
			margin-top: 20px;
		}
	}
`;

const PostColumn = (props) => {
	const {
		backgroundColor,
		anchorId,
		columns,
		alignment,
		linkButton,
		leftImage,
		leftType,
		copy,
		imageCaption,
		title
	} = props || {};

	const LeftColumn = () => (
		<>
			{leftType === 'image' ? (
				<PortraitImage
					image={leftImage}
					alignment={alignment}
					caption={imageCaption}
				/>
			) : (
				<IntroCopyWrap>
					<IntroCopy
						copy={copy}
						flipMobile={leftType === 'image'}
						linkButton={linkButton}
					/>
				</IntroCopyWrap>
			)}
		</>
	);

	const BodyContent = () => (
		<Columns combineColumns={true} columns={columns} title={title} />
	);

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			{title && (
				<HeadingRow flipMobile={leftType === 'image'}>
					<InnerWrapper>
						<Grid>
							<Heading>
								<OverlapHeading title={title} />
							</Heading>
						</Grid>
					</InnerWrapper>
				</HeadingRow>
			)}

			<ThirdsColumn
				flipMobile={false}
				leftColumn={<LeftColumn />}
				rightColumn={<BodyContent />}
			/>
		</InviewWrapper>
	);
};

export default PostColumn;
