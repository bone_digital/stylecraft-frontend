import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';
import InviewWrapper from '../../common/InviewWrapper';
import { Content } from '../../common/WYSWYG';

const TextColumn = styled.div`
	grid-column: span 4;

	@media ${breakpoint('laptop')} {
		grid-column: span 8;
	}
`;

const TextInner = styled(Content)``;

export default function TwoColumnContent({
		backgroundColor,
		anchorId,
		column1Copy,
		column2Copy
	}) {
	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			<InnerWrapper>
				<Grid
					rowGap={'1.333em'}
					rowGapMobile={'1.333em'}
					columns={8}
				>
					<TextColumn>
						<TextInner
							dangerouslySetInnerHTML={{ __html: column1Copy }}
						/>
					</TextColumn>
					<TextColumn>
						<TextInner
							dangerouslySetInnerHTML={{ __html: column2Copy }}
						/>
					</TextColumn>
				</Grid>
			</InnerWrapper>
		</InviewWrapper>
	);
}
