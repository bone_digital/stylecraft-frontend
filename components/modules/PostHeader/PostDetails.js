import Link from 'next/link';
import styled from 'styled-components';
import { ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const PostIntro = styled.div`
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
	flex-direction: column;
	height: 100%;
	a,
	button {
		transition: ${ease('color')};

		&:hover {
			color: ${({ theme, backgroundColor }) =>
				backgroundColor && ['brand', 'pink'].includes(backgroundColor)
					? theme?.colors?.white
					: theme?.colors?.brand};
		}
	}

	@media ${({ theme }) => theme.breakpoints.desktop} {
		padding-bottom: 40px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 24px;
		width: 100%;
	}
`;

const IntroCopy = styled.h2`
	width: 100%;
	max-width: 720px;
	display: block;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		font-size: ${({ theme }) => theme.type.h3[0]};
		line-height: 1.3;
	}

	p {
		font-size: inherit;
		line-height: inherit;
	}
`;

const Details = styled.div`
	width: 100%;
	margin-bottom: 40px;
`;

const DetailsRow = styled.div`
	width: 100%;
	display: flex;
	flex-wrap: wrap;

	&:not(:last-child) {
		margin-bottom: 10px;
	}
`;

const DetailsSub = styled.h5`
	width: 180px;
	flex-shrink: 0;
	text-transform: uppercase;
	letter-spacing: 0.025em;

	@media ${({ theme }) => theme.breakpoints.mobile} {
		width: 50%;
		padding-right: 16px;
	}
`;

const DetailsCopy = styled.h5`
	flex: 1;
	flex-shrink: 0;
	text-transform: uppercase;
	letter-spacing: 0.025em;
`;

const LinkInner = styled.a``;

const PostDetails = ({ terms, content, backgroundColor }) => {
	const categories = terms?.edges;

	const { author, intro, additionalDetails } = content || {};

	return (
		<PostIntro backgroundColor={backgroundColor}>
			<Details>
				{categories &&
					categories?.length > 0 &&
					categories.map(({ node }, index) => {
						return (
							index === 0 && (
								<DetailsRow key={index}>
									<DetailsSub>{'Category'}</DetailsSub>
									<DetailsCopy>
										{node?.link ? (
											<Link
												passHref
												href={searchReplaceLink(
													node?.link
												)}
											>
												<LinkInner title={node?.name}>
													{node?.name}
												</LinkInner>
											</Link>
										) : (
											<>{node?.name}</>
										)}
									</DetailsCopy>
								</DetailsRow>
							)
						);
					})}
				{author && (
					<DetailsRow>
						<DetailsSub>{'Author'}</DetailsSub>
						<DetailsCopy>{author}</DetailsCopy>
					</DetailsRow>
				)}
				{additionalDetails &&
					additionalDetails.length &&
					additionalDetails.map((row, index) => (
						<DetailsRow key={index}>
							<DetailsSub>{row?.subtitle}</DetailsSub>
							<DetailsCopy>{row?.copy}</DetailsCopy>
						</DetailsRow>
					))}
			</Details>

			{intro && <IntroCopy dangerouslySetInnerHTML={{ __html: intro }} />}
		</PostIntro>
	);
};

export default PostDetails;
