import styled from 'styled-components';
import LinkButton from '../../common/LinkButton/index';
import WYSWYG from '../../common/WYSWYG/index';
import { ease } from '../../../styles/theme';

const PostIntro = styled.div`
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
	flex-direction: column;
	height: 100%;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		padding-bottom: 40px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 24px;
	}
`;

const Details = styled.div`
	margin-bottom: 40px;
`;

const ContactMessage = styled.h3`
	position: relative;
	font-weight: 400;
	text-align: left;
	max-width: 350px;
	
	&::before {
		content: '';
		position: absolute;
		bottom: -4px;
		left: 0;
		height: 1px;
		max-width: 130px;
		width: 100%;
		background: ${swatch('black')};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}
	&:hover {
		&::before {
		transform: scaleX(0);
		transform-origin: right;
		}
	}
`;

const Dot = styled.div`
	width: 16px;
	height: 16px;
	border-radius: 50%;
	display: block;
	background: ${({ theme }) => theme.colors.brand};
	margin-right: 22px;
	margin-top: 0.4em;
	flex-shrink: 0;
`;

const Trigger = styled.a`
	position: relative;
	text-align: left;
	display: flex;
	align-items: flex-start;
`;

const ShowroomDetails = () => {
	return (
		<PostIntro>
			<Details>
				<WYSWYG>
					<p>
						145 Flinders Lane
						<br />
						Melbourne VIC 3000
						<br />
						Australia
					</p>

					<p>
						PO Box 18082
						<br />
						Melbourne VIC 3000
						<br />
						Australia
					</p>

					<p>
						t+61 (0)3 9666 4300
						<br />
						f+61 (0)3 9654 1620
						<br />e melbourne@stylecraft.com.au
					</p>

					<p>
						Opening Hours
						<br />
						8:30am - 5:00am
						<br />
						Monday - Friday
					</p>
					<p>
						<LinkButton
							prefix="Meet the"
							title=" Melbourne team."
						/>
					</p>
				</WYSWYG>
			</Details>

			<Trigger>
				<Dot />
				<ContactMessage>
					Contact our showroom to organise a consultation.
				</ContactMessage>
			</Trigger>
		</PostIntro>
	);
};

export default ShowroomDetails;
