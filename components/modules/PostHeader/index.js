import styled from 'styled-components';
import ThirdsColumn from '../../common/ThirdsColumn';
import HalvesColumn from '../../common/HalvesColumn';
import PostDetails from './PostDetails';
import PostFeaturedImage from '../../elements/PostFeaturedImage';
import InviewWrapper from '../../common/InviewWrapper/index';
import OverlapHeading from '../../common/OverlapHeading';
import Grid from '../../common/Grid';
import InnerWrapper from '../../common/InnerWrapper';

const PostHeader = ({ data, post }) => {
	const { terms, title, anchorId } = post || {};

	const { backgroundColor, headingColor, columns, gallery } = data || {};

	const DetailsWrap = () => (
		<PostDetails
			terms={terms}
			content={data}
			backgroundColor={backgroundColor}
		/>
	);

	const FeaturedImageWrap = () => (
		<>
			<PostFeaturedImage gallery={gallery} title={title} />
		</>
	);

	return data && post ? (
		<Row>
			<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
				<Wrapper>
					<Title headingColor={headingColor}>
						<InnerWrapper>
							<Grid>
								<TitleInner>
									<OverlapHeading title={title} post={true} />
								</TitleInner>
							</Grid>
						</InnerWrapper>
					</Title>
					<Body>
						<ThirdsColumn
							flipMobile={false}
							leftColumn={<DetailsWrap />}
							rightColumn={<FeaturedImageWrap />}
						/>

						{columns && columns.length && (
							<HalvesColumn columns={columns} />
						)}
					</Body>
				</Wrapper>
			</InviewWrapper>
		</Row>
	) : null;
};

const Wrapper = styled.div`
	padding-bottom: 100px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 65px;
	}
`;

const Row = styled.div`
	padding-top: 200px;
`;

const Body = styled.div`
	> *:not(:last-child) {
		margin-bottom: 30px;
	}
`;

const Title = styled.div`
	margin-bottom: 0;
	color: ${({ headingColor, theme }) =>
		headingColor ? theme.colors[headingColor] : theme.colors.black};

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const TitleInner = styled.div`
	grid-column: 5 / span 8;

	> div {
		max-width: 100%;
	}
`;

export default PostHeader;
