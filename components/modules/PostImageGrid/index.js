import styled from 'styled-components';
import SectionInner from './SectionInner';
import InviewWrapper from '../../common/InviewWrapper';
import InnerWrapper from '../../common/InnerWrapper';
import OverlapHeading from '../../common/OverlapHeading/index';
import Grid from '../../common/Grid';

const HeadingRow = styled.div`
	display: block;
	position: relative;
	z-index: 5;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const Heading = styled.div`
	grid-column: 5 / span 8;
`;

const PostImageGrid = (props) => {
	const { backgroundColor, copy, gallery, title, linkButton, anchorId } = props;

	return (
		<InviewWrapper id={anchorId} sectionTheme={backgroundColor}>
			{title && (
				<HeadingRow>
					<InnerWrapper>
						<Grid>
							<Heading>
								<OverlapHeading title={title} />
							</Heading>
						</Grid>
					</InnerWrapper>
				</HeadingRow>
			)}

			<SectionInner
				title={title}
				copy={copy}
				gallery={gallery}
				linkButton={linkButton}
			/>
		</InviewWrapper>
	);
};

export default PostImageGrid;
