import styled from 'styled-components';
import ImageBg from '../../common/Thumbnail/index';
import { AnimateSharedLayout, motion, AnimatePresence } from 'framer-motion';
import { useState, useRef } from 'react';
import OverlayCard from './OverlayCard';
import { ease } from '../../../styles/theme';
import { useUI } from '../../../context/UIProvider';

const Wrapper = styled.div`
	position: relative;
	display: ${({ device }) => (device === 'desktop' ? 'block' : 'none')};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: ${({ device }) => (device === 'mobile' ? 'block' : 'none')};
	}

	svg {
		width: 8px !important;
	}
`;

const CloseBG = styled.button`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
`;

const ImageGrid = styled(motion.div)`
	display: grid;
	width: 100%;
	grid-template-columns: repeat(8, minmax(0, 1fr));
	row-gap: 30px;
	column-gap: 30px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		row-gap: 18px;
		column-gap: 18px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		row-gap: 10px;
		column-gap: 10px;
	}
`;

const ImageWrapper = styled.div`
	grid-column: span 4;
	position: relative;

	&::before {
		content: '';
		padding-top: 63.33%;
		display: block;
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
	}

	* {
		position: absolute;
		height: 100%;
		width: 100%;
		top: 0;
		left: 0;
		object-fit: cover;
	}
`;

const CardTrigger = styled.button`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 5;
`;

const MotionImageWrapper = motion(ImageWrapper);

const MotionTrigger = motion(CardTrigger);

const GridContent = ({ gallery }) => {
	const [activeCard, setActiveCard] = useState(null);
	const [activeOffset, setActiveOffset] = useState({});
	const { setPageTheme } = useUI() || {};
	const imageRefs = useRef([]);

	return gallery && gallery.length ? (
		<>
			{activeCard !== null && (
				<CloseBG
					onClick={() => {
						setActiveCard(null);
					}}
				/>
			)}

			<Wrapper device="desktop">
				<AnimateSharedLayout type="crossfade">
					<ImageGrid layout>
						{gallery.map(({ image }, index) => (
							<MotionImageWrapper
								className={
									gallery?.length > 2 && 'cursor-link--view'
								}
								key={`card-${index}`}
								layoutId={`card-container-${index}`}
								layout
								ref={ref => imageRefs.current[index] = ref}
							>
								{gallery?.length > 2 && (
									<MotionTrigger
										onClick={() => {
											if (activeCard === null) {
												setActiveCard(index);
												setActiveOffset(
													index === gallery?.length - 1 || index === gallery?.length - 2
														? { bottom: 0, top: 'unset' }
														: { 
															bottom: 'unset',
															top: imageRefs?.current?.[index]?.offsetTop
														}
												);
												setPageTheme('overlay');
											} else {
												setActiveCard(null);
											}
										}}
									/>
								)}
								<ImageBg
									image={image?.sourceUrl}
									thumbnail={image?.thumbSourceUrl}
									alt={image?.alt}
								/>
							</MotionImageWrapper>
						))}
					</ImageGrid>

					<AnimatePresence initial={false} exitBeforeEnter>
						{activeCard !== null && (
							<OverlayCard
								layoutId={`card-container-${activeCard}`}
								image={gallery[activeCard]?.image}
								thumbnail={gallery[activeCard]?.image}
								activeOffset={activeOffset}
								onClick={() => {
									setActiveCard(null);
								}}
							/>
						)}
					</AnimatePresence>
				</AnimateSharedLayout>
			</Wrapper>

			<Wrapper device="mobile">
				<ImageGrid layout>
					{gallery.map(({ image }, index) => (
						<MotionImageWrapper
							key={`card-${index}`}
							layoutId={`card-container-${index}`}
							layout
						>
							<ImageBg
								image={image?.sourceUrl}
								thumbnail={image?.thumbSourceUrl}
								alt={image?.alt}
							/>
						</MotionImageWrapper>
					))}
				</ImageGrid>
			</Wrapper>
		</>
	) : null;
};

export default GridContent;
