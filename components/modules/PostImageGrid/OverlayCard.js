import styled from 'styled-components';
import Close from '../../../public/icons/close.svg';
import ImageEl from '../../common/ImageEl/index';
import { motion } from 'framer-motion';

const CardTrigger = styled(motion.button)`
	position: absolute;
	top: 0;
	right: 0;
	width: 100%;
	height: 100%;
	z-index: 10;
`;

const CardInner = styled(motion.div)``;

const CloseTrigger = styled(motion.button)`
	width: 44px;
	height: 44px !important;
	border-radius: 99%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 50%;
	left: 0;
	max-width: 44px;
	transform: translate(calc(-100% - 20px), -50%);
	background: ${({ theme }) => theme.colors.white};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		top: auto;
		bottom: 0;
		left: 50%;
		transform: translate(-50%, calc(100% + 20px));
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: none;
	}
`;

const Card = styled(motion.div)`
	position: absolute;
	top: ${({ $activeOffset }) => `${$activeOffset?.top}px` || 0};
	bottom: ${({ $activeOffset }) => $activeOffset?.bottom || 0};
	left: 0;
	width: 100%;
	z-index: 10;

	&::before {
		content: '';
		padding-top: calc(63.33% + 12px);
		display: block;
		width: 100%;
	}

	img {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
`;

const OverlayCard = ({ layoutId, image, thumbnail, activeOffset, onClick }) => {

	return (
		<>
			<Card layoutId={layoutId} $activeOffset={activeOffset}>
				<CardInner>
					<CloseTrigger onClick={onClick}>
						<Close />
					</CloseTrigger>

					<CardTrigger onClick={onClick} />
					<ImageEl
						image={image?.sourceUrl}
						thumbnail={image?.thumbSourceUrl}
						alt={image?.alt}
					/>
				</CardInner>
			</Card>
		</>
	);
};

export default OverlayCard;
