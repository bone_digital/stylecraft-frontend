import styled from 'styled-components';
import GridContent from './GridContent';
import ThirdsColumn from '../../common/ThirdsColumn';
import IntroCopy from '../../elements/IntroCopy';
import InviewWrapper from '../../common/InviewWrapper';
import LinkButton from '../../common/LinkButton';
import MobileModuleHeading from '../../common/MobileModuleHeading';

const IntroWrapper = styled.div`
	margin-bottom: 40px;

	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		> a {
			margin-top: 20px;
		}
	}
`;

const Wrapper = styled.div`
	position: relative;
`;

const SectionInner = ({ title, gallery, copy, linkButton }) => {
	const ImageGallery = () => (
		<>
			<MobileModuleHeading title={title} />
			<GridContent gallery={gallery} />
		</>
	);

	const GalleryIntro = () => (
		<IntroWrapper>
			<IntroCopy copy={copy} linkButton={linkButton} />
		</IntroWrapper>
	);

	return (
		<Wrapper>
			<ThirdsColumn
				flipMobile={false}
				leftColumn={<GalleryIntro />}
				rightColumn={<ImageGallery />}
			/>
		</Wrapper>
	);
};

export default SectionInner;
