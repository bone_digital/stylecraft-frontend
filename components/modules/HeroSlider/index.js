import styled from 'styled-components';
import InnerWrapper from '../../common/InnerWrapper';
import OverlapHeading from '../../common/OverlapHeading';
import SwiperCore, { Navigation, A11y, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { breakpoint } from '../../../styles/theme';
import { useState, useRef } from 'react';
import { motion, AnimateSharedLayout } from 'framer-motion';
import InviewWrapper from '../../common/InviewWrapper';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;
`;

const SlideInner = styled.div`
	top: 0%;
	left: 0%;
	right: 0;
	bottom: 0;
	overflow: hidden;
	height: 100%;
	width: 100%;
	position: absolute;
`;

const HeadingWrapper = styled.div`
	position: relative;
	z-index: 10;
	font-size: ${({ theme }) => theme.type.h1[0]};
	min-height: ${({ theme }) => `${theme.type.h1[1] * 2}em`};
	display: flex;
	align-items: flex-end;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		font-size: ${({ theme }) => theme.typeMobile.h1[0]};
		min-height: ${({ theme }) => `${theme.typeMobile.h1[1] * 3}em`};
	}
`;

const Row = styled.div`
	overflow: hidden;
	margin-top: 132px;
	margin-bottom: 100px;
	/*
	* {
		overflow: visible !important;
	} */

	@media ${breakpoint('tablet')} {
		margin-bottom: 65px;
	}

	@media ${breakpoint('mobile')} {
		margin-top: 100px;
	}

	img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
	/*
	button {
		position: fixed;
		bottom: 0%;
		left: 0;
		z-index: 100;
	} */

	> div > div:first-child {
		max-width: 100%;
	}
`;

const Wrapper = styled(motion.div)`
	position: relative;
	width: 100%;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 5;
	/* max-width: 500px; */
	margin: 0 auto;
`;

const Slide = styled.div`
	position: relative;
	overflow: hidden;
	display: block;

	&::before {
		content: '';
		padding-top: 45%;
		width: 100%;
		display: block;

		@media ${breakpoint('laptop')} {
			padding-top: 66%;
		}
	}
`;

const HeadingInner = styled(motion.div)`
	width: 100%;
`;

const Gallery = styled(motion.div)`
	position: relative;
`;

const Nav = styled.div`
	display: ${({ type }) => (type !== 'mobile' ? 'flex' : 'none')};
	position: absolute;
	top: 0;
	right: 0;
	transform: translateY(-100%);
	padding-bottom: 40px;

	@media ${breakpoint('laptop')} {
		display: ${({ type }) => (type === 'mobile' ? 'flex' : 'none')};
		position: static;
		top: auto;
		right: auto;
		transform: none;
		padding: 20px 0 0 0;
		justify-content: center;
	}
`;

SwiperCore.use([Navigation, A11y, Autoplay]);

export default function HeroSlider({ data }) {
	const { backgroundColor, gallery, anchorId } = data || {};

	const [slideIndex, setSlideIndex] = useState(1);
	const [currentIndex, setCurrentIndex] = useState(0);

	const prevRef = useRef();
	const nextRef = useRef();

	const headingVariant = {
		active: {
			opacity: 1
		},
		inactive: {
			opacity: 0
		}
	};

	return data ? (
		<>
			<InviewWrapper id={anchorId} sectionTheme={backgroundColor} initial={true}>
				{gallery && gallery.length && (
					<Row>
						<InnerWrapper>
							<AnimateSharedLayout>
								<HeadingWrapper layout="position">
									{gallery.map((slide, index) => {
										return (
											index === currentIndex && (
												<HeadingInner
													key={`heading-${index}`}
													variants={headingVariant}
													initial={'inactive'}
													exit={'inactive'}
													animate={'active'}
													layout
												>
													<OverlapHeading
														title={slide?.title}
													/>
												</HeadingInner>
											)
										);
									})}
								</HeadingWrapper>

								<Wrapper layoutId={'slideOuter'}>
									<Nav type="desktop">
										{slideIndex} / {gallery?.length || 1}
									</Nav>

									<Gallery layoutId={'slideInner'}>
										{gallery && gallery?.length > 1 && (
											<>
												<NavElement
													data-cursor="true"
													className="cursor-link cursor-link--carousel-prev"
													direction="prev"
													ref={prevRef}
												/>
												<NavElement
													data-cursor="true"
													className="cursor-link cursor-link--carousel-next"
													direction="next"
													ref={nextRef}
												/>
											</>
										)}

										<Swiper
											loop={true}
											preventInteractionOnTransition={
												true
											}
											speed={400}
											centeredSlides={false}
											slidesPerView="auto"
											spaceBetween={0}
											onSlideChange={({ realIndex }) => {
												setSlideIndex(realIndex + 1);
												setCurrentIndex(realIndex);
											}}
											onInit={(swiper) => {
												swiper.params.navigation.prevEl =
													prevRef.current;
												swiper.params.navigation.nextEl =
													nextRef.current;

												swiper.navigation.init();
												swiper.navigation.update();
											}}
											autoplay={{
												delay: 7000
											}}
										>
											{gallery.map((slide, index) => (
												<SwiperSlide key={index}>
													<Slide>
														<SlideInner>
															{slide?.link ? (
																<Link href={searchReplaceLink(slide?.link?.url)} passHref>
																	<a>
																		<motion.img
																			src={
																				slide?.image
																					?.largeSourceUrl
																			}
																			alt={
																				slide?.image
																					?.alt
																			}
																		/>
																	</a>
																</Link>
															):(
																<motion.img
																	src={
																		slide?.image
																			?.largeSourceUrl
																	}
																	alt={
																		slide?.image
																			?.alt
																	}
																/>
															)}
														</SlideInner>
													</Slide>
												</SwiperSlide>
											))}
										</Swiper>
									</Gallery>
								</Wrapper>

								<Nav type="mobile">{slideIndex} / 4</Nav>
							</AnimateSharedLayout>
						</InnerWrapper>
					</Row>
				)}
			</InviewWrapper>
		</>
	) : null;
}
