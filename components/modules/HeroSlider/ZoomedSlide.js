import styled from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';

const Wrapper = styled.div`
	/* clip-path: polygon(0% 100%, 100% 100%, 100% 100%, 0% 99.999%); */
	position: absolute;
	z-index: 500;
	left: 50%;
	top: 0;
	right: 0;
	transform: translate(-50%, 0);
	overflow: visible;
	display: flex;
	align-items: center;
	height: 100%;
	width: 150vw;
	justify-content: center;
`;

const Slide = styled(motion.div)`
	position: relative;
	width: 100%;

	&::before {
		content: '';
		padding-top: 45%;
		width: 100%;
		display: block;

		@media ${breakpoint('laptop')} {
			padding-top: 98.5%;
		}

		@media ${breakpoint('tablet')} {
			padding-top: 108%;
		}
	}
`;

const SlideInner = styled(motion.div)`
	top: 0%;
	left: 0%;
	right: 0;
	bottom: 0;
	height: 100%;
	width: 100%;
`;

const Image = styled.img`
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
`;

const ZoomedSlide = ({ image, alt }) => {
	// const wrapperVariant = {
	// 	active: {
	// 		clipPath: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)',
	// 		transition: {
	// 			duration: 1.2,
	// 			delay: 0.6
	// 		}
	// 	},
	// 	inactive: {
	// 		clipPath: 'polygon(0% 100%, 100% 100%, 100% 100%, 0% 99.999%)',
	// 		transition: {
	// 			duration: 1.2,
	// 			delay: 0.6
	// 		}
	// 	}
	// }

	// const imgVariant = {
	// 	active: {
	// 		transform: 'translateY(0%)',
	// 		transition: {
	// 			duration: 1.2,
	// 			delay: 0.6
	// 		}
	// 	},
	// 	inactive: {
	// 		transform: 'translateY(20%)',
	// 		transition: {
	// 			duration: 1.2,
	// 			delay: 0.6
	// 		}
	// 	}
	// }

	return image ? (
		<Wrapper>
			<Slide
				layoutId={'slideOuter'}
				transition={{
					duration: 2.4,
					delay: 0.6
				}}
			>
				<SlideInner
					layoutId={'slideInner'}
					transition={{
						duration: 2.4,
						delay: 0.6
					}}
				>
					<Image src={image} alt={alt} />
				</SlideInner>
			</Slide>
		</Wrapper>
	) : null;
};

export default ZoomedSlide;
