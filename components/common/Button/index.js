import link from 'next/link';
import React from 'react';
import styled, { css, keyframes } from 'styled-components';
import { ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const Button = ({
	color,
	disabled,
	title,
	target,
	href,
	onClick,
	type = 'button'
}) => {
	return !href || href === null ? (
		<Trigger
			type={type}
			onClick={onClick}
			title={title}
			aria-label={title}
			disabled={disabled}
			color={color}
		>
			{title}
		</Trigger>
	) : (
		<Link href={searchReplaceLink(href)} passHref>
			<Trigger
				as="a"
				title={title}
				aria-label={title}
				target={target}
				disabled={disabled}
				color={color}
			>
				{title}
			</Trigger>
		</Link>
	);
};

const spinAround = keyframes`
	0% {
		transform: rotate(0deg);
	}

	100% {
		transform: rotate(359deg);
	}
`;

const Trigger = styled.button`
	padding: 9px 28px;
	color: ${({ color, theme }) =>
		color !== 'white' ? theme.colors.white : theme.colors.brand};
	display: inline-flex;
	text-align: center;
	transition: ${ease('all')};
	position: relative;
	justify-content: center;
	align-items: center;
	z-index: 10;
	background: ${({ theme, color }) =>
		color ? theme.colors[color] : theme.colors.brand};
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};

	&::before {
		content: '';
		position: absolute;
		top: 50%;
		left: 50%;
		margin-left: -0.5em;
		margin-top: -0.5em;
		transition: ${ease('all')};
		border: 2px solid
			${({ theme, color }) =>
				color !== 'white' ? theme.colors.white : theme.colors.brand};
		animation: ${spinAround} 0.6s linear infinite;
		border-radius: 999px;
		border-right-color: transparent;
		border-top-color: transparent;
		display: block;
		height: 1em;
		width: 1em;
		opacity: 0;
	}

	&:hover {
		background: ${({ theme }) => theme.colors.black};
		background: ${({ theme, color }) =>
			color !== 'black' ? theme.colors.black : theme.colors.brand};
	}

	${({ disabled }) =>
		disabled &&
		css`
			color: transparent;

			&::before {
				opacity: 1;
			}
		`};
`;

export default Button;
