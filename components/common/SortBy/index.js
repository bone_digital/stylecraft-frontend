import styled from 'styled-components';
import FormField from '../FormField';

const SelectFilter = styled.div`
	display: flex;
	align-items: baseline;
	margin-bottom: 32px;
	display: flex;
	display: ${({ respondTo }) =>
		respondTo ? (respondTo === 'desktop' ? 'flex' : 'none') : 'flex'};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		display: flex;
		display: ${({ respondTo }) =>
			respondTo ? (respondTo !== 'desktop' ? 'flex' : 'none') : 'flex'};
	}
`;

const Label = styled.label`
	margin-right: 16px;
	color: ${({ theme }) => theme.colors.black};
	opacity: 0.4;
`;

const SortBy = ({ options, setSortPostsBy, respondTo }) => {
	const handleChange = (e) => {
		const value = e.target.value;
		const option = options.find(o => o.value === value);
		setSortPostsBy(option);
	};

	return options ? (
		<SelectFilter respondTo={respondTo}>
			<Label>Sort</Label>
			<FormField
				type="select"
				name="country"
				label="Country"
				options={options}
				onChange={handleChange}
			/>
		</SelectFilter>
	) : null;
};

export default SortBy;
