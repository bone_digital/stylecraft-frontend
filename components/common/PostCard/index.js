import styled, { css } from 'styled-components';
import { breakpoint, ease } from '../../../styles/theme';
import ImageEl from '../ImageEl';
import Thumbnail from '../Thumbnail';
import LinkItem from '../LinkHeading';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const Card = styled.div`
	width: 100%;
	position: relative;
	overflow: hidden;
	grid-column: ${({ size }) =>
		size === 'large'
			? 'span 6'
			: size === 'xLarge'
			? 'span 8'
			: size === 'medium'
			? 'span 4'
			: 'span 4'};

	${({ size }) =>
		size !== 'large' &&
		css`
			@media ${breakpoint('laptop')} {
				grid-column: -1 / 1;
			}
		`};

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}

	&:hover {
		&&& img {
			transform: scale(1.1) translate(-50%, -50%);
		}
	}
`;

const ThumbnailWrap = styled.a`
	margin-bottom: 20px;
	position: relative;
	display: flex;
	transition: ${ease('all')};

	@media ${breakpoint('laptop')} {
		margin-bottom: 32px;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 16px;
	}
`;

const Details = styled.a`
	display: block;
	transition: ${ease('opacity')};

	/* @media ${breakpoint('laptop')} {
		.swiper-slide + :not(.swiper-slide-active) & {
			opacity: 0;
		}
	} */
`;

const CardLink = styled.div`
	display: flex;
	align-items: baseline;

	h5 {
		opacity: 0.4;
		margin-right: 20px;
	}
`;

const Title = styled.h3``;

const PostCard = ({ size, post, type, width }) => {
	const ratio = size === 'large' ? '66.71%' : '75.63%';

	const ratioMobile = ratio;

	const featuredImage = post?.featuredImage?.node || post?.image;

	let subheading = 'View';
	let heading =
		type === 'VariableProduct' ||
		type === 'SimpleProduct' ||
		type === 'product'
			? post?.name
			: post?.title;

	if (type === 'Post') {
		if (post?.terms?.edges?.length) {
			subheading = post?.terms?.edges[0]?.node?.name;
		} else {
			subheading = 'News';
		}
	} else if (
		type === 'SimpleProduct' ||
		type === 'VariableProduct' ||
		type === 'product'
	) {
		subheading = post?.product?.brand?.title;
	} else if (type === 'Project') {
		if (post?.terms?.edges?.length) {
			subheading = post?.terms?.edges[0]?.node?.name;
		} else {
			subheading = 'Project';
		}
	}

	return post ? (
		<Card size={size} width={width}>
			<Link href={searchReplaceLink(post?.link || '#')} passHref>
				<ThumbnailWrap title={heading} size={size}>
					{
						<Thumbnail
							ratio={ratio}
							ratioMobile={ratioMobile}
							image={featuredImage?.sourceUrl}
							thumbnail={featuredImage?.thumbSourceUrl}
							alt={featuredImage?.alt}
						/>
					}
				</ThumbnailWrap>
			</Link>

			<Link href={searchReplaceLink(post?.link || '#')} passHref>
				<Details title={heading}>
					<CardLink>
						<LinkItem title={subheading} />
						<Title dangerouslySetInnerHTML={{ __html: heading }} />
					</CardLink>
				</Details>
			</Link>
		</Card>
	) : null;
};

export default PostCard;
