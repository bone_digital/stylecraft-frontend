import { Field } from 'formik';
import { ease } from '../../../styles/theme';
import { useState, useEffect } from 'react';
import styled from 'styled-components';
import Error from '../Error';

const FieldWrap = styled.fieldset`
	max-width: 100%;
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	padding: 0 0;
	margin-left: 0;
	margin-right: 0;
	border: none;
`;

const InputWrap = styled.div`
	display: block;
	position: relative;
	display: flex;
	align-items: start;
	width: 100%;

	&:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const Input = styled(Field)`
	padding: 0 0 6px 0;
	width: 100%;
	border-bottom: 1px solid;
	transition: ${ease('all')};
	display: block;
	border-color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	transition: ${ease('all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
	appearance: none;
	border-radius: 0;

	&::placeholder {
		opacity: 1;
		color: ${({ theme }) => theme.colors.black};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function NameField({
	field,
	error,
	disabled,
	value,
	onBlur,
	onChange,
	setFieldValue
}) {
	const { id, formId, cssClass, isRequired, fieldErrors, inputs, key } =
		field || {};

	const inputId = `input_${formId}_${id}`;
	const htmlId = `field_${formId}_${id}`;

	const [name, setName] = useState(value);

	const handleChange = (e) => {
		const { attributes, value: inputValue } = e.target;
		const inputValueId = attributes?.valueId?.nodeValue;

		setName((prevState) => {
			let newName = {
				...prevState,
				[inputValueId]: inputValue
			};

			return newName;
		});
	};

	useEffect(() => {
		if (
			name &&
			Object.keys(name).length === 0 &&
			Object.getPrototypeOf(name) === Object.prototype
		) {
			// empty
		} else {
			setFieldValue(key, name);
		}
	}, [name]);

	return (
		<FieldWrap id={htmlId} name={key} className={`${cssClass}`.trim()}>
			{inputs.map((input) => {
				const inputKey = `${input?.id}` || '';
				const inputLabel = input?.label || '';
				const placeholder = input?.placeholder || '';
				const isHidden = input?.isHidden || false;
				const valueId = input?.autocompleteAttribute;

				return (
					!isHidden && (
						<InputWrap key={inputKey}>
							<Label htmlFor={inputId}>{inputLabel}</Label>
							<Input
								type="text"
								name={String(`${key}.${inputKey}`)}
								id={inputId}
								valueId={valueId}
								required={Boolean(isRequired)}
								placeholder={placeholder}
								value={value[valueId] || ''}
								disabled={disabled}
								error={error}
								onBlur={onBlur}
								onInput={(e) => handleChange(e)}
							/>
						</InputWrap>
					)
				);
			})}
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
