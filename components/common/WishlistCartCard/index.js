import React from 'react';
import styled from 'styled-components';
import QuantityPicker from '../../modules/ProductMain/QuantityPicker';
import Thumbnail from '../../common/Thumbnail';
import Link from 'next/link';
import { ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import { formatPriceWithoutDecimals } from '../../../lib/source/wordpress/utils';

const Card = styled.div`
	display: flex;
	align-items: flex-start;
	font-size: ${({ theme }) => theme.type.p[0]};
	line-height: 28px;
	transition: ${ease('all')};
	opacity: ${({ updating }) => (updating ? 0.5 : 1)};
	flex-wrap: wrap;
`;

const Title = styled.h3`
	margin-bottom: 8px;
`;

const ProductDetails = styled.div`
	padding-right: 30px;
	max-width: 300px;
`;

const LinkInner = styled.a`
	display: flex;
	width: 100%;
`;

const Header = styled.div`
	margin-bottom: 1.333em;
`;

const ThumbnailWrapper = styled.div`
	position: relative;
	display: flex;
	max-width: 150px;
	width: 100%;
	margin-right: 60px;
	flex-shrink: 0;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		max-width: 100%;
		margin-bottom: 1.333em;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		max-width: 150px;
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		max-width: 100%;
		margin-bottom: 1.333em;
	}
`;

const Clear = styled.button`
	text-decoration: underline;
	margin-top: 1.33em;

	&:not(:last-child) {
		margin-right: 16px;
	}
`;

const Row = styled.div`
	display: flex;
`;

const Subtitle = styled.div`
	width: 38.95%;
`;

const Body = styled.div`
	flex: 1;
`;

const ItemPrice = styled.h3`
	margin-left: auto;
`;

const WishlistCartCard = ({	
	isWishlist = false,
	updating,
	image,
	featuredImage,
	name,
	link,
	designer,
	quantity,
	decreaseQuantity,
	increaseQuantity,
}) => {

	const copy = () => {
		let string = `${quantity} x ${name} - By ${designer?.name}`;

		if (productAttributes) {
			productAttributes?.forEach(({ label, value }) => {
				string += `, ${label}: ${value}`;
			});
		}

		let tempInput = document.createElement('input');
		document.body.appendChild(tempInput);
		tempInput.value = string;
		tempInput.select();
		document.execCommand('copy', false);
		tempInput.remove();
	};


	return (
		<Card updating={updating}>
			<ThumbnailWrapper>
				<Link href={searchReplaceLink(link) || '#'} passHref>

					<LinkInner title={name}>
						{
							<Thumbnail
							ratio={'100%'}
							ratioMobile={'100%'}
							image={image}
							thumbnail={image}
							alt={featuredImage?.node?.alt}
						/>
						}
					</LinkInner>
				</Link>
			</ThumbnailWrapper>

			<ProductDetails>
				<Header>
					<Title> {name}</Title>
					{designer && <Body>{`By ${designer?.name}`}</Body>}
				</Header>

				<Row>
					<Subtitle>Qty</Subtitle>
					<QuantityPicker
						quantity={quantity}
						increaseQuantity={increaseQuantity}
						decreaseQuantity={decreaseQuantity}
						showStock={false}
					/>
				</Row>

				<Row>
					<Clear onClick={decreaseQuantity}>Remove</Clear>
					{isWishlist && <Clear onClick={copy}>Copy Details</Clear>}
				</Row>
			</ProductDetails>
		</Card>
	);
};

export default WishlistCartCard;
