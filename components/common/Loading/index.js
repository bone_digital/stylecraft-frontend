import styled, { keyframes } from 'styled-components';
import { ease, swatch } from '../../../styles/theme';

const bubble = keyframes`
	0% {
		transform: scale(1);
		opacity: 1;
	}

	75%,
	100% {
		transform: scale(0.55);
		opacity: 0.3;
	}
`;

const Loader = styled.div`
	position: relative;
	width: 100%;
	display: flex;
	opacity: ${({ active }) => (active !== null ? (active ? 1 : 0) : 1)};
	align-items: center;
	justify-content: center;
	transition: ${ease('all')};
`;

const LoaderInner = styled.div`
	position: ${({ paging }) => (paging ? 'relative' : 'absolute')};
	min-height: ${({ paging }) => (paging ? 'auto' : '250px')};
	display: flex;
	align-items: center;
	justify-content: center;
	margin: ${({ paging }) => (paging ? '40px 0 75px' : '0 0')};
`;

const LoadingDot = styled.span`
	display: inline-block;
	background-color: ${({ pageTheme }) =>
		pageTheme
			? pageTheme === 'white'
				? swatch('brand')
				: swatch('white')
			: swatch('brand')};
	width: 18px;
	height: 18px;
	margin: 10px;
	border-radius: 100%;
	animation: ${bubble} 0.6s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite
		alternate;

	&:nth-child(2) {
		animation-delay: 0.2s;
	}

	&:nth-child(3) {
		animation-delay: 0.4s;
	}
`;

const Loading = ({ pageTheme, active, paging }) => {
	return (
		<Loader active={active ? true : false}>
			<LoaderInner paging={paging}>
				<LoadingDot pageTheme={pageTheme} />
				<LoadingDot pageTheme={pageTheme} />
				<LoadingDot pageTheme={pageTheme} />
			</LoaderInner>
		</Loader>
	);
};

export default Loading;
