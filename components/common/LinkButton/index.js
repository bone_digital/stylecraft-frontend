import styled from 'styled-components';
import Link from 'next/link';
import { swatch, ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const LinkInner = styled.a`
	display: inline-flex;
	position: relative;
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	transition: color 280ms ease;

	&:hover {
		color: ${swatch('brand')};
	}

	&::before {
		content: '';
		position: absolute;
		bottom: 0px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${swatch('black')};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${LinkInner}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const Title = styled.h5`
	text-transform: ${({ uppercase }) =>
		uppercase ? 'uppercase' : 'capitalize'};
	letter-spacing: 0.025em;
`;

const LinkButton = ({
	prefix = 'View',
	title = 'More',
	type = 'half',
	uppercase = 'true',
	href,
	target,
	onClick
}) => {
	if (href && href !== null && typeof href === 'string') {
		// Checks if the link is external
		let isExternal

		if (href.includes(process.env.NEXT_PUBLIC_CMS_URL) || href.includes(process.env.NEXT_PUBLIC_SITE_URL)) {
			isExternal = false;
		} else {
			isExternal = true;
		}

		return (
			isExternal ?
			(
				<LinkInner title={title} target={target} href={href}>
					<Title uppercase={uppercase}>
						{type === 'half' && <>{prefix}&nbsp;</>}
						<Underline>{title}</Underline>
					</Title>
				</LinkInner>
			) : (
				<Link href={searchReplaceLink(href, isExternal)} passHref>
					<LinkInner title={title} target={target}>
						<Title uppercase={uppercase}>
							{type === 'half' && <>{prefix}&nbsp;</>}
							<Underline>{title}</Underline>
						</Title>
					</LinkInner>
				</Link>
			)
		);
	} else {
		return (
			<LinkInner title={title} as="button" onClick={onClick}>
				<Title uppercase={uppercase}>
					{type === 'half' && <>{prefix}&nbsp;</>}
					<Underline>{title}</Underline>
				</Title>
			</LinkInner>
		);
	}
};

export default LinkButton;
