import styled from 'styled-components';
import ImageBg from '../Thumbnail';
import Grid from '../Grid';
import ImageCaption from '../ImageCaption';

const Column = styled.div`
	height: 100%;
	width: 100%;

	> div {
		height: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.desktop} {
		order: 2;
	}
`;

const ImageWrapper = styled.div`
	grid-column: 2 / span 2;
	display: flex;
	align-items: ${({ alignment }) => (alignment ? alignment : 'flex-start')};
	flex-wrap: wrap;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
	}
`;

const InnerWrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;

	> div + span {
		margin-top: 16px;
	}
`;

const PortraitImage = ({ alignment, image, caption }) => {
	const alignmentArray = {
		top: 'flex-start',
		center: 'center',
		bottom: 'flex-end'
	};

	const imageAlignment = alignment ? alignmentArray[alignment] : null;

	return image ? (
		<Column>
			<Grid columns={4}>
				<ImageWrapper alignment={imageAlignment}>
					<InnerWrapper>
						<ImageBg
							ratio={'140%'}
							image={image?.sourceUrl}
							thumbnail={image?.thumbSourceUrl}
							alt={image?.alt}
						/>

						{caption && <ImageCaption caption={caption} />}
					</InnerWrapper>
				</ImageWrapper>
			</Grid>
		</Column>
	) : null;
};

export default PortraitImage;
