import ReactPlayer from 'react-player';
import styled from 'styled-components';
import MobileModuleHeading from '../MobileModuleHeading';
import { useInView } from 'react-intersection-observer';
import ImageEl from '../ImageEl';
import { useState } from 'react';

const Wrapper = styled.div``;

const VideoWrapper = styled.div`
	position: relative;
	clip-path: ${({ active }) =>
		!active
			? 'polygon(0% 100%, 100% 100%, 100% 100%, 0% 99.999%)'
			: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)'};
	transition: all 0.8s cubic-bezier(0.87, 0, 0.82, 0.99);
	overflow: hidden;
	display: block;

	&::before {
		padding-top: 56.25%;
		content: '';
		display: block;
		width: 100%;
	}

	> * {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		padding-top: 0;
	}

	> div > div {
		padding: 0 0 0 0 !important;
		height: 100%;
		width: 100%;

		> div {
			padding: 0 0 0 0 !important;
			height: 100%;
			width: 100%;
		}
	}
`;

const VideoTrigger = styled.button`
	z-index: 10;
	height: 100%;
	width: 100%;
	opacity: ${({ playing }) => (playing ? 0 : 1)};
	visibility: ${({ playing }) => (playing ? 'hidden' : 'visible')};
	pointer-events: ${({ playing }) => (playing ? 'none' : 'all')};

	img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
`;

const VideoPlayer = ({ video, controls = true, title }) => {
	const [playing, setPlaying] = useState(false);

	const { ref, inView } = useInView({
		triggerOnce: true,
		root: null,
		rootMargin: '-10% 0px',
		delay: 100
	});

	const handlePlay = () => {
		setPlaying(true);
	};

	const { url, thumbnail } = video || null;

	return video ? (
		<Wrapper>
			<MobileModuleHeading title={title} />
			<VideoWrapper active={inView} ref={ref}>
				<VideoTrigger onClick={handlePlay} playing={playing}>
					{thumbnail && (
						<ImageEl
							image={thumbnail?.sourceUrl}
							thumbnail={thumbnail?.thumbSourceUrl}
							alt={thumbnail?.altText}
						/>
					)}
				</VideoTrigger>

				<ReactPlayer
					url={url}
					controls={controls}
					playsInline={true}
					width="100%"
					height="100%"
					playing={playing}
					active={inView.toString()}
					config={{
						vimeo: {
							playerOptions: {
								color: 'EB008B',
								byline: false,
								controls: true,
								title: false,
								playsinline: true,
								responsive: true
							}
						},
						youtube: {
							playerVars: {
								modestbranding: true,
								playsinline: true
							}
						}
					}}
				/>
			</VideoWrapper>
		</Wrapper>
	) : null;
};

export default VideoPlayer;
