import Select, { ValueType, ActionMeta, OptionTypeBase } from 'react-select';

export default function MultiSelectField({ field, error }) {
	const {
		id,
		formId,
		type,
		label,
		description,
		cssClass,
		isRequired,
		choices
	} = field;
	const htmlId = `field_${formId}_${id}`;
	const options =
		choices?.map((choice) => ({
			value: choice?.value,
			label: choice?.text
		})) || [];

	return (
		<div className={`${cssClass}`.trim()}>
			<label htmlFor={htmlId}>{label}</label>
			<Select
				isMulti
				name={String(id)}
				inputId={htmlId}
				required={Boolean(isRequired)}
				options={options}
			/>
			{/* {fieldErrors?.length ? fieldErrors.map(fieldError => (
        <p key={fieldError.id} className="error-message">{fieldError.message}</p>
      )) : null} */}
		</div>
	);
}
