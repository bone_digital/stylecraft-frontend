import styled, { css, keyframes } from 'styled-components';
import { ease } from '../../../styles/theme';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { cartActions } from '../../../store/cartSlice';
import * as pinterest from "../../../lib/tracking/pinterest-tracking";
import Cookies from 'js-cookie';

const spinAround = keyframes`
	0% {
		transform: rotate(0deg);
	}

	100% {
		transform: rotate(359deg);
	}
`;

const Button = styled.button`
	padding: 9px 24px;
	width: 100%;
	color: ${({ theme }) => theme.colors.white};
	display: inline-flex;
	text-align: center;
	transition: ${ease('all')};
	position: relative;
	justify-content: center;
	align-items: center;
	z-index: 10;
	background: ${({ theme, outOfStock }) =>
	outOfStock ? theme.colors.greyMedium : theme.colors.black};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};

	&::before {
		content: '';
		position: absolute;
		top: 50%;
		left: 50%;
		margin-left: -0.5em;
		margin-top: -0.5em;
		transition: ${ease('all')};
		border: 2px solid
			${({ theme, sectionTheme }) =>
	sectionTheme === 'pink' || sectionTheme === 'brand'
		? theme.colors.brand
		: theme.colors.white};
		animation: ${spinAround} 0.6s linear infinite;
		border-radius: 999px;
		border-right-color: transparent;
		border-top-color: transparent;
		display: block;
		height: 1em;
		width: 1em;
		opacity: 0;
	}

	&:hover {
		color: ${({ theme, sectionTheme }) =>
	sectionTheme === 'pink' || sectionTheme === 'brand'
		? theme.colors.brand
		: theme.colors.white};

		background: ${({ theme, sectionTheme }) =>
	sectionTheme === 'pink' || sectionTheme === 'brand'
		? theme.colors.white
		: theme.colors.brand};
	}

	${({ adding }) =>
	adding &&
	css`
			> div {
				opacity: 0;
			}

			&::before {
				opacity: 1;
			}
		`};
`;

const Inner = styled.div``;

const AddToCartButton = ({
		variation,
		product,
		quantity,
		type,
		disabled,
		sectionTheme
	}) => {

	const [cartText, setCartText] = useState(
		disabled ? 'Out of Stock' : 'Add to Cart'
	);

	const [loading, setLoading] = useState(false);
	const userId = useSelector(({ user }) => user.id);

	const productId =
		type === 'VariableProduct'
			? variation?.node?.variationId
			: product?.databaseId;

	const dispatch = useDispatch();

	const addToCart = async () => {
		const cartResponse = await fetch('/api/add-to-cart-rest', {
			method: 'POST',
			credentials: 'same-origin',
			body: JSON.stringify({
				id: productId,
				quantity: quantity,
				userId
			})
		})
			.then((res) => res.json())
			.then((json) => {
				console.log('json-res', json);
				dispatch(
					cartActions.newUpdateCart({
						jsonCart: json,
					})
				)

				if (json?.cart_key) {
					Cookies.set('cart-key', json.cart_key, { expires: 10 });
				}

				setCartText('Added to Cart');

				setTimeout(() => {
					setCartText('Add to Cart');
					setLoading(false);
				}, 800);

				pinterest.event('addtocart', {});

				return json;
			}).catch((err) => {
				console.log(err);

				setCartText('Error');

				setTimeout(() => {
					setCartText('Add to Cart');
					setLoading(false);
				}, 800);
			});
	}

	const addToCartHandler = () => {
		setLoading(true);
		setCartText('Adding...');
		addToCart();
	};

	return (
		<Button
			onClick={addToCartHandler}
			disabled={disabled || loading}
			adding={loading}
			outOfStock={disabled}
			sectionTheme={sectionTheme}
		>
			<Inner>{cartText}</Inner>
		</Button>
	);
};

export default AddToCartButton;
