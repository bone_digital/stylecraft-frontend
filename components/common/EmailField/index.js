import styled from 'styled-components';
import { Field } from 'formik';
import { swatch, ease } from '../../../styles/theme';
import Error from '../Error';

export default function EmailField({
	field,
	error,
	disabled,
	value,
	onChange,
	onBlur
}) {
	const {
		id,
		key,
		formId,
		label,
		cssClass,
		isRequired,
		placeholder,
		fieldErrors
	} = field;

	const htmlId = `field_${formId}_${id}`;

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			<Label htmlFor={htmlId}>{label}</Label>
			<Input
				type="email"
				name={String(key)}
				id={htmlId}
				placeholder={placeholder || ''}
				required={Boolean(isRequired)}
				disabled={disabled}
				error={error || fieldErrors?.length}
				value={value}
				onChange={onChange}
				onBlur={onBlur}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
`;

const Input = styled(Field)`
	padding: 0 0 6px 0;
	border-bottom: 1px solid ${swatch('black')};
	transition: ${ease('all')};
	display: block;
	appearance: none;
	border-radius: 0;

	&::placeholder {
		opacity: 1;
		color: ${swatch('black')};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;
