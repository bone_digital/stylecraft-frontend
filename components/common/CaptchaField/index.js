import ReCAPTCHA from 'react-google-recaptcha';
import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';
import { useRef } from "react";

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
	pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function NumberField({
	field,
	error,
	disabled,
	setFieldValue
}) {
	const { key, id, formId, label, cssClass, fieldErrors } = field || {};
	const recaptchaRef = useRef(0);
	const htmlId = `field_${formId}_${id}`;
	function onChange(value) {
		value = `RECAPTCHA${recaptchaRef.current.getValue()}`;
		setFieldValue(key, value);
	}

	return (
		<FieldWrap className={`${cssClass}`.trim()} disabled={disabled}>
			<Label htmlFor={htmlId}>{label}</Label>

			<ReCAPTCHA
				name={String(id)}
				id={htmlId}
				ref={recaptchaRef}
				sitekey={process.env.NEXT_PUBLIC_GF_SITEKEY}
				onChange={onChange}
			/>

			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
