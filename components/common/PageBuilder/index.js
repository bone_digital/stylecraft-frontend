import styled from 'styled-components';
import HotSpot from '../../modules/HotSpot';
import ImageContent from '../../modules/ImageContent';
import PostImageVideo from '../../modules/PostImageVideo';
import LinkList from '../../modules/LinkList';
import BrandShowcase from '../../modules/BrandShowcase';
import ContactCta from '../../modules/ContactCta';
import NewsletterSignup from '../../modules/NewsletterSignup';
import ColumnsBlock from '../../modules/ColumnsBlock';
import PostCarousel from '../../modules/PostCarousel';
import PostShowroom from '../../modules/PostShowroom';
import OneColumnContent from '../../modules/OneColumnContent';
import TwoColumnContent from '../../modules/TwoColumnContent';
import Accordions from  '../../modules/Accordions';

import {
	cleanModuleId,
	cleanModuleName
} from '../../../lib/source/wordpress/utils';

const PagebuilderWrapper = styled.div`
	section {
		padding: 100px 0;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding: 65px 0;
		}
	}

	> *:first-child {
		padding-top: 132px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 100px;
		}
	}
`;

export default function PageBuilder({ modules, showrooms }) {
	const components = {
		Hotspot: HotSpot,
		ImageContent,
		LinkList,
		BrandShowcase,
		ContactCta,
		NewsletterSignup,
		ColumnsBlock,
		PostCarousel,
		Stores: PostShowroom,
		ImageVideo: PostImageVideo,
		OneColumnContent,
		TwoColumnContent,
		Accordions
	};

	return modules ? (
		<PagebuilderWrapper>
			{modules.map((module) => {
				const name = cleanModuleName(module);
				const moduleId = cleanModuleId(module);

				if (!components[name]) {
					console.log({ message: `Missing: ${name}` });
				} else {
					const Component = components[name];
					return name === 'Stores' ? (
						<Component
							key={moduleId}
							showrooms={showrooms || null}
							{...module}
						/>
					) : (
						<Component key={moduleId} {...module} />
					);
				}
			})}
		</PagebuilderWrapper>
	) : null;
}
