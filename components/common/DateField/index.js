import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;

	input[type='date']::-webkit-inner-spin-button,
	input[type='date']::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	input::-webkit-calendar-picker-indicator {
		display: none;
	}

	input[type='date']::-webkit-input-placeholder {
		visibility: hidden !important;
	}
`;

const Input = styled(Field)`
	max-height: 27.5px;
	padding: 0 0 6px 0;
	border: none;
	border-bottom: 1px solid;
	transition: ${ease('all')};
	display: block;
	outline: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	resize: none;
	border-color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	transition: ${ease('all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};

	&::placeholder {
		opacity: 1;
		color: ${({ theme }) => theme.colors.black};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function DateField({
	field,
	error,
	disabled,
	value,
	onChange,
	onBlur
}) {
	const {
		id,
		formId,
		key,
		label,
		cssClass,
		isRequired,
		placeholder,
		fieldErrors
	} = field;

	const htmlId = `field_${formId}_${id}`;

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			<Label htmlFor={htmlId}>{label}</Label>
			<Input
				type="date"
				name={String(key)}
				id={htmlId}
				required={Boolean(isRequired)}
				required={Boolean(isRequired)}
				placeholder={placeholder || ''}
				value={value}
				disabled={disabled}
				error={error || fieldErrors?.length}
				onBlur={onBlur}
				onChange={onChange}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
