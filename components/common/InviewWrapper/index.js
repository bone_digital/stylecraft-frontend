import { useInView } from 'react-intersection-observer';
import styled, { css } from 'styled-components';
import { ease } from '../../../styles/theme';
import React, { useEffect, useState, useRef } from 'react';
import { useUI } from '../../../context/UIProvider';

const Section = styled.section`
	z-index: 5;
	position: relative;

	> * {
		transition: ${ease('all', 'slow')};
	}

	${({ initial, active }) =>
		!initial &&
		css`
			> * {
				opacity: ${({ active }) => (active ? 1 : 0)};
				transform: ${({ active }) =>
					active
						? 'translate3d(0, 0, 0)'
						: 'translate3d(0, 30px, 0)'};
			}
		`};

	${({ padding }) =>
		!padding &&
		css`
			&&& {
				padding: 0 0;
			}
		`};
`;

const InviewWrapper = ({
	children,
	sectionTheme,
	id,
	initial,
	padding = true,
	hasSibling
}) => {
	const { ref, inView, entry } = useInView({
		triggerOnce: false,
		root: null,
		rootMargin: '-50% 0px',
		delay: 100,
		initialInView: initial
	});

	const { setPageTheme } = useUI() || {};
	const [sectionLoaded, setSectionLoaded] = useState(false);

	useEffect(() => {
		if (inView) {
			if ('inherit' === sectionTheme) {
				// don't change the theme
			} else if (sectionTheme) {
				setPageTheme(sectionTheme);
			} else {
				setPageTheme('white');
			}

			setSectionLoaded(true);
		}
	}, [inView, entry]);

	useEffect(() => {
		async function loadPolyfills() {
			if (
				typeof window !== 'undefined' &&
				typeof window.IntersectionObserver === 'undefined'
			) {
				await import('intersection-observer');
			}
		}
		loadPolyfills();
	}, []);

	return (
		<Section
			ref={ref}
			id={id}
			active={sectionLoaded}
			initial={initial}
			padding={padding}
			hasSibling={hasSibling}
		>
			{children}
		</Section>
	);
};

export default InviewWrapper;
