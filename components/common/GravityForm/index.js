import { Formik, Form } from 'formik';
import { useEffect, useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import styled, { keyframes } from 'styled-components';
import FormPage from './FormPage';
import Rarr from '../../../public/icons/rarr.svg';
import { ease } from '../../../styles/theme';
import Error from '../Error';
import Button from '../Button';
import * as pinterest from "../../../lib/tracking/pinterest-tracking";

/**
 * Basic styled divs
 */
const FormWrapper = styled(motion.div)`
	box-sizing: content-box;
`;

const spinAround = keyframes`
	0% {
		transform: rotate(0deg);
	}

	100% {
		transform: rotate(359deg);
	}
`;

const ConfirmationMessage = styled.div``;

const FormInner = styled(Form)`
	//
`;

const FormButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	display: block;
	transition: ${ease('all')};

	&:not(:last-child) {
		margin-right: 16px;
	}
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${FormButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const Pagination = styled.div`
	display: flex;
	align-items: start;
`;

const PageNumber = styled.div(motion.span);

const FormNav = styled.nav`
	padding-top: 40px;
	display: flex;
	justify-content: space-between;
`;

const ErrorMessages = styled.div`
	padding-bottom: 2em;

	*:first-child {
		padding-top: 0;
		margin-top: 0;
	}
`;

const FooterSubmit = styled.button`
	position: absolute;
	right: 0;
	top: 0.3em;

	&::before {
		content: '';
		display: block;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 24px;
		height: 24px;
	}

	&:hover {
		svg {
			transform: translateX(2px);
		}
	}

	svg {
		width: 4px;
		transition: ${ease('transform')};
	}
`;

const ButtonWrap = styled.div`
	margin-right: 24px;
`;

const Spinner = styled.div`
	transition: ${ease('all')};
	position: relative;
	justify-content: center;
	align-items: center;
	height: 1em;
	width: 100%;

	&::before {
		content: '';
		position: absolute;
		top: 50%;
		left: 50%;
		margin-left: -0.5em;
		margin-top: -0.5em;
		transition: ${ease('all')};
		border: 2px solid ${({ theme }) => theme.colors.brand};
		animation: ${spinAround} 0.6s linear infinite;
		border-radius: 999px;
		border-right-color: transparent;
		border-top-color: transparent;
		display: block;
		height: 1em;
		width: 1em;
	}
`;

const GravityForm = ({
	formID = 1,
	formEmailField = '',
	formProductRequestField = '',
	wishlistItems = null,
	type = 'general',
	buttonType = 'link',
	parentHandleOnSubmit,
	parentHandleAfterSubmit,
	children
}) => {
	/**
	 * Sets the state for loading in the form, the initial form data and the confirmation message on successful submit
	 */
	const [loading, setLoading] = useState(true);
	const [data, setData] = useState(false);
	const [responseErrors, setResponseErrors] = useState([]);
	const [confirmationMessage, setConfirmationMessage] = useState(false);
	const [page, setPage] = useState(1);
	const [initialValues, setInitialValues] = useState(null);
	const [pages, setPages] = useState(null);
	const [button, setButton] = useState(false);
	const [formId, setFormId] = useState(false);

	/**
	 * Initial fetch to get the form data via the API
	 */
	useEffect(() => {
		if (formID && typeof formID === 'number') {
			fetch('/api/get-form', {
				method: 'POST',
				body: JSON.stringify({
					formID
				})
			})
				.then((res) => res.json())
				.then((json) => {
					setData(json);
					setLoading(false);
				});
		}
	}, [formID]);


	const submitText = button?.text || 'Submit';



	useEffect(() => {
		if (data) {
			let { fields, id: formId, title, confirmations, button } = data || {};

			setButton(button);
			setFormId(formId);
			/**
			 * Converts the initial field data from Gravity Forms into Formik's intialValue key pair
			 */
			let initialValuess = {};

			let splitIndexes = [];

			fields = fields?.map((field, index) => {

				if (field?.label?.toUpperCase() === 'EMAIL') {
					field.defaultValue = formEmailField
				}

				field.key = `form_${formID}_${field.id}`;

				if (formProductRequestField) {
					if (field?.key === 'form_1_9') {
						field.defaultValue = `I would like to request pricing for the ${formProductRequestField}`
					}
					if (field?.label?.toUpperCase() === 'I HAVE A GENERAL ENQUIRY') {
						field.defaultValue = 'Request Pricing'
					}
				}
				else if(wishlistItems) {
					if(field?.key === 'form_3_5') {
						field.defaultValue = wishlistItems;
					}
				}
				else {
					if (field?.label?.toUpperCase() === 'PRODUCT REQUEST') {
						return null
					}
				}

				field.key = `form_${formID}_${field.id}`;

				initialValuess[field.key] = field?.defaultValue;

				if (field?.type === 'name' || field?.type === 'address') {
					const inputs = field?.inputs;
					let fieldInitArray = inputs.map((input) => {
						return {
							[input?.autocompleteAttribute]: ''
						};
					});

					let fieldInitObj = fieldInitArray.reduce((obj, item) => {
						return Object.assign(obj, {
							[Object.keys(item)[0]]: ''
						});
					}, {});

					initialValuess[field.key] = fieldInitObj;
				}

				if (field.type === 'page') {
					splitIndexes.push(index);
				}

				return field;
			});

			setInitialValues(initialValuess)

			let pagess;

			if (fields?.length) {
				pagess = fields?.reduceRight((result, value, index) => {
					result[0] = result[0] || [];

					if (splitIndexes.includes(index)) {
						result.unshift([value]);
					} else {
						result[0].unshift(value);
					}

					return result;
				}, []);

				setPages(pagess);
			}
		}
	}, [data]);

	/**
	 * Loading div
	 */
	if (loading || !data) {
		return <Spinner />;
	}

	const handleNext = () => {
		setResponseErrors([]);
		setPage((prevState) =>
			prevState + 1 <= pages?.length ? prevState + 1 : pages?.length
		);
	};

	const handlePrev = () => {
		setResponseErrors([]);
		setPage((prevState) => (prevState - 1 > 0 ? prevState - 1 : 1));
	};

	/**
	 * Handles the form submissions to Next's API
	 * @param values
	 * @param setErrors
	 * @returns {Promise<void>}
	 */
	const handleFormSubmit = async (values, actions) => {
		actions.setErrors({});

		//Add in the Form ID to the values being posted
		values.formID = formID;

		actions.setSubmitting(false);

		if (isFunction(parentHandleOnSubmit)) {
			parentHandleOnSubmit();
		}

		//Send the post request to the Next JS API
		const res = await fetch('/api/submit-form', {
			method: 'POST',
			body: JSON.stringify(values)
		});

		const json = await res.json();

		if (json.is_valid === false) {
			let formErrors = [];
			//Was an error - set the error messages
			const responseErrors = json?.errors_list;
			for (const [key, value] of Object.entries(responseErrors)) {
				actions.setFieldError(String(key), String(value));

				const fieldId = key.replace(`form_${formID}_`, '');
				const errorField = fields.filter((field) => {
					return field?.id === parseInt(fieldId);
				});

				const errorLabel = errorField[0]?.label;

				formErrors.push(`${errorLabel}: ${value}`);
			}

			setResponseErrors(formErrors);

			if (isFunction(parentHandleAfterSubmit)) {
				parentHandleAfterSubmit({
					success: false,
					errors: formErrors
				});
			}
		} else {
			//Show the confirmation message
			setConfirmationMessage(json.confirmation_message);

			pinterest.event('signup', {});
			
			if (isFunction(parentHandleAfterSubmit)) {
				parentHandleAfterSubmit({
					success: true,
					message: json.confirmation_message
				});
			}
		}
	};

	const variants = {
		hidden: {
			opacity: 0,
			transition: {
				duration: 0.2
			}
		},
		visible: {
			opacity: 1,
			transition: {
				duration: 0.2
			}
		}
	};

	return pages ? (
		<AnimatePresence exitBeforeEnter initial={false}>
			{!confirmationMessage ? (
				<FormWrapper
					key={`form-builder-${formId}`}
					variants={variants}
					initial={'hidden'}
					animate={'visible'}
					exit={'hidden'}
				>
					{responseErrors?.length > 0 && type !== 'footer' && (
						<ErrorMessages>
							{responseErrors.map((error, index) => (
								<Error error={error} key={index} />
							))}
						</ErrorMessages>
					)}

					<Formik
						initialValues={initialValues}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);
							handleFormSubmit(values, actions);
						}}
					>
						{({
							values,
							errors,
							touched,
							isSubmitting,
							handleChange,
							handleBlur,
							setFieldValue
						}) => (
							<FormInner key={'form-body'} noValidate>
								{pages?.length &&
									pages.map((fields, pageIndex) => {
										return (
											page == pageIndex + 1 && (
												<FormPage
													isSubmitting={isSubmitting}
													type={type}
													key={`form-page-${pageIndex}`}
													formId={formId}
													fields={fields}
													index={pageIndex}
													errors={errors}
													touched={touched}
													values={values}
													variants={variants}
													handleChange={handleChange}
													handleBlur={handleBlur}
													setFieldValue={
														setFieldValue
													}
												/>
											)
										);
									})}
								<FormNav key={'form-nav-1'}>
									{pages?.length > 1 && (
										<Pagination>
											<PageNumber>
												{`${page} / ${pages?.length}`}
											</PageNumber>
										</Pagination>
									)}
									<Pagination>
										{page > 1 && pages?.length > 1 && (
											<FormButton
												key={`form-prev-${formId}`}
												type="button"
												title={'Previous'}
												onClick={handlePrev}
											>
												<Underline>Prev</Underline>
											</FormButton>
										)}
										{page < pages?.length &&
											pages?.length > 1 && (
												<FormButton
													key={`form-next-${formId}`}
													type="button"
													title={'Next'}
													onClick={handleNext}
												>
													<Underline>Next</Underline>
												</FormButton>
											)}
										{page === pages?.length && (
											<>
												{children && (
													<ButtonWrap>
														{children}
													</ButtonWrap>
												)}
												{type === 'footer' ? (
													<FooterSubmit
														key={`form-submit-${formId}`}
														disabled={isSubmitting}
														type="submit"
													>
														<Rarr></Rarr>
													</FooterSubmit>
												) : buttonType === 'link' ? (
													<FormButton
														key={`form-submit-${formId}`}
														disabled={isSubmitting}
														type="submit"
														title={submitText}
													>
														<Underline>
															{submitText}
														</Underline>
													</FormButton>
												) : (
													<Button
														type={'submit'}
														color={'brand'}
														disabled={isSubmitting}
														title={submitText}
													/>
												)}
											</>
										)}
									</Pagination>
								</FormNav>
							</FormInner>
						)}
					</Formik>
				</FormWrapper>
			) : (
				<FormWrapper
					key={`form-confirmation-${formId}`}
					variants={variants}
					initial={'hidden'}
					animate={'visible'}
					exit={'hidden'}
				>
					<ConfirmationMessage
						dangerouslySetInnerHTML={{
							__html: confirmationMessage
						}}
					/>
				</FormWrapper>
			)}
		</AnimatePresence>
	) : null;
};

function isFunction(candidate) {
	return !!candidate && typeof candidate === 'function'
}

export default GravityForm;
