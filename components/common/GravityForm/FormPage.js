import styled, { css } from 'styled-components';
import { motion } from 'framer-motion';

import NumberField from '../NumberField';
import AddressField from '../AddressField';
import NameField from '../NameField';
import PhoneField from '../PhoneField';
import RadioField from '../RadioField';
import CheckboxField from '../CheckboxField';
import SelectField from '../SelectField';
import TextAreaField from '../TextAreaField';
import TextField from '../TextField';
import TimeField from '../TimeField';
import DateField from '../DateField';
import EmailField from '../EmailField';
import WebsiteField from '../WebsiteField';
import CaptchaField from '../CaptchaField';
import { ease } from '../../../styles/theme';

const components = {
	address: AddressField,
	checkbox: CheckboxField,
	email: EmailField,
	name: NameField,
	phone: PhoneField,
	radio: RadioField,
	select: SelectField,
	textarea: TextAreaField,
	text: TextField,
	time: TimeField,
	date: DateField,
	number: NumberField,
	website: WebsiteField,
	captcha: CaptchaField
};

const Page = styled(motion.div)`
	opacity: ${({ isSubmitting }) => (isSubmitting ? 0.5 : 1)};
	pointer-events: ${({ isSubmitting }) => (isSubmitting ? 'none' : 'all')};
	transition: ${ease('all')};
`;

const Grid = styled.div`
	display: grid;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 30px;
	column-gap: 36px;
	grid-auto-rows: initial;

	${({ type }) =>
		type === 'footer' &&
		css`
			column-gap: 0;

			&&& > * {
				grid-column: -1 / 1;
			}
		`};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 30px;
		column-gap: 24px;
	}

	> * {
		grid-column: span 6;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			grid-column: span 3;
		}

		@media ${({ theme }) => theme.breakpoints.mobile} {
			grid-column: -1 / 1;
		}
	}
`;

const FormPage = ({
	fields,
	formId,
	pageIndex,
	touched,
	values,
	errors,
	variants,
	handleBlur,
	handleChange,
	setFieldValue,
	type,
	isSubmitting
}) => {
	return (
		<Page
			key={`${formId}-${pageIndex}`}
			variants={variants}
			initial={'hidden'}
			animate={'visible'}
			exit={'hidden'}
			isSubmitting={isSubmitting}
		>
			<Grid type={type}>
				{fields?.length &&
					fields.map((field) => {
						const { type, key, id } = field || 'text';

						if (!components[type] || type === 'page') {
							//
						} else {
							const Component = components[type];
							const error = errors[key];
							const fieldTouched = touched[key];
							const fieldValue = values[key];

							return (
								<Component
									key={key}
									error={error}
									touched={fieldTouched}
									formId={formId}
									field={field}
									value={fieldValue}
									onBlur={handleBlur}
									onChange={handleChange}
									setFieldValue={setFieldValue}
								/>
							);
						}
					})}
			</Grid>
		</Page>
	);
};

export default FormPage;
