import Link from 'next/link';
import styled from 'styled-components';
import { breakpoint, ease, swatch } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import LinkItem from '../LinkHeading';

const LinkInner = styled.a`
	display: flex;
	position: absolute;
	top: 0;
	right: 0;
	transform: translateY(-100%);
	padding-bottom: 12px;
	transition: ${ease('color')};

	&:hover {
		color: ${swatch('brand')};
	}

	@media ${breakpoint('tablet')} {
		padding-bottom: 6px;
	}
`;

const ViewMore = ({ title = 'View More', url = '#' }) => {
	return (
		<Link href={searchReplaceLink(url)} passHref>
			<LinkInner title={title}>
				<LinkItem title={title} />
			</LinkInner>
		</Link>
	);
};

export default ViewMore;
