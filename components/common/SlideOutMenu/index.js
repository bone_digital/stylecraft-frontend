import styled, { css } from 'styled-components';
import { breakpoint, swatch } from '../../../styles/theme';
import InnerWrapper from '../InnerWrapper';
import { useState, useEffect } from 'react';
import { motion, AnimateSharedLayout } from 'framer-motion';
import useWindowDimensions from '../../../hooks/useWindowDimensions';

const Wrapper = styled.div`
	position: fixed;
	top: 0;
	right: ${({ direction }) => (direction === 'left' ? 'auto' : '0')};
	left: ${({ direction }) => (direction === 'left' ? '0' : 'auto')};
	height: ${({ unit }) => (unit ? `${100 * unit}px` : '100vh')};
	width: 100%;
	z-index: 20;
	display: flex;
	justify-content: ${({ direction }) =>
		direction === 'left' ? 'flex-start' : 'flex-end'};
	pointer-events: none;

	${({ isActive }) =>
		isActive &&
		css`
			> * {
				pointer-events: all;
			}
		`};

	&::before {
		content: '';
		display: block;
		width: 100%;
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		background: rgba(0, 0, 0, 0.2);
		z-index: -1;
		pointer-events: none;
	}
`;

const AnimatedWrapper = motion(Wrapper);

const Column = styled.div`
	width: 50%;
	background: ${swatch('white')};
	display: flex;
	transform: ${({ active }) =>
		!active ? 'translateX(100%)' : 'translateX(0)'};

	@media ${breakpoint('laptop')} {
		width: 75%;
	}

	@media ${breakpoint('tablet')} {
		width: 100%;
	}

	> div {
		display: flex;
		width: 100%;
		overflow-y: auto;
		flex-direction: column;
	}
`;

const CloseTrigger = styled.button`
	position: fixed;
	top: 0;
	right: ${({ direction }) => (direction !== 'left' ? 'auto' : '0')};
	left: ${({ direction }) => (direction !== 'left' ? '0' : 'auto')};
	height: 100%;
	width: 50%;
	overflow-y: auto;
	z-index: 25;

	@media ${breakpoint('tablet')} {
		display: none;
	}
`;

const CloseTriggerMob = styled.button`
	position: absolute;
	top: 5rem;
	right: 1rem;
	text-decoration: underline;
	display: none;

	@media ${breakpoint('tablet')} {
		display: block;
	}
`;

const AnimatedColumn = motion(Column);

const parentVariant = {
	open: {
		opacity: 1,
		pointerEvents: 'all',
		transition: {
			delayChildren: 0.8,
			staggerChildren: 0.1
		}
	},
	close: {
		opacity: 0,
		pointerEvents: 'none',
		transition: {
			delay: 0.8,
			staggerChildren: 0.1,
			staggerDirection: -1,
			when: 'afterChildren'
		}
	}
};

const SlideOutMenu = ({ children, active, onClick, direction }) => {
	const { height } = useWindowDimensions();
	const [unit, setUnit] = useState(height * 0.01);

	const rightVariant = {
		open: {
			transform: 'translateX(0%)',
			transition: {
				type: 'tween',
				duration: 0.6,
				delayChildren: 0.7,
				staggerChildren: 0.1
			}
		},
		close: {
			transform: 'translateX(120%)',
			transition: {
				type: 'tween',
				duration: 0.6,
				delay: 0.7,
				staggerChildren: 0.1,
				staggerDirection: -1,
				when: 'afterChildren'
			}
		}
	};

	const leftVariant = {
		open: {
			transform: 'translateX(0%)',
			transition: {
				type: 'tween',
				duration: 0.4,
				delayChildren: 0.5,
				staggerChildren: 0.1
			}
		},
		close: {
			transform: 'translateX(-120%)',
			transition: {
				type: 'tween',
				duration: 0.4,
				delayChildren: 0.5,
				staggerChildren: 0.1
			}
		}
	};

	useEffect(() => {
		setUnit(height * 0.01);
	}, [height]);

	return (
		<AnimateSharedLayout>
			{active && <CloseTrigger onClick={onClick} direction={direction} />}

			<AnimatedWrapper
				variants={parentVariant}
				animate={active ? 'open' : 'close'}
				initial={'close'}
				unit={unit}
				direction={direction}
				isActive={active}
			>
				<AnimatedColumn
					variants={direction === 'left' ? leftVariant : rightVariant}
					animate={active ? 'open' : 'close'}
					initial={'close'}
				>
					<InnerWrapper>
						{active && <CloseTriggerMob onClick={onClick} direction={direction}>Close</CloseTriggerMob>}
						{children}
					</InnerWrapper>
				</AnimatedColumn>
			</AnimatedWrapper>
		</AnimateSharedLayout>
	);
};

export default SlideOutMenu;
