import styled, { css } from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';
import Darr from '../../../public/icons/darr.svg';

const FieldWrap = styled.div`
	position: relative;
	cursor: pointer;
	display: flex;
	flex-direction: column;

	${({ width }) =>
		width === 'full' &&
		css`
			flex-direction: row;
			flex-wrap: wrap;
			align-items: start;
			justify-content: space-between;

			> * {
				width: calc(50% - (30px / 2));
			}
		`};

	select,
	input {
		padding: 0 0 9px 0;
		border-bottom: 1px solid;
		transition: ${ease('all')};
		display: block;
		border-color: ${({ error, theme }) =>
			error ? theme.colors.red : theme.colors.black};
		color: ${({ error, theme }) =>
			error ? theme.colors.red : theme.colors.black};
		transition: ${ease('all')};
		opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};

		&::placeholder {
			opacity: 1;
			color: ${({ theme }) => theme.colors.black};
		}
	}
`;

const Label = styled.label`
	padding-bottom: ${({ width }) => (width === 'full' ? '6px' : 0)};
	border-bottom: 1px solid;
	border-color: ${({ theme }) => theme.colors.black};
	color: ${({ theme }) => theme.colors.black};

	height: ${({ width }) => (width === 'full' ? 'auto' : 0)};
	visibility: ${({ width }) => (width === 'full' ? 'visibile' : 'collapse')};
	visibility: ${({ width }) => (width === 'full' ? 1 : 0)};
`;

const Option = styled.option``;

const InputWrap = styled.div`
	display: block;
	position: relative;

	input,
	select {
		width: calc(100% - 3px);
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}

	svg {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translate(0, calc(-50% - 3px));
		width: 8px;
		fill: currentColor;
		transition: ${ease('all')};
		pointer-events: none;
	}
`;

export default function SelectField({
	width,
	setFieldValue,
	field,
	error,
	disabled,
	value,
	onChange,
	formik
}) {
	const {
		id,
		key,
		formId,
		type,
		label,
		fieldErrors,
		cssClass,
		isRequired,
		defaultValue,
		choices
	} = field;

	const htmlId = `field_${formId}_${id}`;

	const handleOnChange = (e) => {
		if (setFieldValue) {
			setFieldValue(String(id), e.target.value);
		}

		if (onChange) {
			onChange(e);
		}
	};

	return (
		<FieldWrap className={`${cssClass}`.trim()} width={width}>
			<Label htmlFor={htmlId} width={width}>
				{label}
			</Label>
			<InputWrap>
				<Field
					name={String(key)}
					id={htmlId}
					required={Boolean(isRequired)}
					disabled={disabled}
					error={error || fieldErrors?.length}
					onChange={handleOnChange}
					as={'select'}
					value={value}
				>
					{choices?.map((choice, index) => (
						<Option key={index} value={choice?.value || ''}>
							{choice?.text || ''}
						</Option>
					))}
				</Field>
				<Darr />
			</InputWrap>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
