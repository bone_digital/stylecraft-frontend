import styled, { css } from 'styled-components';
import { useUI } from '../../../context/UIProvider';
import { ease, swatch, breakpoint } from '../../../styles/theme';

const Content = styled.div`
	h1 {
		margin-bottom: 40px;

		@media ${breakpoint('tablet')} {
			margin-bottom: 48px;
		}
	}

	h2 {
		margin-bottom: 0.5em;
	}

	h3 {
		/* margin: 1em; */
		margin-bottom: 0.5em;
	}

	h4 {
		/* margin: 1em; */
	}

	h5 {
		/* margin: 1em; */
	}

	h6 {
		/* margin: 1em; */
	}

	strong {
		font-weight: 700;
	}

	p,
	ul,
	ol {
		margin-bottom: 1.333em;
	}

	> *:last-child {
		margin-bottom: 0;
	}

	* > a {
		text-decoration: underline;
		transition: ${ease('color')};

		&:hover {
			color: var(--hover-color);
		}
	}
`;

const WYSWYG = ({ children }) => {
	const { backgroundColor } = useUI();

	return (
		<Content
			backgroundColor={backgroundColor}
			dangerouslySetInnerHTML={{ __html: children }}
		/>
	);
};

export { Content };
export default WYSWYG;
