import styled, { css } from 'styled-components';
import Open from '../../../public/icons/open.svg';
import { SlideDown } from 'react-slidedown';
import 'react-slidedown/lib/slidedown.css';
import { ease } from '../../../styles/theme';

const Row = styled.div`
	width: 100%;
	border-bottom: 1px solid rgba(0, 0, 0, 0.5);
	transition: ${ease('all')};

	@media ${({ theme }) => theme.breakpoints.mobile} {
		border-bottom: none;
	}

	&:not(:last-child) {
		margin-bottom: ${({ active }) => (active ? '80px' : '48px')};

		@media ${({ theme }) => theme.breakpoints.mobile} {
			margin-bottom: ${({ active }) => (active ? '72px' : '48px')};
		}
	}

	.react-slidedown {
		transition-duration: 200ms;
		transition-timing-function: 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
	}
`;

const Trigger = styled.button`
	display: flex;
	justify-content: space-between;
	align-items: center;
	width: 100%;
	padding-bottom: 15px;

	@media ${({ theme }) => theme.breakpoints.mobile} {
		border-bottom: 1px solid rgba(0, 0, 0, 0.5);
	}

	svg {
		flex-shrink: 0;
		width: 12px;
		transition: ${ease('all')};
	}

	${({ active }) =>
		active &&
		css`
			svg {
				transform: rotate(222deg);
			}
		`};
`;

const Label = styled.h3``;

const Body = styled.div`
	padding: 20px 0;
	border-top: 1px solid rgba(0, 0, 0, 0.5);

	@media ${({ theme }) => theme.breakpoints.mobile} {
		border-top: none;
	}
`;

const ShowHide = ({ children, title, active, handleToggle }) => {
	return (
		<Row active={active}>
			<Trigger active={active} onClick={handleToggle}>
				<Label>{title !== null ? title : 'View More'}</Label>
				<Open />
			</Trigger>
			<SlideDown>{active ? <Body>{children}</Body> : null}</SlideDown>
		</Row>
	);
};

export default ShowHide;
