import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';

const Wrapper = styled.div`
	margin: 0 auto;
	display: block;
	padding-left: 32px;
	padding-right: 32px;

	@media ${breakpoint('tablet')} {
		padding-left: 16px;
		padding-right: 16px;
	}
`;

const InnerWrapper = ({ children }) => {
	return <Wrapper>{children}</Wrapper>;
};

export { Wrapper };
export default InnerWrapper;
