import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
`;

const Input = styled(Field)`
	padding: 0 0 6px 0;
	border-bottom: 1px solid;
	transition: ${ease('all')};
	display: block;
	appearance: none;
	border-radius: 0;
	
	/* Chrome, Safari, Edge, Opera */
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	-moz-appearance: textfield;

	border-color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	transition: ${ease('all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};

	&::placeholder {
		opacity: 1;
		color: ${({ theme }) => theme.colors.black};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function NumberField({
	field,
	error,
	disabled,
	value,
	onChange,
	onBlur
}) {
	const {
		id,
		key,
		formId,
		type,
		label,
		cssClass,
		isRequired,
		placeholder,
		fieldErrors
	} = field || {};

	const htmlId = `field_${formId}_${id}`;

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			<Label htmlFor={htmlId}>{label}</Label>
			<Input
				type="number"
				name={String(key)}
				id={htmlId}
				required={Boolean(isRequired)}
				placeholder={placeholder || ''}
				value={value}
				disabled={disabled}
				error={error || fieldErrors?.length}
				onChange={onChange}
				onBlur={onBlur}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
