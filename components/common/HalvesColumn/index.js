import Grid from '../Grid';
import styled from 'styled-components';
import ThirdsColumn from '../ThirdsColumn';
import { Content } from '../WYSWYG';
import LinkButton from '../LinkButton';

const Column = styled.div`
	grid-column: span 4;
	position: relative;

	> a {
		margin-top: 24px;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: -1 / 1;

		> a {
			margin-top: 20px;
		}

		&:first-child {
			margin-bottom: 36px;
		}
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		&:first-child {
			margin-bottom: 18px;
		}
	}
`;

const ColumnInner = styled(Content)``;

const HalvesColumn = ({ columns }) => {
	const Content = () => (
		<Grid columns={8} rowGapMobile={0}>
			{columns &&
				columns.map(({ copy, linkButton }, index) => (
					<Column key={index}>
						{copy && (
							<ColumnInner
								dangerouslySetInnerHTML={{ __html: copy }}
							/>
						)}
						{linkButton && linkButton?.linkInner && (
							<LinkButton
								prefix={linkButton?.prefix}
								href={linkButton?.linkInner?.url}
								title={linkButton?.linkInner?.title}
								target={linkButton?.linkInner?.target}
							/>
						)}
					</Column>
				))}
		</Grid>
	);

	return columns ? <ThirdsColumn rightColumn={<Content />} /> : null;
};

export default HalvesColumn;
