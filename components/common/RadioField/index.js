import styled from 'styled-components';
import { Field } from 'formik';
import { ease } from '../../../styles/theme';

const FieldWrap = styled.fieldset`
	max-width: 100%;
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	padding: 0 0;
	margin-left: 0;
	margin-right: 0;
	border: none;
`;

const InputWrap = styled.div`
	display: block;
	position: relative;
	display: flex;
	align-items: start;

	&:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const Input = styled(Field)`
	transition: ${ease('all')};
	display: block;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 5;
	opacity: 0;

	&:checked ~ span::after {
		opacity: 1;
	}
`;

const Checkbox = styled.span`
	height: 14px;
	width: 14px;
	background-clip: content-box;
	border-radius: 2px;
	border: 1px solid rgba(0, 0, 0, 0.5);
	display: block;
	position: relative;
	margin-right: 12px;
	margin-top: 0.175em;

	&::after {
		content: '';
		height: 13px;
		width: 13px;
		position: absolute;
		top: 0;
		left: 0;
		transform: translate(2.5px, -3px);
		background-image: url('/icons/check.svg');
		background-size: contain;
		opacity: 0;
		transition: ${ease('all', 'fast')};
		pointer-events: none;
	}
`;

const Label = styled.label`
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	display: flex;
	align-items: flex-start;

	&:hover span::before {
		opacity: 1;
	}

	> * {
		line-height: 2.5;
	}
`;

const Legend = styled.legend`
	visibility: collapse;
	height: 0;
	width: 0;
	overflow: hidden;
`;

export default function RadioField({
	field,
	error,
	fieldValue,
	disabled,
	onBlur,
	formik,
	onFocus
}) {
	const { id, formId, key, label, cssClass, choices, fieldErrors } = field;

	const toKebabCase = (str) =>
		str &&
		str
			.match(
				/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g
			)
			.map((x) => x.toLowerCase())
			.join('-');

	const htmlId = `field_${formId}_${id}`;
	const formSlug = toKebabCase(label);

	return (
		<FieldWrap id={htmlId} className={`${cssClass}`.trim()}>
			<Legend>{label}</Legend>

			{choices?.map((choice, index) => {
				const value = choice.value;
				const inputlabel = choice?.text || '';
				const slug = toKebabCase(String(value));
				const labelSlug = toKebabCase(String(inputlabel));
				const inputId = `${formId}-${labelSlug}-${formSlug}`;
				const inputName = formik ? id : inputId;
				const choiceLabel = `input_${formId}_${id}_${slug}`;

				return (
					<InputWrap key={inputId}>
						{label && (
							<Label
								htmlFor={choiceLabel}
								aria-label={choiceLabel}
							>
								<Input
									type="radio"
									name={String(key)}
									id={choiceLabel}
									value={String(value)}
									disabled={disabled}
									error={error}
								/>
								<Checkbox></Checkbox>
								{inputlabel}
							</Label>
						)}
					</InputWrap>
				);
			})}

			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
