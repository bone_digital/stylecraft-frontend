import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';

const Container = styled.div`
	display: grid;
	width: 100%;
	grid-template-columns: ${({ columns }) =>
		columns
			? `repeat(${columns}, minmax(0, 1fr))`
			: `repeat(12, minmax(0, 1fr))`};
	row-gap: ${({ rowGap }) => (rowGap ? `${rowGap}` : '16px')};
	column-gap: ${({ columnGap }) => (columnGap ? `${columnGap}` : '36px')};
	grid-auto-rows: ${({ autoRows }) => (autoRows ? `1fr` : 'initial')};

	@media ${breakpoint('tablet')} {
		grid-template-columns: ${({ columnsMobile }) =>
			columnsMobile
				? `repeat(${columnsMobile}, minmax(0, 1fr))`
				: `repeat(6, minmax(0, 1fr))`};
		column-gap: ${({ columnGapMobile }) =>
			columnGapMobile ? `${columnGapMobile}` : '16px'};
		row-gap: ${({ rowGapMobile }) =>
			rowGapMobile ? `${rowGapMobile}` : '16px'};
	}
`;

const Grid = ({
	children,
	columns,
	columnsMobile,
	rowGap,
	columnGap,
	columnGapMobile,
	rowGapMobile,
	autoRows
}) => {
	return (
		<Container
			autoRows={autoRows}
			columns={columns}
			columnsMobile={columnsMobile}
			rowGap={rowGap}
			rowGapMobile={rowGapMobile}
			columnGap={columnGap}
			columnGapMobile={columnGapMobile}
		>
			{children}
		</Container>
	);
};

export { Container };

export default Grid;
