import { gql, useMutation, useQuery } from '@apollo/client';
import { useSelector, useDispatch } from 'react-redux';
import { cartActions } from '../../../store/cartSlice';
import {
	SimpleProductFields,
	VariableProductFields,
	ImageFields
} from '../../../lib/source/wordpress/queries/products';
import LinkButton from '../LinkButton';

/**
 * Basic add to cart mutation
 * @type {DocumentNode | *}
 */
export const UPDATE_CART = gql`
 mutation UPDATE_CART($input: UpdateItemQuantitiesInput!) {
	 updateItemQuantities(input: $input) {
		 cart {
			 total
			 subtotal
			 wholesaleTotal
			 contents {
				 itemCount
				 edges {
				 node {
					 key
					 quantity
					 subtotal
					 product {
						 node {
							 link
							 name
							 wholesalePrice
							 ... on VariableProduct {
								 ${VariableProductFields}
							 }
							 ... on SimpleProduct {
								 ${SimpleProductFields}
							 }
						 }
					 }
					 variation {
						 attributes {
							 name
							 label
							 value
						 }
						 node {
							 name
							 databaseId
							 id
							 link
							 name
							 featuredImage {
								 node {
									 ${ImageFields}
								 }
							 }
							 status
							 stockQuantity
							 stockStatus
							 contentType {
								 node {
									 name
								 }
							 }
						 }
					 }
				 }
			 }
		 }
	 }
	 }
 }
`;

const UpdateCartButton = ({ setDisabled }) => {
	const getTempCart = useSelector(({ cart }) => cart?.tempItems);
	const dispatch = useDispatch();

	let cartItems = [];

	if (getTempCart && getTempCart.length) {
		cartItems = getTempCart.map(({ node }) => {
			const key = node?.key;
			const quantity = node?.quantity;

			return {
				key,
				quantity: parseInt(quantity)
			};
		});
	}

	/**
	 * Sets up mutation
	 * NOTE: onComplete could be used to re-run the get cart function
	 */
	const [updateItemQuantities, { loading, error }] = useMutation(
		UPDATE_CART,
		{
			variables: {
				input: {
					items: cartItems
				}
			},
			onCompleted: ({ updateItemQuantities }) => {
				const cartContent = updateItemQuantities?.cart?.contents;
				const total = updateItemQuantities?.cart?.total;
				const wholesaleTotal =
					updateItemQuantities?.cart?.wholesaleTotal;
				const subtotal = updateItemQuantities?.cart?.subtotal;

				setDisabled(false);
				dispatch(
					cartActions.updateCart({
						content: cartContent,
						total,
						wholesaleTotal,
						subtotal
					})
				);
			},
			onError: (error) => {
				console.log({ message: error });
			}
		}
	);

	const updateCartHandler = () => {
		setDisabled(true);
		updateItemQuantities();
	};

	return (
		<LinkButton
			onClick={updateCartHandler}
			type={'full'}
			title={'Update Cart'}
			uppercase={false}
		/>
	);
};

export default UpdateCartButton;
