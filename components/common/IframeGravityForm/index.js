import styled from 'styled-components';
import { useState, useEffect, useRef } from 'react';
import Loading from '../Loading';
import { ease } from '../../../styles/theme';

function IframeGravityForm(props) {
	const {
		gravityFormsFormId,
	} = props;

	const [frameLoaded, setFrameLoaded] = useState(false);
	const formSrc = `${process.env.NEXT_PUBLIC_WP_URL}?form_embed_id=${gravityFormsFormId}`;

	const lockupRef = useRef(null);
	const iframeRef = useRef(null);
	const [formHeight, setformHeight] = useState(527);

	useEffect(() => {
		function listenForEmbedMessages(messageEvent) {
			const { isTrusted, data, origin } = messageEvent;
			if (data && data.hasOwnProperty('height')) {
				const height = data?.height || null;
				setformHeight(height + 20);
			}
			else
			{
				setformHeight(527);
			}

			if (data && data.hasOwnProperty('scrollToTop')) {
				if (lockupRef && lockupRef?.current) {
					setTimeout(() => {
						lockupRef?.current.scrollTo({
							top: 0,
							behavior: 'smooth'
						});
					}, 50);
				}
			}
		}

		formSrc && window.addEventListener('message', listenForEmbedMessages);

		return () => {
			formSrc &&
				window.removeEventListener('message', listenForEmbedMessages);
		};
	}, [setformHeight]);

	if (formSrc && !gravityFormsFormId) {
		return null;
	}

	return (
		<>
			{formSrc && (
				<div ref={lockupRef}>
					{!frameLoaded && (
                        <LoadingWrapper>
                            <Loading />
                        </LoadingWrapper>
                    )}
					<Iframe
						$formHeight={parseInt(formHeight) + 70}
						width="100%"
						src={formSrc}
						ref={iframeRef}
						onLoad={() => setFrameLoaded(true)}
						$loaded={frameLoaded}
					/>
				</div>
			)}
		</>
	);
};

const Iframe = styled.iframe`
	min-height: 100%;
	border: none;
	${({ $formHeight }) => $formHeight ? `height: ${$formHeight}px;` : ``}
	opacity: ${({ $loaded }) => $loaded ? '1' : '0'};
	transition: ${ease('all')};
`;

const LoadingWrapper = styled.div`
	& > div {
		opacity: 1;
	}
`;

export default IframeGravityForm;
