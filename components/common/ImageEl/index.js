import useProgressiveImg from '../../../hooks/useProgressiveImg';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { useEffect } from 'react';

const Image = styled(motion.img)`
	width: 100%;
	opacity: ${({ blur }) => (blur ? '0.5' : '1')};
	transition: all 0.8s cubic-bezier(0.87, 0, 0.82, 0.99);
`;

const ImageEl = ({ image, thumbnail, alt, setStateCallback }) => {
	const [src, { blur }] = useProgressiveImg(
		thumbnail,
		image,
		setStateCallback
	);

	useEffect(() => {
		if (!blur && blur !== null) {
			if (setStateCallback && typeof setStateCallback === 'function') {
				setStateCallback(true);
			}
		}

		return () => {
			if (setStateCallback && typeof setStateCallback === 'function') {
				setStateCallback(false);
			}
		};
	}, [blur]);

	return image && thumbnail ? (
		<Image src={src} blur={blur} alt={alt} active={src} />
	) : null;
};

export default ImageEl;
