import styled from 'styled-components';
import { breakpoint } from '../../../styles/theme';

const Row = styled.div`
	display: block;
	position: relative;
	z-index: 5;
	max-width: 90%;

	@media ${breakpoint('tablet')} {
		max-width: 75%;
	}

	@media ${breakpoint('mobile')} {
		max-width: calc(65% - 8px);
	}

	+ * {
		position: relative;
		z-index: 0;
	}
`;

const Heading = styled.h1`
	text-indent: -0.05em;
	margin-bottom: ${({ size, theme }) =>
		size === 'large' ? '-0.4em' : '-0.45em'};
	font-size: ${({ size, theme }) =>
		size === 'large' ? theme.type.h1[0] : theme.type.h2[0]};
	line-height: ${({ size, theme }) =>
		size === 'large' ? theme.type.h1[1] : theme.type.h2[1]};

	@media ${({ theme }) => theme.breakpoints.tablet} {
		font-size: ${({ size, theme }) =>
			size === 'large' ? theme.typeMobile.h1[0] : theme.type.h2[0]};
		line-height: ${({ size, theme }) =>
			size === 'large' ? theme.typeMobile.h1[1] : theme.type.h2[1]};
	}
`;

const OverlapHeading = ({ title, size = 'large' }) => {
	return title ? (
		<Row>
			<Heading size={size} dangerouslySetInnerHTML={{ __html: title }} />
		</Row>
	) : null;
};

export default OverlapHeading;
