import styled from 'styled-components';

const Heading = styled.h5`
	text-transform: uppercase;
	display: flex;
	justify-content: flex-end;
	gap: 10px;
`;

const LinkItem = ({ title }) => {
	return <Heading>{title}</Heading>;
};

export default LinkItem;
