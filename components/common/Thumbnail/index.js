import styled, { css } from 'styled-components';
import { defaultFallbackInView } from 'react-intersection-observer';
import { useInView } from 'react-intersection-observer';
import { breakpoint, swatch } from '../../../styles/theme';
import ImageEl from '../ImageEl';
import { motion } from 'framer-motion';
import { useEffect } from 'react';

const ThumbnailWrapper = styled(motion.div)`
	width: 100%;
	background: ${swatch('beige')};
	position: relative;
	transition: all 0.8s cubic-bezier(0.87, 0, 0.82, 0.99);
	overflow: hidden;
	display: inline-flex;

	${({ ratio }) =>
		!ratio &&
		css`
			img {
				transform: scale(1.1);
			}
		`};

	${({ ratio }) =>
		ratio &&
		css`
			&::before {
				padding-top: ${({ ratio }) => (ratio ? ratio : 0)};
				content: '';
				width: 100%;
				display: block;
				height: 0;

				@media ${breakpoint('mobile')} {
					padding-top: ${({ ratioMobile, ratio }) =>
						ratioMobile ? ratioMobile : ratio ? ratio : '80%'};
				}
			}

			&&& img {
				object-fit: cover;
				width: 100%;
				height: 100%;
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate3d(-50%, -50%, 0) scale(1.1);
			}
		`};

	${({ active, ratio }) =>
		active &&
		!ratio &&
		css`
			img {
				transition: all 0.4s ease;
				transform: scale(1);
			}
		`};

	${({ active, ratio }) =>
		active &&
		ratio &&
		css`
			&&& img {
				transition: all 0.4s ease;
				transform: translate(-50%, -50%);
			}
		`};
`;

const ImageBg = ({
	image,
	thumbnail,
	ratio,
	alt = '',
	ratioMobile,
	setStateCallback
}) => {
	defaultFallbackInView(true);

	const { ref, inView } = useInView({
		triggerOnce: true,
		root: null,
		rootMargin: '-10% 0px',
		delay: 0
	});

	useEffect(() => {
		async function loadPolyfills() {
			if (
				typeof window !== 'undefined' &&
				typeof window.IntersectionObserver === 'undefined'
			) {
				await import('intersection-observer');
			}
		}

		loadPolyfills();
	}, []);

	return (
		<>
			<ThumbnailWrapper
				ref={ref}
				ratio={ratio}
				ratioMobile={ratioMobile}
				active={inView}
			>
				<ImageEl
					image={image}
					thumbnail={thumbnail}
					alt={alt}
					active={inView}
					setStateCallback={setStateCallback}
				/>
			</ThumbnailWrapper>
		</>
	);
};

export default ImageBg;
