import styled from 'styled-components';
import PostImageGrid from '../../modules/PostImageGrid';
import PostImageVideo from '../../modules/PostImageVideo';
import PostShowroom from '../../modules/PostShowroom';
import PostColumn from '../../modules/PostColumn';
import SinglePostCarousel from '../../modules/SinglePostCarousel';
import PostCarousel from '../../modules/PostCarousel';

import {
	cleanModuleId,
	cleanModuleName
} from '../../../lib/source/wordpress/utils';

const ArticleBuilder = styled.main`
	position: relative;

	section {
		padding: 100px 0;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding: 65px 0;
		}
	}

	section {
		z-index: 2;
	}

	> *:first-child {
		padding-top: 132px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 100px;
		}
	}
`;

export default function PostBuilder({
	modules,
	showrooms,
	relatedPosts,
	relatedPostOverwrite,
	type
}) {
	const components = {
		ImageGrid: PostImageGrid,
		ImageVideo: PostImageVideo,
		Stores: PostShowroom,
		ColumnsBlock: PostColumn,
		PostCarousel
	};

	return modules ? (
		<ArticleBuilder>
			{modules.map((module) => {
				const name = cleanModuleName(module);
				const moduleId = cleanModuleId(module);

				if (!components[name]) {
					console.log({ message: `Missing: ${name}` });
				} else {
					const Component = components[name];

					return name === 'Stores' ? (
						<Component
							key={moduleId}
							showrooms={showrooms}
							{...module}
						/>
					) : (
						<Component key={moduleId} {...module} />
					);
				}
			})}

			<SinglePostCarousel
				type={type}
				relatedPostsOverwrite={relatedPostOverwrite}
				relatedPosts={relatedPosts}
			/>
		</ArticleBuilder>
	) : null;
}
