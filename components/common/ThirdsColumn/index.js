import styled from 'styled-components';
import InnerWrapper from '../InnerWrapper';
import Grid from '../Grid';

const Wrapper = styled.div`
	> div > div {
		grid-template-areas: 'left right';

		@media ${({ theme }) => theme.breakpoints.desktop} {
			grid-auto-rows: initial;
		}
	}
`;

const LeftColumn = styled.div`
	grid-column: span 4;
	display: flex;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: -1 / 1;
		order: ${({ flipMobile }) => (flipMobile ? 2 : 1)};
		margin-bottom: ${({ flipMobile }) => (flipMobile ? 0 : '60px')};
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: -1 / 1;
		margin-bottom: ${({ flipMobile }) => (flipMobile ? 0 : '48px')};
	}
`;

const RightColumn = styled.div`
	grid-column: span 8;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		grid-column: -1 / 1;
		order: ${({ flipMobile }) => (flipMobile ? 1 : 2)};
		margin-bottom: ${({ flipMobile }) => (flipMobile ? '60px' : 0)};
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: ${({ flipMobile }) => (flipMobile ? '48px' : 0)};
	}
`;

const ThirdsColumn = ({ leftColumn, rightColumn, flipMobile }) => {
	return leftColumn || rightColumn ? (
		<Wrapper>
			<InnerWrapper>
				<Grid rowGap={'0'} rowGapMobile={'0'} autoRows={true}>
					<LeftColumn flipMobile={flipMobile}>
						{leftColumn}
					</LeftColumn>

					<RightColumn flipMobile={flipMobile}>
						{rightColumn}
					</RightColumn>
				</Grid>
			</InnerWrapper>
		</Wrapper>
	) : null;
};

export default ThirdsColumn;
