import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
`;

const Input = styled(Field)`
	min-height: 27.5px;
	max-height: 400px;
	padding: 0 0 6px 0;
	border: none;
	border-bottom: 1px solid;
	transition: ${ease('all')};
	display: block;
	outline: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	resize: vertical;
	border-color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	transition: ${ease('all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
	appearance: none;

	&::placeholder {
		opacity: 1;
		color: ${({ theme }) => theme.colors.black};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function TextAreaField({
	field,
	error,
	disabled,
	value,
	onChange,
	onBlur
}) {
	const {
		id,
		key,
		formId,
		type,
		label,
		cssClass,
		isRequired,
		placeholder,
		fieldErrors
	} = field;

	const htmlId = `field_${formId}_${id}`;

	const [height, setHeight] = useState('auto');

	useEffect(() => {
		setHeight('auto');
		// InputRef.current.scrollHeight * 1.005 magic number to fix issue of text box shirking as you type after manually resizing. 
		setHeight(`${InputRef.current.scrollHeight * 1.005}px`);
	}, [value]);

	const InputRef = React.createRef();

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			<Label htmlFor={htmlId}>{label}</Label>
			<Input
				ref={InputRef}
				rows="1"
				as={'textarea'}
				name={String(key)}
				id={htmlId}
				required={Boolean(isRequired)}
				placeholder={placeholder || ''}
				value={value}
				disabled={disabled}
				error={error || fieldErrors?.length}
				onBlur={onBlur}
				onChange={onChange}
				style={{ height }}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
