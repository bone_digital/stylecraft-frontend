import { useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import Header from '../../layout/Header';
import MainMenu from '../../layout/MainMenu';
import FormMenu from '../../layout/FormMenu';
import Footer from '../../layout/Footer';
import { motion } from 'framer-motion';
import { useUI } from '../../../context/UIProvider';
import { swatch, ease } from '../../../styles/theme';
import AccountMenu from '../../layout/AccountMenu';
import { CART_QUERY } from '../../../lib/source/wordpress/queries/cart';
import { cartActions } from '../../../store/cartSlice';
import { PopupForm } from '../PopupForm/PopupForm';

const menus = require('../../../json/menus.json');
const options = require('../../../json/options.json');

const Wrapper = styled.div`
	background: ${swatch('white')};
	z-index: 2;
	position: relative;
	display: flex;
	flex-direction: column;

	main {
		min-height: 100vh;
	}
`;

const WrapperInner = styled.div`
	background: ${({ background }) =>
		background ? swatch(background) : swatch('white')};
	--hover-color: ${({ background }) =>
		background === 'pink' ? swatch('white') : swatch('brand')};
	transition: ${ease('background', 'slow')};
`;

const Main = styled.main``;

const MotionMain = motion(Main);

const License = styled.div`
	opacity: 0;
	visibility: hidden;
	width: 0;
	height: 0;
`;

export default function Layout({ children, props }) {
	const router = useRouter();

	const { backgroundColor, toggleMenu, updateCursor } = useUI();

	const contactForm = options?.contact_form;
	const createAccountIntro = options?.create_account_intro;
	const createAccountTerms = options?.create_account_terms;
	const downloadMessage = options?.download_message;
	const footerCopy = options?.footer_copy;

	const headerMenu = menus?.header;
	const mainMenu = menus?.main;
	const footerA = menus?.footerA;
	const footerB = menus?.footerB;
	const footerC = menus?.footerC;
	const footerD = menus?.footerD;

	const variants = {
		hidden: { opacity: 0 },
		visible: { opacity: 1 }
	};

	const dispatch = useDispatch();

	const { loading, data: initialCartData, refetch } = useQuery(CART_QUERY);

	useEffect(() => {
		if (initialCartData != null) {
			const cartContent = initialCartData?.cart?.contents;
			const total = initialCartData?.cart?.total;
			const subtotal = initialCartData?.cart?.subtotal;

			dispatch(
				cartActions.addItem({
					content: cartContent,
					total,
					subtotal
				})
			);
		}
	}, [initialCartData]);

	useEffect(() => {
		updateCursor();

		const handleRouteChange = (url) => {
			toggleMenu(false);
		};

		router.events.on('routeChangeComplete', handleRouteChange);

		return () => {
			router.events.off('routeChangeComplete', handleRouteChange);
		};
	}, []);

	const fontLicense = `<!--//
		/**
			* @license
			* MyFonts Webfont Build ID 734968
			*
			* The fonts listed in this notice are subject to the End User License
			* Agreement(s) entered into by the website owner. All other parties are
			* explicitly restricted from using the Licensed Webfonts(s).
			*
			* You may obtain a valid license from one of MyFonts official sites.
			* http://www.fonts.com
			* http://www.myfonts.com
			* http://www.linotype.com
			*
			*/
		//-->
	`;

	return (
		<>
			<Wrapper>
				<License dangerouslySetInnerHTML={{ __html: fontLicense }} />
				<WrapperInner background={backgroundColor}>
					<Header menu={headerMenu} />
					<MotionMain
						initial="hidden"
						animate="visible"
						variants={variants}
					>
						{children}
					</MotionMain>
				</WrapperInner>

				<MainMenu menu={mainMenu} mobileMenu={headerMenu} />
				<AccountMenu
					introCopy={createAccountIntro}
					termsCopy={createAccountTerms}
					downloadCopy={downloadMessage}
				/>
				<FormMenu formId={contactForm} />
				<PopupForm />
			</Wrapper>
			<Footer
				background={backgroundColor}
				copy={footerCopy}
				footerA={footerA}
				footerB={footerB}
				footerC={footerC}
				footerD={footerD}
			/>
		</>
	);
}
