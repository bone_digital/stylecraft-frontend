import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { breakpoint, swatch, ease } from '../../../styles/theme';

const FieldWrap = styled.fieldset`
	max-width: 100%;
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	padding: 0 0;
	margin-left: 0;
	margin-right: 0;
	border: none;
`;

const Input = styled(Field)`
	padding: 0 0 6px 0;
	border-bottom: 1px solid ${swatch('black')};
	transition: ${ease('all')};
	display: block;
	appearance: none;
	border-radius: 0;

	&::placeholder {
		opacity: 1;
		color: ${swatch('black')};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function PhoneField({
	field,
	value,
	disabled,
	error,
	onBlur,
	onChange
}) {
	const {
		id,
		formId,
		key,
		label,
		cssClass,
		placeholder,
		isRequired,
		fieldErrors
	} = field;

	const htmlId = `field_${formId}_${id}`;

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			{label && <Label htmlFor={htmlId}>{label}</Label>}
			<Input
				type="tel"
				name={String(key)}
				id={htmlId}
				required={Boolean(isRequired)}
				placeholder={placeholder || ''}
				disabled={disabled}
				error={error}
				onBlur={onBlur}
				value={value}
				onChange={onChange}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
