import Cookies from 'js-cookie';
import { gql, useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { cartActions } from '../../../store/cartSlice';
import { userActions } from '../../../store/userSlice';
import { useEffect } from 'react';
import jwtDecode from 'jwt-decode';
import cart from "../../elements/Cart";

export default function GetUser() {

	const dispatch = useDispatch();
	const user = useSelector(({ user }) => user);

	let loginData = Cookies.get('ussr-data');
	loginData = loginData ? JSON.parse(atob(loginData)) : null;

	const jwtRefreshToken = loginData?.login?.refreshToken;
	const cookieUser = loginData?.login?.user;

	useEffect(() => {
		if (!user?.loggedIn && jwtRefreshToken) {
			userLogin({
				variables: {
					jwtRefreshToken
				}
			});
		}
		else {
			dispatch(userActions.confirmStatus());
			/**
			 * User is not logged in
			 */
			const wishlistItems = localStorage.getItem('sc-wishlist');
			const wishlistItemsCount = localStorage.getItem('sc-wishlist-count');

			if( wishlistItems && Number(wishlistItemsCount) )
			{
				dispatch(
					cartActions.updateWishlist({
						items: JSON.parse(wishlistItems),
						count: wishlistItemsCount,
					})
				);
			}

			const loadCart = async () => {
				const cartResponse = await fetch('/api/get-cart', {
					method: 'POST',
					credentials: 'same-origin',
					body: JSON.stringify({
					})
				})
					.then((res) => res.json())
					.then((json) => {
						dispatch(
							cartActions.newUpdateCart({
								jsonCart: json,
							})
						)
						dispatch(
							cartActions.updateNewCartStatus({ isLoading: false })
						);

						if (json?.cart_key) {
							Cookies.set('cart-key', json.cart_key, { expires: 10 });
						}

						return json;
					}).catch((err) => {
						console.log(err);
					});
			}

			/**
			 * Get the Cart
			 */
			// Check if the cookie exists
			const cart_key = Cookies.get('cart-key');
			if( cart_key && typeof cart_key != 'undefined' )
			{
				loadCart();
			}
		}
	},[])

	const [userLogin, { loading, error }] = useMutation(REFRESH_TOKEN, {
		variables: {
			jwtRefreshToken
		},
		onCompleted: (data) => {

			const authToken = data?.refreshJwtAuthToken?.authToken;
			const decoded = jwtDecode(authToken);
			const userId = decoded?.data?.user?.id;
			const cookieUserId = cookieUser?.userId;

			if (cookieUserId && cookieUserId == userId) {
				dispatch(userActions.login({ user: cookieUser }));
				loginData.login.authToken = authToken;
				Cookies.set('ussr-data', btoa(JSON.stringify(loginData)));
				Cookies.set('token', authToken);
				Cookies.set('refresh-token', jwtRefreshToken);

				const getWishlistAndCart = async () => {

					dispatch(
						cartActions.updateNewCartStatus({ isLoading: true })
					);
					dispatch(
						cartActions.updateWishlistStatus({ isLoading: true })
					);

					const wishlistResponse = await fetch('/api/get-wishlist', {
						method: 'POST',
						body: JSON.stringify({
							userId
						})
					})
						.then((res) => res.json())
						.then((json) => {

							dispatch(
								cartActions.updateWishlist({
									items: json?.wishlist,
									count: json?.wishlist?.length || 0,
								})
							);
							dispatch(
								cartActions.updateWishlistStatus({ isLoading: false })
							);
							return json;
						}).catch((err) => {
							console.log(err);
						});

					const cartResponse = await fetch('/api/get-cart', {
						method: 'POST',
						body: JSON.stringify({
							userId
						})
					})
						.then((res) => res.json())
						.then((json) => {
							dispatch(
								cartActions.newUpdateCart({
									jsonCart: json,
								})
							)
							dispatch(
								cartActions.updateNewCartStatus({ isLoading: false })
							);

							if (json?.cart_key) {
								Cookies.set('cart-key', json.cart_key, { expires: 10 });
							}

							return json;
						}).catch((err) => {
							console.log(err);
						});

				}

				getWishlistAndCart();

			} else {
				Cookies.remove('ussr-data');
				Cookies.remove('token');
				Cookies.remove('refresh-token');
			}

			dispatch(userActions.confirmStatus());
		},
		onError: (error) => {
			console.log({ error: error });
			dispatch(userActions.confirmStatus());
		}
	});

	return (
		<></>
	);
}


const REFRESH_TOKEN = gql`
	mutation RefreshAuthToken($jwtRefreshToken: String!) {
		refreshJwtAuthToken(input: { jwtRefreshToken: $jwtRefreshToken }) {
			authToken
		}
	}
`;
