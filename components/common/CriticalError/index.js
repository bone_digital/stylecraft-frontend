import styled from 'styled-components';
import InnerWrapper from '../InnerWrapper';

const Wrapper = styled.div`
	padding: 75px 0;
`;

const CriticalError = () => {
	return (
		<Wrapper>
			<InnerWrapper>
				<h1>Something went wrong.</h1>
			</InnerWrapper>
		</Wrapper>
	);
};

export default CriticalError;
