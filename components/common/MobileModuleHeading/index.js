import styled from 'styled-components';
import OverlapHeading from '../OverlapHeading';

const MobileHeading = styled.div`
	display: none;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: block;
	}
`;

const MobileModuleHeading = ({ title }) => {
	return title ? (
		<MobileHeading>
			<OverlapHeading title={title} />
		</MobileHeading>
	) : null;
};

export default MobileModuleHeading;
