import React from 'react';
import styled from 'styled-components';

const CaptionColumn = styled.h6`
	letter-spacing: 0.025em;
	text-transform: uppercase;

	&:not(:last-child) {
		margin-right: 16px;
	}

	&:only-child {
		margin-right: 0;
	}

	&:first-child {
		color: ${({ theme }) => theme.colors.greyMedium};
		flex-shrink: 0;
	}

	p {
		font-size: inherit;
	}
`;

const CaptionBlock = styled.span`
	display: flex;
	width: 100%;

	p {
		font-size: inherit;
		line-height: inherit;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		flex-wrap: wrap;
	}
`;

const ImageCaption = ({ caption }) => {
	const { prefix, copy } = caption || null;

	return caption ? (
		<CaptionBlock>
			{prefix && <CaptionColumn>{prefix}</CaptionColumn>}
			{copy && (
				<CaptionColumn dangerouslySetInnerHTML={{ __html: copy }} />
			)}
		</CaptionBlock>
	) : null;
};

export default ImageCaption;
