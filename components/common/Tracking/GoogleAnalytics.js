import Script from 'next/script';

const GoogleAnalytics = () => {
	const GA = process.env.NEXT_PUBLIC_MEASUREMENT_ID;

	if( !GA )
	{
		return <></>
	}
	//<Script src={`https://www.googletagmanager.com/gtag/js?id=${GA}`} strategy='afterInteractive' />
	return (
		<>
			<Script id="google-analytics" strategy="afterInteractive">
				{`
				(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','${GA}');

					window.dataLayer = window.dataLayer || [];
					function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());
					gtag('config', '${GA}');
				`}
			</Script>
			<noscript dangerouslySetInnerHTML={{ __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=${GA}" height="0" width="0" style="display:none;visibility:hidden"></iframe>`}}></noscript>
		</>
	);

}

export default GoogleAnalytics;
