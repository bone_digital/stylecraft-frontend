import styled, { css } from 'styled-components';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, A11y } from 'swiper';
import { useState, useRef } from 'react';
import { breakpoint, swatch } from '../../../styles/theme';
import ImageBg from '../Thumbnail';
import List from './List';
import Dot from './Dot';

const Container = styled.div``;

const Gallery = styled.div`
	position: relative;
`;

const ImageWrapper = styled.div`
	position: relative;
`;

const Nav = styled.div`
	position: absolute;
	top: 0;
	width: 100%;

	${({ ratio }) =>
		ratio &&
		css`
			&::before {
				padding-top: ${({ ratio }) => (ratio ? ratio : '80%')};
				content: '';
				width: 100%;
				display: block;
				height: 0;

				@media ${breakpoint('mobile')} {
					padding-top: ${({ ratioMobile, ratio }) =>
						ratioMobile ? ratioMobile : ratio ? ratio : '80%'};
				}
			}
		`};
`;

const NavElement = styled.button`
	position: absolute;
	top: 0;
	width: 33.33%;
	height: 100%;
	left: ${({ direction }) => (direction === 'prev' ? 0 : 'auto')};
	right: ${({ direction }) => (direction !== 'prev' ? 0 : 'auto')};
	z-index: 10;
	opacity: 0;
`;

SwiperCore.use([Navigation, A11y]);

const Hotspot = ({ ratio, ratioMobile, content }) => {
	const [active, setActive] = useState(null);
	const prevRef = useRef();
	const nextRef = useRef();

	return content ? (
		<Container>
			<Gallery>
				<Nav ratio={ratio} ratioMobile={ratioMobile}>
					{content && content?.length > 1 && (
						<>
							<NavElement
								data-cursor="true"
								className="cursor-link cursor-link--carousel-prev"
								direction="prev"
								ref={prevRef}
							/>
							<NavElement
								className=" cursor-link cursor-link--carousel-next"
								direction="next"
								ref={nextRef}
							/>
						</>
					)}
				</Nav>

				<Swiper
					loop={content?.length > 1 ? true : false}
					allowTouchMove={content.length > 1 ? true : false}
					preventInteractionOnTransition={true}
					speed={400}
					centeredSlides={false}
					slidesPerView={1}
					spaceBetween={0}
					onInit={(swiper) => {
						swiper.params.navigation.prevEl = prevRef.current;
						swiper.params.navigation.nextEl = nextRef.current;

						swiper.navigation.init();
						swiper.navigation.update();
					}}
				>
					{content?.map((slide, index) => (
						<SwiperSlide key={index}>
							<ImageWrapper>
								{slide?.image && (
									<ImageBg
										ratio={ratio}
										ratioMobile={ratioMobile}
										image={slide?.image?.sourceUrl}
										thumbnail={slide?.image?.thumbSourceUrl}
										alt={slide?.image?.alt}
									/>
								)}
								{slide?.hotspots &&
									slide?.hotspots.map((dot, index) => (
										<Dot
											dot={dot}
											key={index}
											index={index}
											active={active}
											onHover={setActive}
										/>
									))}
							</ImageWrapper>

							{slide?.hotspots && (
								<List
									content={slide?.hotspots}
									active={active}
									onHover={setActive}
								/>
							)}
						</SwiperSlide>
					))}
				</Swiper>
			</Gallery>
		</Container>
	) : null;
};

export default Hotspot;
