import Link from 'next/link';
import styled from 'styled-components';
import { swatch, ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const HotSpot = styled.span`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	width: 44px;
	height: 44px;
	position: absolute;
	left: ${({ xPos }) => (xPos ? `${xPos}%` : 0)};
	top: ${({ yPos }) => (yPos ? `${yPos}%` : 0)};
	z-index: 5;
	text-transform: uppercase;
	transition: ${ease('all')};
	transform: ${({ active }) =>
		active ? 'translate(-50%, -50%) scale(1.1)' : 'translate(-50%, -50%)'};
	opacity: 0;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		width: 33px;
		height: 33px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		width: 22px;
		height: 22px;
		font-size: 10px;
	}

	.swiper-slide-active & {
		opacity: 1;
	}

	&::before {
		content: '';
		background: ${swatch('white')};
		border-radius: 50%;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		z-index: -1;
		transition: ${ease('all')};
	}

	&:hover {
		&::before {
			transform: translate(-50%, -50%) scale(1.1);
		}
	}
`;

const Dot = ({ dot, index, onHover, active }) => {
	const { xPosition, yPosition } = dot || {};

	const convertIndextoLetter = (number) => {
		if (!number) {
			return 'A';
		}
		return String.fromCharCode(96 + parseInt(number + 1));
	};

	return (
		<>
			{dot?.product?.link ? (
			<Link
				href={searchReplaceLink(dot?.product?.link) || '#'}
				passHref
			>
				<HotSpot
				xPos={xPosition}
				yPos={yPosition}
				onMouseEnter={() => onHover(index)}
				onMouseLeave={() => onHover(null)}
				active={active === index}
				>
				{convertIndextoLetter(index)}
				</HotSpot>
			</Link>
			) : (
			<HotSpot
				xPos={xPosition}
				yPos={yPosition}
				onMouseEnter={() => onHover(index)}
				onMouseLeave={() => onHover(null)}
				active={active === index}
			>
				{convertIndextoLetter(index)}
			</HotSpot>
			)}
		</>		
	);
};

export default Dot;
