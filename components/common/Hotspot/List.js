import Link from 'next/link';
import styled from 'styled-components';
import { ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const ListWrap = styled.ul`
	display: block;
	opacity: 0;
	transition: ${ease('opacity')};

	.swiper-slide-active & {
		opacity: 1;
	}
`;

const Item = styled.li`
	display: flex;
	width: 100%;
	font-weight: ${({ active }) => (active ? 500 : 400)};
	transition: ${ease('font-weight')};

	* {
		font-weight: inherit;
		transition: ${ease('all')};
	}

	&:hover {
		font-weight: ${({ active }) => (active ? 500 : 400)};
	}

	&:not(:last-child) {
		margin-bottom: 1em;
	}
`;

const Trigger = styled.a`
	width: auto;
	display: flex;
`;

const Inner = styled.h6`
	display: flex;
	width: 100%;
	font-weight: inherit;
`;

const Column = styled.div`
	text-align: left;
	text-transform: uppercase;
	letter-spacing: 0.025em;

	&:first-child {
		width: 20px;
		margin-right: 32px;
	}

	&:not(:last-child) {
		margin-right: ${({ section }) =>
			section ? 'calc(120px - 1ch)' : 'calc(50px - 1ch)'};
	}

	&:not(:first-child) {
		width: ${({ exUnit }) => (exUnit ? `calc(${exUnit + 1}ch)` : 'auto')};
	}
`;

const List = ({ content, section = false, active, onHover }) => {
	let longestWord = 0;

	if (content.length) {
		content.forEach(({ product }) => {
			const brand = product?.product?.brand;
			if (brand?.title && brand?.title.length > longestWord) {
				longestWord = brand?.title.length;
			}
		});
	}

	const convertIndextoLetter = (number) => {
		if (!number) {
			return 'A';
		}
		return String.fromCharCode(96 + parseInt(number + 1));
	};

	return content.length ? (
		<ListWrap>
			{content.map(({ product }, index) => (
				<Item
					key={index}
					index={index}
					active={active === index}
					onMouseEnter={() => onHover(index)}
					onMouseLeave={() => onHover(null)}
				>
					<Link
						href={searchReplaceLink(product?.link) || '#'}
						passHref
					>
						<Trigger title={product?.name}>
							<Inner>
								<Column>{convertIndextoLetter(index)}</Column>
								{product && (
									<Column
										exUnit={longestWord}
										section={section}
									>
										{product?.product?.brand?.title}
									</Column>
								)}
								{product?.name && (
									<Column>{product?.name}</Column>
								)}
							</Inner>
						</Trigger>
					</Link>
				</Item>
			))}
		</ListWrap>
	) : null;
};

export default List;
