import styled from 'styled-components';
import { Field } from 'formik';
import { breakpoint, swatch, ease } from '../../../styles/theme';
import Darr from '../../../public/icons/darr.svg';
import LinkHeading from '../../common/LinkHeading';

const ErrorMessage = styled.div`
	padding-top: 5px;
`;

const FieldWrap = styled.div`
	max-width: 100%;
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	/* overflow: hidden; */

	@media ${breakpoint('tablet')} {
		max-width: 200px;
	}
`;

const InputWrap = styled.div`
	display: block;
	position: relative;

	&::after {
		content: '';
		position: absolute;
		bottom: 0;
		left: 0;
		width: calc(100% - 3px);
		height: 1px;
		background: ${swatch('black')};
		pointer-events: none;
	}

	input,
	select {
		width: 100%;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		cursor: pointer;
	}

	svg {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translate(0, calc(-50% - 3px));
		width: 8px;
		fill: currentColor;
		transition: ${ease('all')};
		pointer-events: none;
	}
`;

const Input = styled(Field)`
	padding: 0 8px 6px 0;
	transition: ${ease('all')};
	display: block;
	/* white-space: ${({ type }) =>
		type === 'select' ? 'nowrap' : 'initial'}; */

	&::placeholder {
		opacity: 1;
		color: ${swatch('black')};
	}
`;

const Label = styled.label`
	height: ${({ hidden }) => (hidden ? '0' : 'auto')};
	visibility: ${({ hidden }) => (hidden ? 'collapse' : 'visible')};
	opacity: ${({ hidden }) => (hidden ? '0' : '1')};

	> * {
		line-height: 2.5;
	}
`;

const FormField = ({
	label,
	name,
	type,
	placeholder,
	hideLabel = true,
	required,
	disabled,
	error,
	onBlur,
	onFocus,
	onChange,
	options
}) => {
	return (
		<FieldWrap type={type}>
			{label && (
				<Label
					htmlFor={`form-${name}`}
					aria-label={`form-${name}`}
					hidden={hideLabel}
				>
					<LinkHeading title={name} />
				</Label>
			)}
			<InputWrap>
				{type === 'select' ? (
					<Input
						id={`form-${name}`}
						name={name}
						required={required}
						placeholder={placeholder}
						disabled={disabled}
						error={error}
						onBlur={onBlur}
						onFocus={onFocus}
						onChange={onChange}
						as={'select'}
						type={type}
					>
						{options &&
							options.map(({ value, label }, index) => (
								<option value={value} key={index}>
									{label}
								</option>
							))}
					</Input>
				) : (
					<Input
						id={`form-${name}`}
						type={type}
						name={name}
						required={required}
						placeholder={placeholder}
						disabled={disabled}
						error={error}
						onBlur={onBlur}
						onFocus={onFocus}
						onChange={onChange}
					/>
				)}
				{type === 'select' && <Darr />}
			</InputWrap>
			{error && (
				<ErrorMessage>
					<h5>{error}</h5>
				</ErrorMessage>
			)}
		</FieldWrap>
	);
};

export default FormField;
