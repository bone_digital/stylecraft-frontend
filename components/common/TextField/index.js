import styled from 'styled-components';
import { Field } from 'formik';
import Error from '../Error';
import { ease } from '../../../styles/theme';

const FieldWrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	cursor: pointer;
`;

const Input = styled(Field)`
	padding: 0 0 6px 0;
	border-bottom: 1px solid;
	transition: ${ease('all')};
	display: block;
	border-color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	color: ${({ error, theme }) =>
		error ? theme.colors.red : theme.colors.black};
	transition: ${ease('all')};
	opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
	appearance: none;
	border-radius: 0;

	&::placeholder {
		opacity: 1;
		color: ${({ theme }) => theme.colors.black};
	}
`;

const Label = styled.label`
	height: 0;
	visibility: collapse;
	opacity: 0;
`;

export default function TextField({
	field,
	error,
	disabled,
	value,
	onChange,
	onBlur
}) {
	const {
		id,
		formId,
		label,
		cssClass,
		isRequired,
		placeholder,
		fieldErrors,
		key
	} = field || {};

	const htmlId = `field_${formId}_${id}`;

	return (
		<FieldWrap className={`${cssClass}`.trim()}>
			<Label htmlFor={key}>{label}</Label>
			<Input
				type="text"
				name={key}
				id={key}
				fieldid={htmlId}
				required={Boolean(isRequired)}
				placeholder={placeholder || ''}
				value={value}
				disabled={disabled}
				error={error || fieldErrors?.length}
				onChange={onChange}
				onBlur={onBlur}
			/>
			{error && <Error error={error} />}
			{fieldErrors?.length &&
				fieldErrors.map((fieldError) => (
					<Error key={fieldError.id} error={fieldError.message} />
				))}
		</FieldWrap>
	);
}
