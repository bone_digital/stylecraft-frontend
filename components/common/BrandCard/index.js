import styled, { css } from 'styled-components';
import { breakpoint, ease } from '../../../styles/theme';
import ImageEl from '../ImageEl';
import Thumbnail from '../Thumbnail';
import LinkItem from '../LinkHeading';
import Link from 'next/link';
import searchReplaceLink from '../../../utils/searchReplaceLink';

const Card = styled.div`
	width: 100%;
	overflow: hidden;
	grid-column: ${({ columnsPerRow }) => (columnsPerRow === 3 && size !== 'large' ? 'span 4' : 'span 6')};

	&:hover {
		&&& img {
			transform: scale(1.1) translate(-50%, -50%);
		}
	}

	${({ size }) =>
		size !== 'large' &&
		css`
			@media ${breakpoint('laptop')} {
				grid-column: -1 / 1;
			}
		`};

	@media ${breakpoint('tablet')} {
		grid-column: -1 / 1;
	}
`;

const ThumbnailWrap = styled.a`
	margin-bottom: 20px;
	position: relative;
	display: flex;
	overflow: hidden;
	transition: ${ease('all')};

	@media ${breakpoint('laptop')} {
		margin-bottom: 32px;
	}

	@media ${breakpoint('tablet')} {
		margin-bottom: 16px;
	}
`;

const Details = styled.a`
	display: block;
	transition: ${ease('opacity')};

	@media ${breakpoint('laptop')} {
		.swiper-slide-active + .swiper-slide & {
			opacity: 0;
		}
	}
`;

const CardLink = styled.div`
	display: flex;
	align-items: baseline;

	h5 {
		opacity: 0.4;
		margin-right: 20px;
	}
`;

const Title = styled.h3``;

const BrandCard = ({ size, post, columnsPerRow }) => {
	const ratio = size === 'large' ? '49.45%' : '75.63%';

	const ratioMobile = '75.63%';

	const featuredImage = post?.featuredImage?.node;

	return post ? (
		<Card size={size} columnsPerRow={columnsPerRow}>
			<Link href={searchReplaceLink(post?.link)} passHref>
				<ThumbnailWrap>
					<Thumbnail
						ratio={ratio}
						ratioMobile={ratioMobile}
						image={featuredImage?.sourceUrl}
						thumbnail={featuredImage?.thumbSourceUrl}
						alt={featuredImage?.alt}
					/>
				</ThumbnailWrap>
			</Link>

			<Link href={searchReplaceLink(post?.link)} passHref>
				<Details>
					<CardLink>
						<LinkItem title={'Meet'} />
						<Title>{post?.title}</Title>
					</CardLink>
				</Details>
			</Link>
		</Card>
	) : null;
};

export default BrandCard;
