import styled from 'styled-components';
import { useState, useEffect } from 'react';
import ShowHide from '../ShowHide';

const Wrapper = styled.div``;

const Accordian = ({ children, align = 'start', activeIndex }) => {
	const [active, setActive] = useState(false);
	const [rows, setRows] = useState(false);

	useEffect(() => {

		const allRows =
			Object.prototype.toString.call(children) == '[object Array]'
				? children
				: [children];

		setRows(allRows);

		if (rows && rows.length) {
			const rowActive = [...Array(rows.length)].map((_, index) => {
				return {
					active: activeIndex === index ? true : false
				};
			});

			setActive(rowActive);
		}
	}, [children, rows]);

	const handleToggle = (clickedIndex) => {
		setActive((prevState) => {
			if (prevState?.[clickedIndex]?.active === true) {
				return prevState.map((_) => {
					return {
						active: false
					};
				});
			}

			return prevState?.map((_, index) => {
				let activeState = index === clickedIndex;

				return {
					active: activeState
				};
			});
		});
	};

	return rows && rows.length > 0 ? (
		<Wrapper>
			{rows.map((child, index) => {
				const title = child?.props?.title ?? child?.props?.title;

				return (
					child && (
						<ShowHide
							title={title}
							active={active[index]?.active === true}
							key={index}
							handleToggle={() => handleToggle(index)}
						>
							{child}
						</ShowHide>
					)
				);
			})}
		</Wrapper>
	) : null;
};

export default Accordian;
