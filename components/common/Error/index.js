import styled from 'styled-components';

const ErrorMessage = styled.div`
	padding-top: 1em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};
`;

const Error = ({ error }) => {
	return <ErrorMessage>{error}</ErrorMessage>;
};

export default Error;
