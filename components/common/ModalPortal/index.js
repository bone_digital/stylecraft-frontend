import { createPortal } from 'react-dom';
import usePortal from '../../../hooks/usePortal';
import { useState, useEffect } from 'react';
import styled from 'styled-components';

const ModalPortal = ({ id, children, active }) => {
	const [mounted, setMounted] = useState(false);
	const target = usePortal(id);

	useEffect(() => {
		setMounted(true);
	}, []);

	return mounted ? createPortal(children, target) : null;
};

export default ModalPortal;
