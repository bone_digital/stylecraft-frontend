import styled from 'styled-components';
import LinkItem from '../LinkHeading';
import { useInView } from 'react-intersection-observer';
import { motion } from 'framer-motion';

const Card = styled.div``;

const Title = styled.h3``;

const NoResultsCard = () => {
	const { ref, inView } = useInView({
		triggerOnce: true,
		root: null,
		rootMargin: '-10% 0px',
		delay: 100
	});

	return (
		<Card>
			<Title>No Results</Title>
		</Card>
	);
};

export default NoResultsCard;
