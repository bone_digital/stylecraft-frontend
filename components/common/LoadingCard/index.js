import styled, { keyframes } from 'styled-components';
import Thumbnail from '../Thumbnail';

const loadingAnimation = keyframes`
	0% {
		opacity: 1;
	}

	100% {
		opacity: 0.5;
	}
`;

const Card = styled.div`
	width: 100%;
	position: relative;
	grid-column: span 4;
	animation: ${loadingAnimation} 1.2s linear infinite alternate;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		grid-column: -1 / 1;
	}
`;

const ThumbnailWrap = styled.a`
	margin-bottom: 20px;
	position: relative;
	display: flex;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		margin-bottom: 32px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: 16px;
	}
`;

const LoadingCard = () => {
	return (
		<Card>
			<ThumbnailWrap title={'heading'}>
				<Thumbnail ratio={'75.63%'} ratioMobile={'75.63%'} />
			</ThumbnailWrap>
		</Card>
	);
};

export default LoadingCard;
