import Grid from '../Grid';

const SubGrid = ({ children }) => {
	return (
		<Grid columns={6} columnsMobile={6} rowGap={'64px'}>
			{children}
		</Grid>
	);
};

export default SubGrid;
