import styled from 'styled-components';
import { useEffect, useRef } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { breakpoint } from '../../../styles/theme';
import ImageEl from '../ImageEl';
import GravityForm from '../../common/GravityForm';
import { useUI } from '../../../context/UIProvider';

const transitionVariants = {
	hidden: { opacity: 0, transition: { duration: 0.525 } },
	visible: { opacity: 1, transition: { duration: 0.525 } }
};

function PopupForm () {
	const wrapperRef = useRef(null);
	const { toggleFormPopup, popupActive, popupData } = useUI() || {};

	const {
		formId = null,
		image = null,
		heading = null,
		formEmailField = ''
	} = popupData || {};

	// lock body scroll.
	useEffect(() => {
		if (popupActive) {
			document.body.classList.add('modal-active');
		}
		else {
			document.body.classList.remove('modal-active');
		}
	}, [popupActive]);

	const dismissPopup = (e) => {
		if (wrapperRef.current === e.target) {
			toggleFormPopup(false);
		}
	}

	return (
		<>
			<AnimatePresence>
				{popupActive && (
					<AnimationWrapper
						variants={transitionVariants}
						initial="hidden"
						animate="visible"
						exit="hidden"
						>
						<ViewportWrapper ref={wrapperRef} onClick={dismissPopup}>
							<PopupWrapper>
								<ImageWrapper>
									<ImageLockup>
										<ImageEl
											image={image?.sizes?.medium}
											thumbnail={image?.sizes?.thumbnail}
											alt={image?.alt}
											/>
									</ImageLockup>
								</ImageWrapper>
								<FormWrapper>
									{heading &&
										<HeadingLockup>
											{heading}
										</HeadingLockup>
									}
									<FormLockup 
										formID={parseInt(formId)}
										buttonType={'button'}
										formEmailField={formEmailField}
										/>
								</FormWrapper>
							</PopupWrapper>
						</ViewportWrapper>
					</AnimationWrapper>
				)}
			</AnimatePresence>
		</>
	);
};

export {
	PopupForm
}

const AnimationWrapper = styled(motion.div)`
	z-index: 10000;
`;

const ViewportWrapper = styled.div`
	position: fixed;
	z-index: 10000;
	height: 100vh;
	width: 100vw;
	top: 0;
	left: 0;
	background-color: rgba(0, 0, 0, 0.2);
	display: flex;
	justify-content: center;
	align-items: center;
`

const PopupWrapper = styled.div`
	background-color: #EBE9E3;
	width: 90%;
	max-width: 1080px;
	height: auto;
	display: flex;

	@media ${breakpoint('tablet')} {
		flex-direction: column;
	}
`

const ImageWrapper = styled.div`
	position: relative;
	flex: 0.5;
	width: 100%;
	background-color: ${({ theme }) => theme.colors.brand};
	overflow: hidden;
`

const ImageLockup = styled.div`
	position: relative;
	height: 100%;
	width: 100%;
	margin-left: 8px;

	img {
		position: absolute;
		height: 100%;
		width: 100%;
		object-fit: cover;
		object-position: center;
	}
`

const FormWrapper = styled.div`
	position: relative;
	flex: 0.5;
	padding: 32px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: stretch;
	width: 100%;

	& > * {
		display: flex;
		width: 100%;
		flex-direction: column;
	}

	& div[type="general"] {
		display: flex;
		flex-direction: column;
		margin-right: 32px;
	}

	& fieldset {
		& legend {
			visibility: visible;
			width: 100%;
			margin-bottom: 24px;
		}
	}
`

const FormLockup = styled(GravityForm)``

const HeadingLockup = styled.h3`
	margin-bottom: 24px;
`