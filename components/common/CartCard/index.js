import React from 'react';
import styled from 'styled-components';
import QuantityPicker from '../../modules/ProductMain/QuantityPicker';
import Thumbnail from '../../common/Thumbnail';
import Link from 'next/link';
import { ease } from '../../../styles/theme';
import searchReplaceLink from '../../../utils/searchReplaceLink';
import { formatPriceWithoutDecimals } from '../../../lib/source/wordpress/utils';

const Card = styled.div`
	display: flex;
	align-items: flex-start;
	font-size: ${({ theme }) => theme.type.p[0]};
	line-height: 28px;
	transition: ${ease('all')};
	opacity: ${({ updating }) => (updating ? 0.5 : 1)};
	flex-wrap: wrap;
`;

const Title = styled.h3`
	margin-bottom: 8px;
`;

const ProductDetails = styled.div`
	padding-right: 30px;
	max-width: 300px;
`;

const LinkInner = styled.a`
	display: flex;
	width: 100%;
`;

const Header = styled.div`
	margin-bottom: 1.333em;
`;

const ThumbnailWrapper = styled.div`
	position: relative;
	display: flex;
	max-width: 150px;
	width: 100%;
	margin-right: 60px;
	flex-shrink: 0;

	@media ${({ theme }) => theme.breakpoints.laptop} {
		max-width: 100%;
		margin-bottom: 1.333em;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		max-width: 150px;
		margin-bottom: 0;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		max-width: 100%;
		margin-bottom: 1.333em;
	}
`;

const Clear = styled.button`
	text-decoration: underline;
	margin-top: 1.33em;

	&:not(:last-child) {
		margin-right: 16px;
	}
`;

const Row = styled.div`
	display: flex;
`;

const Subtitle = styled.div`
	width: 38.95%;
`;

const Body = styled.div`
	flex: 1;
`;

const ItemPrice = styled.h3`
	margin-left: auto;
`;

const CartCard = ({
	productInfo,
	removeItem,
	updating,
	setCartQuantity,
	isWholeseller,
	wishlist = false,
	wishListImage
}) => {
	const { product } = productInfo;
	const subtotal = productInfo?.totals?.subtotal_tax + productInfo?.totals?.total;
	const cartQuantity = productInfo?.quantity?.value;

	const name = productInfo?.name;
	const designer = productInfo?.designer;
	const wholesalePricesJSON = product?.node?.wholesalePrice;

	let wholesalePrice = 0;
	let wholesaleSubtotal = 0;

	if (wholesalePricesJSON) {
		let wholesalePricesObj = JSON.parse(wholesalePricesJSON);

		if (productInfo?.variation) {
			const variationId = productInfo?.variation?.node?.databaseId;
			wholesalePrice = wholesalePricesObj[variationId]?.wholesaleprice;
		} else if (wholesalePricesObj?.wholesale_price) {
			wholesalePrice = wholesalePricesObj?.wholesale_price;
		}

		wholesalePrice = parseInt(wholesalePrice);

		wholesaleSubtotal = parseInt(wholesalePrice * productInfo?.quantity);

		wholesaleSubtotal = `\$${wholesaleSubtotal.toFixed(2)}`;
	}

	const productDetails =
		productInfo?.variation === null
			? productInfo?.product?.node
			: productInfo?.variation?.node;

	let productImage = productInfo?.cart_image;
	if( !productImage )
	{
		productImage = productInfo?.image?.sizes?.card ? productInfo?.image?.sizes?.card : productInfo?.featured_image;
	}

	const productAttributes = productInfo?.meta?.variation;

	const { featuredImage, stockQuantity } = productDetails || {};

	const link = `/product/${productInfo?.slug}`;

	const copy = () => {
		let string = `${cartQuantity} x ${name} - By ${designer?.name}`;

		if (productAttributes) {
			Object.entries(productAttributes)?.map(([ label, value ], index) => {
				string += `, ${label}: ${value}`;
			});
		}

		let tempInput = document.createElement('input');
		document.body.appendChild(tempInput);
		tempInput.value = string;
		tempInput.select();
		document.execCommand('copy', false);
		tempInput.remove();
	};


	return (
		<Card updating={updating}>
			<ThumbnailWrapper>
				<Link href={searchReplaceLink(link) || '#'} passHref>

					<LinkInner title={name}>
						{
							<Thumbnail
							ratio={'100%'}
							ratioMobile={'100%'}
							image={wishlist ? wishListImage : productImage}
							thumbnail={wishlist ? wishListImage : productImage}
							alt={featuredImage?.node?.alt}
						/>
						}
					</LinkInner>
				</Link>
			</ThumbnailWrapper>

			<ProductDetails>
				<Header>
					<Title> {name}</Title>
					{designer && <Body>{`By ${designer}`}</Body>}
				</Header>

				{productAttributes &&
					Object.entries(productAttributes)?.map(([ label, value ], index) => (
						<Row key={index}>
							<Subtitle>{label}</Subtitle>
							<Body>{value}</Body>
						</Row>
					))
				}

				<Row>
					<Subtitle>Qty</Subtitle>
					<QuantityPicker
						quantity={cartQuantity}
						setQuantity={setCartQuantity}
						showStock={false}
						stock={stockQuantity}
					/>
				</Row>

				<Row>
					<Clear onClick={removeItem}>Remove</Clear>
					{wishlist && <Clear onClick={copy}>Copy Details</Clear>}
				</Row>
			</ProductDetails>
			{!isWholeseller ? (
				<>{subtotal && <ItemPrice>{`A$${subtotal.toFixed(2)}`}</ItemPrice>}</>
			) : (
				<>
					{wholesaleSubtotal && (
						<ItemPrice>{`A$${(Math.round(wholesaleSubtotal * 100) / 10000).toFixed(2)}`}</ItemPrice>
					)}
				</>
			)}
		</Card>
	);
};

export default CartCard;
