import { InView } from 'react-intersection-observer';
import { getAllRESTProductCategories } from '../../../lib/source/wordpress/api';
import Meta from '../../../components/common/Meta';
import Layout from '../../../components/common/Layout';
import InnerWrapper from '../../../components/common/InnerWrapper';
import styled from 'styled-components';
import { useState, useEffect, useRef } from 'react';
import { motion } from 'framer-motion';
import Loading from '../../../components/common/Loading';
import {
	archiveSortOptions,
	defaultArchiveSortOption
} from '../../../lib/settings';
import { useRouter } from 'next/router';
import { useUI } from '../../../context/UIProvider';
import { shouldDisableDynamicPathSSG } from '../../../lib/source/wordpress/utils';
import { isObject } from 'lodash';
import ArchiveHeaderV2 from '../../../components/elements/ArchiveHeaderV2';
import { ease } from '../../../styles/theme';
import PostCard from '../../../components/common/PostCard';
import ProductFiltersV2 from '../../../components/elements/ProductFiltersV2';
import { nanoid } from '@reduxjs/toolkit';
import ArchiveActiveFiltersList from '../../../components/elements/ArchiveActiveFiltersList';

function getDefaultSortOption() {
	if (!archiveSortOptions?.length) {
		return null;
	}

	let option = archiveSortOptions?.find(
		(o) => o.value === defaultArchiveSortOption
	);
	if (!option && archiveSortOptions) {
		option = archiveSortOptions[0];
	}

	return option;
}

const menuVariants = {
	open: {
		width: 'calc(50% + 36px)'
	},
	close: {
		width: 'calc(33.33% + 36px)'
	}
};

const archiveRowVariants = {
	hidden: {
		opacity: 0,
		transition: {
			duration: 0.2
		}
	},
	visible: {
		opacity: 1,
		transition: {
			duration: 0.2
		}
	}
};

function createFilterItem(data = {}) {
	const defaults = {
		isSelected: false,
		runtimeId: '',
		type: '',
		id: 0, // used by categories. Still needed???
		slug: '',
		label: '',
		order: 0,
		parentId: 0,
		value: 0, // new - the value to be used in the search param. Might be slug, id, etc.
		onClick: null, // needed?
		children: [] // needed?
	};

	if (!data) {
		return defaults;
	}

	const keysAllowlist = Object.keys(defaults);
	const filteredInitialData = {};
	Object.keys(data).forEach((key) => {
		if (keysAllowlist.includes(key)) {
			filteredInitialData[key] = data[key];
		}
	});

	return Object.assign(defaults, filteredInitialData);
}

export default function ShopSubCategory({
	filtersData,
	products,
	max,
	title,
	categoryId,
	defaultRange
}) {
	const sortOptions = archiveSortOptions;
	const archive = useRef(null);
	// const page = useRef(1);
	const [menuActive, setMenuActive] = useState(false);
	const [filters, setFilters] = useState([]);
	const [page, setPage] = useState(1);
	const [shop, setShop] = useState(false);
	const [posts, setPosts] = useState([]);
	const [isFetchingPosts, setIsFetchingPosts] = useState(false);
	const [sortBy, setSortBy] = useState(getDefaultSortOption());
	const [currQuery, setCurrQuery] = useState({
		query: '',
		posts: [],
		maxPages: 0,
		maxPosts: 0
	});
	const router = useRouter();

	const { setPageTheme } = useUI() || {};

	useEffect(() => {
		setPageTheme('white');
	}, []);

	useEffect(() => {
		if (filtersData) {
			const allNormalised = [];
			for (const groupKey in filtersData) {
				let {
					type,
					filters: rawFilters,
					rewrite_tag
				} = filtersData[groupKey];

				if (
					typeof rawFilters === 'object' &&
					!Array.isArray(rawFilters)
				) {
					rawFilters = Object.values(rawFilters);
				}

				rawFilters?.forEach((filter) => {
					const normalType = rewrite_tag || type;
					const normalised = normaliseFilter(filter, normalType);
					allNormalised.push(normalised);
				});
			}

			// Set initial selected state based on window query params
			const url = new URL(window.location);
			const params = url.searchParams.entries();
			const keyedParams = {};
			for (const [key, value] of params) {
				if (!keyedParams.hasOwnProperty(key)) {
					keyedParams[key] = [];
				}

				keyedParams[key].push(value);
			}

			for (const nf of allNormalised) {
				if (keyedParams[nf.type]?.includes(nf.value)) {
					nf.isSelected = true;
				}
			}
			setFilters(allNormalised);

			let page = 1;
			if ('page' in keyedParams) {
				const value = keyedParams.page;
				if (Array.isArray(value) && value?.length) {
					page = value[0];
				} else if (!isNaN(value)) {
					page = value;
				}

				try {
					page = parseInt(page);
				} catch (err) {
					// do something here?
				}
			}
			setPage(page);

			let sort;
			if ('sort' in keyedParams) {
				let value = keyedParams.sort;
				value = Array.isArray(value) ? value[0] : value;
				sort = value || null;
			}

			let order;
			if ('order' in keyedParams) {
				let value = keyedParams.order;
				value = Array.isArray(value) ? value[0] : value;
				order = value || null;
			}

			let sortOption;
			if (sort && order) {
				const option = sortOptions.find(
					(o) => o.slug === sort && o.sortby === order
				);
				if (option) {
					sortOption = option;
					setSortBy(option);
				}
			}

			if (!sortOption) {
				sortOption = getDefaultSortOption();
			}

			let isShop = false;
			if ('shop' in keyedParams) {
				isShop = true;
				setShop(true);
			}

			const selected = allNormalised.filter((nf) => nf.isSelected);

			updateQuery(selected, page, sortOption, isShop);
		}
	}, [filtersData]);

	const filterGroups = [];
	if (isObject(filtersData)) {
		for (const groupKey in filtersData) {
			const {
				type,
				label,
				multiple,
				rewrite_tag,
				base_parent_id,
				isToggle
			} = filtersData[groupKey];

			const newGroup = {
				type,
				label,
				multiple,
				isToggle: isToggle || false,
				rewriteTag: rewrite_tag,
				baseParentId: base_parent_id || 0,
				filters: filters?.filter((f) => {
					if (rewrite_tag && f.type === rewrite_tag) {
						return f;
					} else if (f.type === type) {
						return f;
					}
				})
			};

			filterGroups.push(newGroup);
		}
	}

	function selectFilter(filter) {
		if (!filter?.type) return;

		const filterGroup = filterGroups?.find((g) => g.type === filter.type);
		const canSelectMultiple = !!filterGroup?.multiple;

		let newFilters;
		if (!filters || !Array.isArray(filters)) {
			newFilters = [];
		} else {
			newFilters = filters.slice();
		}

		let isAlreadySelected = false;
		for (const nf of newFilters) {
			const isSameFilterType = nf.type === filter.type;
			if (!canSelectMultiple && isSameFilterType) {
				nf.isSelected = false;
			}

			if (nf.runtimeId === filter.runtimeId) {
				if (nf.isSelected && filter.isSelected) {
					isAlreadySelected = true;
				}

				nf.isSelected = true;
			}
		}

		if (isAlreadySelected) return;

		setFilters(newFilters);
		onSelectionChange(newFilters.filter((f) => f.isSelected));
	}

	function deselectFilter(filter) {
		if (!filter?.type) return;

		if (!filters || !Array.isArray(filters)) {
			setFilters([]);
		}

		const newFilters = filters.slice();
		for (const nf of newFilters) {
			if (nf.runtimeId === filter.runtimeId) {
				nf.isSelected = false;
				break;
			}
		}

		setFilters(newFilters);
		onSelectionChange(newFilters.filter((f) => f.isSelected));
	}

	function onSelectionChange(newSelections) {
		setPage(1);
		updateQuery(newSelections, 1, null, shop);
	}

	async function updateQuery(
		freshFilters = null,
		freshPage = null,
		freshSort = null,
		isShop,
		resetPosts = true,
	) {
		const uncheckedFilters = freshFilters || filters || [];
		const queryFilters = uncheckedFilters?.filter((f) => f.isSelected); // protect against adding whole array of filters
		const queryPage = freshPage || page || 1;
		const querySort = freshSort || sortBy || getDefaultSortOption();

		const windowParamsString = computeQueryParamaters(
			queryFilters,
			queryPage,
			querySort,
			filterGroups,
			isShop
		);
		updateWindowQuery(windowParamsString);

		const privateParams = {};
		if (queryPage > 1 && resetPosts) {
			privateParams.prior_pages = 'true';
		}
		const queryParamsString = computeQueryParamaters(
			queryFilters,
			queryPage,
			querySort,
			filterGroups,
			shop,
			privateParams
		);

		setIsFetchingPosts(true);

		try {
			const data = await doFetch(queryParamsString);

			const foundPosts = data.posts || [];
			if (resetPosts) {
				setPosts(foundPosts);
			} else {
				if (posts?.length) {
					setPosts(posts.concat(foundPosts));
				} else {
					setPosts(foundPosts);
				}
			}

			setCurrQuery({
				query: windowParamsString,
				posts: data.posts,
				maxPages: data.max,
				maxPosts: data.maxPosts
			});
		} catch (err) {
			console.log('fetch data error', err);
			setCurrQuery({
				query: windowParamsString,
				posts: [],
				maxPages: 0,
				maxPosts: 0
			});
		}

		setIsFetchingPosts(false);
	}

	function updateWindowQuery(paramsString) {
		const currentPath = router.asPath;
		const url = new URL(
			currentPath,
			window.location.protocol + '//' + window.location.host
		);
		let newUrl = router.asPath.replace(url.search, '');
		if (newUrl.endsWith('?')) {
			newUrl = newUrl.slice(0, -1);
		}

		if (paramsString?.length) {
			newUrl += '?' + paramsString;
		}

		router.replace(newUrl, '', {
			scroll: false,
			shallow: true
		});
	}

	/**
	 *
	 *
	 * @returns data
	 * @throws JSON parse error, fetch errors
	 */
	async function doFetch(paramsString) {
		let endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-products?category=${defaultRange}`;
		if (paramsString) {
			endpoint += '&' + paramsString;
		}

		endpoint += '&locale=' + router.locale;

		setIsFetchingPosts(true);
		const response = await fetch(endpoint, {
			method: 'GET'
		});

		const json = await response.json();
		const data = JSON.parse(json);

		return data;
	}

	async function handleLoadmore() {
		if (isFetchingPosts) return;

		const maxPages = currQuery?.maxPages || 0;
		const newPage = page + 1;
		if (newPage > maxPages) return;

		setPage(newPage);

		updateQuery(null, newPage, null, shop, false);
	}

	function handleSortSelection(option) {
		setSortBy(option);

		updateQuery(null, null, option, shop, 1, true);
	}

	const currentPageMaxPosts = getPageMaxPosts(page);

	let displayPosts = posts?.length ? [...posts] : [];
	if (isFetchingPosts) {
		if (displayPosts.length < currentPageMaxPosts) {
			const numFillers = currentPageMaxPosts - displayPosts.length;
			for (let i = 0; i < numFillers; i++) {
				displayPosts.push({
					name: '&nbsp;', // required so title has height
					id: nanoid()
				});
			}
		}
	}

	const hasPosts = !!displayPosts?.length;
	const firstRowPostCount = 2;
	const firstRowPosts = displayPosts.slice(0, firstRowPostCount);
	const remainingPosts = displayPosts.slice(firstRowPostCount);
	const showLoadmore = hasPosts;
	const selectedFilters = filters?.filter((f) => f.isSelected) || [];

	return (
		<>
			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<ArchiveHeaderV2
							key={'archive-header'}
							title={title}
							menuActive={menuActive}
							filters={selectedFilters}
							onDismissFilter={deselectFilter}
							sortBy={sortBy}
							sortOptions={sortOptions}
							setSortBy={handleSortSelection}
						/>

						<FilterBackground
							variants={menuVariants}
							animate={menuActive ? 'open' : 'close'}
							initial={false}
							key="filter-background"
						/>

						<ArchiveBody key={'body-desktop'} respondTo={'desktop'}>
							<ArchiveGrid>
								<ArchiveCardWrapper
									key={'filters'}
									menuActive={menuActive}
									isLoading={false}
									$isFilters={true}
									variants={{
										open: {
											width: '50%'
										},
										close: {
											width: '33.33%'
										}
									}}
									initial={'close'}
									animate={menuActive ? 'open' : 'close'}
									exit={'visible'}
								>
									<ProductFiltersV2
										type="product"
										filters={filters}
										filterGroups={filterGroups}
										onSelectFilter={selectFilter}
										onDeselectFilter={deselectFilter}
										menuActive={menuActive}
										setMenuActive={setMenuActive}
										sortBy={sortBy}
										sortOptions={sortOptions}
										setSortBy={handleSortSelection}
										categoryId={categoryId}
									/>
								</ArchiveCardWrapper>

								{hasPosts &&
									firstRowPosts.map((post) => {
										return (
											<ArchiveCardWrapper
												key={post.id}
												menuActive={menuActive}
												isLoading={isFetchingPosts}
												variants={archiveRowVariants}
												initial={'hidden'}
												// initial={'visible'}
												animate={'visible'}
												exit={'hidden'}
											>
												<PostCard
													ratio={'100%'}
													ratioMobile={'71.42%'}
													post={post}
													type={'product'}
												/>
											</ArchiveCardWrapper>
										);
									})}
							</ArchiveGrid>

							{!!remainingPosts?.length && (
								<ArchiveGrid>
									{remainingPosts.map((post) => {
										return (
											<ArchiveCardWrapper
												key={post.id}
												menuActive={menuActive}
												isLoading={isFetchingPosts}
												variants={archiveRowVariants}
												initial={'hidden'}
												// initial={'visible'}
												animate={'visible'}
												exit={'hidden'}
											>
												<PostCard
													ratio={'100%'}
													ratioMobile={'71.42%'}
													post={post}
													type={'product'}
												/>
											</ArchiveCardWrapper>
										);
									})}
								</ArchiveGrid>
							)}
						</ArchiveBody>

						<ArchiveBody key={'body-mobile'} respondTo={'mobile'}>
							<ProductFiltersV2
								type="product"
								filters={filters}
								filterGroups={filterGroups}
								onSelectFilter={selectFilter}
								onDeselectFilter={deselectFilter}
								menuActive={menuActive}
								setMenuActive={setMenuActive}
								sortBy={sortBy}
								sortOptions={sortOptions}
								setSortBy={handleSortSelection}
								categoryId={categoryId}
							/>

							<ArchiveActiveFiltersList
								respondTo="mobile"
								filters={selectedFilters}
								menuActive={menuActive}
								onDismissFilter={deselectFilter}
							/>

							<ArchiveGrid>
								{hasPosts &&
									displayPosts.map((post) => {
										return (
											<ArchiveCardWrapper
												key={post.id}
												menuActive={menuActive}
												isLoading={isFetchingPosts}
												variants={archiveRowVariants}
												initial={'hidden'}
												// initial={'visible'}
												animate={'visible'}
												exit={'hidden'}
											>
												<PostCard
													ratio={'100%'}
													ratioMobile={'71.42%'}
													post={post}
													type={'product'}
												/>
											</ArchiveCardWrapper>
										);
									})}
							</ArchiveGrid>
						</ArchiveBody>

						{menuActive && (
							<CloseMenu
								key={'close-menu'}
								onClick={() => setMenuActive(false)}
							/>
						)}

						{showLoadmore && (
							<InView onChange={handleLoadmore}>
								<Loading
									pageTheme={'white'}
									active={isFetchingPosts}
									paging={true}
								/>
							</InView>
						)}
					</InnerWrapper>
				</Archive>
			</Layout>
		</>
	);
}

function computeQueryParamaters(
	queryFilters,
	queryPage,
	querySort,
	filterGroups,
	shop,
	customParams = {}
) {
	const params = new URLSearchParams();

	// TODO: add in any query strings that aren't a filter
	for (const record of queryFilters) {
		const filterGroup = filterGroups?.find((g) => g.type === record.type);
		const customRewriteTag = filterGroup?.rewriteTag;

		const tag = customRewriteTag || record.type;
		params.append(tag, record.value);

		params.set(tag, params.getAll(tag).join(','));
	}

	params.set('page', queryPage);

	if (querySort?.slug) {
		params.set('sort', querySort.slug);
	}

	if (querySort?.sortby) {
		params.set('order', querySort.sortby);
	}

	if (shop) {
		params.set('shop', shop);
	}

	if (isObject(customParams)) {
		for (const p of Object.keys(customParams)) {
			params.set(p, customParams[p]);
		}
	}

	const paramsString = params.toString();
	return paramsString;
}

/**
 *
 * @param {int} page
 */
function getPageMaxPosts(page, accumulate = true) {
	if (isNaN(page) || page <= 0) {
		return 0;
	}

	const firstPageMax = 5;
	const normalPageMax = 6;
	if (page === 1) {
		return firstPageMax;
	}

	if (accumulate) {
		return firstPageMax + normalPageMax * (page - 1);
	} else {
		return normalPageMax;
	}
}

function normaliseFilter(filter, type = null) {
	const filterType = type || filter.type || 'unknown';

	const initialData = {
		type: filterType,
		runtimeId: `${filterType}::${filter.slug}`,
		slug: filter.slug,
		value: filter.slug,
		isSelected: false
	};

	if (filter.id) {
		initialData.id = filter.id; // not sure if this is required
	}

	if (filter.title) {
		initialData.label = filter.title;
	}

	if (filter.order) {
		initialData.order = filter.order;
	}

	if (filter.parentId || filter.parent) {
		initialData.parentId = filter.parentId || filter.parent;
	}

	const normalised = createFilterItem(initialData);

	return normalised;
}

const Archive = styled.div`
	overflow: hidden;

	> div {
		position: relative;
		min-height: 100vh;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		background: ${({ theme }) => theme.colors.grey};
	}
`;

const FilterBackground = styled(motion.div)`
	position: absolute;
	top: 0;
	left: 0;
	width: calc(33.33% + 36px);
	height: 100%;
	z-index: 0;
	background: ${({ theme }) => theme.colors.grey};
	transform: translateX(-32px);

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const CloseMenu = styled.button`
	position: fixed;
	top: 0;
	right: 0;
	width: 50%;
	height: 100%;
	z-index: 10;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

const ArchiveBody = styled(motion.div)`
	margin-bottom: -64px;
	position: relative;
	z-index: 2;
	min-height: 200px;
	display: ${({ respondTo }) => (respondTo === 'desktop' ? 'block' : 'none')};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: ${({ respondTo }) =>
			respondTo !== 'desktop' ? 'block' : 'none'};
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		margin-bottom: -80px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		margin-bottom: -54px;
	}
`;

const ArchiveGrid = styled(motion.div)`
	display: flex;
	flex-wrap: nowrap;
	margin: 0 -18px;

	&:nth-child(2) {
		flex-wrap: wrap;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		flex-wrap: wrap;
	}
`;

const ArchiveCardWrapper = styled(motion.div)`
	flex-shrink: 0;
	width: 33.33%;
	padding: 0 18px;
	margin-bottom: 64px;
	transition: ${({ $isFilters }) => ($isFilters ? 'none' : ease('all'))};

	@media ${({ theme }) => theme.breakpoints.laptop} {
		width: 50%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		width: 100%;
		margin-bottom: 80px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		margin-bottom: 54px;
	}

	& > div {
		transition: ${ease('all')};
		opacity: ${({ isLoading }) => (isLoading ? '0.5' : '1')};
	}
`;

/**
 * Returns the data based on static file generation,
 * alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {products: *}}>}
 */
export async function getStaticProps(context) {
	const params = context.params;
	const categories = params.slug;
	const locale = context.locale;

	const category = categories[categories.length - 1];

	let rows = [];

	// const filters = await getFilters(category);
	// // const filters = await getArchiveFilters(category);

	// return {
	// 	props: {
	// 		products: [],
	// 		max: 100000,
	// 		filters: JSON.parse(filters),
	// 		archiveFilters: {},
	// 		title: '',
	// 		defaultRange: category
	// 	},
	// 	// Next.js will attempt to re-generate the page:
	// 	revalidate: 600 // In seconds - 600 = 10 minutes
	// };

	/**
	 * 0.03 (archive)
	 * 2.7 (category)
	 */

	const productsQuery = getProducts(category, locale);
	const filtersQuery = getArchiveFilters(category, locale);

	const queries = await Promise.all([productsQuery, filtersQuery]);
	const [productsJson, filterJson] = queries;

	const data = JSON.parse(productsJson);
	const filtersObj = JSON.parse(filterJson);

	const filters = filtersObj?.filters;
	const title = filtersObj?.name || '';
	const categoryId = filtersObj?.id;

	let max = null;

	if (data) {
		max = data?.max;

		let posts = data?.posts;

		if (posts && posts?.length) {
			const productsHeader = posts.slice(0, 2);

			rows.push(productsHeader);

			if (posts?.length > 2) {
				const productsBody = posts.slice(2);

				rows.push(productsBody);
			}
		}
	}

	const products = data?.posts && data?.posts?.length ? rows : [[null]];

	return {
		props: {
			products,
			max,
			filtersData: filters,
			title,
			categoryId,
			defaultRange: category
		},
		// Next.js will attempt to re-generate the page:
		revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

async function getProducts(category, locale) {
	const endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-products?locale=${locale}&category=${category}`;
	const response = await fetch(endpoint, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	});

	return await response.json();
}

async function getArchiveFilters(category, locale) {
	const endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-product-archive-filters?category=${category}&locale=${locale}`;
	const response = await fetch(endpoint, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	});

	return await response.json();
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths({ locales }) {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allCategories = await getAllRESTProductCategories();

	const paths = [];
	allCategories.forEach((category) => {
		const slug = category?.slug;

		locales.forEach((locale) => {
			paths.push({
				params: { slug: [`${slug}`] },
				locale
			});
		});
	});

	return {
		paths: paths || [],
		fallback: true
	};
}
