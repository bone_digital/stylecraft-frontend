import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import Meta from '../../components/common/Meta';
import { useMemo, useEffect } from 'react';
import {
	getAllRESTProducts, getBrand,
	getProduct,
	getProductBrandData,
	getRESTRelatedProductCards,
	getRelatedProducts
} from "../../lib/source/wordpress/api";
import { shopPrefix } from '../../lib/settings';
import Layout from '../../components/common/Layout';
import Loading from '../../components/common/Loading';
import ProductMain from '../../components/modules/ProductMain';
import ProductPostCarousel from '../../components/modules/ProductPostCarousel';
import ProductCollection from '../../components/modules/ProductCollection';
import { retrievePageBuilderModules, shouldDisableDynamicPathSSG } from "../../lib/source/wordpress/utils";
import PageBuilder from "../../components/common/PageBuilder";
import { useUI } from '../../context/UIProvider';

/**
 * @param  props0
 * @param  props0.post
 */
export default function Product(props) {
	const { data, slug, redirect, brandData, relatedProducts } = props;

	let product = data?.product || {};
	const siteOptions = data?.acfOptions?.siteOptions;
	const router = useRouter();
	const brand = product?.product?.brand;
	const modules = retrievePageBuilderModules(product)

	useEffect(() => {
		if (redirect) {
			router.push('/');
		}
	}, []);

	const { setPageTheme } = useUI() || {};
	setPageTheme('white');

	const userRoles = useSelector(({ user }) => user.roles);

	const isWholeseller = useMemo(() => {
		if (userRoles && userRoles?.length) {
			return (
				userRoles.filter(({ node }) => {
					const roleName = node?.name;

					return (
						roleName === 'default_wholesaler' ||
						roleName === 'administrator' ||
						roleName === 'wholesaler' ||
						roleName === 'wholesale'
					);
				})?.length > 0
			);
		}

		return false;
	}, [userRoles]);

	if ((!router.isFallback && !slug) || router.isFallback) {
		//Hasn't finished rendering
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const seo = product?.seo?.fullHead;

	return (
		<>
			<Meta seo={seo} />
			<Layout>
				<ProductBuilder>
					<ProductMain
						product={product}
						siteOptions={siteOptions}
						isWholeseller={isWholeseller}
					/>

					<ProductPostCarousel product={product} relatedProducts={relatedProducts}/>
				</ProductBuilder>

				{brand && <ProductCollection brandData={brandData} brand={brand} />}

				{modules && (
					<PageBuilder modules={modules} showrooms={null} />
				)}
			</Layout>
		</>
	);
}

const ProductBuilder = styled.main`
	section {
		padding: 100px 0;
		z-index: 2;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding: 65px 0;
		}
	}

	> *:first-child {
		padding-top: 132px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 100px;
		}
	}
`;

/**
 * Sets the data up at build time to pre render.
 * Can be changed to getServerSideProps for server side rendering, ie no pre-building
 *
 * @param params
 * @return {Promise<{props: {postData: *}}>}
 */
export async function getStaticProps(context) {
	const params = context.params;
	const slug = params.slug;
	let data = null;

	try {
		data = await getProduct(slug);
	} catch (error) {
		console.log(error);
		console.log(`message: ${slug} - ${error.message}`);
	}

	// Check if there are brand products
	let brandData = null;
	if (data?.product?.product?.brand )
	{
		try {
			brandData = await getProductBrandData(slug);
			if ( brandData?.product?.product?.brand )
			{
				brandData = brandData.product.product.brand;
			}
		}
		catch (error) {
			brandData = null;
			console.log(error);
			console.log(`message - brand data error - : ${slug} - ${error.message}`);
		}
	}

	const relatedProductsQuery = await getRESTRelatedProductCards(data?.product?.databaseId, context.locale, 3);
	let relatedProducts = null;
	try {
		relatedProducts = JSON.parse(relatedProductsQuery);
	}
	catch (err) {
		//
	}

	const currentLocale = context.locale;
	const productLocale = data?.product?.product?.locales;
	let redirect = false;

	switch (productLocale) {
		case 'all':
			break;
		case 'au':
			if (currentLocale === 'en-SG') {
				redirect = true;
			}
			break;
		case 'sg':
			if (currentLocale === 'en-AU') {
				redirect = true;
			}
			break;
	}

	return {
		props: {
			data,
			slug,
			relatedProducts,
			brandData,
			redirect
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 30 // In seconds - 600 = 10 minutes
	};
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths({ locales }) {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allProducts = await getAllRESTProducts();

	const paths = [];
	allProducts.forEach((product) => {
		locales.forEach((locale) => {
			paths.push({
				params: { slug: `${product?.slug}` },
				locale
			});
		});
	});

	return {
		paths: paths || [],
		fallback: true
	};
}


async function getProducts(category, locale) {
	const endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-products?category=${category}&locale=${locale}`;
	const response = await fetch(endpoint, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
		},
	});

	return await response.json();
}

function randomizeArray(array) {
	return array?.sort(function(a, b){return 0.5 - Math.random()});
}

function removeCurrentProduct(array, product) {
	return array?.posts?.filter((post) => post.id !== product?.databaseId);
}
