import { InView } from 'react-intersection-observer';
import { getAllRESTProductRange } from '../../../lib/source/wordpress/api';
import Meta from '../../../components/common/Meta';
import Layout from '../../../components/common/Layout';
import InnerWrapper from '../../../components/common/InnerWrapper';
import styled from 'styled-components';
import { useState, useEffect, useRef } from 'react';
import { motion } from 'framer-motion';
import ArchiveHeader from '../../../components/elements/ArchiveHeader';
import ProductArchive from '../../../components/elements/ProductArchive';
import Loading from '../../../components/common/Loading';
import { archiveSortOptions } from '../../../lib/settings';
import useProductFilters from '../../../hooks/useProductFilters';
import useHandleSort from '../../../hooks/useHandleSort';
import useFetchProducts from '../../../hooks/useFetchProducts';
import useGetProductQueryFilters from '../../../hooks/useGetProductQueryFilters';
import { useRouter } from 'next/router';
import { useUI } from '../../../context/UIProvider';
import { shouldDisableDynamicPathSSG } from '../../../lib/source/wordpress/utils';

export default function ShopRangeCategory({
	filters,
	products,
	max,
	title,
	defaultRange
}) {
	const options = archiveSortOptions;
	const archive = useRef(null);
	const page = useRef(1);
	const [loadMoreTrigger, setLoadMoreTrigger] = useState(false);
	const [menuActive, setMenuActive] = useState(false);
	const router = useRouter();
	const archiveType = 'range';

	useEffect(() => {
		if (!router.isReady) {
			return;
		}
		console.log('ready!', router.query)
	}, [router.isReady]);

	const { setPageTheme } = useUI() || {};
	setPageTheme('white');

	const {
		brandFilters,
		designerFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		rangeFilters,
		priceFilters,
		activeFilters,
		australianMade,
		newProducts,
		userInteracted,
		handleBrandFilterAdd,
		handleDesignerFilterAdd,
		handleProductFilterAdd,
		handleLeadtimeFilterAdd,
		handleEnvironmentFilterAdd,
		handlePriceFilterAdd,
		handleRangeFilterAdd,
		handleAustralianMadeAdd,
		handleNewProductsAdd,
		handleShopFilterAdd,
		handleActiveFilters,
		handleRemoveFilter,
		setUserInteracted,
		// selectedFilters,
	} = useProductFilters(page, filters);

	const { handleSetSortBy, sortBy } = useHandleSort(options);

	const {
		loading,
		appending,
		filteredProducts,
		setFilteredProducts,
		createFilterUrl,
		createFilterUrlOnly,
		fetchProducts,
		fetchProductsFromUrl,
		doLoadMore,
	} = useFetchProducts(
		null,
		page,
		max,
		loadMoreTrigger,
		sortBy,
		userInteracted,
		handleActiveFilters,
		designerFilters,
		brandFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		priceFilters,
		rangeFilters,
		australianMade,
		newProducts,
		defaultRange,
		archiveType,
	);

	const selectedFilters = useGetProductQueryFilters(filters);

	useEffect(() => {
		if (router.isReady && selectedFilters !== null) {
			fetchProductsFromUrl();
		}
	}, [router.isReady, selectedFilters])

	useEffect(() => {
		if (router.isReady) {
			createFilterUrl()
		}
	}, [sortBy])


	/**
	 * Close menu
	 */
	const handleCloseMenu = () => {
		setMenuActive(false);
	};


	const menuVariant = {
		open: {
			width: 'calc(50% + 36px)'
		},
		close: {
			width: 'calc(33.33% + 36px)'
		}
	};


	const onLoadmoreChange = (inView) => {
		if (inView && router.isReady) {
			doLoadMore();
		}
	}

	const hasProducts = !!filteredProducts?.length;
	const showLoadmore = router.isReady && hasProducts;

	return (
		<>

			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<ArchiveHeader
							key={'archive-header'}
							title={title}
							menuActive={menuActive}
							filters={activeFilters}
							onClick={handleRemoveFilter}
							options={options}
							setSortBy={handleSetSortBy}
						/>


						<FilterBackground
							variants={menuVariant}
							animate={menuActive ? 'open' : 'close'}
							initial={false}
							key="filter-background"
						/>

						<ProductArchive
							key="archive-body"
							loading={loading}
							products={filteredProducts}
							filters={filters}
							setUserInteracted={setUserInteracted}
							activeFilters={activeFilters}
							handleBrandFilterAdd={handleBrandFilterAdd}
							handleDesignerFilterAdd={
								handleDesignerFilterAdd
							}
							handleProductFilterAdd={
								handleProductFilterAdd
							}
							handleLeadtimeFilterAdd={
								handleLeadtimeFilterAdd
							}
							handleEnvironmentFilterAdd={
								handleEnvironmentFilterAdd
							}
							handlePriceFilterAdd={handlePriceFilterAdd}
							handleRangeFilterAdd={handleRangeFilterAdd}
							handleAustralianMadeAdd={
								handleAustralianMadeAdd
							}
							handleNewProductsAdd={handleNewProductsAdd}
							setMenuActive={setMenuActive}
							menuActive={menuActive}
							handleRemoveFilter={handleRemoveFilter}
							options={options}
							setSortBy={handleSetSortBy}
							sortBy={sortBy}
						/>


						{menuActive && (
							<CloseMenu
								key={'close-menu'}
								onClick={handleCloseMenu}
							/>
						)}

						{showLoadmore && (
							<InView onChange={onLoadmoreChange}>
								<Loading
									pageTheme={'white'}
									active={appending}
									paging={true}
									/>
							</InView>
						)}

					</InnerWrapper>

				</Archive>
			</Layout>
		</>
	)
}

const Archive = styled.div`
	overflow: hidden;

	> div {
		position: relative;
		min-height: 100vh;
	}

	@media ${({ theme }) => theme.breakpoints.laptop} {
		background: ${({ theme }) => theme.colors.grey};
	}
`;

const FilterBackground = styled(motion.div)`
	position: absolute;
	top: 0;
	left: 0;
	width: calc(33.33% + 36px);
	height: 100%;
	z-index: 0;
	background: ${({ theme }) => theme.colors.grey};
	transform: translateX(-32px);

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const CloseMenu = styled.button`
	position: fixed;
	top: 0;
	right: 0;
	width: 50%;
	height: 100%;
	z-index: 10;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

/**
 * Returns the data based on static file generation,
 * alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {products: *}}>}
 */
export async function getStaticProps(context) {
	const params = context.params;
	const range = params.slug;
	const locale = context.locale;

	let rows = [];

	const productsQuery = getProducts(range, locale);
	const filtersQuery = getFilters(range);

	const queries = await Promise.all([productsQuery, filtersQuery]);
	const [getData, getFilterData] = queries;

	const data = JSON.parse(getData);
	const filtersObj = JSON.parse(getFilterData);

	const filters = filtersObj?.filters;
	const title = filtersObj?.name;

	let max = null;

	if (data) {
		max = data?.max;

		let posts = data?.posts;

		if (posts && posts?.length) {
			const productsHeader = posts.slice(0, 2);

			rows.push(productsHeader);

			if (posts?.length > 2) {
				const productsBody = posts.slice(2);

				rows.push(productsBody);
			}
		}
	}

	const products = data?.posts && data?.posts?.length ? rows : [[null]];

	return {
		props: {
			products,
			max,
			filters,
			title,
			defaultRange: range
		},
		// Next.js will attempt to re-generate the page:
		revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

async function getProducts(range, locale) {
	const endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-products?locale=${locale}&range=${range}`;

	const response = await fetch(endpoint, {
		method: 'GET'
	});

	return await response.json();
}

async function getFilters(range) {
	const endpoint = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-product-range-categories?range=${range}`;

	const response = await fetch(endpoint, {
		method: 'GET'
	});

	return await response.json();
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths({ locales }) {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allCategories = await getAllRESTProductRange();

	const paths = [];
	allCategories.forEach((category) => {
		locales.forEach((locale) => {
			paths.push({
				params: { slug: `${category?.slug}` },
				locale
			});
		});
	});

	return {
		paths: paths || [],
		fallback: true
	};
}
