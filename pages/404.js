import Layout from '../components/common/Layout';
import Meta from '../components/common/Meta';
import styled from 'styled-components';
import Link from 'next/link';
import InnerWrapper from '../components/common/InnerWrapper/index';
import Grid from '../components/common/Grid';
import { ease } from '../styles/theme';

const Wrapper = styled.section`
	width: 100%;
	display: flex;
	align-items: center;
	padding-top: 220px;
	padding-bottom: 100px;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-bottom: 132px;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		padding-top: 100px;
	}
`;

const ErrorMessage = styled.h3`
	width: 100%;
	display: block;
	position: relative;
	font-weight: 400;
	text-align: left;

	&::before {
		content: '';
		position: absolute;
		bottom: -4px;
		left: 0;
		height: 1px;
		max-width: 130px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	&:hover {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const Dot = styled.div`
	width: 16px;
	height: 16px;
	border-radius: 50%;
	display: block;
	background: ${({ theme }) => theme.colors.brand};
	margin-right: 22px;
	margin-top: 0.4em;
`;

const Trigger = styled.a`
	position: relative;
	grid-column: span 6;
	text-align: left;
	display: flex;
	align-items: flex-start;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: -1 / 1;
	}
`;

/**
 * @param  props0
 * @param  props0.page
 */
export default function Custom404(props) {
	return (
		<>
			<Meta />
			<Layout props={props}>
				<Wrapper>
					<InnerWrapper>
						<Grid>
							<Trigger>
								<Dot />
								<Link href="/" passHref>
									<ErrorMessage title="Return Home">
										Cant find what you are looking for?
										<br />
										Return home.
									</ErrorMessage>
								</Link>
							</Trigger>
						</Grid>
					</InnerWrapper>
				</Wrapper>
			</Layout>
		</>
	);
}
