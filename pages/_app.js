import 'normalize.css';
import { GlobalStyles } from '../styles/global';
import { useState, useEffect } from 'react';
import { UIProvider } from '../context/UIProvider';
import { ApolloProvider } from '@apollo/client';
import client from '../lib/source/wordpress/apolloClient';
import { ThemeProvider } from 'styled-components';
import { theme } from '../styles/theme';
import { DefaultSeo } from 'next-seo';
import '../styles/styles.css';
import CursorWrapper from '../components/layout/CursorWrapper';
import Head from 'next/head';
import * as ga from '../lib/tracking/google-analytics';
import * as fb from '../lib/tracking/facebook-pixel';
import * as pinterest from '../lib/tracking/pinterest-tracking';
import GoogleAnalytics from '../components/common/Tracking/GoogleAnalytics';
import FacebookPixel from '../components/common/Tracking/FacebookPixel';
import GetUser from '../components/common/GetUser';

const options = require('../json/options.json');

/**
 * REDUX: Import the provider and our store, then wrap whatever components should have access to it
 */
import { Provider } from 'react-redux';
import store from '../store';
import { useRouter } from 'next/router';
import Pinterest from '../components/common/Tracking/Pinterest';

/**
 * @param  props0
 * @param  props0.Component
 * @param  props0.pageProps
 */
function WebApp({ Component, pageProps }) {
	const [cursorRefresh, setCursorRefresh] = useState(0);
	const router = useRouter();
	const routerEvents = router.events;

	const handleCursorRefresh = () => {
		setCursorRefresh((prevState) => {
			return prevState + 1;
		});
	};

	const handleExitComplete = () => {
		window.scrollTo(0, 0);
		setCursorRefresh((prevState) => {
			return prevState + 1;
		});

		setTimeout(() => {
			setCursorRefresh((prevState) => {
				return prevState + 1;
			});
		}, 1000);
	};

	useEffect(() => {
		console.log(
			'%c 🦴 Built by Bone - bone.digital 🦴',
			'background-color: black; color: #FFF; font-size: 12px; font-family: monospace; padding: 6px 10px;'
		);
		pinterest.event('pagevisit', {});
	}, []);

	//Setup google tracking
	useEffect(() => {
		const handleRouteChange = (url) => {
			ga.pageview(url);
			fb.pageview();
			pinterest.pageview();
			pinterest.event('pagevisit', {});
		};

		routerEvents.on('routeChangeComplete', handleRouteChange);

		return () => {
			routerEvents.off('routeChangeComplete', handleRouteChange);
		};
	}, [routerEvents]);

	return (
		<ThemeProvider theme={theme}>
			<ApolloProvider client={client}>
				<Provider store={store}>
					<UIProvider>
						<GetUser />
						<DefaultSeo />
						<Head>
							<link
								rel="icon"
								type="image/svg+xml"
								href="/favicon.svg"
							/>
							<meta name='google-site-verification' content='4TBVhWOzVhJaG9x9YxN5bBHaBFFe2Sk18n2l5LZOVv0' />
						</Head>
						<GlobalStyles />
						<GoogleAnalytics />
						<FacebookPixel />
						<Pinterest />
						<CursorWrapper
							handleCursorRefresh={handleCursorRefresh}
							cursorRefresh={cursorRefresh}
							exitComplete={handleExitComplete}
						>
							<Component {...pageProps} />
						</CursorWrapper>
					</UIProvider>
				</Provider>
			</ApolloProvider>
		</ThemeProvider>
	);
}

export default WebApp;
