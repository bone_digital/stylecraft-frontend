import { useRouter } from 'next/router';
import { getAllRESTPages, getPage, getPageById } from '../lib/source/wordpress/api';
import Layout from '../components/common/Layout';
import Meta from '../components/common/Meta';
import Loading from '../components/common/Loading';
import { retrievePageBuilderModules, shouldDisableDynamicPathSSG } from '../lib/source/wordpress/utils';
import PageBuilder from '../components/common/PageBuilder';
import PreviewBar from '../components/elements/PreviewBar';
import Account from '../components/elements/Account';
import { getSingleById } from '../lib/source/wordpress/rest-api';

const systemPages = require('../json/system-pages.json');

/**
 * @param  props0
 * @param  props0.page
 */
export default function DefaultPage(props) {
	const router = useRouter();
	const page = props?.data?.page || null;
	const seo = page?.seo?.fullHead;
	const showrooms = props?.data?.showrooms;

	const myAccountSystemPage = systemPages?.find((page) => page?.shortcode === '[woocommerce_my_account]');
	const isAccountPage = myAccountSystemPage?.page_id == page?.databaseId;

	if (
		(!router.isFallback && !page?.slug && !props.isPreview) ||
		router.isFallback
	) {
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const modules = retrievePageBuilderModules(page);

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout props={props}>
				{isAccountPage && (
					<Account />
				)}
				{modules && (
					<PageBuilder modules={modules} showrooms={showrooms} />
				)}
			</Layout>
		</>
	);
}

/**
 * Gets all the possible page paths
 */
export async function getStaticPaths() {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allPages = await getAllRESTPages();

	const paths = allPages.map((page) => {
		return `/${page?.slug}`;
	});

	return {
		paths: paths || [],
		fallback: true
	};
}

/**
 * @param  props0
 * @param  props0.params
 */
export async function getStaticProps(context) {
	const { params, preview, previewData } = context;

	const {postID, previewID} = previewData || {};

	console.log({postID, previewID})
	let data;
	if (preview && postID) {
		data = await getPageById(postID, previewData)
		// data = await getSingleById('page', postID, previewData);
	}
	else {
		data = await getPage(params.slug, preview, previewData);
	}

	return {
		props: {
			data,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}
