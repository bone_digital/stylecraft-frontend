import { InView } from 'react-intersection-observer';
import { useSelector } from 'react-redux';
import Meta from '../components/common/Meta';
import Layout from '../components/common/Layout';
import InnerWrapper from '../components/common/InnerWrapper';
import styled from 'styled-components';
import { useState, useEffect, useRef, useMemo } from 'react';
import { useRouter } from 'next/router';
import useLoadMore from '../hooks/useLoadMore';
import _ from 'lodash';
import Loading from '../components/common/Loading';
import { archiveSortOptions } from '../lib/settings';
import { motion } from 'framer-motion';
import ProductArchive from '../components/elements/ProductArchive';
import SearchHeader from '../components/elements/SearchHeader';
import useProductFilters from '../hooks/useProductFilters';

export default function Blog({ filters, locale }) {
	const options = archiveSortOptions;
	const archive = useRef(null);
	const [page, setPage] = useState(1);
	const [maxPages, setMaxPages] = useState(1);
	const [loadMoreTrigger, setLoadMoreTrigger] = useState(false);
	const [menuActive, setMenuActive] = useState(false);
	const [loading, setLoading] = useState(false);
	const [appending, setAppending] = useState(false);
	const [sortBy, setSortBy] = useState(null);
	const [fetchedPosts, setFetchedPosts] = useState([]);
	const [filteredPosts, setFilteredPosts] = useState([
		[false, false],
		[false, false, false]
	]);
	const [filterByType, setFilterByType] = useState(null);
	const [maxProductsCategories, setMaxProductsCategories] = useState({
		brandMax: 0,
		postMax: 0,
		productMax: 0
	});
	const [isMainQueryComplete, setIsMainQueryComplete] = useState(false);

	const {
		brandFilters,
		designerFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		rangeFilters,
		priceFilters,
		activeFilters,
		australianMade,
		newProducts,
		userInteracted,
		handleBrandFilterAdd,
		handleBrandFilterRemove,
		handleDesignerFilterAdd,
		handleDesignerFilterRemove,
		handleProductFilterAdd,
		handleProductFilterRemove,
		handleLeadtimeFilterAdd,
		handleLeadtimeFilterRemove,
		handleEnvironmentFilterAdd,
		handleEnvironmentFilterRemove,
		handlePriceFilterAdd,
		handlePriceFilterRemove,
		handleRangeFilterAdd,
		handleRangeFilterRemove,
		handleAustralianMadeAdd,
		// TO-DO
		handleAustralianMadeRemove,
		handleNewProductsAdd,
		// TO-DO
		handleNewProductsRemove,
		handleActiveFilters,
		handleRemoveFilter,
		setUserInteracted,
		resetFilters
	} = useProductFilters(page);

	const router = useRouter();
	const searchQuery = router?.query?.search;

	/**
	 * Close menu
	 */
	const handleCloseMenu = () => {
		setMenuActive(false);
	};

	/**
	 * @param {array} posts
	 * @param {boolean} loadMore
	 */
	const organisePosts = (posts = [], loadMore = false) => {
		const emptyPosts = [[null]];
		if (!posts || posts.length === 0) {
			setFilteredPosts(emptyPosts);
		}

		if (!loadMore) {
			let rows = [];
			const productsHeader = posts.slice(0, 2);

			const productsBody = posts.length > 2 ? posts.slice(2) : null;

			rows.push(productsHeader);

			if (productsBody) {
				rows.push(productsBody);
			}

			setFilteredPosts(rows);
		} else {
			setFilteredPosts((prevState) => {
				let postArray = prevState;
				if (postArray.length === 2) {
					postArray[1].push(...posts);
				}

				return [...postArray];
			});
		}
	};

	const userRoles = useSelector(({ user }) => user.roles);
	const isWholeseller = useMemo(() => {
		if (userRoles && userRoles?.length) {
			return (
				userRoles.filter(({ node }) => {
					const roleName = node?.name;

					return (
						roleName === 'default_wholesaler' ||
						roleName === 'administrator' ||
						roleName === 'wholesaler'
					);
				})?.length > 0
			);
		}

		return false;
	}, [userRoles]);

	/**
	 * Fetch REST API posts
	 *
	 * TODO: refactor so we don't have initialFetch _and_ loadMore - these are opposites of each
	 * other so only one is required.
	 *
	 * @param {boolean} loadMore
	 */
	async function handleFetchPosts(initialFetch = false, loadMore = false, customArgs = {}) {
		setLoading(true);
		if (initialFetch) {
			setIsMainQueryComplete(false);
		}

		const emptyResults = [];

		const defaultArgs = {
			search: searchQuery,
			type: filterByType,
			designerFilters,
			brandFilters,
			productFilters,
			leadtimeFilters,
			environmentFilters,
			priceFilters,
			rangeFilters,
			australianMade,
			newProducts,
			sortBy,
			page: page != null ? page : 1
		}

		const searchArgs = Object.assign(defaultArgs, customArgs);

		if (isWholeseller === true) {
			searchArgs.isWholeseller = true;
		}

		if (locale !== null) {
			searchArgs.locale = locale;
		}

		fetch('/api/search', {
			method: 'POST',
			mode: 'same-origin',
			body: JSON.stringify(searchArgs)
		})
			.then((response) => response.json())
			.then((data) => {
				if (data) {
					let posts = data.posts;

					if (posts.length <= 0) {
						posts = emptyResults;
					}

					setMaxPages(data.max_pages);

					if (initialFetch) {
						setFetchedPosts(posts);
						setMaxProductsCategories({
							brandMax: data?.brand?.total_posts || 0,
							postMax: data?.post?.total_posts || 0,
							productMax: data?.product?.total_posts || 0
						});
					}

					// If posts
					if (posts && posts.length > 0) {
						organisePosts(posts, !initialFetch && loadMore);
					} else {
						organisePosts(emptyResults);
						return;
					}
				} else {
					organisePosts(emptyResults);
					return;
				}
			})
			.catch((error) => {
				console.log({ message: error });
				setFetchedPosts([]);
				organisePosts(emptyResults);
			})
			.finally(() => {
				setTimeout(() => {
					setLoading(false);
					setAppending(false);

					if (initialFetch) {
						setIsMainQueryComplete(true);
					}
				}, 200);
			});
	}

	// On load, get search query
	useEffect(() => {
		if (!searchQuery) return;

		handleFetchPosts(true);
	}, [searchQuery]);

	useEffect(() => {
		if (!isMainQueryComplete) return;

		console.log('filterbytype')
		// if (!filterByType) return;

		resetFilters();
		setPage(1);
		handleFetchPosts(false, false, {page: 1});



	}, [filterByType, setIsMainQueryComplete]);

	/**
	 * Handles filter change
	 * Reset page number
	 */
	useEffect(() => {
		if (userInteracted && filterByType === 'product') {
			setPage(1);
			handleActiveFilters();
			handleFetchPosts(false);
		}
	}, [
		brandFilters,
		designerFilters,
		productFilters,
		leadtimeFilters,
		environmentFilters,
		priceFilters,
		australianMade,
		newProducts,
		userInteracted
	]);

	const toTitle = (string) => {
		if (!string || string == null) {
			return '';
		}

		return string
			.split('-')
			.map((word) => {
				return word.slice(0, 1).toUpperCase() + word.slice(1);
			})
			.join(' ');
	};

	// Paging
	useEffect(() => {
		if (loadMoreTrigger && page < maxPages) {
			const newPage = page + 1;
			setPage(newPage);
			setAppending(true);
			handleFetchPosts(false, true, {page: newPage});
		}
	}, [loadMoreTrigger, maxPages]);

	const menuVariant = {
		open: {
			width: 'calc(50% + 36px)'
		},
		close: {
			width: 'calc(33.33% + 36px)'
		}
	};

	/**
	 * Receive sort value from select, find object in array
	 * @param {string} sortValue
	 */
	const handleSortBy = (sortValue) => {
		let sortFilter = options.find((filter) => {
			return filter?.value === sortValue;
		});

		setSortBy(sortFilter);
	};

	const showLoadmoreTrigger = !!(fetchedPosts?.length && !loading)

	return (
		<>
			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<SearchHeader
							key={'search-archive-header'}
							title={toTitle(searchQuery)}
							menuActive={menuActive}
							filteredPosts={filteredPosts}
							cachedResults={fetchedPosts}
							onClick={handleRemoveFilter}
							filterByType={filterByType}
							activeFilters={activeFilters}
							setFilterByType={setFilterByType}
							userInteracted={userInteracted}
							maxProductsCategories={maxProductsCategories}
						/>

						{filteredPosts.length > 0 && (
							<>
								<FilterBackground
									variants={menuVariant}
									animate={menuActive ? 'open' : 'close'}
									initial={false}
									key="filter-background"
								/>

								<ProductArchive
									key="search-archive-body"
									loading={loading}
									products={filteredPosts}
									filters={filters}
									setUserInteracted={setUserInteracted}
									activeFilters={activeFilters}
									handleDesignerFilterAdd={
										handleDesignerFilterAdd
									}
									handleBrandFilterAdd={handleBrandFilterAdd}
									handleProductFilterAdd={
										handleProductFilterAdd
									}
									handleLeadtimeFilterAdd={
										handleLeadtimeFilterAdd
									}
									handleEnvironmentFilterAdd={
										handleEnvironmentFilterAdd
									}
									handlePriceFilterAdd={handlePriceFilterAdd}
									handleRangeFilterAdd={handleRangeFilterAdd}
									handleAustralianMadeAdd={
										handleAustralianMadeAdd
									}
									handleNewProductsAdd={handleNewProductsAdd}
									setMenuActive={setMenuActive}
									menuActive={menuActive}
									handleDesignerFilterRemove={
										handleDesignerFilterRemove
									}
									handleBrandFilterRemove={
										handleBrandFilterRemove
									}
									handleProductFilterRemove={
										handleProductFilterRemove
									}
									handleLeadtimeFilterRemove={
										handleLeadtimeFilterRemove
									}
									handleEnvironmentFilterRemove={
										handleEnvironmentFilterRemove
									}
									handlePriceFilterRemove={
										handlePriceFilterRemove
									}
									handleRangeFilterRemove={
										handleRangeFilterRemove
									}
									handleRemoveFilter={handleRemoveFilter}
									options={options}
									setSortBy={handleSortBy}
									sortBy={sortBy}
									filterByType={filterByType}
									setFilterByType={setFilterByType}
									type={'search'}
								/>
							</>
						)}

						{menuActive && (
							<CloseMenu
								key={'close-menu'}
								onClick={handleCloseMenu}
							/>
						)}

						{showLoadmoreTrigger && (
							<InView onChange={(inView) => setLoadMoreTrigger(inView)}>
								<Loading
									pageTheme={'white'}
									active={appending}
									paging={true}
								/>
							</InView>
						)}
					</InnerWrapper>
				</Archive>
			</Layout>
		</>
	);
}

const Archive = styled.div`
	display: block;
	min-height: 100vh;
	padding-top: 145px;
	position: relative;
`;

const FilterBackground = styled(motion.div)`
	position: absolute;
	top: 0;
	left: 0;
	width: calc(33.33% + 36px);
	height: 100%;
	z-index: 0;
	background: ${({ theme }) => theme.colors.grey};
	transform: translateX(-32px);

	@media ${({ theme }) => theme.breakpoints.laptop} {
		display: none;
	}
`;

const CloseMenu = styled.button`
	position: fixed;
	top: 0;
	right: 0;
	width: 50%;
	height: 100%;
	z-index: 10;

	@media ${({ theme }) => theme.breakpoints.desktop} {
		display: none;
	}
`;

/**
 * Returns the data based on static file generation,
 * alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {products: *}}>}
 */
export async function getStaticProps(context) {
	const getFilters = await fetch(
		`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-product-categories`,
		{
			method: 'GET'
		}
	).then((response) => {
		return response.json();
	});

	const locale = context.locale;

	const getFilterData = await getFilters;
	const filtersObj = JSON.parse(getFilterData);

	const filters = filtersObj?.filters;

	return {
		props: {
			filters,
			locale
		},
	};
}
