import { useRouter } from 'next/router';
import { getPage } from '../../lib/source/wordpress/api';
import Layout from '../../components/common/Layout';
import Meta from '../../components/common/Meta';
import Loading from '../../components/common/Loading';
import { retrievePageBuilderModules } from '../../lib/source/wordpress/utils';
import PageBuilder from '../../components/common/PageBuilder';
import PreviewBar from '../../components/elements/PreviewBar';

/**
 * @param  props0
 * @param  props0.page
 */
export default function DefaultPage(props) {
	const router = useRouter();
	const page = props?.data?.page;
	const seo = page?.seo?.fullHead;
	const showrooms = props?.page?.showrooms;
	/*
	if ((!router.isFallback && !page?.slug) || router.isFallback) {
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	} */

	const modules = retrievePageBuilderModules(page);

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout props={props}>
				{modules && (
					<PageBuilder modules={modules} showrooms={showrooms} />
				)}
			</Layout>
		</>
	);
}

/**
 * @param  props0
 * @param  props0.params
 */
export async function getStaticProps({ preview }) {
	const data = await getPage('showroom-archive');

	return {
		props: {
			data: data,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}
