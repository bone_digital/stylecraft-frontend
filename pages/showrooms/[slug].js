import styled from 'styled-components';
import { useRouter } from 'next/router';
import Meta from '../../components/common/Meta';

import {
	getShowroom,
	getAllRESTShowroom,
	getRelatedShowrooms
} from '../../lib/source/wordpress/api';
import { showroomPrefix } from '../../lib/settings';
import { retrievePostBuilderModules, shouldDisableDynamicPathSSG } from '../../lib/source/wordpress/utils';
import Layout from '../../components/common/Layout';
import Loading from '../../components/common/Loading';
import ShowroomHeader from '../../components/modules/ShowroomHeader';
import PostBuilder from '../../components/common/PostBuilder';
import PreviewBar from '../../components/elements/PreviewBar';
/**
 * @param  props0
 * @param  props0.post
 */
export default function Showroom(props) {
	const data = props.data;
	const relatedPosts = props.relatedPosts;

	const router = useRouter();
	const staffShowroom = data?.staffShowroom;
	const showroom = data?.showroom;
	const title = showroom?.title;
	const showroomFields = showroom?.showroom;

	if ((!showroom?.slug && !props.isPreview) || router.isFallback) {
		//Hasn't finished rendering
		return <Loading />;
	}

	const seo = showroom?.seo?.fullHead;
	const modules = retrievePostBuilderModules(showroom);
	const relatedPostOverwrite = showroom?.relatedShowrooms?.relatedPosts;

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout>
				<ShowroomHeader showroom={showroomFields} title={title} />

				{modules && (
					<PostBuilder
						modules={modules}
						type={'showroom'}
						showrooms={staffShowroom}
						relatedPosts={relatedPosts}
						relatedPostOverwrite={relatedPostOverwrite}
					/>
				)}
			</Layout>
		</>
	);
}

/**
 * Sets the data up at build time to pre render.
 * Can be changed to getServerSideProps for server side rendering, ie no pre-building
 *
 * @param params
 * @return {Promise<{props: {postData: *}}>}
 */
export async function getStaticProps(context) {
	const { params, preview, previewData } = context;

	let relatedPosts = null;
	let data = null;

	try {
		const id = preview ? parseInt(previewData?.postID) : params?.slug;

		data = await getShowroom(id, preview);

		const postId = data?.showroom?.showroomId;

		if (postId) {
			relatedPosts = await getRelatedShowrooms(postId);
		}
	} catch (error) {
		console.log(`message: ${params.slug} - ${error.message}`);
	}

	return {
		props: {
			data,
			relatedPosts,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the post:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths() {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allPosts = await getAllRESTShowroom();

	const paths = allPosts.map((post) => {
		return {
			params: { slug: `${post?.slug}` }
		};
	});

	return {
		paths: paths || [],
		fallback: true
	};
}
