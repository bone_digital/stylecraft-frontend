export default async function filteredProducts(req, res) {
	let queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-products?`;

	if (req.body) {
		let { filters } = JSON.parse(req.body) || null;
		if( !filters.includes('sort') )
		{
			filters += '&sort=date';
			filters = filters.replace('order=ASC', 'order=DESC');
		}
		queryURL += filters;
	}

	try {
		const products = await fetch(queryURL, {
			method: 'GET',
			headers: {
                'Content-Type': 'application/json',
            },
		})
			.then((response) => response.json())
			.then((data) => JSON.parse(data));

		res.status(200).json(products);
	} catch (error) {
		res.status(200).json(error.message);
	}
}
