export default async function handler(req, res) {
	// Check the secret to confirm this is a valid request
	if (req.query.token !== process.env.INVALIDATE_SECRET_TOKEN)
	{
		return res.status(401).json({ message: 'Invalid token' });
	}

	const slug = req.query.revalidate_path;
	let archivePageSlug = '';
	if( slug.includes('showroom'))
	{
		archivePageSlug = '/showroom-archive/';
	}
	else if( slug.includes('project') )
	{
		archivePageSlug = '/project';
	}
	else if( slug.includes('product') )
	{
		archivePageSlug = '/product';
	}
	else if( slug.includes('brand') )
	{
		archivePageSlug = '/brand';
	}
	else if( slug.includes('blog') )
	{
		archivePageSlug = '/blog';
	}

	try
	{
		await res.revalidate(`${slug}`);
		if( archivePageSlug )
		{
			await res.revalidate(`${archivePageSlug}`);
		}
		return res.json({ revalidated: true });
	}
	catch (err)
	{
		// If there was an error, Next.js will continue
		// to show the last successfully generated page
		return res.status(500).send('Error revalidating');
	}
}
