import { postForm } from '../../lib/source/wordpress/api';

export default async function handler(req, res) {
	const formBody = JSON.parse(req.body);
	const formID = formBody?.formID;

	//Clean the data
	let values = {};

	for (const key in formBody) {
		if (key.includes('form_')) {
			//It's a value
			const newKey = key.replace(`form_${formID}_`, 'input_');
			if (formBody[key].includes('RECAPTCHA')) {
				values['g-recaptcha-response'] = formBody[key].replace(
					'RECAPTCHA',
					''
				);
			}

			//tostring for cleaning subscribe popup checkboxes
			values[newKey] = formBody[key].toString();
		}
	}

	//Submit to the server
	let data = await postForm(formID, values);

	//Check if errors
	if (data?.is_valid === false) {
		//Clean the data to work with Formik
		let errors_list = {};
		let messages = data?.validation_messages;
		if (messages) {
			Object.keys(messages).map((index) => {
				const key = `form_${formID}_${index}`;
				errors_list[key] = messages[index];
			});
		}
		data.errors_list = errors_list;
	}
	res.status(200).json(data);
}
