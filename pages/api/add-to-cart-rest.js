import { getAuthToken } from '../../lib/source/cocart/api';

export default async function addToCart(req, res) {
	
	const reqCookies = req?.headers?.cookie?.split(';')
		.map(v => v.split('='))
		.reduce((acc, v) => {
			acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
			return acc;
		}, {});

	const cookieAuthToken = reqCookies?.token;
	const cookieRefreshToken = reqCookies?.['refresh-token'];
	const cookieCartKey = reqCookies?.['cart-key'];

	const searchParams = new URLSearchParams();

	if (req.body) {
		const {
			id,
			quantity,
			userId,
		} = JSON.parse(req.body) || {};

		if (!id || !quantity) {
			res.status(400).json({ code: 400, msg: 'Missing id or quantity' });
			return;
		}

		if (cookieCartKey && userId != cookieCartKey) {
			searchParams.set('cart_key', cookieCartKey);
		}

		if (id) {
			searchParams.set('id', id);
		}

		if (quantity) {
			searchParams.set('quantity', quantity);
		}
	}

	const queryURL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}cocart/v2/cart/add-item?${searchParams.toString()}`;
	
	try {

		let authToken, headers;

		if (cookieAuthToken && cookieRefreshToken) {
			authToken = await getAuthToken(cookieAuthToken, cookieRefreshToken);
		}

		if (authToken) {
			headers = {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${authToken}`
			}
		} else {
			headers = {
				'Content-Type': 'application/json',
			}
		}

		const cart = await fetch(queryURL, {
			method: 'POST',
			headers
		})
			.then((response) => response.json());

		res.status(200).json(cart);
	} catch (error) {
		res.status(400).json({ code: 400, msg: `Add To Cart Error: ${error.message}` });
	}
}
