import client from '../../lib/source/wordpress/apolloClient';
import { gql } from '@apollo/client';
import {
	SimpleProductFields,
	VariableProductFields,
	ImageFields
} from '../../lib/source/wordpress/queries/products';


/**
 * Basic add to cart mutation
 * @type {DocumentNode | *}
 */
const ADD_TO_CART_MUTATION = gql`
	mutation ADD_TO_CART($productId: Int!, $quantity:Int!) {
		addToCart(input:{
			productId:$productId,
			quantity:$quantity
		}) {
				clientMutationId
				cart {
					total
					wholesaleTotal
					subtotal
					contents {
						itemCount
						edges {
							node {
								key
								subtotal
								quantity
								product {
									node {
										link
										name
										wholesalePrice
										... on VariableProduct {
											${VariableProductFields}
										}
										... on SimpleProduct {
											${SimpleProductFields}
										}
									}
								}
								variation {
									attributes {
										name
										label
										value
									}
									node {
										name
										databaseId
										id
										link
										name
										featuredImage {
											node {
												${ImageFields}
											}
										}
										status
										stockQuantity
										stockStatus
										contentType {
											node {
												name
											}
										}
									}
								}
							}
					}
				}
			}
		}
	}
`;

export default async function handler(req, res) {
	// console.log(req.body)
	// res.status(200).json({true: 'true'});
	//if (!req.body) return;

	const { variables } = JSON.parse(req.body);

	try {
		const addToCart = await client.mutate({
			mutation: ADD_TO_CART_MUTATION,
			variables
		});
		res.status(200).json(addToCart);
	} catch (error) {
		res.status(200).json(error.message);
	}
}
