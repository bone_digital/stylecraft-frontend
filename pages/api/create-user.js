export default async function createUser(req, res) {
	let queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/create-user?`;

	if (req.body) {
		const {
			email,
			firstName,
			lastName,
			description,
			password,
			roles,
			subscribe,
			abn,
			location
		} = JSON.parse(req.body) || {};

		if (email) {
			queryURL += `&email=${email}`;
		}

		if (firstName) {
			queryURL += `&first_name=${firstName}`;
		}

		if (lastName) {
			queryURL += `&last_name=${lastName}`;
		}

		if (description) {
			queryURL += `&description=${description}`;
		}

		if (password) {
			queryURL += `&password=${password}`;
		}

		if (roles) {
			queryURL += `&roles=${roles}`;
		}
		else
		{
			queryURL += `&roles=wholesale-pending`;
		}

		if (abn) {
			queryURL += `&abn=${abn}`;
		}

		if (location) {
			queryURL += `&location=${location}`;
		}

		if (subscribe) {
			queryURL += `&subscribe=${subscribe}`;
		}
	}

	queryURL = queryURL.slice(0, -1);

	try {
		const user = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())
			.then((data) => JSON.parse(data));

		res.status(200).json(user);
	} catch (error) {
		res.status(500).json({ statusCode: 500, message: error.message });
	}
}
