import { getCustomer } from '../../lib/source/woo/api';

export default async function handler(req, res) {
	const reqBody = JSON.parse(req.body);
	let userId = reqBody?.userId;

	
	if (!userId || userId === null) {
		res.status(500).json({ message: `User ID is undefined` });
	} else {
		const data = await getCustomer(userId);
		res.status(200).json(data);
	}
}
