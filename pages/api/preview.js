import {
	getBrand,
	getHomePage,
	getPage,
	getPageById,
	getPost,
	getProduct,
	getProject,
	getShowroom
} from '../../lib/source/wordpress/api';

import {
	BRANDS_PREFIX,
	PROJECTS_PREFIX,
	PRODUCTS_PREFIX,
	SHOWROOMS_PREFIX,
	BLOG_PREFIX
} from '../../lib/source/config';

export default async function handler(req, res) {
	let { secret, id, slug, token } = req.query;

	if (
		!process.env.WORDPRESS_PREVIEW_SECRET ||
		secret !== process.env.WORDPRESS_PREVIEW_SECRET ||
		(!id && !slug)
	) {
		return res.status(401).json({
			message: 'Invalid token',
			key: process.env.WORDPRESS_PREVIEW_SECRET,
			id: id,
			slug: slug
		});
	}

	/**
	 * Get the post type, then check if it exists via a query
	 */
	const postType = req.query.post_type || 'page';

	let data = false;

	const previewData = {
		token: token,
		postID: req.query.id,
		previewID: req.query.preview_id
	};

	let url = '/';

	/**
	 * Add additional post types here if required
	 */
	if ('home' === slug) {
		data = await getHomePage(previewData, id);
		url = '/';
	} else if ('brand' === postType) {
		data = await getBrand(id, previewData);
		url = `${BRANDS_PREFIX}/${slug}`;
	} else if ('product' === postType) {
		data = await getProduct(slug);
		url = `${PRODUCTS_PREFIX}/${slug}`;
	} else if ('post' === postType) {
		data = await getPost(id, previewData);
		url = `${BLOG_PREFIX}/${slug}`;
	} else if ('project' === postType) {
		data = await getProject(id, previewData);
		url = `${PROJECTS_PREFIX}/${slug}`;
	} else if ('showroom' === postType) {
		data = await getShowroom(id, previewData);
		url = `${SHOWROOMS_PREFIX}/${slug}`;
	} else {
		data = await getPageById(id, previewData);
		url = `/${slug}`;
	}

	/**
	 * If no data exists, return an error
	 */
	if (!data) {
		return res.status(401).json({ message: 'Invalid slug' });
	}

	/**
	 * Set preview data then redirect to the page
	 */
	res.setPreviewData(previewData, { maxAge: 45 });

	/**
	 * Redirect to the final url
	 */
	res.redirect(url);
}
