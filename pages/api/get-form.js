import { getForm } from '../../lib/source/wordpress/api';

export default async function handler(req, res) {
	const formBody = JSON.parse(req.body);
	let formID = formBody?.formID;

	if (!formID || formID === null) {
		res.status(500).json({ message: `Form ID is undefined` });
	} else {
		const data = await getForm(formID);

		res.status(200).json(data);
	}
}
