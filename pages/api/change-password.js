export default async function createUser(req, res) {

	const searchParams = new URLSearchParams();

	if (req.body) {
		const {
			currentPassword,
			newPassword,
			newPasswordRepeated,
			userId
		} = JSON.parse(req.body) || {};

		if (userId) {
			searchParams.set('userId', userId);
		}

		if (currentPassword) {
			searchParams.set('currentPassword', currentPassword);
		}

		if (newPassword) {
			searchParams.set('newPassword', newPassword);
		}

		if (newPasswordRepeated) {
			searchParams.set('newPasswordRepeated', newPasswordRepeated);
		}
	}

	const queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/change-password?${searchParams.toString()}`;

	try {
		const result = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())

		res.status(200).json(result);
	} catch (error) {
		res.status(400).json({ code: 400, msg: error.message });
	}
}
