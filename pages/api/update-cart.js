import { getAuthToken } from '../../lib/source/cocart/api';

export default async function updateCart(req, res) {

	const reqCookies = req?.headers?.cookie?.split(';')
		.map(v => v.split('='))
		.reduce((acc, v) => {
			acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
			return acc;
		}, {});

	const cookieAuthToken = reqCookies?.token;
	const cookieRefreshToken = reqCookies?.['refresh-token'];
	const cookieCartKey = reqCookies?.['cart-key'];

	const searchParams = new URLSearchParams();

	const {
		itemKey,
		quantity,
		userId
	} = JSON.parse(req?.body) || {};

	if (!itemKey || (!quantity && quantity !== 0)) {
		res.status(400).json({ code: 400, msg: 'Missing id or quantity' });
		return;
	}

	if (cookieCartKey && userId != cookieCartKey) {
		searchParams.set('cart_key', cookieCartKey);
	}

	let method = 'POST';

	if (quantity) {
		searchParams.set('quantity', quantity);
	}
	else if( quantity === 0 )
	{
		method = 'DELETE';
	}

	const queryURL = `${process.env.NEXT_PUBLIC_WP_URL}${process.env.NEXT_PUBLIC_RESTAPI_ENDPOINT}cocart/v2/cart/item/${itemKey}?${searchParams.toString()}`;

	try {

		let authToken, headers;

		if (cookieAuthToken && cookieRefreshToken) {
			authToken = await getAuthToken(cookieAuthToken, cookieRefreshToken);
		}

		if (authToken) {
			headers = {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${authToken}`
			}
		} else {
			headers = {
				'Content-Type': 'application/json',
			}
		}

		const cart = await fetch(queryURL, {
			method: method,
			headers,
			data: JSON.stringify({
				quantity,
				return_cart: true
			})
		})
			.then((response) => response.json());

		res.status(200).json(cart);
	} catch (error) {
		res.status(400).json({ code: 400, msg: `Add To Cart Error: ${error.message}` });
	}
}
