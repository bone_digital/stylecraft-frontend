export default async function updateWishlist(req, res) {

	const searchParams = new URLSearchParams();

	const {
		userId = null,
		add = null,
		remove = null,
		merge = null
	} = JSON.parse(req?.body) || {};

	if (userId) {
		searchParams.set('userId', userId);
	}

	if (add) {
		searchParams.set('add', add);
	}

	if (remove) {
		searchParams.set('remove', remove);
	}

	if (merge) {
		searchParams.set('merge', JSON.stringify(merge));
	}

	const queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/update-wishlist?${searchParams.toString()}`;

	try {
		const result = await fetch(queryURL, {
			method: 'GET',
		})
			.then((response) => response.json())

		res.status(200).json(result);
	} catch (error) {
		res.status(400).json({ code: 400, msg: error.message });
	}
}
