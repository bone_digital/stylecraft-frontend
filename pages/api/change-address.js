export default async function createUser(req, res) {
	let queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/change-address?`;

	if (req.body) {
		const {
			firstName,
			lastName,
			address1,
			company,
			city,
			state,
			country,
			postcode,
			userId,
			type
		} = JSON.parse(req.body) || {};

		if (userId) {
			queryURL += `&userId=${userId}`;
		}

		if (type) {
			queryURL += `&type=${type}`;
		}

		if (firstName) {
			queryURL += `&firstName=${firstName}`;
		}

		if (lastName) {
			queryURL += `&lastName=${lastName}`;
		}

		if (address1) {
			queryURL += `&address1=${address1}`;
		}

		if (company) {
			queryURL += `&company=${company}`;
		}

		if (city) {
			queryURL += `&city=${city}`;
		}

		if (state) {
			queryURL += `&state=${state}`;
		}

		if (country) {
			queryURL += `&country=${country}`;
		}

		if (postcode) {
			queryURL += `&postcode=${postcode}`;
		}
	}

	try {
		const user = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())
			.then((data) => JSON.parse(data));

		res.status(200).json(user);
	} catch (error) {
		res.status(400).json({ statusCode: 400, message: error.message });
	}
}
