export default async function search(req, res) {
	let cmsURL = process.env.NEXT_PUBLIC_CMS_URL;
	let queryURL = `${cmsURL}/wp-json/stylecraft/v1/search?`;

	if (req.body) {
		const {
			search,
			type,
			designerFilters,
			brandFilters,
			productFilters,
			leadtimeFilters,
			environmentFilters,
			priceFilters,
			rangeFilters,
			australianMade,
			newProducts,
			sortBy,
			page,
			isWholeseller = false,
			locale = null
		} = JSON.parse(req.body) || {};

		if (search) {
			let searchQuery = `search=${search}`;

			queryURL += `${searchQuery}&`;
		}

		if (brandFilters && brandFilters.length) {
			let brandArray = brandFilters.map((filter) => {
				return filter?.slug;
			});

			let brandQuery = `brand=${brandArray.join()}`;

			queryURL += `${brandQuery}&`;
		}

		if (type) {
			let typeQuery = `type=${type}`;

			queryURL += `${typeQuery}&`;
		}

		if (designerFilters && designerFilters.length) {
			let designerArray = designerFilters.map((filter) => {
				return filter?.slug;
			});

			let designerQuery = `designer=${designerArray.join()}`;

			queryURL += `${designerQuery}&`;
		}

		if (productFilters && productFilters.length) {
			let productArray = productFilters.map((filter) => {
				return filter?.slug;
			});

			let productQuery = `category=${productArray.join()}`;

			queryURL += `${productQuery}&`;
		}

		if (leadtimeFilters && leadtimeFilters.length) {
			let leadtimeArray = leadtimeFilters.map((filter) => {
				return filter?.slug;
			});

			let leadtimeQuery = `lead_time=${leadtimeArray.join()}`;

			queryURL += `${leadtimeQuery}&`;
		}

		if (environmentFilters && environmentFilters.length) {
			let environmentArray = environmentFilters.map((filter) => {
				return filter?.slug;
			});

			let environmentQuery = `environmental=${environmentArray.join()}`;

			queryURL += `${environmentQuery}&`;
		}

		if (rangeFilters && rangeFilters.length) {
			let rangeArray = rangeFilters.map((filter) => {
				return filter?.slug;
			});

			let rangeQuery = `range=${rangeArray.join()}`;

			queryURL += `${rangeQuery}&`;
		}

		if (priceFilters && priceFilters.length) {
			let priceArray = priceFilters.map((filter) => {
				return filter?.slug;
			});

			let priceQuery = `price=${priceArray.join()}`;

			queryURL += `${priceQuery}&`;
		}

		if (australianMade === true) {
			queryURL += `australian_made=${australianMade}&`;
		}

		if (newProducts === true) {
			queryURL += `new=${newProducts}&`;
		}

		if (isWholeseller === true) {
			queryURL += `wholesale=${isWholeseller}&`;
		}

		if (locale) {
			queryURL += `locale=${locale}&`;
		}

		if (sortBy) {
			queryURL += `sort=${sortBy?.slug}&`;
			queryURL += `order=${sortBy?.sortby}&`;
		}

		if (page && typeof page === 'number') {
			let pageQuery = `page=${page}`;

			queryURL += `${pageQuery}&`;
		}
	}

	queryURL = queryURL.slice(0, -1);

	try {
		const products = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())
			.then((data) => JSON.parse(data));

		res.status(200).json(products);
	} catch (error) {
		res.status(200).json(error.message);
	}
}
