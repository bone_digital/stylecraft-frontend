export default async function createUser(req, res) {

	const searchParams = new URLSearchParams();

	if (req.body) {
		const {
			email
		} = JSON.parse(req.body) || {};

		if (email) {
			searchParams.set('email', email);
		}
	}

	const queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/reset-password?${searchParams.toString()}`;

	try {
		const result = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())

		res.status(200).json(result);
	} catch (error) {
		res.status(400).json({ code: 400, msg: error.message });
	}
}
