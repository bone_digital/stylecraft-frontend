export default async function filteredProducts(req, res) {
	let queryURL = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-brands?`;

	if (req.body) {
		const { sortBy, page } = JSON.parse(req.body) || {};

		if (page && typeof page === 'number') {
			let pageQuery = `page=${page}`;

			queryURL += `${pageQuery}&`;
		}

		if (sortBy) {
			queryURL += `sort=${sortBy?.slug}&`;
			queryURL += `order=${sortBy?.sortby}&`;
		}
	}

	queryURL = queryURL.slice(0, -1);

	try {
		const products = await fetch(queryURL, {
			method: 'GET'
		})
			.then((response) => response.json())
			.then((data) => JSON.parse(data));

		res.status(200).json(products);
	} catch (error) {
		res.status(200).json(error.message);
	}
}
