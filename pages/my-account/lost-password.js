import { useRouter } from 'next/router';
import Layout from '../../components/common/Layout';
import Meta from '../../components/common/Meta';
import styled from 'styled-components';
import { useState } from 'react';
import { Formik, Form, Field } from 'formik';
import PasswordField from '../../components/common/PasswordField';
import * as Yup from 'yup';
import { ease } from '../../styles/theme';


const PageTitle = styled.h2`
	margin-bottom: 64px;
`

const Wrapper = styled.div`
	margin-bottom: 64px;
	grid-column: 5 / span 4;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: 2 / span 4;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const FormWrapper = styled(Form)`
	width: 100%;
`;

const Grid = styled.div`
	display: grid;
	grid-template-columns: repeat(12, minmax(0, 1fr));
	row-gap: 60px;
	column-gap: 36px;
	grid-auto-rows: initial;

	&:first-child {
		margin-bottom: 30px;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(6, minmax(0, 1fr));
		row-gap: 30px;
		column-gap: 24px;
	}
`;

const Column = styled.div`
	grid-column: 5 / span 4;
	display: flex;
	justify-content: space-between;
	align-items: start;
	flex-wrap: wrap;
	gap: 20px;

	> div {
		width: 100%;
	}

	@media ${({ theme }) => theme.breakpoints.tablet} {
		grid-column: 2 / span 4;
	}

	@media ${({ theme }) => theme.breakpoints.mobile} {
		grid-column: -1 / 1;
	}
`;

const SubmitButton = styled.button`
	flex-shrink: 0;
	flex-grow: 0;
	transition: ${ease('all')};
	margin-top: 30px;
`;

const Underline = styled.div`
	display: inline-block;
	position: relative;
	padding-bottom: 8px;

	&::before {
		content: '';
		position: absolute;
		bottom: -2px;
		right: 0;
		height: 1px;
		width: 100%;
		background: ${({ theme }) => theme.colors.black};
		transform: scaleX(1);
		transition: ${ease('transform', 'fast')};
		transform-origin: left;
	}

	${SubmitButton}:hover & {
		&::before {
			transform: scaleX(0);
			transform-origin: right;
		}
	}
`;

const ErrorMessage = styled.div`
	padding-bottom: 2em;
	font-size: ${({ theme }) => theme.type.h6[0]};
	line-height: ${({ theme }) => theme.type.h6[1]};
	color: ${({ theme }) => theme.colors.red};

	strong {
		font-weight: normal;
	}
`;


/**
 * @param  props0
 * @param  props0.page
 */
export default function Page(props) {
	const router = useRouter();
	const page = props?.data?.page || null;
	const seo = page?.seo?.fullHead;
	const showrooms = props?.data?.showrooms;

	const [newPassword, setNewPassword] = useState('');
	const [newPasswordRepeated, setNewPasswordRepeated] = useState('');
	const [formError, setFormError] = useState(null);
	const [showConfirmation, setShowConfirmation] = useState(false);

	const resetKey = router?.query?.key;
	const userId = router?.query?.id;

	console.log({resetKey, userId}, router.query)

	const newPasswordField = {
		id: 'newPassword',
		key: 'newPassword',
		formId: 'registerForm',
		type: 'text',
		label: 'New Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'New password',
		fieldErrors: {}
	};

	const newPasswordRepeatedField = {
		id: 'newPasswordRepeated',
		key: 'newPasswordRepeated',
		formId: 'registerForm',
		type: 'text',
		label: 'Repeat New Password',
		cssClass: null,
		isRequired: true,
		placeholder: 'Repeat New Password',
		fieldErrors: {}
	};

	const registerSchema = Yup.object().shape({
		newPassword: Yup.string().required('New Password is required'),
		newPasswordRepeated: Yup.string()
			.required('Repeat New Password is required')
			.matches(newPassword, 'Passwords must match'),
	});

	const updatePassword = async (values, actions) => {

		if (values?.newPassword !== values?.newPasswordRepeated) {
			setFormError('Passwords must match');
			actions.setSubmitting(false);
			return;
		}

		// console.log('values', values);
		// actions.setSubmitting(false);
		// return;

		const url = `${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/reset-password-with-key`

		try {
			const response = await fetch(url, {
				method: 'POST',
				headers: {
					['Content-type']: 'application/json',
				},
				body: JSON.stringify({
					key: resetKey,
					userId: userId,
					newPassword: values?.newPassword,
					newPasswordRepeated: values?.newPasswordRepeated,
				})
			});

			const data = await response.json();

			if (data.error) {
				setFormError(data.error);
			}
			else {
				setShowConfirmation(true);
			}

			actions.setSubmitting(false);
		} catch (error) {
			const decodeHtmlCharCodes = (str) => {
				let element = document.createElement('textarea');
				element.innerHTML = str;

				return element.value;
			};

			if (error) {
				setFormError(decodeHtmlCharCodes(String(error?.message)));
			}
		} finally {
			actions.setSubmitting(false);
		}
	};

	const isRouterReady = router.isReady;
	const isValidResetLink = isRouterReady && router.query.key && router.query.id;

	return (
		<>
			<Meta seo={seo} />
			<Layout props={props}>
				<PageWrapper>
					<Grid>
					<Wrapper>
						<PageTitle>Password reset</PageTitle>

						{isRouterReady && !isValidResetLink && (
							<p>The reset link is invalid.</p>
						)}

						{isRouterReady && isValidResetLink && (
							<>
								{showConfirmation ? (
									<>
										Password reset successfully!
									</>
								) : (
									<>


										<Formik
											initialValues={{
												key: 'howdy',
												newPassword,
												newPasswordRepeated,
											}}
											validationSchema={registerSchema}
											onSubmit={(values, actions) => {
												updatePassword(values, actions);
											}}
										>
											{({
												values,
												errors,
												touched,
												isSubmitting,
												handleChange,
												handleBlur
											}) => (
												<FormWrapper noValidate>
														{formError && (
															<ErrorMessage dangerouslySetInnerHTML={{ __html: formError }} />
														)}
														<Column>
															<PasswordField
																field={newPasswordField}
																error={touched.newPassword && errors.newPassword}
																value={values.newPassword}
																disabled={isSubmitting}
																onChange={handleChange}
																onBlur={handleBlur}
															/>
															<PasswordField
																field={newPasswordRepeatedField}
																error={touched.newPasswordRepeated && errors.newPasswordRepeated}
																value={values.newPasswordRepeated}
																disabled={isSubmitting}
																onChange={handleChange}
																onBlur={handleBlur}
															/>
															<Field
																type="hidden"
																name="key"
															/>
														</Column>

														<Column>
															<SubmitButton
																type="submit"
																title="Submit"
																disabled={isSubmitting}
															>
																<Underline>Submit</Underline>
															</SubmitButton>
														</Column>

												</FormWrapper>
											)}
										</Formik>
									</>
								)}
							</>
						)}


					</Wrapper>
					</Grid>
				</PageWrapper>
			</Layout>
		</>
	);
}




const PageWrapper = styled.div`
	padding-top: 132px;

	@media ${({ theme }) => theme.breakpoints.tablet} {
		padding-top: 100px;
	}
`;


/**
 * @param  props0
 * @param  props0.params
 */
export async function getStaticProps(context) {
	// const { params, preview, previewData } = context;

	// let data = null;

	// const id = preview ? parseInt(previewData?.postID) : params?.slug;

	// try {
	// 	data = await getHomePage(preview, id);
	// } catch (error) {
	// 	console.log(error);
	// 	console.log(`message: Home Page - ${error.message}`);
	// }

	return {
		props: {
			data: {},
		},
		// Next.js will attempt to re-generate the page:
		//revalidate: 600 // In seconds - 600 = 10 minutes
	};
}
