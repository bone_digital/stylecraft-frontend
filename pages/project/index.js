import { InView } from 'react-intersection-observer';
import Meta from '../../components/common/Meta';
import Layout from '../../components/common/Layout';
import InnerWrapper from '../../components/common/InnerWrapper';
import styled from 'styled-components';
import { useRef, useState } from 'react';
import useLoadMore from '../../hooks/useLoadMore';
import useSortPosts from '../../hooks/useSortPosts';
import useProjectFilters from '../../hooks/useProjectFilters';
import _ from 'lodash';
import Loading from '../../components/common/Loading';
import PostArchive from '../../components/elements/PostArchive';
import BlogHeader from '../../components/elements/BlogHeader';
import useFetchProjects from '../../hooks/useFetchProjects';
import useHandleSort from '../../hooks/useHandleSort';
import { getAllRESTProjectCategories } from '../../lib/source/wordpress/api';

const Archive = styled.div`
	display: block;
	min-height: 100vh;
	padding-top: 145px;
	position: relative;
	background: ${({ theme }) => theme.colors.grey};
`;

export default function ProjectBlog({ posts, max, filterCategories }) {
	const archive = useRef(null);

	const [loadMoreTrigger, setLoadMoreTrigger] = useState(false);

	const { sortBy } = useHandleSort();
	const { handleFilterBy } = useProjectFilters();

	const { loading, appending, fetchedPosts, setFetchedPosts } =
		useFetchProjects(posts, max, loadMoreTrigger, sortBy);

	useSortPosts(fetchedPosts, setFetchedPosts, sortBy, false);

	return (
		<>
			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<BlogHeader
							options={filterCategories}
							title="Projects"
							handleFilterBy={handleFilterBy}
						/>
						<PostArchive
							posts={fetchedPosts}
							loading={loading}
							sortBy={sortBy}
						/>
						<InView onChange={(inView) => setLoadMoreTrigger(inView)}>
							<Loading
								pageTheme={'white'}
								active={appending}
								paging={true}
								/>
						</InView>
					</InnerWrapper>
				</Archive>
			</Layout>
		</>
	);
}

/**
 * Returns the data based on static file generation, alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {allPosts: *}}>}
 */
export async function getStaticProps() {
	const getPosts = await fetch(
		`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-projects?`,
		{
			method: 'GET',
			headers: {
                'Content-Type': 'application/json',
            },
		}
	);

	const getData = await getPosts.json();
	const data = JSON.parse(getData);

	const projectCategories = await getAllRESTProjectCategories();
	const filterCategories = await getFilterCategories(projectCategories)

	const max = data?.max;
	const posts = data?.posts;

	return {
		props: {
			posts,
			max,
			filterCategories
		},
		// Next.js will attempt to re-generate the page:
		//revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

function getFilterCategories(allCategories) {
	allCategories?.forEach(category => {
		category.label = category?.title
		category.value = category?.slug
	});

	const latestFilter = {
		value: '',
		slug: '',
		label: 'Latest'
	}

	allCategories?.unshift(latestFilter);

	return allCategories
}
