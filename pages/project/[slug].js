import styled from 'styled-components';
import { useRouter } from 'next/router';
import Meta from '../../components/common/Meta';

import {
	getAllRESTProjects,
	getProject,
	getRelatedProjects
} from '../../lib/source/wordpress/api';
import { projectPrefix } from '../../lib/settings';
import { retrievePostBuilderModules, shouldDisableDynamicPathSSG } from '../../lib/source/wordpress/utils';
import Layout from '../../components/common/Layout';
import Loading from '../../components/common/Loading';
import PostHeader from '../../components/modules/PostHeader/index';
import PostBuilder from '../../components/common/PostBuilder';
import PreviewBar from '../../components/elements/PreviewBar';

/**
 * @param  props0
 * @param  props0.post
 */
export default function Project(props) {
	const data = props.data;
	const relatedPosts = props.relatedPosts;

	const router = useRouter();

	const showrooms = data?.showrooms;
	const project = data?.project;

	if (
		(!router.isFallback && !project?.slug && !props.isPreview) ||
		router.isFallback
	) {
		//Hasn't finished rendering
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const seo = project?.seo?.fullHead;
	const postHeader = project?.post?.postHeader;
	const modules = retrievePostBuilderModules(project);
	const relatedPostOverwrite = project?.relatedProjects?.relatedPosts;

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout>
				<PostHeader data={postHeader} post={project} />
				{modules && (
					<PostBuilder
						type={'project'}
						modules={modules}
						showrooms={showrooms}
						relatedPosts={relatedPosts}
						relatedPostOverwrite={relatedPostOverwrite}
					/>
				)}
			</Layout>
		</>
	);
}

/**
 * Sets the data up at build time to pre render.
 * Can be changed to getServerSideProps for server side rendering, ie no pre-building
 *
 * @param params
 * @return {Promise<{props: {postData: *}}>}
 */
export async function getStaticProps(context) {
	const { params, preview, previewData } = context;
	let data = null;
	let relatedPosts = null;

	try {
		const id = preview ? parseInt(previewData?.postID) : params.slug;
		
		data = await getProject(id, preview);
		
		const projectId = data?.project?.projectId;

		if (projectId) {
			relatedPosts = await getRelatedProjects(projectId);
		}
	} catch (error) {
		console.log(`message: ${params.slug} - ${error.message}`);
	}

	return {
		props: {
			data,
			relatedPosts,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the post:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths() {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allProjects = await getAllRESTProjects();

	const paths = allProjects.map((project) => {
		return `${projectPrefix}${project?.slug}`;
	});

	return {
		paths: paths || [],
		fallback: true
	};
}
