import { useRouter } from 'next/router';
import { getHomePage } from '../lib/source/wordpress/api';
import Layout from '../components/common/Layout';
import Meta from '../components/common/Meta';
import HeroSlider from '../components/modules/HeroSlider';
import Loading from '../components/common/Loading';
import { retrievePageBuilderModules } from '../lib/source/wordpress/utils';
import PageBuilder from '../components/common/PageBuilder';
import PreviewBar from '../components/elements/PreviewBar';
import { useEffect } from 'react';

/**
 * @param  props0
 * @param  props0.page
 */
export default function Page(props) {
	const router = useRouter();
	const page = props?.data?.page || null;
	const seo = page?.seo?.fullHead;
	const showrooms = props?.data?.showrooms;

	if (
		(!router.isFallback && !page?.slug && !props.isPreview) ||
		router.isFallback
	) {
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const heroModule = page?.home?.heroModule;
	const modules = retrievePageBuilderModules(page);

	return (
		<>
			<Meta seo={seo} />
			<Layout props={props}>
				{props.isPreview && <PreviewBar preview={props.isPreview} />}
				<HeroSlider data={heroModule} />
				{modules && (
					<PageBuilder modules={modules} showrooms={showrooms} />
				)}
			</Layout>
		</>
	);
}

/**
 * @param  props0
 * @param  props0.params
 */
export async function getStaticProps(context) {
	const { params, preview, previewData } = context;

	let data = null;

	const id = preview ? parseInt(previewData?.postID) : params?.slug;

	try {
		data = await getHomePage(preview, id);
	} catch (error) {
		console.log(error);
		console.log(`message: Home Page - ${error.message}`);
	}

	return {
		props: {
			data,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the page:
		//revalidate: 600 // In seconds - 600 = 10 minutes
	};
}
