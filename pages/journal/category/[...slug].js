import { InView } from 'react-intersection-observer';
import { getAllRESTBlogCategories } from '../../../lib/source/wordpress/api';
import { useRef, useState } from 'react';
import styled from 'styled-components';
import InnerWrapper from '../../../components/common/InnerWrapper';
import PostArchive from '../../../components/elements/PostArchive';
import Loading from '../../../components/common/Loading';
import Meta from '../../../components/common/Meta';
import BlogHeader from '../../../components/elements/BlogHeader';
import Layout from '../../../components/common/Layout';
import useFetchPosts from '../../../hooks/useFetchPosts';
import useHandleSort from '../../../hooks/useHandleSort';
import useBlogFilters from '../../../hooks/useBlogFilters';
import { archiveSortOptions, blogPrefix } from '../../../lib/settings';
import useLoadMore from '../../../hooks/useLoadMore';
import useSortPosts from '../../../hooks/useSortPosts';
import { shouldDisableDynamicPathSSG } from '../../../lib/source/wordpress/utils';

const Archive = styled.div`
	display: block;
	min-height: 100vh;
	padding-top: 145px;
	position: relative;
	background: ${({ theme }) => theme.colors.grey};
`;

const BlogCategory = ({ posts, max, title, filterCategories }) => {
	const options = archiveSortOptions;
	const archive = useRef(null);

	const [loadMoreTrigger, setLoadMoreTrigger] = useState(false);

	const { sortBy } = useHandleSort();
	const { handleFilterBy } = useBlogFilters();

	const { loading, appending, fetchedPosts, setFetchedPosts } = useFetchPosts(
		posts,
		max,
		loadMoreTrigger,
		sortBy
	);

	useSortPosts(fetchedPosts, setFetchedPosts, sortBy);

	return (
		<>
			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<BlogHeader
							options={filterCategories}
							handleFilterBy={handleFilterBy}
							title={title}
						/>
						<PostArchive
							posts={fetchedPosts}
							loading={loading}
							sortBy={sortBy}
						/>
						<InView onChange={(inView) => setLoadMoreTrigger(inView)}>
							<Loading
								pageTheme={'white'}
								active={appending}
								paging={true}
								/>
						</InView>
					</InnerWrapper>
				</Archive>
			</Layout>
		</>
	);
};

export default BlogCategory;

/**
 * Returns the data based on static file generation, alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {allPosts: *}}>}
 */
export async function getStaticProps({ params }) {
	const category = params.slug;

	const getPosts = await fetch(
		`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-posts?category=${category}`,
		{
			method: 'GET'
		}
	).then((response) => {
		return response.json();
	});

	const getData = await getPosts;
	const data = JSON.parse(getData);

	const blogCategories = await getAllRESTBlogCategories();

	const filterCategories = await getFilterCategories(blogCategories, category)

	const max = data?.max;
	const posts = data?.posts;
	const title = data?.title;

	return {
		props: {
			posts,
			max,
			title,
			category,
			filterCategories
		},
		// Next.js will attempt to re-generate the page:
		revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths() {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allCategories = await getAllRESTBlogCategories();
	const paths = allCategories.map((category) => {
		return `${blogPrefix}category/${category?.slug}`;
	});

	return {
		paths: paths || [],
		fallback: true
	};
}

async function getFilterCategories(allCategories, currentCategory) {
	allCategories?.forEach(category => {
		category.label = category?.title
		category.value = category?.slug
	});

	const latestFilter = {
		value: '',
		slug: '',
		label: 'Latest'
	}

	allCategories?.unshift(latestFilter);

	allCategories.sort((a, b) => {
		if (a.slug === currentCategory[0]) {
			return -1
		} else if (b.slug === currentCategory[0]) {
			return 1
		} else {
			return 0
		}
	})

	return allCategories
}
