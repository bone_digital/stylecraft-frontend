import { InView } from 'react-intersection-observer';
import Meta from '../../components/common/Meta';
import Layout from '../../components/common/Layout';
import InnerWrapper from '../../components/common/InnerWrapper';
import styled from 'styled-components';
import { useRef, useState } from 'react';
import BlogHeader from '../../components/elements/BlogHeader';
import PostArchive from '../../components/elements/PostArchive';
import Loading from '../../components/common/Loading';
import { archiveSortOptions } from '../../lib/settings';
import useLoadMore from '../../hooks/useLoadMore';
import useFetchPosts from '../../hooks/useFetchPosts';
import useHandleSort from '../../hooks/useHandleSort';
import useBlogFilters from '../../hooks/useBlogFilters';
import useSortPosts from '../../hooks/useSortPosts';
import { useUI } from '../../context/UIProvider';
import { getAllRESTBlogCategories } from '../../lib/source/wordpress/api';

const Archive = styled.div`
	display: block;
	min-height: 100vh;
	padding-top: 145px;
	position: relative;
`;

export default function Blog({ posts, max, filterCategories }) {
	const archive = useRef(null);

	const { setPageTheme } = useUI() || {};
	setPageTheme('grey');

	const [loadMoreTrigger, setLoadMoreTrigger] = useState(false);

	const { sortBy } = useHandleSort();
	const { handleFilterBy } = useBlogFilters();

	const { loading, appending, fetchedPosts, setFetchedPosts } = useFetchPosts(
		posts,
		max,
		loadMoreTrigger,
		sortBy
	);

	useSortPosts(fetchedPosts, setFetchedPosts, sortBy, false);

	return (
		<>
			<Meta seo={false} />
			<Layout>
				<Archive ref={archive}>
					<InnerWrapper>
						<BlogHeader
							options={filterCategories}
							handleFilterBy={handleFilterBy}
							title={'Our Journal'}
						/>
						<PostArchive
							posts={fetchedPosts}
							loading={loading}
							sortBy={sortBy}
						/>
						
						<InView onChange={(inView) => setLoadMoreTrigger(inView)}>
							<Loading
								pageTheme={'white'}
								active={appending}
								paging={true}
								/>
						</InView>
					</InnerWrapper>
				</Archive>
			</Layout>
		</>
	);
}

/**
 * Returns the data based on static file generation, alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {allPosts: *}}>}
 */
export async function getStaticProps() {
	const getPosts = await fetch(
		`${process.env.NEXT_PUBLIC_CMS_URL}/wp-json/stylecraft/v1/fetch-posts?`,
		{
			method: 'GET'
		}
	);

	const getData = await getPosts.json();
	const data = JSON.parse(getData);

	const blogCategories = await getAllRESTBlogCategories();
	const filterCategories = await getFilterCategories(blogCategories)

	const max = data?.max;
	const posts = data?.posts;

	return {
		props: {
			posts,
			max,
			filterCategories
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

function getFilterCategories(allCategories) {
	allCategories?.forEach(category => {
		category.label = category?.title
		category.value = category?.slug
	});

	const latestFilter = {
		value: '',
		slug: '',
		label: 'Latest'
	}

	allCategories?.unshift(latestFilter);

	return allCategories
}
