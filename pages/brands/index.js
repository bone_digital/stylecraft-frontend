import Meta from '../../components/common/Meta';
import Layout from '../../components/common/Layout';
import Loading from '../../components/common/Loading';
import { getPage } from '../../lib/source/wordpress/api';
import PageBuilder from '../../components/common/PageBuilder';
import { retrievePageBuilderModules } from '../../lib/source/wordpress/utils';
import { useRouter } from 'next/router';
import PreviewBar from '../../components/elements/PreviewBar';

export default function Blog(props) {
	const router = useRouter();
	const page = props?.data?.page || null;
	const seo = page?.seo?.fullHead;
	const showrooms = props?.page?.showrooms;

	if (
		(!router.isFallback && !page?.slug && !props.isPreview) ||
		router.isFallback
	) {
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const modules = retrievePageBuilderModules(page);

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout props={props}>
				{modules && (
					<PageBuilder modules={modules} showrooms={showrooms} />
				)}
			</Layout>
		</>
	);
}

/**
 * Returns the data based on static file generation, alternatively use getServerSideProps for server side rendering.
 * Server side rendering will build the page on request - e.g. like PHP.
 *
 * @return {Promise<{props: {allPosts: *}}>}
 */
export async function getStaticProps({ params, preview }) {
	const slug = 'Brand Archive';
	let data = null;

	try {
		data = await getPage('brand-archive');
	} catch (error) {
		console.log(`message: ${slug} - ${error.message}`);
	}

	return {
		props: {
			data,
			isPreview: preview || false
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}
