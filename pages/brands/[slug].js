import { useRouter } from 'next/router';
import { useEffect } from 'react';
import {
	getAllRESTBrands,
	getBrand,
	getRelatedBrands,
	getRestBrand
} from '../../lib/source/wordpress/api';
import { brandPrefix } from '../../lib/settings';
import Layout from '../../components/common/Layout';
import Meta from '../../components/common/Meta';
import Loading from '../../components/common/Loading';
import PreviewBar from '../../components/elements/PreviewBar';

import styled from 'styled-components';
import { retrievePageBuilderModules, shouldDisableDynamicPathSSG } from '../../lib/source/wordpress/utils';
import PageBuilder from '../../components/common/PageBuilder';
import PostCarousel from '../../components/modules/PostCarousel';

const BrandWrapper = styled.div`
	section {
		padding-top: 100px;

		@media ${({ theme }) => theme.breakpoints.tablet} {
			padding-top: 65px 0;
		}
	}
`;

/**
 * @param  props0
 * @param  props0.page
 */
export default function Page(props) {
	const { data, slug, redirect } = props;

	const router = useRouter();
	const brand = data?.brand;
	const showrooms = data?.showrooms;
	const seo = brand?.seo?.fullHead;

	useEffect(() => {
		if (redirect) {
			router.push('/');
		}
	}, [redirect]);

	let modules = brand ? retrievePageBuilderModules(brand) : [];

	if (!slug || router.isFallback) {
		return <Loading pageTheme={'white'} active={true} paging={true} />;
	}

	const relatedProducts = {
		posts: brand?.brand?.defaultRelatedProducts,
		title: 'Featured Designs',
		link: {
			title: 'View All',
			url: `/brands/collection/${brand?.slug}`
		}
	}

	const relatedPosts = {
		posts: brand?.brand?.relatedPosts,
		title: 'Journal',
		link: {
			title: 'View All',
			url: `/journal`
		}
	}

	return (
		<>
			<Meta seo={seo} />
			<PreviewBar preview={props.isPreview} />
			<Layout>
				<BrandWrapper>
					{modules && (
						<PageBuilder modules={modules} showrooms={showrooms} />
					)}
					{relatedProducts?.posts?.length > 1 && (
						<PostCarousel posts={relatedProducts?.posts} title={relatedProducts?.title} linkButton={relatedProducts?.link} />
					)}
					{relatedPosts?.posts?.length > 1 && (
						<PostCarousel posts={relatedPosts?.posts} title={relatedPosts?.title} linkButton={relatedPosts?.link} />
					)}
				</BrandWrapper>
			</Layout>
		</>
	);
}

/**
 * Sets the data up at build time to pre render.
 * Can be changed to getServerSideProps for server side rendering, ie no pre-building
 *
 * @param params
 * @return {Promise<{props: {postData: *}}>}
 */
export async function getStaticProps(context) {
	const { params, preview, previewData } = context;

	let data = null;

	try {
		const id = preview ? parseInt(previewData?.postID) : params.slug;

		data = await getBrand(id, preview);
	} catch (error) {
		console.log(`message: ${params.slug} - ${error.message}`);
	}

	const currentLocale = context?.locale;
	const brandLocale = data?.brand?.brand?.locales;
	let redirect = false;

	switch (brandLocale) {
		case 'all':
			break;
		case 'au':
			if (currentLocale === 'en-SG') {
				redirect = true;
			}
			break;
		case 'sg':
			if (currentLocale === 'en-AU') {
				redirect = true;
			}
			break;
	}

	return {
		props: {
			data,
			slug: params.slug,
			isPreview: preview || false,
			redirect
		},
		// Next.js will attempt to re-generate the page:
		// revalidate: 600 // In seconds - 600 = 10 minutes
	};
}

/**
 * Sets the default paths to be built during build time, enables the fallback to attempt to generate if it does exist
 *
 * @return {Promise<{paths: *[], fallback: boolean}>}
 */
export async function getStaticPaths({ locales }) {
	if (shouldDisableDynamicPathSSG()) {
		return {
			paths: [],
			fallback: true
		};
	}

	const allBrands = await getAllRESTBrands();

	const paths = [];
	allBrands.forEach((brand) => {
		locales.forEach((locale) => {
			paths.push({
				params: { slug: `${brandPrefix}${brand?.slug}` },
				locale
			});
		});
	});

	return {
		paths: paths || [],
		fallback: true
	};
}
