import { NextResponse } from 'next/server';

export function middleware(request) {
	const url = request.nextUrl.clone();

	if (`${url}`.includes('stylecraft.asia')) {
		let new_url = 'https://stylecraft.com.au/singapore-update/';
		return NextResponse.redirect(new_url, 308);
	}
	else if (`${url}`.includes('stylecraft-asia.bonestaging')) {
		let new_url = 'https://stylecraft.bonestaging.com.au/singapore-update/';
		return NextResponse.redirect(new_url, 308);
	}

	if (url.pathname.includes('/brand-archive')) {
		url.pathname = '/brands';
		return NextResponse.redirect(url);
	}
	else if(url.pathname.includes('/showroom-archive')) {
		url.pathname = '/showrooms';
		return NextResponse.redirect(url);
	}
	else if(url.pathname.includes('/blog')) {
		url.pathname = '/journal';
		return NextResponse.redirect(url);
	}
}
